(* ::Package:: *)

(*
Package name: Blade
Version: 2024-02-06
Copyright: xxxxx
Description: Block-triangular improved Feynman integral decomposition
*)
(*
If you find any bugs, or want to make suggestions, 
please contact us:
guanxin0507@pku.edu.cn, xiao.liu@physics.ox.ac.uk, 
yqma@pku.edu.cn, wuwenhao21@mails.ucas.ac.cn. 
*)


If[MemberQ[Contexts[],"Blade`"],
	Unprotect["Blade`*"];
	Remove["Blade`*`*"];
	(*leaving Names["Blade`*"] unchanged*)
	"ClearAll["~~#~~"]"&/@Names["Blade`*"]//ToExpression; 
];
WriteString["stdout", "Blade: a pacakge for block-triangular form improved Feynman integral decomposition.
Version 2023/01/04.\n"];
WriteString["stdout", "Author: Xin Guan, Xiao Liu, Yan-Qing Ma and Wen-Hao Wu.\n"];
BeginPackage["Blade`"];
$BLBladePath;
BL;


(*Load FiniteFlow.m inside the Blade package*)
Begin["`FiniteFlow`"];
Get[FileNameJoin[{DirectoryName[$InputFileName],"install.m"}]];
AppendTo[$LibraryPath,$BLFiniteFlowLibraryPath];
If[FileExistsQ[#],Get[#],Print["error: can not find FiniteFlow.m"];Abort[]]&[FileNameJoin[{$BLFiniteFlowPath, "FiniteFlow.m"}]];
Off[Remove::rmnsm];
Remove["Blade`FiniteFlow`$*Path"];
On[Remove::rmnsm];
End[];


(*All files to be loaded*)
filesOfBlade=FileNames["*.wl",FileNameJoin[{$InputFileName//DirectoryName,#}],Infinity]&/@
	{"BLGeneral","BLIBPSystem","BLSearch","BLSolve"}//Flatten;
	
(*First time of load: figure out global symbols and `Private` symbols*)
Get/@filesOfBlade;

(*Claim global symbols and `Private` symbols, but clear their definitions*)
"ClearAll["~~#~~"]"&/@Select[Names["Blade`*"],
	!MemberQ[{"filesOfBlade"},#]&]//ToExpression;
"ClearAll["~~#~~"]"&/@Names["Blade`Private`*"]//ToExpression;
(*Remove symbols in other Contexts*)
Remove@Evaluate[#<>"*"]&/@Complement[Contexts["Blade`*"],
	{"Blade`","Blade`Private`"}];

(*Allow `Private` symbols used in any sub-constexts*)	
AppendTo[$ContextPath,"Blade`Private`"];
(*load files*)
Get/@filesOfBlade;

Remove[filesOfBlade];



$BLBladePath::usage ="$BLBladePath, directory where 'Blade.wl' should exist";
$BLBladePath=$InputFileName//DirectoryName;
$BLBladeIBPPath = FileNameJoin[{$BLBladePath,"BLAddOns/BladeIBP"}];
BL::usage= "BL, a symbol represents Feynman integrals, e.g. BL[topo, {1,1,1,1,0,0}], where 'topo' denotes the integral family name, and {1,1,1,1,0,0} represents the powers of denominators. A third argument will be introduced to label the general integrand when there are more than two general integrands.";


EndPackage[];
Protect["Blade`*"];
(*Unprotect[$LCVerbose,SUNN,$LCDefinedForm,Pair,$MomentumRelation,LCmonitor,$D,AmplitudeSymbol];*)
Unprotect[BLFamilyInf,BLIBPParameter,BLSearchParameter,BLNthreads,BLRawMasters,BLPrintLevel,$BLWorkingDirectory];
"Unprotect["~~#~~"]"&/@Names["Blade`*Path"]//ToExpression;
BLPrintLevel=7;
WriteString["stdout","Number of defined symbols="<>
	ToString@Length@Names["Blade`*"]<>
	". Please see ?Blade`* for help.\n"
];


Blade`Private`BLInitialize[];
