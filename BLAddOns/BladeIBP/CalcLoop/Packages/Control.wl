(* ::Package:: *)

(* ::Section::Closed:: *)
(*Begin*)


$LCVerbose::"usage"="$LCVerbose is a parameter to control the depth of messages to be printed.";
WriteMessage::"usage"="WriteMessage[exp__] prints 'exp' as a string.";
LCTiming::"usage"="LCTiming[exp] evaluates 'exp' and prints the wall-time used.";
RemoveLocalSymbols::usage="RemoveLocalSymbols[pkg] removes all local symbols in the context 'pkg'.";
$LCDefinedForm={TraditionalForm};


Begin["`Private`"];

JString::usage="JString[exp__] generats a string based on input 'exp'.";

TDBox::usage="TDBox[x] is MakeBoxes[x,TraditionalForm].
TDBox[x_,y__] is RowBox@(TDBox/@{x,y}).";

$GlobalSpace::"usage"="$GlobalSpace is a parameter to control the current indentation.";
$GlobalSpace=0;

$GlobalSpaceN::"usage"="$GlobalSpaceN is a parameter to control the encrease of indentation at each step.";
$GlobalSpaceN=6;

LCMonitor::"usage"="LCMonitor[exp,i] is the same as Monitor but it is active only if $LCVerbose>0&&$LCVerbose\[GreaterEqual]($GlobalSpace/$GlobalSpaceN) (when LCTiming is active).";

DebugPrint::"usage"="DebugPrint[exp] prints 'exp' if OptionValue[\"DebugQ\"] is True. Or else, it does nothing.";

ErrorPrint::"usage"="ErrorPrint[exp] prints 'exp' and then pause OptionValue[Pause] seconds.";

End[];


Begin["`Control`"];


(* ::Section::Closed:: *)
(*JString*)


JString[a_Symbol|a_Integer|a_Rational|a_Real|a_String|a_Times|a_Plus]:=
	StringReplace[ToString@a,"-"->"m"];
JString[a_Symbol|a_String,b_Symbol|b_String|b_Integer]:=
	StringReplace[ToString[a]<>ToString[b],"-"->"m"];
JString[a_,b_,c__]:=JString[JString[a,b],c];
JString[x___,a_[b___],y___]:=JString[x,a,b,y];


(* ::Section::Closed:: *)
(*TDBox*)


TDBox[x_]:=MakeBoxes[x,TraditionalForm];
TDBox[x_,y__]:=RowBox@(TDBox/@{x,y});


(* ::Section::Closed:: *)
(*WriteMessage*)


Options[WriteMessage]={"Length"->40,"Spacings":>$GlobalSpace};
WriteMessage[exps___,OptionsPattern[]]:=If[$LCVerbose>0&&$LCVerbose>=($GlobalSpace/$GlobalSpaceN),
	WriteString["stdout",
		StringJoin@Table[" ", {OptionValue["Spacings"]}],
		If[Head@#===String,#,ToString[#,InputForm]]&/@({exps})//StringJoin,"\n"
	]
];


(* ::Section::Closed:: *)
(*LCTiming*)


Attributes[LCTiming]={HoldAll};
Options[LCTiming]={"Print"->True};
LCTiming[exp_,opt:OptionsPattern[]]:=Module[
	{name},
	name=Hold[exp][[1,0]]//ToString;
	LCTiming[exp,name//Evaluate,opt]
];
LCTiming[exp_,name_,OptionsPattern[]]:=Module[
	{time,res,temp=$GlobalSpace,DateDifference2},
	DateDifference2[x___]:=DateDifference[x][[1]];
	
	If[OptionValue["Print"]===False||$LCVerbose<=($GlobalSpace/$GlobalSpaceN),
		$GlobalSpace+=$GlobalSpaceN;
		res=exp;
		$GlobalSpace-=$GlobalSpaceN;
		Return[res]
	];
	
	Block[
		{$GlobalSpace=temp},
		WriteMessage["Begin "<>name<>"  ..."];
		$GlobalSpace+=$GlobalSpaceN;
		time=DateList[];
		res=exp;
		$GlobalSpace-=$GlobalSpaceN;
		
		WriteMessage["      "<>name<>" use time : ",
			(StringInsert[#,".",-4]&)@(If[StringLength@#<4,StringPadLeft[#,4,"0"],#]&)@
			ToString@Round[DateDifference2[time,DateList[]]*24*3600*10^3],"s,   LeafCount = ",
			Which[StringLength@#<=3,#,
			StringLength@#<=6,StringPart[#,1;;-4]<>"."<>StringPart[#,-3;;-1]<>"K",
			True,StringPart[#,1;;-7]<>"."<>StringPart[#,-6;;-4]<>"M"
			]&@ToString@LeafCount@res
		];
	];
	
	Return[res];
];


(* ::Section::Closed:: *)
(*LCMonitor*)


Attributes[LCMonitor]={HoldAll};
LCMonitor[exp_,i_]:=If[$LCVerbose>0&&$LCVerbose>=($GlobalSpace/$GlobalSpaceN),Monitor[exp,i],exp];


(* ::Section::Closed:: *)
(*DebugPrint*)


Options[DebugPrint]={"DebugQ"->False};
DebugPrint[x_,OptionsPattern[]]:=If[OptionValue["DebugQ"]&&$LCVerbose>0&&$LCVerbose>=($GlobalSpace/$GlobalSpaceN),Print[x];Pause[0.1]];


(* ::Section::Closed:: *)
(*ErrorPrint*)


Options[ErrorPrint]={Pause->0.1};
ErrorPrint[exp___,OptionsPattern[]]:=(Print[exp//Short];Pause[OptionValue[Pause]]);


(* ::Section::Closed:: *)
(*RemoveLocalSymbols*)


RemoveLocalSymbols[pkg_]:=(Remove@@Evaluate@Names[pkg~~"`"~~__~~"$"~~DigitCharacter..];ClearSystemCache[];)


(* ::Section::Closed:: *)
(*End*)


End[];
