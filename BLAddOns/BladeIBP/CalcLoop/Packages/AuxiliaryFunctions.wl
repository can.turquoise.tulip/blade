(* ::Package:: *)

(* ::Section::Closed:: *)
(*Begin*)


CountIndexes::usage="CountIndexes[exp_,index_] returns the maximal number of each index appeared in 'exp'. \
If OptionValue[\"CheckQ\"]===True], it will also check whether summands have the same structure of \
non-dummy indexes.";


Begin["`Private`"];


lcTimes::usage="lcTimes is similar to Times, but it does not express multiplication of same terms \
as Powers.";
lcTimesForm::usage="lcTimesForm changes multiplication to lcTimes.
If OptionValue[\"PowerExpand\"] is True, positive powers are expressed as multiplications.";

lcPlus::usage="lcPlus is similar to Plus, but it does not express summation of same terms \
as Times.";
lcPlusForm::usage="lcPlusForm changes summation to lcTimes.";

inputString::usage="inputString[exp_] change 'exp' to string with InputForm.";


associationPlus
associationAddTo


End[];


Begin["`LCAuxiliary`"];


inputString[exp_]:=ToString[exp,InputForm];


(* ::Section::Closed:: *)
(*lcTimes/lcTimesForm*)


Attributes[lcTimes]={Orderless};
lcTimes[lcTimes[a___],b___]:=lcTimes[a,b];
lcTimes[a_*lcTimes[b___],c___]:=a*lcTimes[b,c];


Attributes[lcTimesForm]={Listable};
Options[lcTimesForm]={"PowerExpand"->False};
lcTimesForm[exp_,OptionsPattern[]]:=If[OptionValue["PowerExpand"]===True,
	#//.lcTimes[a_^n_?IntegerQ,b___]/;n>0:>lcTimes[Sequence@@ConstantArray[a,n],b],
	#]&@If[
		Head@exp===Times,
		lcTimes@@exp,
		lcTimes@exp
	];


(* ::Section::Closed:: *)
(*lcPlus/lcPlusForm*)


Attributes[lcPlus]={Orderless};
lcPlus[lcPlus[a___],b___]:=lcPlus[a,b];
lcPlus[a_+lcPlus[b___],c___]:=a+lcPlus[b,c];


Attributes[lcPlusForm]={Listable};
lcPlusForm[exp_]:=If[Head@exp===Plus,lcPlus@@exp,lcPlus@exp];


(* ::Section::Closed:: *)
(*CountIndexes*)


Options[CountIndexes]={"CheckQ"->True};


CountIndexes[exp_,index_,opt:OptionsPattern[]]:=Module[
	{checkQ,list},
	
	checkQ=OptionValue["CheckQ"];
	
	If[FreeQ[exp,index],Return[{}]];
	
	Switch[Head@exp,
		index,{{exp,1}},
		Plus, list=CountIndexes[#,index,opt]&/@List@@exp;
			  If[checkQ&&(Length@Union[Select[#,##[[2]]===1&]&/@list])>1,
			  	Print["Inhomogeneous ",index," detected: ",{exp//Short, list}];
			  	Abort[];
			  ];
			  list=SplitBy[Join@@list//Sort,First];
			  Return[{#[[1,1]],Max@#[[All,2]]}&/@list],
		Power, list=CountIndexes[exp[[1]],index,opt];
			   Return[{#[[1]],exp[[2]]*#[[2]]}&/@list],
		_, list=CountIndexes[#,index,opt]&/@List@@exp;
			  list=SplitBy[Join@@list//Sort,First];
			  Return[{#[[1,1]],Plus@@#[[All,2]]}&/@list];
	]
];


(* ::Section::Closed:: *)
(*associationPlus*)


Attributes[associationPlus]={};


associationPlus[{}]:=Association[];
associationPlus[x:{asso1_,___}]/;Head@asso1=!=Association:=Plus@@x;
associationPlus[assoList:{_Association..}]:=Module[
	{keys,asso},
	
	keys=Union[Join@@(Keys/@assoList)];
	
	asso=Association@@Thread[keys->Association[]];
	
	Do[
		Do[
			asso[key][i]=assoList[[i]][key]
			,{key,Keys@assoList[[i]]}
		]
		,{i,Length@assoList}
	];
	
	(*** add elements recursively ***)
	Do[asso[key]=associationPlus[Values@asso@key],{key,keys}];
	
	asso
];


(* ::Section::Closed:: *)
(*associationAddTo*)


Attributes[associationAddTo]={HoldAll};


associationAddTo[asso_@key_,value_]:=Module[
	{exi},
	exi=Lookup[asso,Key@key,0];
	asso[key]=exi+value;
];


associationAddTo[asso_@asso1_@key_,value_]:=Module[
	{exi},
	If[!KeyExistsQ[asso,asso1],asso[asso1]=Association[]];
	exi=Lookup[asso[asso1],Key@key,0];
	asso[asso1][key]=exi+value;
];


associationAddTo[asso_@asso1_@asso2_@key_,value_]:=Module[
	{exi},
	If[!KeyExistsQ[asso,asso1],asso[asso1]=Association[]];
	If[!KeyExistsQ[asso[asso1],asso2],asso[asso1][asso2]=Association[]];
	exi=Lookup[asso[asso1][asso2],Key@key,0];
	asso[asso1][asso2][key]=exi+value;
];


(* ::Section::Closed:: *)
(*End*)


End[];
