(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


Begin["`Private`"];
MMATranspose;
End[];


Begin["`LCAuxiliary`"];


MMATranspose[{}]={};
MMATranspose[exp_]/;exp=!={}:=Transpose[exp];


(* ::Section::Closed:: *)
(*End*)


End[];
