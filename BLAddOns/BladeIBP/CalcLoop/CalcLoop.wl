(* ::Package:: *)

(*
Package name: CalcLoop
Version: 2022-07-10
Copyright: 2022 Yan-Qing Ma
Description: tools to calculate Feynman amplitudes with any number of loops
*)
(*
If you find any bugs, or want to make suggestions, 
please contact Yan-Qing Ma at yqma@pku.edu.cn. 
*)


If[MemberQ[Contexts[],"CalcLoop`"],
	Unprotect["CalcLoop`*"];
	Remove["CalcLoop`*`*"];
	(*leaving Names["CalcLoop`*"] unchanged*)
	"ClearAll["~~#~~"]"&/@Names["CalcLoop`*"]//ToExpression; 
];
WriteString["stdout", "CalcLoop: a tool for automated multiloop calculation in quantum field theory.
Version 2022/07/10.\n"];
BeginPackage["CalcLoop`"];
$CalcLoopDirectory;
LCmonitor;


(*All files to be loaded*)
filesOfCalcLoop=FileNames["*.wl",FileNameJoin[{"CalcLoop",#}],Infinity]&/@
	{"Packages"}//Flatten;
	
(*First time of load: figure out global symbols and `Private` symbols*)
Get/@filesOfCalcLoop;

(*Claim global symbols and `Private` symbols, but clear their definitions*)
"ClearAll["~~#~~"]"&/@Select[Names["CalcLoop`*"],
	!MemberQ[{"filesOfCalcLoop"},#]&]//ToExpression;
"ClearAll["~~#~~"]"&/@Names["CalcLoop`Private`*"]//ToExpression;
(*Remove symbols in other Contexts*)
Remove@Evaluate[#<>"*"]&/@Complement[Contexts["CalcLoop`*"],
	{"CalcLoop`","CalcLoop`Private`"}];

(*Allow `Private` symbols used in any sub-constexts*)	
AppendTo[$ContextPath,"CalcLoop`Private`"];
(*load files*)
Get/@filesOfCalcLoop;

Remove[filesOfCalcLoop];



$CalcLoopDirectory=$InputFileName//DirectoryName;


EndPackage[];
Protect["CalcLoop`*"];
Unprotect[$LCVerbose,SUNN,$LCDefinedForm,Pair,$MomentumRelation,LCmonitor,$D,AmplitudeSymbol];
$LCVerbose=3;
WriteString["stdout","Number of defined symbols="<>
	ToString@Length@Names["CalcLoop`*"]<>
	". Please see ?CalcLoop`* for help.\n"
];
