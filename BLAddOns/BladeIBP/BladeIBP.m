(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


SetDirectory[DirectoryName@$InputFileName];
<<CalcLoop`CalcLoop`;
ResetDirectory[];
BeginPackage["BladeIBP`",{"CalcLoop`"}];


BL::usage="BL, symbol that represents Feynman integral";


SetDim::usage="SetDim[d0_] set space time dimension to be 'd0'";
NewBasis::usage="NewBasis[family,loopdenominators,loopmomenta,externalmomenta,momentumconservation,scalarproducts] define family. Information is stored in FamilyInfo[family].
If \"GenerateIBP\" is set to True(by default), it will generate IBP and LI identities.";
AnalyzeSectors::usage="AnalyzeSectors[family,top] analyze zero sectors and symmetries of the integral family 'family'. \
Results are stored in ZeroSectors[family], NonZeroSectors[family], UniqueSectors[family] and MappedSectors[family].
Only sub-sectors of top-level sector determined by 'top' are analyzed. \
'top' is a list of Integer consists of 1 and 0, e.g. {1,1,1,1,1,1,1,0,0}, where 1 denotes denorminator, 0 denotes irreducible scalar products.
\"Prescription\": a list of Integer consists of 1 and 0 that has one-to-one correspondence to FamilyInfo[family][\"Loop\"], e.g {1,0,-1}. \
Propagators that constain loop momenta whose prescription equals to 1 has +i0 Feynman prescription. \
Propagators that constain loop momenta whose prescription equals to -1 has -i0 Feynman prescription. \
Loop momenta that has 0 prescription is a cut momenta to be integrated. 
\"CutDs\": a list of Integer consists of 1 and 0, e.g. {0,0,0,0,0,1,1,0,0}, where 1 denotes cut denominators, 0 denotes un-cut denominators.";
FamilyInfo::usage="FamilyInfo[family] return information of the integral family 'family'.";


ZeroSectors::usage="ZeroSectors[family] return zero sectors of the integral family 'family'.";
ZeroQ::usage="ZeroQ[int] return True if int equals to zero";
NonZeroSectors::usage="NonZeroSectors[family] return non-zero sectors of the integral family 'family'.";
MappedSectors::usage="MappedSectors[family] return sectors that can be mapped to unique sectors using symmetries in the integral family 'family'.";
MappedQ::usage="MappedQ[sect] return True if the 'sect' belongs to MappedSectors, otherwise, return False.";
UniqueSectors::usage="UniqueSectors[family] return unique sectors in the integral family 'family'.";
ExtMappedSectors::usage="ExtMappedSectors[family] return sectors that can be mapped to another integral family";
ExtMappedQ::usage="ExtMappedQ[sect] return True if the 'sect' belongs to ExtMappedSectors, otherwise, return False.";


IBP::usage="IBP[family] Integration-By-Part identities of 'family'";
LI::usage="LI[family] Lorentz-Invariance identities of 'family'";
SymMap::usage="SymMap[family][seeds_List] Symmetry relations of 'family' for a list of integrals 'seeds'";
SectorRelations::usage="SectorRelations[exp0_] apply symmetries among Feynman integrals to the 'exp0' and return the result";
sectorMap::usage="sectorMap[sect] return the unique sector that is equivalent to 'sect'";
GenIds::usage="GenIds[type][sector,seeds] generate identities of \"type\". Possible types are \"IBP\",\"LI\",\"SR\" and \"Map\".";
GenCutIds::usage="GenCutIds[type_][sector_,seeds_] analogous to GenIds except that sub-level sectors are set to zero.";


GenSeeds::usage="GenSeeds[sec0_,{mind,maxd},minr,maxr,filter_:{}] generate seed integrals belong to fsector, 
whose rank ranges from minr to minr and dots ranges from mind to maxd";


IntegralOrdering::usage="IntegralOrdering, an integer represents integral ordering. \
1: BLIntPropagators>BLIntDots>BLIntRank, 2: BLIntPropagators>BLIntRank>BLIntDots, \
3: BLIntPropagators>BLIntRank+BLIntDots>BLIntDots>BLIntRank, 4: BLIntPropagators>BLIntRank+BLIntDots>BLIntRank>BLIntDots\:ff0c \
where '>' means the left hand side has higher priority than the right hand side.";
IntRank::usage="IntRank[int] return rank of int. the negative sum of negative powers of propagators, e.g. IntRank[BL[topo,{1,1,2,-1,-1}] = 2";
IntDots::usage="IntDots[int] return dots of int. the sum of positive powers of propagators minus the number of propagators with positive power,e.g. IntDots[BL[topo,{1,1,2,-1,-1}] = 1";
IntPropagators::usage="IntPropagators[int] return the number of propagators with positive power, e.g. IntPropagators[BL[topo,{1,1,2,-1,-1}] = 3.";
IntSector::usage="IntSector[int] return sector, e.g. IntSector[BL[topo,{1,1,2,-1,-1}]] = BL[topo,{1,1,1,0,0}].";
SectorOrSubsectorQ::usage="SectorOrSubsectorQ[fsec1_,fsec2_] return True if 'fsec1' is the same as 'fsec2' or 'fsec1' is a sub-sector of 'fsec2', otherwise, return False.";
SortIntegrals::usage="SortIntegrals[ints_] sort 'ints' according to IntegralOrdering";
IntegralsIn::usage="IntegralsIn[expr_] collect Feynman integrals in the expr";
EliminateZeroSectors::usage="EliminateZeroSectors[expr_] set integrals that belong to ZeroSectors to 0";


FastMIs::usage="FastMIs[sect_], return maximal-cut master integrals for the sector 'sect'.";
AutoDetermine::usage="AutoDetermine[topo_] determine master integrals for the integral family 'topo' using the maximal-cut method.";
MMALinearSolveLearn::usage="MMALinearSolveLearn[eqs_,paras_,allvars_,neededvars_] solve linear equations 'eqs' whose variables are 'allvars' and
coefficients are functions of 'paras'. Return independent variables that is sufficient to express 'neededvars'.";


GetAllInts::usage="GetAllInts[intsfiles_] get all Feynman integrals in the files 'intsfiles' and sort integrals";
FastGenIds::usage="FastGenIds[fam_,GetSeeds_] generate a system of linear equations for the integral family 'fam', \
including \"IBP\",\"LI\",\"Map\" and \"SR\" identities. seeds are specified by the function 'GetSeeds'.";
SerializeFastIds::usage="SerializeFastIds[todofilesin_,rules_, paras_] apply replacement rules 'rules' to identities located in 'todofilesin' \
and transform these identites to .json format, which can be parsed by FiniteFlow. Coefficients of identities are functions of 'paras'.";
WriteSystemJSON::usage="WriteSystemJSON[eqsfiles_,allints_,needed_,paras_] generate the file that contains information of the linear system located in 'eqsfiles'. \
The file will be parsed by FiniteFlow.";


Begin["`Private`"];


(* ::Section:: *)
(*IBP+LI*)


firstNonZero[list_]:=Module[{i=1},While[list[[i]]===0&&i<=Length@list,i++];
If[i>Length@list,Print["zero list"];Return[-1],Return[i]]];
(* convertion between polynomial z1^n1 ... zk^nk and index expression f[n1,...,nk] *)
mon2F[poly_,zlist_,fsymb_]/;Head[poly]=!=List:=Total[(fsymb@@Exponent[#,zlist])*(#/.(#->1&/@zlist))&/@If[Head[poly]===Plus,List@@poly,List@poly]];
mon2F[vec_List,zlist_,fsymb_]:=mon2F[#,zlist,fsymb]&/@vec;
f2Mon[expr_,zlist_,fsymb_]:=expr/.fsymb[ind__]:>Times@@Thread@Power[zlist,{ind}];
(*Note that the sign of index expression of BL2Mon is different with f2Mon.*)
BL2Mon[expr_,zlist_]:=expr/.BL[fam_,ind_]:>Times@@Thread@Power[zlist,-ind];


(*generating seeds at levels in List n_List or below level n_Integer*)
(*generating seeds at levels in List n_List or below level n_Integer of nprop_Integer propagators*)
seedsGenPos[n_Integer,nprop_]:=Join@@Table[DeleteDuplicates@(Join@@(Permutations/@(IntegerPartitions[i+nprop,{nprop}]-1))),{i,0,n}];
seedsGenPos[n_List,nprop_]:=Join@@Table[DeleteDuplicates@(Join@@(Permutations/@(IntegerPartitions[n[[i]]+nprop,{nprop}]-1))),{i,Length@n}];
seedsGenerate[sector_List,r_?IntegerQ,s_?IntegerQ]:=Module[{corner,inddots,indrank,seeds,npd,sec},
npd=Length@sector;sec=Flatten@Position[sector,1];
corner=Plus@@(UnitVector[npd,#]&/@sec);
inddots=(# . (UnitVector[npd,#]&/@sec))&/@seedsGenPos[r,Length[sec]];
indrank=(# . (UnitVector[npd,#]&/@Complement[Range[npd],sec]))&/@seedsGenPos[s,npd-Length[sec]];
seeds=Outer[(corner+#1-#2)&,inddots,indrank,1]//Flatten[#,1]&;
Return[seeds];
];
(* ordering of FIs according to Kira *)
orderingfi[fi_]:={Total@#[[1]],#[[1]],Total@#[[2]],Total@#[[3]],-#[[3]],-#[[2]]}&@{#/.{b_?Positive->1,b_?Negative->0},Select[List@@#,Positive],-Select[List@@#,NonPositive]}&@(List@@fi)
BLSort[FIlist_]:=SortBy[FIlist,orderingfi];


SP2PD[pdlist_,loopmom_,extmom_,spsRep_]:=Module[{scalarproducts,pd2sp,sp2pd,sp2pdrule,npd=Length@pdlist},
scalarproducts=Join[Union@Flatten@Outer[Times,loopmom,loopmom],Flatten@Outer[Times,loopmom,extmom]];
pd2sp=Expand[pdlist]/.Thread[Rule[scalarproducts,Array["sp",npd]]]/.spsRep;(*express pdlist in terms of sp[i]*)
sp2pd=LinearSolve[#[[2]],-#[[1]]+Array["z",npd]]&@CoefficientArrays[Expand[pdlist]/.Thread[Rule[scalarproducts,Array["sp",npd]]]/.spsRep,Array["sp",npd]];(*express scalarproduces in terms of z[i]*)
sp2pdrule=Thread[Rule[scalarproducts,sp2pd]];
Return[sp2pdrule];
];


derivL[pdlist_,loopmom_,extmom_,spsRep_,sprule_]:=(Expand[Expand[Outer[Times,D[#,{loopmom}],Join[loopmom,extmom]]]/.spsRep/.sprule])&/@pdlist
(*20230221: vacuum*)
derivP[pdlist_,loopmom_,extmom_,spsRep_,sprule_]:=If[extmom==={},{},(Expand[Expand[Outer[Times,D[#,{extmom}],extmom]]/.spsRep/.sprule])&/@pdlist];
(*derivative of propagator denominators in terms of loop momenta or external momenta: q_j dD_i/dl_k, p_j dD_i/dp_k*)

genIBP[derivl_]:=Module[{genRel,dim=OptionValue["dimension"]},
genRel[j_Integer,k_Integer,ind_,derivmat_]:=Module[{eqmon,eqf,npd=Length@derivmat},
eqmon=If[j==k,$d,0]+Sum[-ind[[i]]*derivmat[[i,j,k]]/"z"[i],{i,npd}];
eqmon=Expand[eqmon/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqf=mon2F[eqmon,"z"/@Range[npd],"f"]/.{"f"[vec__]:>"f"@@(-{vec})};
Return[eqf//Collect[#,_f]&];
];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivl],derivl]&/@(Join@@Array[{#1,#2}&,Dimensions[derivl][[2;;3]]])
];

genLI[derivp_,extmom_,spsRep_]:=Module[{genRel,ne=Length@extmom},
genRel[j_Integer,k_Integer,ind_,derivmat_]:=Module[{eqLI,eqLIf,npd=Length@derivmat},eqLI=Sum[-ind[[i1]]*Sum[(Times@@extmom[[{j,i2}]]/.spsRep)*derivmat[[i1,i2,k]]-(Times@@extmom[[{k,i2}]]/.spsRep)*derivmat[[i1,i2,j]],{i2,ne}]/"z"[i1],{i1,npd}];
eqLI=Expand[eqLI/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqLIf=mon2F[eqLI,"z"/@Range[npd],"f"]/.{"f"[vec__]:>"f"@@(-{vec})};
Return[eqLIf];
];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivp],derivp]&/@(Join@@Table[Table[{i,j},{j,i+1,ne}],{i,ne}])
];


(*integrand: F*D_i^{-v_i} -----IBP----> dq_j/dl_k + (-v_i) q_j * dD_i/dl_k * 1/D_i + q_j* 1/F * dF/dD_i * dD_i/dl_k \[Equal]0
 where 1/F * dF/dD_i is denoted by 'extra'[[i]]*)
(*extra: can be expressed as linear combinations of integrals BL[fam_,{n1,...,nk}]*)
(*TODO: use syzygy to handle 1/("z"[1]+"z"[2]) like propagator*)
genIBP2[derivl_]:=Module[{genRel,extra0,bl2mon},
genRel[j_Integer,k_Integer,ind_,derivmat_,extra_]:=Module[{fac,eqmon,eqf,npd=Length@derivmat},
eqmon=If[j==k,$d,0]+Sum[-ind[[i]]*derivmat[[i,j,k]]/"z"[i],{i,npd}]+Sum[extra[[i]]*derivmat[[i,j,k]],{i,npd}];
eqmon=Expand[eqmon/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqf=mon2F[eqmon,"z"/@Range[npd],interalF]/.{interalF[vec__]:>interalF@@(-{vec})};
(*Collect: in case (1/x +1)BL[..] and -(1-t)BL[..] -(-1+t)BL[..] that lead fflow failed.*)
Return[eqf//Collect[#,_interalF,Together]&];
];
extra0=If[ValueQ@$ExtraIntDeriv,BL2Mon[$ExtraIntDeriv,"z"/@Range[Length@derivl]],ConstantArray[0,Length@derivl]];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivl],derivl,extra0]&/@(Join@@Array[{#1,#2}&,Dimensions[derivl][[2;;3]]])
];

(**integrand: F*D_i^(-v_i) ------LI------  *)
genLI2[derivp_,extmom_,spsRep_]:=Module[{extra0,genRel,ne=Length@extmom},
genRel[j_Integer,k_Integer,ind_,derivmat_,extra_]:=Module[{eqLI,eqLIf,npd=Length@derivmat},
eqLI=Sum[-ind[[i1]]*Sum[(Times@@extmom[[{j,i2}]]/.spsRep)*derivmat[[i1,i2,k]]-(Times@@extmom[[{k,i2}]]/.spsRep)*derivmat[[i1,i2,j]],{i2,ne}]/"z"[i1],{i1,npd}]
     +Sum[extra[[i1]]*Sum[(Times@@extmom[[{j,i2}]]/.spsRep)*derivmat[[i1,i2,k]]-(Times@@extmom[[{k,i2}]]/.spsRep)*derivmat[[i1,i2,j]],{i2,ne}],{i1,npd}];
eqLI=Expand[eqLI/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqLIf=mon2F[eqLI,"z"/@Range[npd],interalF]/.{interalF[vec__]:>interalF@@(-{vec})};
Return[eqLIf//Collect[#,_interalF,Together]&];
];
extra0=If[ValueQ@$ExtraIntDeriv,BL2Mon[$ExtraIntDeriv,"z"/@Range[Length@derivp]],ConstantArray[0,Length@derivp]];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivp],derivp,extra0]&/@(Join@@Table[Table[{i,j},{j,i+1,ne}],{i,ne}])
]


(*integrand: F_\[Rho]*D_i^{-v_i} -----IBP----> F_\[Rho] *dq_j/dl_k + F_\[Rho] *(-v_i) q_j * dD_i/dl_k * 1/D_i + q_j * dF_\[Rho]/dD_i * dD_i/dl_k \[Equal]0
 where dF_\[Rho]/dD_i === A_{\[Rho]i}^\[Beta] F_\[Beta], A is denoted by 'extra'[[\[Rho],i,\[Beta]]]*)
(*$ExtraIntDeriv: linear combinations of integrals BL[fam_,{n1,...,nk}], dimension-> \[Rho]*i*\[Beta]*)
(*TODO: use syzygy to handle 1/("z"[1]+"z"[2]) like propagator*)
genIBP3[derivl_]:=Module[{genRel,extra0,bl2mon},
genRel[j_Integer,k_Integer,ind_,derivmat_,extra_]:=Module[{fac,eqmon,eqf,npd=Length@derivmat},
eqmon=Table[
	Ftag[r]*If[j==k,$d,0]+Ftag[r]*Sum[-ind[[i]]*derivmat[[i,j,k]]/"z"[i],{i,npd}]+Sum[Ftag[bb]*extra[[r,i,bb]]*derivmat[[i,j,k]],{i,npd},{bb,Length[extra]}]
	,{r,1,Length@extra}];
eqmon=Expand[eqmon/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqf=mon2F[eqmon,"z"/@Range[npd],interalF]/.{interalF[vec__]:>interalF@@(-{vec})};
(*Collect: in case (1/x +1)BL[..] and -(1-t)BL[..] -(-1+t)BL[..] that lead fflow failed.*)
Return[eqf];
];
extra0=BL2Mon[$ExtraIntDeriv,"z"/@Range[Length@derivl]];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivl],derivl,extra0]&/@(Join@@Array[{#1,#2}&,Dimensions[derivl][[2;;3]]])//Flatten
];

(**integrand: F*D_i^(-v_i) ------LI------  *)
genLI3[derivp_,extmom_,spsRep_]:=Module[{extra0,genRel,ne=Length@extmom},
genRel[j_Integer,k_Integer,ind_,derivmat_,extra_]:=Module[{eqLI,eqLIf,npd=Length@derivmat},
eqLI=Table[
	Ftag[r]*Sum[-ind[[i1]]*Sum[(Times@@extmom[[{j,i2}]]/.spsRep)*derivmat[[i1,i2,k]]-(Times@@extmom[[{k,i2}]]/.spsRep)*derivmat[[i1,i2,j]],{i2,ne}]/"z"[i1],{i1,npd}]
     +Sum[Ftag[bb]*extra[[r,i1,bb]]*Sum[(Times@@extmom[[{j,i2}]]/.spsRep)*derivmat[[i1,i2,k]]-(Times@@extmom[[{k,i2}]]/.spsRep)*derivmat[[i1,i2,j]],{i2,ne}],{i1,npd},{bb,Length[extra]}]
     ,{r,Length@extra}];
eqLI=Expand[eqLI/(Times@@Thread@Power["z"/@Range[npd],ind])];
eqLIf=mon2F[eqLI,"z"/@Range[npd],interalF]/.{interalF[vec__]:>interalF@@(-{vec})};
Return[eqLIf];
];
extra0=BL2Mon[$ExtraIntDeriv,"z"/@Range[Length@derivp]];
genRel[#[[1]],#[[2]],Array["n"<>ToString[#]&,Length@derivp],derivp,extra0]&/@(Join@@Table[Table[{i,j},{j,i+1,ne}],{i,ne}])//Flatten
]


Ftag/:Ftag[r_]*BL[a__]:=BL[a,r];
FtagSimplify[exp_]:=If[Length[$ExtraIntDeriv]<=1,exp/.BL[a_,b_,c_]:>BL[a,b],exp];


(*20240205: support multiple extra integrands under the condition that their derivatives w.r.t 'Den' are closed.*)
relationGeneral3[famname_,pdlist_,loopmom_,extmom_,spsRep_]:=Module[{sprule,derivl,derivp,relationgeneral,relationspecific,npd=Length[pdlist]},
sprule=SP2PD[pdlist,loopmom,extmom,spsRep];
derivl=derivL[pdlist,loopmom,extmom,spsRep,sprule];
derivp=derivP[pdlist,loopmom,extmom,spsRep,sprule];
Return[{Function[Evaluate[FtagSimplify[Collect[Expand[genIBP3[derivl]/.interalF[ind__]:>BL[famname,{ind}],Ftag[_]|BL[__]],_BL,Together]]/.Thread[Array["n"<>ToString[#1]&,npd]->Array[Slot,npd]]]],
Function[Evaluate[FtagSimplify[Collect[Expand[genLI3[derivp,extmom,spsRep]/.interalF[ind__]:>BL[famname,{ind}],Ftag[_]|BL[__]],_BL,Together]]/.Thread[Array["n"<>ToString[#1]&,npd]->Array[Slot,npd]]]]}]];


relationGeneral[famname_,pdlist_,loopmom_,extmom_,spsRep_,OptionsPattern[]]:=Module[{sprule,derivl,derivp,relationgeneral,relationspecific,npd=Length@pdlist,dim=OptionValue["dimension"]},
sprule=SP2PD[pdlist,loopmom,extmom,spsRep];
derivl=derivL[pdlist,loopmom,extmom,spsRep,sprule];
derivp=derivP[pdlist,loopmom,extmom,spsRep,sprule];
relationgeneral=genIBP[derivl]~Join~genLI[derivp,extmom,spsRep];
Return[relationgeneral/.{"f"[ind__]:>j@@Join[{famname},{ind}]}];
]
Options[relationSpecific]={Cut->{}};
relationSpecific[famname_,seedslist_,pdlist_,loopmom_,extmom_,spsRep_,OptionsPattern[]]:=Module[{sprule,derivl,derivp,relationgeneral,relationspecific,npd=Length@pdlist,cut=OptionValue[Cut],dim=OptionValue["dimension"]},
sprule=SP2PD[pdlist,loopmom,extmom,spsRep];
derivl=derivL[pdlist,loopmom,extmom,spsRep,sprule];
derivp=derivP[pdlist,loopmom,extmom,spsRep,sprule];
relationgeneral=genIBP[derivl]~Join~genLI[derivp,extmom,spsRep];
relationspecific=Join@@Table[(relationgeneral/.Thread[Array["n"<>ToString[#]&,npd]->seedslist[[i]]])/.{"f"[ind__]/;Or@@(#<=0&/@{ind}[[cut]])->0},{i,Length@seedslist}];
relationspecific=DeleteCases[relationspecific,0];
Return[relationspecific/.{"f"[ind__]:>j@@Join[{famname},{ind}]}];
]


(* ::Section:: *)
(*ZeorSector+Symmetry*)


(* ::Subsection::Closed:: *)
(*toStandProp*)


(*after momentum conservation*)
(*scalarproducts: pi*pj\[Rule]xxx*)
(*Expand[(a_i l_i + b_j p_j)^2 - c]/.scalarproducts \[Equal] Expand[propagator]/.scalarproducts*)
(*scalarproducts: scalar products among external momenta*)
(*TODO: *)
(*one solution: done
several solution(e.g. 2l1 * p1,where l1^2\[Rule]0, p1^2\[Rule]0): pick one
no solution: Expand[(a_i l_i +b_j p_j)*(c_i p_i) - d]/.scalarproducts \[Equal]Expand[propagator]/.scalarproducts:
			one solution: done
			several solution(e.g. (l1+l2)*p1, where l1*p1\[Rule]0: pick one
			no solution: non-standard GPD, Close Symmetry!*)


Options[toStandProp]={"Range"->2};


toStandProp[prop_List,loopmom_,extmom_,scalarproducts_,OptionsPattern[]]:=Transpose[toStandProp[#,loopmom,extmom,scalarproducts]&/@prop];


toStandProp[prop_,loopmom_,extmom_,scalarproducts_,OptionsPattern[]]:=Module[
{sp,rep,moms,ansatz,tmp,eqs,sol,ff,free,para,nor,masssol,numeric,supsol,newsol},
If[Max[Exponent[prop,#]&/@loopmom]>2,Return[{prop,False}]];
Attributes[sp]={Orderless};
rep=Table[ii*jj->sp[ii,jj],{ii,loopmom},{jj,Join[loopmom,extmom]}]//Flatten//DeleteDuplicates;

(*put loopmom at left, to normalize q.n*)
moms=Join[loopmom,extmom];
ansatz=Sum[ff[i]*moms[[i]],{i,1,Length@moms}]^2+ff[Length[moms]+1];
tmp=(ansatz-prop//Expand)/.scalarproducts/.rep;
(*homogenous and non-homogenous term*)
eqs=Join[Coefficient[tmp,#]&/@Values[rep],{tmp/.Thread[Values[rep]->0]}]//DeleteCases[#,0]&;
Off[Solve::svars];
sol=Solve[Thread[eqs==0],Array[ff,Length@moms+1]];
On[Solve::svars];
If[Length[sol]>=1,
sol=sol[[-1]];
(*case 1:*)
 If[AllTrue[Values[sol],NumberQ],
  Return[{Expand[{Sum[ff[i]*moms[[i]],{i,1,Length@moms}],Sum[ff[i]*moms[[i]],{i,1,Length@moms}],ff[Length[moms]+1],1}/.sol],True}]
  ,
(*case 2:*)
  tmp=Together@Values[sol];
  free=Cases[tmp,_ff,Infinity]//Union;
(*20230602: pick one solution: fix bug of 1/(ff[2]-ff[3]), shortest mass term > shortest momentum term*)
  newsol=Table[supsol=Thread[free->xx];If[AnyTrue[Denominator[tmp],(#/.supsol)===0&],Nothing,Join[supsol,Association[sol]/.supsol//Normal]],{xx,Tuples[{0,1},Length@free]}];
  newsol=SortBy[newsol,{LeafCount[ff[Length[moms]+1]/.#],-Count[Values[#],0]}&][[1]];
  Return[{Expand[{Sum[ff[i]*moms[[i]],{i,1,Length@moms}],Sum[ff[i]*moms[[i]],{i,1,Length@moms}],ff[Length[moms]+1],1}/.newsol],True}]
]
];

(*case 3:*)
ansatz=Sum[ff[i]*moms[[i]],{i,1,Length@moms}]*Sum[ff[Length[moms]+i]*extmom[[i]],{i,1,Length@extmom}]+ff[Length[moms]+Length[extmom]+1];
tmp=(ansatz-prop//Expand)/.scalarproducts/.rep;
(*homogenous and non-homogenous term*)
eqs=Join[Coefficient[tmp,#]&/@Values[rep],{tmp/.Thread[Values[rep]->0]}]//DeleteCases[#,0]&;
Off[Solve::svars];
sol=Solve[Thread[eqs==0],Array[ff,Length[moms]+Length[extmom]+1]];
On[Solve::svars];
If[Length[sol]>=1,
	sol=sol[[-1]];
	(*case 1:*)
 If[AllTrue[Values[sol],NumberQ],
    (*normalzation*)
	nor=SelectFirst[Array[ff,Length@loopmom]/.sol,#=!=0&];
	Return[{Expand[{Sum[ff[i]*moms[[i]],{i,1,Length@moms}]/nor,Sum[ff[Length[moms]+i]*extmom[[i]],{i,1,Length@extmom}]*nor,ff[Length[moms]+Length[extmom]+1],1}/.sol],True}]
	,
	(*case 2:*)
	free=Cases[Values[sol],_ff,Infinity]//Union//Sort;
	(* for linear propagator, result may be not unique if the mass term can be removed by redefinition of q.n (e.g {l1+p1,p1+p2,0,1} vs. {l1,p1+p2,p1*p2,1}, where p1.p1\[Rule]0) so one must make sure that the mass term can not be removed.*)  
	(*if mass term m depends on free paramter f[], try to FindInstance such that m\[Equal]0. Assign other parameters(s,t,...) a number;
    if exists, ok
    otherwise, can not be removed (i.e. non-trival relation among scalar products)*)
	If[!FreeQ[(ff[Length[moms]+Length[extmom]+1]/.sol),_ff],
		para=OptionValue["Range"]; (*range for ff[], i.e 1/para \[LessEqual] Abs[ff[]] \[LessEqual] para*)
		eqs=ff[Length[moms]+Length[extmom]+1]/.sol/.ff[a_]:>1/para! ff[a]/.rep;
		numeric=Thread[Complement[Variables[eqs],free]->RandomInteger[9999,Length@Complement[Variables[eqs],free]]];
		eqs=eqs/.numeric;
		tmp=Select[Denominator[Together@Values[sol]],!FreeQ[#,_ff]&]/.numeric//Union;
		masssol=FindInstance[eqs==0&&(And@@Thread[tmp!=0])&&(And@@(-para^2<=#<=para^2&/@free)),free,Integers];
		If[masssol=!={},
			masssol=masssol[[1]];
			masssol[[All,2]]=masssol[[All,2]]/(para!) ;
			sol=Join[Association[sol]/.masssol//Normal,masssol] (*mass term is removable*)
			];
	];

	(*20230602: pick one solution: shortest mass term > shortest momentum term*)
	free=Cases[Values[sol],_ff,Infinity]//Union//Sort;
	newsol=Table[supsol=Thread[free->xx];If[AnyTrue[Denominator[Together@Values[sol]],(#/.supsol)===0&],Nothing,Join[supsol,Association[sol]/.supsol//Normal]],{xx,Tuples[{0,1},Length@free]}];
    newsol=SortBy[newsol,{LeafCount[ff[Length[moms]+1]/.#],-Count[Values[#],0]}&][[1]];
	(*p1 * ff[]p1 ===0, ff[]\[Rule]0*)
	newsol=Join[newsol,Thread[Complement[Array[ff,Length[moms]+Length[extmom]+1],Keys[newsol]]->0]];
	nor=SelectFirst[Array[ff,Length@loopmom]/.newsol,#=!=0&];
	Return[{Expand[{Sum[ff[i]*moms[[i]],{i,1,Length@moms}]/nor,Sum[ff[Length[moms]+i]*extmom[[i]],{i,1,Length@extmom}]*nor,ff[Length[moms]+Length[extmom]+1],1}/.newsol],True}];
	]
];
(*case 3:*)
Return[{prop,False}]
];


(*analogous to "ExtraZero" in DecomposeFamily2, i.e add replacement rules arise from other cut propagators*)
(*toStandProp[{l1^2,-2l1*p1+s},{l1},{p1},{p1^2\[Rule]s}] \[Rule] {{{l1,l1,0,1},{l1-p1/2,-2 p1,0,1}},{True,True}}
  toStandProp[{l1^2,-2l1*p1+s},{l1},{p1},{p1^2\[Rule]s},{1,1},{0}] \[Rule] {{{l1,l1,0,1},{l1-p1,l1-p1,0,1}},{True,True}}*)
(*For example/4_higgs_rapidity: {l1,k1-k2 u,0,1} \[Rule] {k2-k1/u-(l1 u)/2,k2-k1/u-(l1 u)/2,(4 s-msq u^3)/(4 u),1} *)
toStandProp[prop_List,loopmom_,extmom_,scalarproducts_,cutindex_,pres_,OptionsPattern[]]:=Module[
{cutmoms,res,sps},
cutmoms=Pick[loopmom,pres,0];
sps=toStanRep[extmom,scalarproducts];
res=Table[
(*20230608: fix bug. one should not drop cut propagators, because the cut propagator is not equal to delta function*)
(*sps=toStanRep[Join[cutmoms,extmom],Join[Thread[(prop[[Complement[Flatten[Position[cutindex,1]],{ii}]]])->0],scalarproducts]];*)
toStandProp[prop[[ii]],loopmom,extmom,sps]
,{ii,1,Length@prop}];
Transpose[res]
];


(* ::Subsection::Closed:: *)
(*propConvert*)


(*transform user-defined propagators to GPD form. e.g. (l1+p1)^2 -msq \[Rule] {l1+p1,l1+p1,-msq,1}*)
(*{q,q,m,1} and {q,p,m,1} is allowed*)
(*TODO: propConvert: Propagators must be expressed as products of loop momenta and external momenta plus mass. mandelstam variables are not allowed*)
propConvert[expr_List,loopmom_,extmom_]:=propConvert[#,loopmom,extmom]&/@expr;
propConvert[expr_,loopmom_,extmom_]:=Module[{mass,mom},
mass=expr/. (#->0&/@Join[loopmom,extmom]);
(*square*)
mom=Factor[expr-mass]/.(a_:1)Power[b__,2]/;a>0:>Expand[Sqrt[a]*{b,b}];
If[Head[mom]===List,Return[Join[mom,{mass,1}]]];
(*linear*)
mom=mom/.Times[a__,b___]/;!FreeQ[a,Alternatives@@loopmom]:>{a,Times[b]};
If[Head[mom]===List,Return[Join[mom,{mass,1}]]];
(*other*)
Print["error: can not handle general propagator",expr];Abort[]];


(* ::Subsection::Closed:: *)
(*toSPRep*)


toSPRep[spsReprule_]:=Rule[#[[1]]/.{Times[a_,b_]:>SP[a,b],Power[a_,2]:>SP[a,a]},Expand@#[[2]]]&/@spsReprule;


(* ::Subsection::Closed:: *)
(*ExternalSymmetry*)


(*Return a list of external momenta replacements that keep mandelstam variables invariant*)
(*The first element is Identity transformation*)
(*ext: independent external momenta*)
(*rule: scalar products of independent external momenta.*)
ExternalSymmetry[ext0_,cut0_,rule0_]:=Module[
{ext,rule, list, mass, ems, rep, keys, perms, eqs},

(*Treat cut as external momenta, to restore momentum conservation*)
ext = Flatten[{ext0,cut0}];
(*Transform rule0 to standard replacements,e.g. (p1+p2)^2\[Rule]s is not allowed. p1*p2\[Rule]s/2 is allowed*)
rule = toStanRep[ext,rule0];

(*Restore the depedendent external momenta from independent external momentas*)
(*Sign matters in square propagators, e.g. to flip {k3^2,(k1-k3-k4)^2}, transformation should be k3\[Rule]k1-k3-k4 instead of k3\[Rule]-k1+k3+k4*)
list=Tuples[{-1,1},Length@ext];
list=Table[Rule[list[[i]] . ext,((list[[i]] . ext)^2//Expand)/.rule//Expand],{i,1,Length@list}];

(*List all possible groups of momenta that leaves p_i^2 invariant.Each elements is important since sign matters*)
mass=Thread[ext ->(ext^2/.rule)];
list=#[[1]]->GatherBy[Append[mass,"x"->#[[2]]],Values]&/@list;

(*Distinguish integrated momenta 'cut0' and unintegrated(external) momenta 'ext0'*)
(*If there exists integrated momenta, the dependent external momenta('x') would be treated as cut propagator*)
(*Otherwise, 'x' is treated as external momenta*)
list=Table[Rule[llist[[1]],Join@@(GatherBy[#,Function[{xx},MemberQ[If[cut0=!={},ext0,Join[ext0,{"x"}]],Keys[xx]]]]&/@llist[[2]])],{llist,list}];

(*List all possible permutations that leaves mandelstam variables(\"eqs\" below) invariant*)
eqs=rule/.Rule[a_,b_]:>a-b;

ems=Join@@Table[
rep=list[[i,1]];
keys=Flatten[Keys/@list[[i,2]]];
perms=Distribute[Permutations[Keys[#]]&/@list[[i,2]],List,List,List,Join]/.("x"->rep)//Expand;
perms=Table[If[AllTrue[eqs,Expand[(#/.Thread[keys->perm]//Expand)/.rule]===0&],perm,Nothing],{perm,perms}];
(ext/.Thread[keys->#])&/@perms
,{i,1,Length@list}];
ems=DeleteDuplicates[ems];
If[!MemberQ[ems,ext],Print["error: unknown external symmetry patten"];Abort[],ems=Join[{ext},ems]//DeleteDuplicates];
ems=Thread[ext->#]&/@ems;

ems];


(*rep: only involve 'ext'*)
toStanRep[ext_,rep_]:=Module[{vars,sol},
vars=Table[Rule[ext[[i]] * ext[[j]],"SP"[ext[[i]],ext[[j]]]],{i,1,Length[ext]},{j,i,Length[ext]}]//Flatten;
Off[Solve::svars];
sol=Solve[Expand[rep]/.vars/.Rule->Equal,Values[vars]][[1]];
On[Solve::svars];
sol/.(Reverse/@vars)
];


(* ::Subsection:: *)
(*extraCutSymmetry*)


(*ExternalSymmetry can not handle {{k1+l1,k1+l1,-eta,1},{k1-k4,k1-k4,-mt2,1},{k3,k3,-mt2,1},{k3-k4,k3-k4,0,1}}, where k1-k4 \[TwoWayRule] k3 are symmetric cut *)
(*extraCutSymmetry: try all possible permuations of cut propagators and external symmetries*)
(*cutgpds: e.g. {{k1-k4,k1-k4,-mt2,1},{k3,k3,-mt2,1},{k3-k4,k3-k4,0,1}*)
(*psmoms: integrated cut momenta, e.g. {k3,k4}*)
(*extsymmetry: external symmetry, without psmoms, e.g. {{k1\[Rule]k1},{k1\[Rule]-k1}}*)
extraCutSymmetry[cutgpds_,psmoms_,extmoms_,extsymmetry_,SPRep_]:=Module[{cutmoms,sym,allmoms,f,props},
(*GatherBy massterm, to accelerate calculation*)
cutmoms=GatherBy[cutgpds,#[[3]]&][[All,All,1]];
sym=Permutations/@cutmoms;
(*propagator is unchanged by multiplying momenta by -1*)
sym=Join@@@Map[With[{sign=Tuples[{-1,1},Length[#]]}, Table[#*sign[[i]],{i,Length@sign}]]&,sym,{2}]; (*<-- {{{k3,k4},{k4,k3}...},{....}}, where k3 and k4 belong to same group*)
sym=Flatten/@Tuples[Join[{extmoms/.extsymmetry},sym]];
(*Integral is unchanged by multiplying all momenta by -1, pick unique one*)
sym=Sort[{-#,#}][[2]]&/@sym//DeleteDuplicates;
allmoms=Join[extmoms,psmoms];
	(***For each i, find rules of momenta replacement to change sym[[1]] to sym[[i]].
		'sym' looks like {{p1\[Rule]p1,p2\[Rule]p2,p3\[Rule]p3,...,q1\[Rule]q1,q2\[Rule]q2},{p1\[Rule]p2,p2\[Rule]p1,p3\[Rule]p3,...,q1\[Rule]q1,q2\[Rule]q2},...}.***)
	sym=Table[Solve[Thread[(sym[[i]])==(sym[[1]]/.Thread[allmoms->(f/@allmoms)])],(f/@allmoms)]//
		If[#=!={},#[[1]],Nothing]&,{i,Length@sym}]/.f->Identity;
(*1(-1)*)
props=GPDExplicit2[GPD[{#1,#2,#3,-1}]&@@@cutgpds,SPRep];
sym=Select[sym,ContainsAll[props,GPDExplicit2[GPD[{#1,#2,#3,-1}]&@@@(cutgpds/.#),SPRep]]&]]


(* ::Subsection:: *)
(*SPExpand2 + GPDExplicit2+MomentumQ*)


(*All variables except momentums are regarded as numbers, which means they can be factored out from SP[l,l]. e.g. SP[k1, y k2]*)
SPExpand2[exp00_,SPRep00_]:=CalcLoop`Private`SPExpand[exp00,SPRep00]//.SP[a_?(Cases[{#},Alternatives@@famMomentum,Infinity]==={}&)*pi00_,pj00_]:>a*SP[pi00,pj00]/.SPRep00;   
GPDExplicit2[exp00_,SPRep00_]:=SPExpand2[GPDExplicit[exp00,SPRep00],SPRep00];        
Unprotect[MomentumQ];
MomentumQ[l_]:=MemberQ[famMomentum,l];


(* ::Subsection::Closed:: *)
(*DefineFamilies2*)


(*ExtraZero: Using CutPrapagators\[Equal]0 to help judge zero sectors. 
For instance, (l1^2 * (l1+k3)^2 * k3^2 * (k3-k1)^2), where the first two are loop propagators and last two are cut propagators.
Once we assume final results don't contain dots on cut propagators, then we can set this sector to be zero during intermediate computation*)
(*Question is:  what does dots mean? *)
Options[DefineFamilies2]={"ExternalSymmetry"->{},"ExtraZero"->False, "Parallelize"->False,"CutInformation"->{{},{}},"DefineSectorQ"->True,
"KnownFamilies"->Association[],"KnownSectors"->Association[],"MonitorQ"->True,"ISP"->2};
(*topsector: {1,1,1,1,0,0}*)
(*cutindex: {0, 0, 0, 1, 1, 0}*)
(*gpds: {{{l,l,0,1},...},{{l,l,0,1},...},...}*)
(*loopmoms: don't include phase space integration! : {l1,l2...} or {{l1,l2},{l3,l4},...}*)
(*momE: independent external momenta, {p1,p2,...}*)
(*SPRep: scalar products among momE : {SP[p1,p1]\[Rule]0,...}*)
DefineFamilies2[topsector_,cutindex_,gpds_,loopmoms_List,momE_List,SPRep_,OptionsPattern[]]:=Module[
	{monitor,timing,cutmoms,psmoms,loopMs,vars,adds,$FamilyInf,$SectorInf,table,gpdlist,nd,
	i,j,famNo,secNo,den2SP,sp2Den,Di,denCur2Uni,denUni2Cur,secInf,n,list,rep,symRep,dens,denj,sec,
	famInf,X,uniqueMom,uniqueMomSort,uniqueMomSec,repInv,symRep2,$FamilyInfi,$SectorInfi, 
	extsyms, posi, extrep, denjnocut, cutprops, cutSPRep},  (*add by Xin Guan*)
	
	If[OptionValue["MonitorQ"]===True,
		monitor=Monitor;
		timing=LCTiming;
		,
		monitor=List;
		timing=Identity
	];
	
	(*** cut momenta and momenta of phase space integration***)
	{cutmoms,psmoms}=OptionValue["CutInformation"];
	
	loopMs=Flatten[{loopmoms,psmoms}];
	vars=Outer[SP,Join[loopMs,momE],Join[loopMs,momE]]/.SPRep//Variables;
	vars=Select[vars,!FreeQ[#,Alternatives@@loopMs]&];
	
	$FamilyInf=OptionValue["KnownFamilies"];
	$SectorInf=OptionValue["KnownSectors"];

	table=If[OptionValue["Parallelize"],
		SetOptions[ParallelTable,DistributedContexts->All];ParallelTable,
		Table
	];
	
	(*Maintain user-defined ordering of denominators. GPDCombine would sort denominators*)
	gpdlist=gpds; (*add by Xin Guan*)
	nd=Length@gpdlist;
	
	(*TODO: For multi-family definition*)
	(*uniqueMom=table[
		denj=gpdlist[[j]];
		If[denj===1,denj={}];
		LoopSymmetry[denj,loopmoms,SPRep,"CutInformation"->{cutmoms,psmoms}],{j,1,nd}
	];*)
	
	(*In fact, nd === 1*)
	monitor[For[i=1,i<=nd,i++,
		
		(*Pick the user-defined denominators*)
		dens=gpdlist[[i]];
		n=Length@Flatten[Position[topsector,1]];	
			
		(*If the corresponding sector has not been defined, define a new family.*)
		If[dens===0||KeyExistsQ[$SectorInf,dens],Continue[]];
		famNo=Length@$FamilyInf+1;	
		$FamilyInfi=Association["No"->famNo];		
		
		(*Define rules*)
		den2SP=Thread[Array["Den"[famNo],Length@dens]->GPDExplicit2[GPD[{#1,#2,#3,-1}]&@@@dens,SPRep]];    (*add by Xin Guan*)
		sp2Den=Solve[den2SP/.Rule->Equal,vars];
		If[Length@sp2Den=!=1,Print["Error in define Families: sol. ",den2SP,vars,dens,gpds];Abort[]];
		$FamilyInfi["SP2Den"]=sp2Den[[1]]//Expand;
		
		(*Explore all sectors in the family.*)
		list=SortBy[Table[PadLeft[IntegerDigits[j,2],n],{j,0,2^n-1}],Total]//Reverse;
		list=Table[ReplacePart[ConstantArray[0,Length[dens]],Thread[Flatten[Position[topsector,1]]->llist]],{llist,list}];  (*add by Xin Guan*)
		
		(*** continue if no sector information is required ***)
		If[OptionValue["DefineSectorQ"]===False,
			$FamilyInf[dens]=$FamilyInfi;
			Continue[]
		];
		
		(*Remove sectors that don't contain all cut propagators before any analysis*)
		Do[If[AnyTrue[list[[j]]-cutindex,Negative],$FamilyInfi[list[[j]]]=0]     (*add by Xin Guan*)
		  ,{j,Length@list}];
		list=Select[list,AllTrue[#-cutindex,NonNegative]&];      (*add by Xin Guan*)
		
		(*Symmetries from permutations of external momenta that leave mandelstam variables invariant*)
		extsyms=If[OptionValue["ExternalSymmetry"]==={},Thread[momE->momE],OptionValue["ExternalSymmetry"]];  (*add by Xin Guan*)
		
		(*List of lists of {denj,rep}*)
		(*LoopSymmetry can not handle cut propagators because function branchClassify only explore symmetries of loopmoms \[Rule] must consider*)
		(*We set user-defined cut propagators as standard propagators manually. This is allowed because 'extsyms' keep cut propagators invariant.*)
		cutprops = dens[[Position[cutindex,1]//Flatten]];    (*add by Xin Guan*)
		(*If open "ExtraZero", LoopSymmetry would take advantage of CutPropagator\[Equal]0 to judge ZeroSectors*)
		cutSPRep = If[OptionValue["ExtraZero"], toSPRep@toStanRep[Join[psmoms,momE],Join[Thread[(("Den"[famNo]/@Flatten[Position[cutindex,1]])/.den2SP)->0],SPRep]/.SP:>Times],SPRep];
		uniqueMomSec=table[
			denjnocut = dens[[Position[list[[j]]-cutindex,1]//Flatten]];
			Table[
			
				{denj,rep}=LoopSymmetry[denjnocut/.extrep,loopmoms,cutSPRep,"CutInformation"->{cutmoms/.extrep,psmoms}];
				
				(*If there is no pure loopmoms, we needed to find the unique form. 
				Integrals are unchanged by multiplying all momenta by -1, or by sorting denominators*)
				If[Length[loopmoms]===0,
					denj[[All,1;;2]] = CalcLoop`LoopSymmetry`branchClassify[denj,psmoms,{}][[1]]*denj[[All,1;;2]]//Expand;
					denj=SortBy[denj,{Table[Coefficient[#[[1]],loopMs[[-i]]],{i,Length@loopMs}],LeafCount@#[[1]]}&];
				];
				(*In principle, {denj,rep} is ok. But we would like to show cut propagators explicitly in $SectorInf*)
				If[denj===0,{denj,rep},{Join[denj,cutprops],rep}]
				,{extrep,extsyms}],{j,1,Length@list}  (*add by Xin Guan*)
			
		];
		
		For[j=1,j<=Length@list,j++,
			sec=list[[j]];
			
			(*One permutation is enough to identify zero sectors*)
			{denj,rep}=uniqueMomSec[[j,1]];  (*add by Xin Guan*)
			If[denj===0,$FamilyInfi[sec]=0;Continue[]];
			
			(*If the corresponding sector has been defined, relate it to the defined one.*)
			(*We should check all external momenta permutations*)
			(*We denote Denominators after external momenta permutation as Den'. Its den2SP should be modified as den2SP/.extrep*)
			(*We obtain transformations between Den' and UniqeSectors in the following step*)
			(*However, the Den' and Den represent the same Feynman integral, so no further transformation is needed*)
			If[AnyTrue[uniqueMomSec[[j,All,1]],KeyExistsQ[$SectorInf,#]&],
				posi=FirstPosition[uniqueMomSec[[j]],_?(KeyExistsQ[$SectorInf,#[[1]]]&),{},{1},Heads->False][[1]]; (*add by Xin Guan*)
				extrep=extsyms[[posi]];  (*add by Xin Guan*)
				{denj,rep}=uniqueMomSec[[j,posi]];  (*add by Xin Guan*)
				secInf=$SectorInf[denj];
				denCur2Uni=SPExpand2[den2SP/.extrep/.rep[[1]]/.secInf["Mom.Stan2Curr"],SPRep]/.
					Lookup[$FamilyInf,Key@secInf@"Family",$FamilyInfi]["SP2Den"]//Expand;  (*add by Xin Guan*)
				(*** calculate inverse mapping ***)
				denUni2Cur=Solve[Table["den"[k]==denCur2Uni[[k,2]],{k,Length@denCur2Uni}]/.Rule->Equal,
					Union@Cases[denCur2Uni[[All,2]],"Den"[_][_],Infinity]][[1]]/.
					Thread[Array["den",Length@denCur2Uni]->denCur2Uni[[All,1]]]//Expand;
				$FamilyInfi[sec]=Association["No"->secInf@"No","Den.Curr2Uniq"->denCur2Uni,
					"Den.Uniq2Curr"->denUni2Cur,"Mom.Curr2Stan"->rep[[1]],"Mom.ExtSym"->extrep];  (*add by Xin Guan*)
				Continue[]
			];
			
			
			(*If the corresponding sector has not been defined, define a new sector.*)
			(*do not forget to merge extsyms*)
			rep=Join@@First[GatherBy[Table[{uniqueMomSec[[j,kk,1]],Join[#,extsyms[[kk]]]&/@uniqueMomSec[[j,kk,2]]},{kk,1,Length[extsyms]}],First]][[All,2]]; (*<-- include external symmetry*)
			
			secNo=Length@$SectorInf+1;
			$FamilyInfi[sec]=secNo;
			(*** change standard denominators to current denominators ***)
			repInv=Solve[Thread[Array[X,Length@Join[loopMs,momE,cutmoms]]==(Join[loopMs,momE,cutmoms]/.rep[[1]])],Join[loopMs,momE,cutmoms]][[1]]/.
				Thread[Array[X,Length@Join[loopMs,momE,cutmoms]]->Join[loopMs,momE,cutmoms]];  (*<--- complete transformation rule, include loop, cut and external*)
			(*** symmetries of denominators: from replacements of loop momenta ***)
			symRep=Thread[#[[All,1]]->Expand[#[[All,2]]/.repInv]]&/@rep[[2;;-1]];
			(*** symmetries of denominators: from replacements of denominators and ISPs ***)
			symRep2=Expand[SPExpand2[den2SP/.#,SPRep]/.sp2Den[[1]]]&/@symRep;
			$SectorInf[denj]=Association["No"->secNo,"Family"->dens,"Sector"->sec,
									"Mom.Stan2Curr"->repInv,"Mom.Syms"->symRep,"Den.Syms"->symRep2];
		];
		
		$FamilyInf[dens]=$FamilyInfi;
	],{i,"out of",nd}];
	
	WriteMessage["Number of Families = ",Length@$FamilyInf,
		". Number of Sectors = ",Length@$SectorInf,"."];
	
	(*** define replacement rule for each input . In practice, there is only one family***)
	{$FamilyInf,$SectorInf,Table[
		
		$FamilyInf[[i]]["SP2Den"]
		,{i,1,nd}
	]}
];


(* ::Section:: *)
(*BasicFunction*)


(* ::Subsection::Closed:: *)
(*setDimension*)


SetDim[d0_]:=InternalD=d0;
$d:=InternalD;


(* ::Subsection::Closed:: *)
(*Integrals*)


IntSector[int_]:=BL[int[[1]],(If[#>0,1,0]&/@(int[[2]]))];
IntPropagators[int_]:=Length@Select[int[[2]],#>0&];
IntDots[int_]:=Total@Select[int[[2]]-1,#>0&];
IntRank[int_]:=-Total@Select[int[[2]],#<0&];
SectorOrSubsectorQ[fsec1_,fsec2_]:=AllTrue[fsec1[[2]]-fsec2[[2]],#<=0&];


SortIntegrals[ints_]:=SortBy[ints, DefaultWeight];
IntegralsIn[expr_]:=Cases[{expr},_BL,Infinity]//DeleteDuplicates;


(*1 by default*)
IntegralOrdering=1;
DefaultWeight:=IntegralWeight[IntegralOrdering];


(*sectors are ordered by the number of lines*)
(*1: ISP are as simpler as dots *)
(*2: dots are as simpler as ISP*)
(*3: The first complexity is the sum of dots and ISP. If the sum is equal, ISP are regarded as simpler *)
(*4: Like 3, but if the sum is equal, dots are regarded as simpler*)
IntegralWeight[order_]:=Switch[order,
1, Function[{int},{-IntPropagators[int], -IntDots[int], -IntRank[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
2, Function[{int},{-IntPropagators[int], -IntRank[int], -IntDots[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
3, Function[{int},{-IntPropagators[int], -IntDots[int] -IntRank[int], -IntDots[int], -IntRank[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
4, Function[{int},{-IntPropagators[int], -IntDots[int] -IntRank[int], -IntRank[int], -IntDots[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
_, Print["error: unknown integral ordering", Abort[]]]


(* ::Subsection:: *)
(*NewBasis*)


ClearAll[FamilyInfo];
FamilyInfo=Association[];


Options[NewBasis]={"GenerateIBP"->True,"ExtraIntDeriv"->{}};
NewBasis[family_,loopdenominators_,loopmomenta_,externalmomenta_,momentumconservation_,scalarproducts_,OptionsPattern[]]:=Module[
{indepleg, sps, eqs, vars, pos, paras},

indepleg=Select[externalmomenta,FreeQ[Keys@momentumconservation,#]&];
sps=Outer[Times,loopmomenta,Join[loopmomenta,indepleg]]//Flatten//DeleteDuplicates;
If[Length@loopdenominators=!=Length@sps,Print["error: overdetermined basis"];Abort[]];
If[Length@MaximalGroup[Coefficient[loopdenominators/.momentumconservation,#]&/@sps]=!=Length@sps,Print["error: not a valid basis"];Abort[]];
sps=Outer[Times,indepleg,indepleg]//Flatten//DeleteDuplicates;
If[Length@MaximalGroup[Coefficient[scalarproducts/.momentumconservation/.Rule[a00_,b00_]:>a00-b00,#]&/@sps]=!=Length@sps,Print["error: not a valid scalar products"];Abort[]];

FamilyInfo[family]=Association[{}];
FamilyInfo[family]["Family"] = family;
FamilyInfo[family]["Propagators"] = loopdenominators/.momentumconservation;
FamilyInfo[family]["Loop"] = loopmomenta; 
FamilyInfo[family]["Leg"] = externalmomenta;
FamilyInfo[family]["MomCons"] = momentumconservation;
FamilyInfo[family]["IndepLeg"] = Select[externalmomenta,FreeQ[Keys@momentumconservation,#]&];
FamilyInfo[family]["Replacements"] = toStanRep[FamilyInfo[family]["IndepLeg"],scalarproducts/.momentumconservation];

(*used in function relationGeneral3. to generate generalized IBP&LI identities*)
$ExtraIntDeriv = If[OptionValue["ExtraIntDeriv"]=!={},OptionValue["ExtraIntDeriv"],ConstantArray[0,{1,Length@loopdenominators,1}]];


(*used in function SPExpand2. to handle SP[k1, y k2]*)
famMomentum=Join[FamilyInfo[family]["Loop"],FamilyInfo[family]["Leg"]];

(*IBP*)
If[OptionValue["GenerateIBP"],
{IBP[family],LI[family]}=relationGeneral3[family,FamilyInfo[family]["Propagators"],FamilyInfo[family]["Loop"],FamilyInfo[family]["IndepLeg"],FamilyInfo[family]["Replacements"]];
];

(* Symmetry Relation *)
SymMap[family]=Join@@symmetryEquation[family,#,FamilyInfo[family]["Propagators"],FamilyInfo[family]["Loop"],FamilyInfo[family]["IndepLeg"],{},FamilyInfo[family]["Replacements"]]&;
(*SymMap[family] applies to a list of seeds*)

(*(*2022.0625: vacuum*)
BTSTUToSP = If[scalarproducts==={},{},
eqs=(#[[1]]-#[[2]]==0)&/@scalarproducts;
vars=Values@scalarproducts//Variables;
pos=MaximalGroup[CoefficientArrays[eqs,vars][[2]]//Normal];
Solve[eqs[[pos]],vars][[1]]];*)

(*BTMassScale = Complement[Variables[{loopdenominators,Values@scalarproducts}],Join[loopmomenta,externalmomenta]]; *)
];


relationGeneral2[famname_,pdlist_,loopmom_,extmom_,spsRep_]:=Module[{sprule,derivl,derivp,relationgeneral,relationspecific,npd=Length[pdlist]},
sprule=SP2PD[pdlist,loopmom,extmom,spsRep];
derivl=derivL[pdlist,loopmom,extmom,spsRep,sprule];
derivp=derivP[pdlist,loopmom,extmom,spsRep,sprule];
Return[{Function[Evaluate[genIBP2[derivl]/.interalF[ind__]:>BL[famname,{ind}]/.Thread[Array["n"<>ToString[#1]&,npd]->Array[Slot,npd]]]],
Function[Evaluate[genLI2[derivp,extmom,spsRep]/.interalF[ind__]:>BL[famname,{ind}]/.Thread[Array["n"<>ToString[#1]&,npd]->Array[Slot,npd]]]]}]];


MaximalGroup[{}]:={};
MaximalGroup[matrix_]:= Flatten[FirstPosition[#,1]&/@Select[RowReduce[Transpose@matrix], !AllTrue[#, #===0&]&]];


(* ::Subsection:: *)
(*AnalyzeSectors*)


(*Treat external symmetry as cut? 
We must input the real momentum conservation as cut propagators to explore full symmetry.
If user does not input real momentum conservation,e.g aug\[Rule]aug, we should call function ExternalSymmetry to find it out.*)
(*Distinguish external symmetry and cut?
We should input "CutInformation" for LoopSymmetry to explore symmetries among cut momentum.
If we use Cut propagators, the function Solve costs a lot of time.
Else if we input symmetric cut momentum, as demonstrated in usage, we should call function ExternalSymmetry to learn symmetric cut momentum from cut propagators.*)


(*top: {1,1,1,1,1,1,1,0,0} . 1 denotes denorminator, 0 denotes irreducible scalar products*)
(*Prescription: {1,0,-1} \[Rule] {l+i0+, cut momenta, l-i0+}*)
Options[AnalyzeSectors]={"CutDs"->Automatic,"Prescription"->Automatic, "ExtSyms"->True, "CloseSyms"->False,"ExtraZero"->True};
AnalyzeSectors[family_,top_,OptionsPattern[]]:=Module[{cut,pres, flag, n,list, loopmoms, cutmoms, extsyms, curr,fam,sec,sym,loop,ord,posi, paras, tmp},
FamilyInfo[family]["Top"] = top;
cut = If[OptionValue["CutDs"]===Automatic, ConstantArray[0,Length@top],OptionValue["CutDs"]];
FamilyInfo[family]["Cut"] = cut;
pres = If[OptionValue["Prescription"]===Automatic, ConstantArray[1,Length@FamilyInfo[family]["Loop"]], OptionValue["Prescription"]];
FamilyInfo[family]["Prescription"]= pres;
FamilyInfo[family]["ExtSyms"] = ExternalSymmetry[FamilyInfo[family]["IndepLeg"],{},FamilyInfo[family]["Replacements"]];

SPRep = toSPRep@FamilyInfo[family]["Replacements"];
If[OptionValue["ExtraZero"],
  {GPDList,flag} = toStandProp[FamilyInfo[family]["Propagators"],FamilyInfo[family]["Loop"],FamilyInfo[family]["IndepLeg"],FamilyInfo[family]["Replacements"],FamilyInfo[family]["Cut"],FamilyInfo[family]["Prescription"]];
  ,
  {GPDList,flag} = toStandProp[FamilyInfo[family]["Propagators"],FamilyInfo[family]["Loop"],FamilyInfo[family]["IndepLeg"],FamilyInfo[family]["Replacements"]]
  ];
closeSymFlag = False;

(*If propagators can not be expressed as GPD form, close symmetry forcibly*)
If[OptionValue["CloseSyms"]||!(And@@flag),
closeSymFlag = True;
If[!OptionValue["CloseSyms"],Print["Caution: Symmetry is closed due to non-standard propagators -> ",Pick[GPDList,flag,False]]];
n=Length@Flatten[Position[top,1]];
list=SortBy[Table[PadLeft[IntegerDigits[j,2],n],{j,0,2^n-1}],Total]//Reverse;
list=Table[ReplacePart[ConstantArray[0,Length[top]],Thread[Flatten[Position[top,1]]->llist]],{llist,list}];
If[!AllTrue[Flatten[$ExtraIntDeriv],#===0&],
  Print["Caution: ZeroSector is closed due to generalized-integrand. Non-positive index cut-propagators still treated as zero sectors."];
  NonZeroSectors[family]=BL[family,#]&/@With[{pos=Flatten@Position[FamilyInfo[family]["Cut"],1]},Select[list,AllTrue[#[[pos]],#=!=0&]&]];
  ZeroSectors[family]=BL[family,#]&/@With[{pos=Flatten@Position[FamilyInfo[family]["Cut"],1]},Select[list,AnyTrue[#[[pos]],#===0&]&]];
  ,
NonZeroSectors[family]={};
ZeroSectors[family]={};
Do[If[AnyTrue[llist[[Flatten[Position[FamilyInfo[family]["Cut"],1]]]],#===0&]||ZeroSectorQ[Pick[FamilyInfo[family]["Propagators"],llist,1],FamilyInfo[family]["Loop"],SPRep],
		AppendTo[ZeroSectors[family],BL[family,llist]],
		AppendTo[NonZeroSectors[family],BL[family,llist]]
		];
	,{llist,list}];
];
UniqueSectors[family]=NonZeroSectors[family];
MappedSectors[family]={};
ExtMappedSectors[family]={};
Print[StringTemplate["`1` zero sectors found, `2` nonzero sectors found."][Length[ZeroSectors[family]],Length[NonZeroSectors[family]]]];
Print[StringTemplate["`1` unique sectors found, `2` mapped sectors found, `3` extmapped sectors found."][Length[UniqueSectors[family]],Length[MappedSectors[family]],Length[ExtMappedSectors[family]]]];
Return[{}];
];

(*Define family. For the convenience of DefineFamilies2, we combine extsyms and cut-symmetry together*)

(*distinguish left loop, cut , right loop momenta*)
loopmoms=Pick[FamilyInfo[family]["Loop"],pres,#]&/@{1,-1}/.{}->Nothing;
cutmoms=Pick[FamilyInfo[family]["Loop"],pres,0];

(***explore symmetric cut and external momenta symmetry***)
extsyms = ExternalSymmetry[FamilyInfo[family]["IndepLeg"],cutmoms,Join[Thread[FamilyInfo[family]["Propagators"][[Flatten[Position[cut,1]]]]->0],FamilyInfo[family]["Replacements"]]];
extsyms = Join[extsyms,extraCutSymmetry[GPDList[[Flatten[Position[cut,1]]]],cutmoms,FamilyInfo[family]["IndepLeg"],FamilyInfo[family]["ExtSyms"],SPRep]]//DeleteDuplicates;
If[!OptionValue["ExtSyms"],extsyms=Select[extsyms,FamilyInfo[family]["IndepLeg"]===(FamilyInfo[family]["IndepLeg"]/.#)&]];
{$FamilyInf,$SectorInf,$FamilyRep}=DefineFamilies2[top,cut,{GPDList},loopmoms,FamilyInfo[family]["IndepLeg"],SPRep,"CutInformation"->{{},cutmoms},"ExternalSymmetry"->extsyms];

(*ZeroSectors*)
ZeroSectors[family]=BL[family,#]&/@Keys[Select[Normal[$FamilyInf[[1]]][[3;;]],Values[#]===0&]];
NonZeroSectors[family]=BL[family,#]&/@Keys[Select[Normal[$FamilyInf[[1]]][[3;;]],Values[#]=!=0&]];
Print[StringTemplate["`1` zero sectors found, `2` nonzero sectors found."][Length[ZeroSectors[family]],Length[NonZeroSectors[family]]]];

(*Symmetries*)
UniqueSectors[family]=BL[family,#]&/@Keys[Select[Normal[$FamilyInf[[1]]][[3;;]], NumberQ[Values[#]]&&Positive[Values[#]]&]];
MappedSectors[family]=BL[family,#]&/@Keys[Select[Normal[$FamilyInf[[1]]][[3;;]], !NumberQ[Values[#]]&]];
ExtMappedSectors[family]={};
Print[StringTemplate["`1` unique sectors found, `2` mapped sectors found, `3` extmapped sectors found."][Length[UniqueSectors[family]],Length[MappedSectors[family]],Length[ExtMappedSectors[family]]]];

(*whether to Together[]*)
paras = Complement[IBP[family][Sequence@@RandomInteger[999,Length[FamilyInfo[family]["Propagators"]]]]/.BL[a___]:>"yy"//Variables,{"yy"}];
tmp=Flatten[{Table[Values[$SectorInf[[i]]["Den.Syms"]],{i,1,Length@$SectorInf}],Table[If[Head[kkk]===Association,Join[Values[kkk["Den.Curr2Uniq"]],Values[kkk["Den.Uniq2Curr"]]],{}],{kkk,$FamilyInf[[1]]}]}];
togetherFlagSym = AnyTrue[tmp,!PolynomialQ[#,paras]&];
];


SubSectors[fsec_]:=Module[{corner,posi,s1},
corner=fsec[[2]];
posi=Position[corner,_?Positive]//Flatten;
s1=Tuples[{0,1},Length[posi]];
s1=Map[PadRight[#,Length[corner]]&,s1];
posi=FirstPosition[Join[posi,Complement[Range[Length[corner]],posi]],#]&/@Range[Length[corner]]//Flatten;
BL[fsec[[1]],#]&/@Map[corner-#&,s1[[All,posi]]]
];


(* ::Subsection:: *)
(*SectorRelations*)


SectorRelations[exp0_,opt:OptionsPattern[]]/;(Head[exp0]=!=List):=SectorRelations[{exp0},opt][[1]];


Options[SectorRelations]={"NumeratorSymmetry"->True};
SectorRelations[exp0_List,OptionsPattern[]]:=Module[{topo,exp,asso},

topo=FirstCase[exp0,_BL,{},Infinity];
If[topo==={},Return[exp0],topo=topo[[1]]];

(*Transform BL[topo,index] to GPD[....]*SP[...], where numerators are expressed as SP[..] and denorminators are expressed as GPD[..[*)
exp=BL2GPD[exp0,GPDList,SPRep];

(*"e"[i] is a tag of i-th expresion*)
exp=Separate[Plus@@(Array["e",Length@exp]*exp),_GPD];
exp[[2]]=GPDExplicit2[#,SPRep]&/@exp[[2]];

asso=Table[
CalcLoop`Family`mapSingleFamily[$FamilyInf,$SectorInf,$FamilyRep[[1]],exp[[1,i]],exp[[2,i]],"NumeratorSymmetry"->OptionValue["NumeratorSymmetry"]]
,{i,Length@exp[[2]]}];
asso = CalcLoop`Private`associationPlus@asso;
(*CalcLoop`FI \[Rule] BladeIBP`BL \[Rule] Blade`BL*)
asso=Plus@@(Flatten[Normal@Values@asso]/.Rule->Times/.FI:>BL/.BL[a_,b_]:>BL[topo,b]);

If[asso===0,Return[{0}]];
(*restore "e"[i]*)
asso= Collect[#,_BL,Factor]&@Normal@CoefficientArrays[asso,Array["e",Length@exp0]][[2]];
asso
];


BL2GPD[expr_,gpdlist_,SPRep_]:=Module[{gpdform},
gpdform=ReplaceRepeated[expr/.{BL[famname_,ind_]:>GPD@@Table[Append[#[[i,1;;3]],ind[[i]]],{i,Length[#]}]&@gpdlist},{GPD[c1___,{a1_,a2_,b_,pow_},c2___]/;pow<=0:>(Distribute@SP[a1,a2]+b)^(-pow)*GPD[c1,c2]}];
(*gpdform=gpdform/.{SP[-a1_,-a2_]:>SP[a1,a2],SP[-a1_,a2_]:>-SP[a1,a2],SP[a1_,-a2_]:>-SP[a1,a2]}/.SPRep;*)
gpdform = SPExpand2[gpdform,SPRep];
Return[gpdform];
]


(* generating symmetry relations bewteen and within sectors*)
Options[symmetryEquation]={"GeneralCut"->{},"FamilyDef"->{},"TrimUniqueSector"->False,"UniqSecFindMinTerm"->True,"AddSubsector"->False};
symmetryEquation[famname_,seedslist_,pdlist_,loopmom_,extmom_,conservation_,spsRep_,OptionsPattern[]]:=
Module[{familyInf,sectorInf,famRep,fis,fisGrouped,maxes,dots,rank,sec,fisnew,currSec,uniqSec,currSecNo,currDenSyms,
symmEq,symmEqs,symmEqsZ={},symmEqsU={},symmEqsM={},npd=Length@pdlist,cut=OptionValue["GeneralCut"],cutFI},
cutFI[expr_,cut_]:=expr/.{FI[a_,ind_List]/;!ContainsAll[Flatten@Position[ind/.{b_/;b<=0->0,b_/;b>0->1},1],cut]:>0};

fis=FI[1,#]&/@seedslist;
fisGrouped=GroupBy[fis,#[[2]]/.{_?Negative:>0,_?Positive:>1}&];
Print["sectors involved: ",Keys[fisGrouped]];

(* read the results of DefineFamilies *)
If[OptionValue@"FamilyDef"=!={},{familyInf,sectorInf,famRep}=OptionValue@"FamilyDef",familyInf=$FamilyInf;sectorInf=$SectorInf;famRep=$FamilyRep];
(*Print["familyInf: ",familyInf,"\nsectorInf: ",sectorInf];*)

(*Print["sectors: ",Table[Head[familyInf[[1]]@Keys[fisGrouped][[i]]],{i,Length@fisGrouped}]];*)
While[Length@fisGrouped>0,
maxes=MaximalBy[Keys@fisGrouped,CalcLoop`Family`orderingFI[FI[1,#]]&];
Table[currSec=maxes[[i]];
If[!KeyExistsQ[familyInf[[1]],currSec],Print["error: unknown sector -> ",currSec];Abort[]];
currSecNo=familyInf[[1]][currSec];(*Print["current sector: ",currSec];*)
dots=Max[Plus@@(Select[#[[2]],#>1&]-1)&/@fisGrouped[currSec]];
		rank=Max[-Plus@@Select[#[[2]],#<0&]&/@fisGrouped[currSec]];
Switch[Head@currSecNo,Integer,
If[currSecNo==0,
(*Print["  zero sector: ",currSec];*)AppendTo[symmEqsZ,symmEq=(#->0)&/@fisGrouped[currSec]]
,
currDenSyms=Select[sectorInf,#["No"]==currSecNo&][[1]]["Den.Syms"];
(*Print["  unique sector: ",currSec,"\n  symmetry: ",currDenSyms//MatrixForm];*)
If[OptionValue["TrimUniqueSector"]==True,
(*symmEq=CalcLoop`Family`symFullReduce[currDenSyms,currSec,dots,rank,"SolveQ"->False];*)
symmEq=symFullReduce2[currDenSyms,currSec,dots,rank,"SolveQ"->False,"FindMinTerm"->OptionValue["UniqSecFindMinTerm"]],
symmEq=Join@@Table[CalcLoop`Family`generateSymmetryEqs2[fisGrouped[currSec],currDenSyms[[j]]],{j,Length@currDenSyms}];
symmEq=Map[CalcLoop`Family`FIJoin[Expand[#/.{Hold[a_]:>a},_FI],1,npd]&,symmEq,{2}];
];
(* use generateSymmetryEqs2 to generate eqautions *)
If[OptionValue["AddSubsector"]==True,
fisnew=GroupBy[Cases[symmEq,_FI,Infinity],#[[2]]/.{_?Negative:>0,_?Positive:>1}&];
Do[If[MemberQ[Keys@fisGrouped,key],
fisGrouped[key]=Join[fisGrouped[key],fisnew[key]],
fisGrouped[key]=fisnew[key]],{key,Keys@fisnew}]];
fisGrouped[currSec]//Unset;
AppendTo[symmEqsU,symmEq]]

,Association,
uniqSec = sectorInf[[currSecNo["No"]]]["Sector"];
(*Print["  mapped sector: ",currSec,"\n  mapped to: ",uniqSec,"\n  symmetry: ",currSecNo["Den.Curr2Uniq"]//MatrixForm];*)
symmEq=CalcLoop`Family`generateSymmetryEqs2[fisGrouped[currSec],currSecNo["Den.Curr2Uniq"]];
fisGrouped[currSec]//Unset;
AppendTo[symmEqsM,Map[CalcLoop`Family`FIJoin[Expand[#/.{Hold[a_]:>a},_FI],1,npd]&,symmEq,{2}]]
]
,{i,Length@maxes}]];

{symmEqsZ,symmEqsU,symmEqsM}=Table[
symmEqs=DeleteCases[Flatten@(symmEqs0/.{Rule[a_,b_]:>a-b}),0];(* turn eqs in the form of "Rule[lhs,rhs]" into the form lhs-rhs *)
symmEqs=DeleteCases[cutFI[symmEqs,cut],0];(* perform cut *)
symmEqs/.{FI[1,ind_List]:>BL[famname,ind]},{symmEqs0,{symmEqsZ,symmEqsU,symmEqsM}}];
Print["#Symmetry Relations:  ",Total[Length/@{symmEqsZ,symmEqsU,symmEqsM}]];
Return[{symmEqsZ,symmEqsU,symmEqsM}];
]
(* symmetry mapping only for a single family *)


symFullReduce2//ClearAll;
Options[symFullReduce2]={"SolveQ"->True,"Solver"->SparseRowReduce,"FindMinTerm"->True};
symFullReduce2[symRep0_,sec_,dots_,rank_,OptionsPattern[]]:=Module[
	{symRep,head,hold,famNo,length,pairRep,ints,eqs,fis,map,eqFun,dens,eqsLearn,sol,keys},
	
	symRep=Expand//@symRep0;
	If[Length@symRep===0||symRep==={{}},Return[{},Module]];
	head=FirstCase[symRep[[1]],"Den"[_][_],Missing[],Infinity][[0]];
	famNo=head[[1]];
	length=symRep[[1]]//Length;
	(*** hold terms independent of 'head' ***)
	Attributes[hold]=Listable;
	hold[0]=0;
	hold[a_->b_]:=hold[a]->hold[b];	
	hold[a_head*(b_:1)+c_:0]:=a*b+hold[c];
	
	(*** find pairs of replacements, each one is the inverse of the other one ***)
	If[OptionValue["FindMinTerm"]==False,
	pairRep=CalcLoop`Family`findSymmetryPairs@symRep;
	pairRep=Join[symRep[[#[[1]]]],Complement[Reverse/@symRep[[#[[2]]]],symRep[[#[[1]]]]]]&/@pairRep;
	pairRep=hold[Collect[#,_head,Together]&/@pairRep];(*add Collect[#,_head,Together]&/@ to simplify the expression*)
	
	eqs=Join@@Table[
		ints=CalcLoop`Family`generateSeeds[PadRight[sec,Length@pairRep[[i]]],dots,rank];
		ints=CalcLoop`Family`eqsWithCost[pairRep[[i]],ints]
	,{i,Length@pairRep}];
	eqs=Select[eqs,#[[1,1]]=!=#[[1,2]]&];
	eqs=SortBy[eqs,Last];	
	eqs=eqs[[All,1]]/.Rule[a_,b_]:>a-b;
	,
	ints=CalcLoop`Family`generateSeeds[sec,dots,rank];
	ints=FI[famNo,#]&/@CalcLoop`Family`generateSeeds[sec,dots,rank];
	eqs=Join@@Table[CalcLoop`Family`generateSymmetryEqs2[ints,symRep[[i]]],{i,1,Length@symRep}];
	eqs=Select[eqs,#[[1]]=!=#[[2]]&];
	eqs=eqs/.Rule[a_,b_]:>a-b;
	
	];
	(*** By reducing partial equations and introducing only the most complicated FIs, 
		 reducible FIs can be identified ***)
	fis=CalcLoop`Family`symTopReduce[symRep,sec,dots,rank](*//LCTiming*);
	fis=fis[[All,1]];
	(*** By reducing all equations but introducing only the most complicated FIs, 
		 relevant equations (ordered by complexity) can be identified ***)
	dens=DeleteCases[symRep[[1,All,1]]sec,0];
	map=Association@@Thread[fis->Range[fis//Length]];
	eqFun[i_]:=Module[
		{eqsi},
		eqsi=eqs[[i]]/.Thread[dens->dens^-1]/.Thread[dens^_?Negative->0]/.Thread[dens->dens^-1]/.
				_hold->0//CalcLoop`Family`FIJoin[#,famNo,length]&//Expand;
		eqsi=Association@@Sort[If[Head@#===Plus,List@@#,{#}]&@eqsi/.
				x_FI*y_:1:>If[KeyExistsQ[map,x],(map[x]->y),Nothing]]/.
				Association[0]->Association[]
	];
	eqsLearn=CalcLoop`Private`SparseRowReduce[eqFun,Length@eqs,"MaxNumberOfEquations"->(Length@fis)](*//LCTiming*);
	
	(*** Generate relevant equations with all FIs ***)
	eqs=eqs[[eqsLearn//Keys//Sort]];
	eqs=CalcLoop`Family`FIJoin[eqs,famNo,length]/.hold->Identity/.Hold[a_]:>a(*//LCTiming*);
	If[OptionValue["SolveQ"]===False,Return[eqs,Module]];
	
	(*** put most complicated FIs on the leftmost ***)
	fis=Reverse@SortBy[Union@Cases[eqs,_FI,Infinity],CalcLoop`Family`orderingFI];
	
	(*** Solve the useful equations ***)
	If[OptionValue["Solver"]=!=RowReduce,
		eqs=CalcLoop`Family`linearEqs2Sparse[eqs,fis,FI];
		outt=eqs;
		sol=CalcLoop`Private`SparseRowReduce[eqs,Length@eqs](*//LCTiming*);
		,
		eqs=CalcLoop`Family`linearEqs2Sparse[eqs,fis,FI,"Form"->SparseArray];
		sol=RowReduce[eqs]//SparseArray//ArrayRules(*//LCTiming*);
		sol=Association@@(#[[All,1,1]]->Association@@Thread[#[[All,1,2]]->#[[All,2]]]&/@
				GatherBy[DeleteCases[sol,Rule[{_Blank,_},_]],#[[1,1]]&]);
	];

	map=Association@@Thread[Range[fis//Length]->fis];
	
	Table[keys=Keys@sol[i];
			map@keys[[1]]->map@keys[[1]]-(map/@keys) . Values@sol[i]//Expand,{i,Keys@sol}]
];


symmetryEquation2[famname_,pdlist_,loopmom_,extmom_,spsRep_]:=
Module[{},Return[Function[Join@@symmetryEquation[famname,Slot,pdlist,loopmom,extmom,{},spsRep]]];
]


(* directly use mapping rule to reduce mapped sector integrals *)
sectorMap[intg0_BL]:=Module[{intg=intg0,npd,currSec,currSecNo,denCurr2Uniq},
currSec=intg[[2]]/.{_?Positive:>1,_Negative:>0};npd=Length@currSec;
currSecNo=$FamilyInf[[1]][currSec];
If[Head@currSecNo=!=Integer,
intg=CalcLoop`Family`FISplit[FI[1,intg[[2]]]]/.currSecNo["Den.Curr2Uniq"];
intg=CalcLoop`Family`FIJoin[intg,1,npd]/.{FI[1,a_List]:>BL[intg0[[1]],a]}
,
If[currSecNo==0,intg=0]];
Return[intg];
];
sectorMap[intglist_List]:=sectorMap/@intglist;


(* ::Subsection::Closed:: *)
(*Judge*)


ZeroQ[int_]:=MemberQ[ZeroSectors[int[[1]]],IntSector[int]];
ExtMappedQ[int_]:=MemberQ[ExtMappedSectors[int[[1]]],IntSector[int]];
MappedQ[int_]:=MemberQ[MappedSectors[int[[1]]],IntSector[int]];


EliminateZeroSectors[expr_]:=(#/.Dispatch[(#->0)&/@Select[IntegralsIn[#],ZeroQ[#]&]])&@expr;


(* ::Section:: *)
(*IBPSystem*)


(* ::Subsection::Closed:: *)
(*GenSeeds*)


FrobeniusSolve2[{},0]:={{}};
FrobeniusSolve2[{},b_]:={};
FrobeniusSolve2[a_, b_]:=FrobeniusSolve[a,b];


GenSeeds[sec0_,{mind_,maxd_},minr_,maxr_,filter_:{}]:=Module[{reserve,corner,posi,s1,s2,seeds},
corner=sec0[[2]];
posi=Position[corner,_?Positive]//Flatten;
reserve={};
Do[s1=FrobeniusSolve2[ConstantArray[1,Length[posi]],dot];
s2=-FrobeniusSolve2[ConstantArray[1,Length[corner]-Length[posi]],rank];
seeds=Flatten[Outer[Join,s1,s2,1,1],1];
reserve=Join[reserve,seeds];
,{dot,mind,maxd},{rank,minr,maxr}];
posi=FirstPosition[Join[posi,Complement[Range[Length[corner]],posi]],#]&/@Range[Length[corner]]//Flatten;
reserve = BL[sec0[[1]],#]&/@Map[corner+#&,reserve[[All,posi]]];
If[filter=!={},
reserve=Select[reserve,(And@@(Table[filter[[i]][#],{i,Length[filter]}]))&]];

reserve
];


(* ::Subsection:: *)
(*GenIds*)


(*keep sector for CutIds*)
GenIds["IBP"][sector_,seeds_]:=EliminateZeroSectors[Join@@(IBP[sector[[1]]]@@#[[2]]&/@seeds)];
GenIds["LI"][sector_,seeds_]:=EliminateZeroSectors[Join@@(LI[sector[[1]]]@@#[[2]]&/@seeds)];
(*GenIds["SR"][sector_,seeds_]:=Thread[seeds-SectorRelations[seeds,"NumeratorSymmetry"->True]];
GenIds["Map"][sector_,seeds_]:=Thread[seeds-SectorRelations[seeds,"NumeratorSymmetry"->False]];*)
(*20230524: (1/x + 1)BL[...] or (-2 (79/65-s-t)-2 (-(79/65)+s+t)) BL[...]*)
GenIds["SR"][sector_,seeds_]:=If[closeSymFlag,{},With[{res=EliminateZeroSectors[SymMap[sector[[1]]]@(#[[2]]&/@seeds)]},If[togetherFlagSym, Collect[res,_BL,Together], Collect[res,_BL,Expand]]]];
GenIds["Map"][sector_,seeds_]:=If[closeSymFlag, {}, With[{res=EliminateZeroSectors[SymMap[sector[[1]]]@(#[[2]]&/@seeds)]},If[togetherFlagSym, Collect[res,_BL,Together], Collect[res,_BL,Expand]]]];
GenIds["ExtMap"][sector_,seeds_]:={0};


(*seeds should belong to sector*)
GenCutIds[type_][sector_,seeds_]:=(#/.Dispatch[(#->0)&/@Select[IntegralsIn[#],(IntPropagators[#]<IntPropagators[sector])&]])&@GenIds[type][sector,seeds];


(* ::Subsection::Closed:: *)
(*FastGenIds*)


IdTypes={"IBP","LI","SR","Map","ExtMap"};


SectorString[int_]:=ToString[int[[1]]]<>StringJoin[ToString/@int[[2]]];


PartitionWithLength[list_,length_]:=Module[{mod},
mod = Mod[Length[list],length];
If[mod===0,Partition[list,length],
Join[Partition[list[[;;-mod-1]],length],{list[[-mod;;]]}]]
]


Options[FastGenIds]={"Directory"->Automatic,"LaunchKernels"->0,"GetSectors"->Automatic,"MaxIdsPerFile"->100000,"GenIds"->GenIds,"DeleteTrivalIds"->True};
FastGenIds[fam_,GetSeeds_,OptionsPattern[]]:=Module[
{dir,GetSectors,maxidsperfile, GenIds,deletetrivalids, NIds,IdSeeds},

dir=If[TrueQ[OptionValue["Directory"]===Automatic],Directory[],OptionValue["Directory"]];
If[!DirectoryQ[#],CreateDirectory[#]]&[dir];
GetSectors=OptionValue["GetSectors"];
maxidsperfile = OptionValue["MaxIdsPerFile"];
GenIds=OptionValue["GenIds"];
deletetrivalids = TrueQ[OptionValue["DeleteTrivalIds"]];
If[TrueQ[GetSectors==Automatic],
    Clear[GetSectors];
    GetSectors["IBP"]=UniqueSectors[fam];
    GetSectors["LI"]=UniqueSectors[fam];
    GetSectors["SR"]=UniqueSectors[fam];
    GetSectors["Map"]=MappedSectors[fam];
    GetSectors["ExtMap"]=If[TrueQ[#[[0]]==List],#,{}]&[ExtMappedSectors[fam]];
  ];

(NIds["IBP"][#]=Length[IBP[#[[1]]][[1]]];)&/@GetSectors["IBP"];
(NIds["LI"][#]=Length[LI[#[[1]]][[1]]];)&/@GetSectors["LI"];
NIds["SR"][a_]:=1;
NIds["Map"][a_]:=1;
NIds["ExtMap"][a_]:=1;

If[$KernelCount===0,LaunchKernels[OptionValue["LaunchKernels"]]];

(*seeds_*.mx *)
Do[If[Length[GetSectors[idtype]]>0,
Print["Seeding Id type ->", idtype]; IdSeeds[idtype]=Join@@ParallelTable[
If[TrueQ[NIds[idtype][sect]>0],Module[{seeds,filenames,ii},
SetDirectory[dir];
seeds = {sect,#}&/@PartitionWithLength[GetSeeds[idtype][sect], Max[Quotient[maxidsperfile,NIds[idtype][sect]],1]];
filenames = Table[StringRiffle[{"seeds",ToString[idtype],SectorString[sect],ToString[ii]},"_"]<>".mx",{ii,Length@seeds}];
Do[Export[filenames[[ii]],seeds[[ii]],"MX"],{ii,Length@seeds}];
ResetDirectory[];
filenames]
,{}]
,{sect,GetSectors[idtype]},DistributedContexts->All]
];
,{idtype,IdTypes}];

(*ids_*.mx, ints_*.mx*)
Do[If[TrueQ[IdSeeds[idtype][[0]]===List]&&Length[IdSeeds[idtype]]>0,
Print["====================="];
Print["Id type -> ",idtype, " # chunks -> ",Length@IdSeeds[idtype]];
Print["====================="];
ParallelDo[
SetDirectory[dir];
Print[idtype, " #", ii, " of ", Length@IdSeeds[idtype]];
Print[" -> " , "done in ", AbsoluteTiming[Module[{sect,seeds,ids},
{sect,seeds}=Import[IdSeeds[idtype][[ii]]];
ids = GenIds[idtype][sect,seeds];
If[deletetrivalids,ids = DeleteCases[ids,0,{1}];];
Export["ids_"<>ToString[sect[[1]]]<>"_"<>ToString[idtype]<>ToString[ii]<>".mx",ids,"MX"];
Export["ints_"<>ToString[sect[[1]]]<>"_"<>ToString[idtype]<>ToString[ii]<>".mx",IntegralsIn[ids],"MX"];]]];
ResetDirectory[];

,{ii, Length@IdSeeds[idtype]},DistributedContexts->All];
];
,{idtype,IdTypes}];

]


(* ::Subsection:: *)
(*Interface to FiniteFlow*)


Options[GetAllInts]={"IntegralWeight"->Hold[DefaultWeight]};
GetAllInts[intsfiles_,OptionsPattern[]]:=Module[
{ints,file},
ints = Association[{}];
Do[
(ints[#]=1;)&/@Import[file];
,{file,intsfiles}];
ints = Keys[ints];

SortBy[ints,ReleaseHold@OptionValue["IntegralWeight"]]
];


Options[SerializeFastIds]={"IntegralWeight"->Hold[DefaultWeight],"UserDefinedInts"->{},"ExtraInts"->{},"LaunchKernels"->0};
SerializeFastIds[todofilesin_,rules_, paras_,OptionsPattern[]]:=Module[
{todofiles, IntegralWeight,userdefinedints,extraints,allints,nints,position},

todofiles = todofilesin;
IntegralWeight=ReleaseHold@OptionValue["IntegralWeight"];
userdefinedints = OptionValue["UserDefinedInts"];
extraints = OptionValue["ExtraInts"];

Print["Generating list of integrals ..."];
allints = Join[userdefinedints,GetAllInts[StringReplace[#,{"ids_"->"ints_"}]&/@todofilesin,
                                 "IntegralWeight"->IntegralWeight],extraints];
Print[" -> list of integrals generated"];
nints= Length@allints;
position = Association[{}];
Do[position[allints[[ii]]]=ii-1,{ii,1,nints}];
Clear[allints];

(*posints_*.mx *)
Print["Exporting positions maps ..."];
Do[
  Export[StringReplace[file,{"ids_"->"posints_"}],
         ((#)->position[#])&/@Join[userdefinedints,Import[StringReplace[file,{"ids_"->"ints_"}]],extraints],
         "MX"];
  ,{file,todofiles}];
Print[" -> positions maps exported"];
Clear[position];

If[$KernelCount===0,LaunchKernels[OptionValue["LaunchKernels"]]];
(*sids*.json, nids_*.m *)
Print["Serialize identities ..."];
Print["-> all in :", AbsoluteTiming[ParallelDo[
Print["File: ",file];
Print["File: ",file," done in ",AbsoluteTiming[Module[{ids,integralsin,thisposition},
    thisposition = Association[{}];
    (thisposition[#[[1]]]=#[[2]])&/@Import[StringReplace[file,{"ids_"->"posints_"}]];
    integralsin = Join[userdefinedints,IntegralsIn[#],extraints]&;
    ids=((#/.rules)==0)&/@Import[file];
    
    FiniteFlow`FFSparseEqsToJSON[StringReplace[file,{"ids_"->"sids_",".mx"->".json"}],
                      paras,ids,nints,integralsin,thisposition];
    Export[StringReplace[file,{"ids_"->"nids_",".mx"->".m"}],Length[ids]];
   ]]]  
,{file,todofiles},DistributedContexts->All]]];    
];


Options[WriteSystemJSON]={"FileName"->"system.json"};
WriteSystemJSON[eqsfiles_,allints_,needed_,paras_,OptionsPattern[]]:=Module[
{neqs},
neqs = Total@(Import/@(StringReplace[#,{"sids_"->"nids_",".json"->".m"}]&/@eqsfiles));
Print["neqs = ",neqs];
FiniteFlow`FFSparseSystemToJSON[OptionValue["FileName"],neqs,allints,paras,eqsfiles,"NeededVars"->needed]
];


(* ::Section:: *)
(*MaximalCut*)


(* ::Subsection::Closed:: *)
(*solveSectors*)


(*args: a list of argument*)
(*different length of 'args' correspond to different mode*)
(*{power} | {need,sd,sr} | {td,tr,sd,sr}*)
Options[solveSectors]={"GenCutIds"->GenCutIds,"LinearReduce"->MMALinearSolveLearn,"Numeric"->{},"CutNoDot"->False,"CloseSymmetry"->False,"ExtraInts"->{}};
solveSectors[sector_,args_List,OptionsPattern[]]:=Module[
{GenCutIds, LinearReduce,topo,cutposi,filter,need,tr,td,power,sr,sd,time1,time2,rels,unisector,all,invs, paras, learn,maptrue,symrels},

(*specify identity generating funtion*)
GenCutIds=OptionValue["GenCutIds"];
LinearReduce=OptionValue["LinearReduce"];

(*seeds filter*)
topo=sector[[1]];
cutposi=Position[FamilyInfo[topo]["Cut"],1]//Flatten;
filter:=If[OptionValue["CutNoDot"],{If[(#[[2]][[cutposi]]=!=ConstantArray[1,Length[cutposi]]),False,True]&},{}];

(*************generate target and linear equations**********)
time1=AbsoluteTime[];
Switch[Length[args],
1,
	{power}=args;
	need=Table[GenSeeds[sector,{dd,dd},rr,rr,filter],{dd,0,power},{rr,0,power-dd}]//Flatten;
	rels=Flatten@Table[genIbpLi[sector,dd,rr,filter],{dd,0,power},{rr,0,power-dd}];
	time2=AbsoluteTime[];
	(*Print["power: ",power, ", #eqs ",Length@rels," time: ", time2-time1]*),

3,
	{need,sd,sr}=args;
	tr=Max[IntRank/@need];
	td=Max[IntDots/@need];
	rels=Flatten@Table[genIbpLi[sector,dd,rr,filter],{dd,0,sd},{rr,0,sr}];
	(*rank and dots for symmetry relations is determined by target integrals*)
	(*rank\[Equal]0 may lead to redudant masters, rank=1 is enough?*)
	If[!OptionValue["CloseSymmetry"],
		rels =Join[rels,genSrMap[sector,td,tr,filter]];
	];
	time2=AbsoluteTime[];
	(*Print["rank: ",sr,", dots: ",sd, ", #eqs ",Length@rels," time: ", time2-time1]*),
4,
	{td,tr,sd,sr}=args;
	need=GenSeeds[sector,{0,td},0,tr,filter];
	rels=Flatten@Table[genIbpLi[sector,dd,rr,filter],{dd,0,sd},{rr,0,sr}];
	If[!OptionValue["CloseSymmetry"],
		rels =Join[rels,genSrMap[sector,td,tr,filter]];
	];
	time2=AbsoluteTime[];
	(*Print["rank: ",sr,", dots: ",sd, ", #eqs ",Length@rels," time: ", time2-time1]*),
_,
	Print["error: unknown input of solveSectors ->",args];Abort[]	
];

(*numeric replacement. degeneracy at special phase point*)
If[OptionValue["Numeric"]=!={},rels=rels/.OptionValue["Numeric"]];

all=SortIntegrals[IntegralsIn@rels];

(*fix bug: the input sector has priority to be masters*)
(*Note: keep origin ordering within mappedsectors and uniquesectors*)
all={True,False}/.GroupBy[all,IntSector[#]=!=sector&]//Flatten//DeleteCases[#,True|False]&;

(*fix bug: introduce ExtraInts for function "determineSeeds"*)
If[OptionValue["ExtraInts"]=!={},
all={False,True}/.GroupBy[all,MemberQ[OptionValue["ExtraInts"],#]&]//Flatten//DeleteCases[#,True|False]&;];

(*General enough to be applied to non-standard IBP equations*)
(*invs=Complement[Variables[{FamilyInfo[topo]["Propagators"],Values@FamilyInfo[topo]["Replacements"]}],Join[FamilyInfo[topo]["Loop"],FamilyInfo[topo]["Leg"]]];*)
paras = Complement[rels/.BL[a___]:>"yy"//Variables,{"yy"}];

(*20240205: in cases where the integrand is general*)
need=FtagSimplify[Flatten[need/.BL[a_,b_]:>(BL[a,b,#]&/@Range[Length[$ExtraIntDeriv]])]];

(*linear solve and give independent variables*)
learn = LinearReduce[Thread[rels==0],paras,all,need];

time1=AbsoluteTime[];
(*Print["#masters ",Length@learn," time: ", time1-time2];*)
Return[{sector,learn}]
];


(* ::Subsection:: *)
(*basisTransformation*)


(*'oldmis' is minimal and complete*)
(*return new master integrals which satisfy:
1. reversible with 'oldmis'(Subsets ?);
2. preferred for the IntegralOrdering ( e.g. no dots for IntegralOrdering\[Rule]1*)
Options[basisTransformation]=Join[{"MaxIncrement"->2},Options[solveSectors]];
basisTransformation[sect_,oldmis_,inisd_,inisr_,OptionsPattern[]]:=Module[
{sd,sr,ordering,nmis,maxincre, nincre, tmp,opt,res},
{sd,sr}={inisd,inisr};
ordering=IntegralOrdering;
nmis=Length[oldmis];
maxincre=OptionValue["MaxIncrement"];

opt=(#->OptionValue[#])&/@Keys[Options[solveSectors]];
tmp=solveSectors[sect,{oldmis,sd,sr},Sequence@@opt];

res=Catch@Switch[ordering,
1, If[AllTrue[IntDots/@tmp[[2]],#===0&]&&Length[tmp[[2]]]<=nmis,Throw[tmp],
	nincre=1;
	While[nincre<=maxincre*3,
		{sr,sd}={sr,sd}+Switch[Mod[nincre-1,3],0,{1,0},1,{-1,1},2,{1,0}];
		tmp=solveSectors[sect,{oldmis,sd,sr}, Sequence@@opt];
		If[AllTrue[IntDots/@tmp[[2]],#===0&]&&Length[tmp[[2]]]<=nmis,Throw[tmp]];
		nincre++
	];
	Print["error: basisTransformation for sector: ",sect," failed"];Abort[]],

2, If[AllTrue[IntRank/@tmp[[2]],#===0&]&&Length[tmp[[2]]]<=nmis,Throw[tmp],
	nincre=1;
	While[nincre<=maxincre*3,
		{sr,sd}={sr,sd}+Switch[Mod[nincre-1,3],0,{0,1},1,{1,-1},2,{0,1}];
		tmp=solveSectors[sect,{oldmis,sd,sr}, Sequence@@opt];
		If[AllTrue[IntRank/@tmp[[2]],#===0&]&&Length[tmp[[2]]]<=nmis,Throw[tmp]];
		nincre++
	];
	Print["error: basisTransformation for sector: ",sect," failed"];Abort[]],
_, Print["error: basisTransformation do not support IntegralOrdering: ",ordering];Abort[]
];
res[[2]]
];


(* ::Subsection:: *)
(*determineSeeds*)


(*reduce target integrals(defined by 'td','tr') to 'oldmis', return required seeds {'sd','sr'}*)
(*"ExtraInts" is necessary because IBP system generated in basisTransformation may be not enough to reduce the whole rank-dots integrals.
Then master integrals obtained in step2 may differ from the standarded ordering master integrals*)
(*make sure the integrals appeared in symmetries can be reduced*)
Options[determineSeeds]=Join[{"MaxIncrement"->2},Options[solveSectors]];
determineSeeds[sect_,td_,tr_,oldmis_,OptionsPattern[]]:=Module[
{sd,sr,maxincre,opt,tmp,nincre},

{sd,sr}={td,tr};
maxincre=OptionValue["MaxIncrement"];
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[solveSectors],Except["ExtraInts"]]];

tmp=solveSectors[sect,{td,tr,sd,sr},"ExtraInts"->oldmis,Sequence@@opt];
(*Avoid IntegralOrdering*)
(*TODO: anybug?*)
If[ContainsExactly[tmp[[2]],oldmis],Return[{sd,sr}]];
(*If[Length@tmp[[2]]===Length@oldmis,Return[{sd,sr}]];*)

(*TODO: different strategy for IntegralOrdering*)
nincre=1;
While[nincre<=maxincre*3,
{sr,sd}={sr,sd}+Switch[Mod[nincre-1,3],0,{0,1},1,{1,-1},2,{0,1}];
tmp=solveSectors[sect,{td,tr,sd,sr}, "ExtraInts"->oldmis,Sequence@@opt];
If[ContainsExactly[tmp[[2]],oldmis],Break[]];
(*If[Length@tmp[[2]]===Length@oldmis,Break[]];*)
nincre++];

If[nincre<=maxincre*3,Return[{sd,sr}],Print["error: determineSeeds for: ",{td,tr,oldmis}," failed"];Abort[]];
];


(* ::Subsection:: *)
(*AutoDetermine*)


(*determine maximal-cut master integrals for a sector*)
(*1. close symmetry, set IntegralOrdering to 3 | 4 locally, increase power(rank+dots) of seeds and reduce seeds, untill there is one power fully reduced 
 \[Rule] minimal basis without symmetry obtained, denoted by M1*)
(*2. close symmetry, basisTransformation according to global IntegralOrdering \[Rule]
 minimal basis for IntegralOrdering without symmetry obtained,denoted by M2*)
(*3. close symmetry, reduce target integrals whose rank and dots are less or equal than minimal basis M2, keep the seeds required
\[Rule] all integrals in symmetry relations of M2 can be reduced*)
(*4. include symmetry relations in the linear system obtained at step3, perform the reduction
\[Rule] minimal basis with symmetry obtained*)
(*step1 is the most time-consuming, time for generating symmetries is reduced dramatically*)
Options[AutoDetermine]=Join[Options[solveSectors],{"MaxIncrement"->2, "StartingPower"->2, "HighestPower"->5, "CheckSymmetry"->True, "Nthreads"->Automatic}];
AutoDetermine[sect_BL,OptionsPattern[]]:=Module[
{maxincre, wp,mintargetpower, opt, ordering, res, sr,sd,tmp, td,tr,nincre,newbasis,time1,time2},
time1=AbsoluteTime[];
Clear[ibpliCache];
Clear[symmetryCache];

maxincre=OptionValue["MaxIncrement"];
mintargetpower=OptionValue["StartingPower"];
wp=mintargetpower;
ordering=IntegralOrdering;

(*obtain minimal basis*)
(*close symmetry because it cost a lot of time*)
(*sort integrals according to power(rank+dot)*)
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[AutoDetermine],FilterRules[Options[solveSectors],Except["CloseSymmetry"]]]];
Block[{IntegralOrdering=Switch[ordering,1|3,3,2|4,4,_,Print["error: unknown IntegralOrdering"];Abort[]]},
res=Catch@While[True,
tmp=solveSectors[sect,{wp},"CloseSymmetry"->True,Sequence@@opt];
tmp[[2]]=pickMasters[tmp[[2]],wp,mintargetpower];
If[FreeQ[tmp,$Failed],Throw[tmp]];
If[wp < OptionValue["HighestPower"],wp++,Throw[tmp]]]];

time2=AbsoluteTime[];
(*judge*)
If[!FreeQ[res,$Failed],Print["error: ",sect," not fully reduced"];Abort[]];
If[res[[2]]==={},Print[sect[[2]]," done in: ",time2-time1,"s."];Return[res]];
If[Length[res[[2]]]===1&&IntRank[res[[2,1]]]===0&&IntDots[res[[2,1]]]===0,Print[sect[[2]]," done in: ",time2-time1,"s."];Return[res]];

(*transform master integrals to prefered-basis(stipulated by IntegralOrdering), without symmetry*)
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[AutoDetermine],FilterRules[Options[basisTransformation],Except["CloseSymmetry"]]]];
newbasis=Switch[ordering,
1,
	If[AllTrue[IntDots/@res[[2]],#===0&],
	res[[2]],
	sd=Max[IntDots/@res[[2]]];
	(*dots must be reduced to rank integrals whose power is larger*)
	sr=Max[(IntRank[#]+IntDots[#])&/@Select[res[[2]],IntDots[#]>0&]]+1;
	basisTransformation[sect,res[[2]],sd,sr,"CloseSymmetry"->True,Sequence@@opt]],
2,
	If[AllTrue[IntRank/@res[[2]],#===0&],
	res[[2]],
	sd=Max[(IntRank[#]+IntDots[#])&/@Select[res[[2]],IntRank[#]>0&]]+1;
	sr=Max[IntRank/@res[[2]]];
	basisTransformation[sect,res[[2]],sd,sr,"CloseSymmetry"->True,Sequence@@opt]],
3|4,
	res[[2]]];

time2=AbsoluteTime[];
If[OptionValue["CloseSymmetry"],Print[sect[[2]]," done in: ",time2-time1,"s."];Return[{sect,newbasis}]];

(*improvement: if no SR symmetry*)
If[$SectorInf[[sectorMap[sect][[2]]/.Dispatch[$FamilyInf[[1]]]]]["Den.Syms"]==={},
	Print[sect[[2]]," done in: ",time2-time1,"s."];Return[{sect,newbasis}]];

(*reduce integrals involved in symmetry*)
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[AutoDetermine],FilterRules[Options[determineSeeds],Except["CloseSymmetry"]]]];
td=Max[IntDots/@newbasis];
tr=Max[IntRank/@newbasis];
{sd,sr}=determineSeeds[sect,td,tr,newbasis,"CloseSymmetry"->True,Sequence@@opt];

(*introduce symmetry relations*)
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[AutoDetermine],FilterRules[Options[solveSectors],Except["CloseSymmetry"]]]];
res=solveSectors[sect,{newbasis,sd,sr},"CloseSymmetry"->False, Sequence@@opt];

time2=AbsoluteTime[];
Print[sect[[2]]," done in: ",time2-time1,"s."];
res
];


(*determine maximal-cut master integrals for the family 'topo'*)
AutoDetermine[topo_Symbol,OptionsPattern[]]:=Module[{opt,nthreads,fastmis,unisector,time1,time2},

opt=(#->OptionValue[#])&/@Keys[Options[AutoDetermine]];

ClearAll[FastMIs];
nthreads=OptionValue["Nthreads"];
If[nthreads===Automatic,nthreads=$ProcessorCount];
If[$KernelCount==0,LaunchKernels[nthreads]];

time1=AbsoluteTime[];
fastmis=ParallelTable[AutoDetermine[ss,Sequence@@opt],{ss,NonZeroSectors[topo]},DistributedContexts->All,Method->"FinestGrained"];

(*Check whether fully reduced*)
If[!FreeQ[fastmis,$Aborted|$Failed],Print["error: fast determinemi reduction failed"];Return[$Failed]];

Table[FastMIs[ss[[1]]]=ss[[2]],{ss,fastmis}];
time2=AbsoluteTime[];
Print["fast determineMI done in: ", time2-time1];

(*MappedSectors and UniqueSectors should have the same number of master integrals*)
If[!TrueQ[OptionValue["CloseSymmetry"]]&&OptionValue["CheckSymmetry"],
unisector=sectorMap@MappedSectors[topo];
If[(Length@FastMIs[#]&/@MappedSectors[topo]) =!= (Length@FastMIs[#]&/@unisector),Print["error: fast determinemi check sysmmetry failed"];Return[$Failed]]];
];


(* ::Subsection:: *)
(*genIbpLi+genSrMap*)


(*if one level of integrals are fully reduced, return master integrals, otherwise, return $Failed*)
pickMasters[mis_,power_,mintargetpower_]:=Module[{index},
(*reducible sector*)
If[mis==={},Return[{}]];
(*level\[Rule]index is fully reduced*)
index=Complement[Range[mintargetpower,power],(IntRank[#]+IntDots[#])&/@mis//Union];
If[index==={},Return[$Failed],Return[Select[mis,(IntRank[#]+IntDots[#])<index[[1]]&]]]
];


(*generate ibp&li identities. cache for each rank&dot*)
genIbpLi[sector_,sd_,sr_,filter_:{}]:=If[Head[ibpliCache[sector,sd,sr]]===List,
	ibpliCache[sector,sd,sr],
	ibpliCache[sector,sd,sr]=Join[
		GenCutIds["IBP"][sector,GenSeeds[sector,{sd,sd},sr,sr,filter]],
		GenCutIds["LI"][sector,GenSeeds[sector,{sd,sd},sr,sr,filter]]]
];


(*generate SR identities.  cache for 0-rank&0-dot*)
genSrMap[sector_,td_,tr_,filter_:{}]:=Module[{maptrue,unisector,symrels},
	maptrue=MemberQ[MappedSectors[sector[[1]]],sector];
	unisector=sectorMap[sector];
	Which[
	  Head[symmetryCache[sector,td,tr]]===List&&Length[symmetryCache[sector,td,tr]]>0,
	    symrels=symmetryCache[sector, td, tr],
	  Head[symmetryCache[sector,td,tr-1]]===List&&Length[symmetryCache[sector,td,tr-1]]>0,
	    symrels=Join[symmetryCache[sector,td,tr-1],
	                If[maptrue,
	                  Join[GenCutIds["SR"][unisector,GenSeeds[unisector,{0,td},tr-1,tr,filter]],GenCutIds["Map"][sector,GenSeeds[sector,{0,td},tr-1,tr,filter]]]
	                  ,
	                  GenCutIds["SR"][sector,GenSeeds[sector,{0,td},tr-1,tr,filter]]]],
	  Head[symmetryCache[sector,td-1,tr]]===List&&Length[symmetryCache[sector,td-1,tr]]>0,
	    symrels=Join[symmetryCache[sector,td-1,tr],
	                If[maptrue,
	                  Join[GenCutIds["SR"][unisector,GenSeeds[unisector,{td-1,td},0,tr,filter]],GenCutIds["Map"][sector,GenSeeds[sector,{td-1,td},0,tr,filter]]]
	                  ,
	                  GenCutIds["SR"][sector,GenSeeds[sector,{td-1,td},0,tr,filter]]]],
	  True,	    
		symrels=If[maptrue,
					Join[GenCutIds["SR"][unisector,GenSeeds[unisector,{0,td},0,tr,filter]],GenCutIds["Map"][sector,GenSeeds[sector,{0,td},0,tr,filter]]]
					,
					GenCutIds["SR"][sector,GenSeeds[sector,{0,td},0,tr,filter]]]];
	symmetryCache[sector,td,tr]=symrels;
	Return[symrels]];


(* ::Subsection::Closed:: *)
(*MMALinearSolveLearn*)


(*solve eqs and return master integrals*)
(*TODO: efficiency?*)
MMALinearSolveLearn[eqs_,paras_,allvars_,neededvars_]:=Module[{prime,numeric, mat,posi,sol, asso,needid},

If[Head[eqs]=!=List,Print["error: equations should be a list"];Return[$Failed]];
If[DeleteCases[eqs,0,{1}]==={},Return[neededvars]];

prime=9223372036854766387;
numeric=Thread[paras->RandomInteger[{0,prime},Length[paras]]];
mat=CoefficientArrays[eqs/.numeric,allvars][[2]]//Normal;
mat=Select[RowReduce[mat,Modulus->prime],AnyTrue[#,#=!=0&]&];
sol=Function[{list},posi=Position[list,Except[0],{1},Heads->False]//Flatten;posi[[1]]->posi[[2;;]]]/@mat//Dispatch;

asso=Association[{}];
Do[asso[allvars[[i]]]=i,{i,Length@allvars}];
needid=asso/@neededvars;
allvars[[needid/.sol//Flatten//DeleteDuplicates]]
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];


Print["BladeIBP: a package for generating IBP, LI and Symmetry relations."] 
