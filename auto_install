#! /bin/bash

# installation path of dependencies
prefix="/usr/local"
# source directory of dependencies
srcdir="/usr/local/src"
# installation directory of Blade
bladedir=$PWD
echo -e "\e[32m \n blade will be installed at \e[35m $bladedir/blade/blade-main \e[0m"
  
# check whether blade is installed 
if [ -f $bladedir/blade/blade-main/install_manifest.txt ]; then
  echo -e "\e[32m \n   blade is installed \e[0m"
  exit
else
  :
fi

# check whether $bladedir is writable
mkdir $bladedir/blade_check_user_privilege
if [ -d $bladedir/blade_check_user_privilege ]; then
  rm -rf $bladedir/blade_check_user_privilege
else
  echo -e "\e[31m please put this script under a folder that is writable. \e[0m"
  exit
fi 

# check whether fflow is installed and to reinstall fflow or not
if [ -f $prefix/lib/libfflow.so ] && [ -f $prefix/include/fflow/graph.hh ] ; then
  echo -e "\e[32m \n   FFLOW library detected! \e[0m"
  echo -e "\e[39m \nIf FiniteFlow works well, no other dependencies are required. \nYou can Quit this script and follow instructions in README.md to install Blade\n or Continue this script to install Blade automatically(reinstall FFLOW). \e[0m" 
  count=0;
  while [ $count -le 1 ]
  do 
    read -p 'Continue (Y/n): ' ys
    case "$ys" in 
      "Y")
        break
	;;
      "n")
        exit
        ;;
      *)
        count=$(($count+1))
        ;; 
    esac
  done

  if [ $count -eq 2 ]; then
    exit
  else
    :
  fi
fi

# input password once for all
read -s -p 'Please input password for sudo: ' pswd

echo $pswd | sudo -S mkdir -p /usr/local/src/blade_check_root_password
if [ -d /usr/local/src/blade_check_root_password ]; then
  echo $pswd | sudo -S rm -rf /usr/local/src/blade_check_root_password
else
  echo -e "\e[31m password is wrong! \e[0m"
  exit
fi

echo -e "\e[32m \n Start installation... \e[0m"

# update or install basic packages
echo -e "\e[32m \n   Update repositories... \e[0m"
echo $pswd | sudo -S apt update

echo -e "\e[32m \n   apt install language-pack-en \e[0m"
echo $pswd | sudo -S apt install -y language-pack-en

# function to check whether package is installed (based on apt-cache)
pkg_exists_q() {
  cacheLANG=$LANG
  cacheLANGUAGE=$LANGUAGE
  export LANG=en_US.UTF-8
  export LANGUAGE=en_US
  pkg_exists_q_flag=$(apt-cache policy $1 |grep none)
  if [ "${pkg_exists_q_flag}" ]; then
    pkg_exists_q_flag=0
  else
    pkg_exists_q_flag=1
  fi
  export LANG=$cacheLANG
  export LANGUAGE=$cacheLANGUAGE
  echo $pkg_exists_q_flag
}

# check whether gcc is installed(not necessarily the latest version)
if [ $(pkg_exists_q gcc) -eq 1 ]; then
  echo -e "\e[32m \n   gcc is installed \e[0m"
else
  echo -e "\e[32m \n   apt install gcc \e[0m"
  echo $pswd | sudo -S apt install -y gcc
fi

# check whether g++ is installed(not necessarily the latest version)
if [ $(pkg_exists_q g++) -eq 1 ]; then
  echo -e "\e[32m \n   g++ is installed \e[0m"
else
  echo -e "\e[32m \n   apt install g++ \e[0m"
  echo $pswd | sudo -S apt install -y g++
fi

echo -e "\e[32m \n   apt install make \e[0m"
echo $pswd | sudo -S apt install -y make

echo -e "\e[32m \n   apt install cmake \e[0m"
echo $pswd | sudo -S apt install -y cmake

echo -e "\e[32m \n   apt install m4 \e[0m"
echo $pswd | sudo -S apt install -y m4

echo -e "\e[32m \n   apt install uuid-dev \e[0m"
echo $pswd | sudo -S apt install -y uuid-dev

echo -e "\e[32m \n   apt install git \e[0m"
echo $pswd | sudo -S apt install -y git

echo -e "\e[32m \n   apt install unzip \e[0m"
echo $pswd | sudo -S apt install -y unzip

# GMP
if [ -f $prefix/lib/libgmp.so ] && [ -f $prefix/include/gmp.h ] ; then
  echo -e "\e[32m \n   gmp is installed \e[0m"
else
  if [ -d $srcdir/gmp/gmp-6.2.1 ]; then
    :
  else
    echo -e "\e[32m \n   Download gmp \e[0m"
    echo $pswd | sudo -S mkdir -p $srcdir/gmp
    cd $srcdir/gmp
    echo $pswd | sudo -S wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.bz2
    echo $pswd | sudo -S tar -jxvf gmp-6.2.1.tar.bz2
  fi

  echo -e "\e[32m \n   Install gmp \e[0m"
  cd $srcdir/gmp/gmp-6.2.1
  echo $pswd | sudo -S ./configure
  echo $pswd | sudo -S make
  echo $pswd | sudo -S make check
  echo $pswd | sudo -S make install
fi

# MPFR
if [ -f $prefix/lib/libmpfr.so ] && [ -f $prefix/include/mpfr.h ] ; then
  echo -e "\e[32m \n   mpfr is installed \e[0m"  
else
  if [ -d $srcdir/mpfr/mpfr-4.2.0 ]; then
    :
  else
    echo -e "\e[32m \n   Download mpfr \e[0m"
    echo $pswd | sudo -S mkdir -p $srcdir/mpfr
    cd $srcdir/mpfr
    echo $pswd | sudo -S wget https://www.mpfr.org/mpfr-4.2.0/mpfr-4.2.0.tar.gz
    echo $pswd | sudo -S tar -xvf ./mpfr-4.2.0.tar.gz
  fi

  echo -e "\e[32m \n   Install mpfr \e[0m"
  cd $srcdir/mpfr/mpfr-4.2.0
  echo $pswd | sudo -S ./configure
  echo $pswd | sudo -S make
  echo $pswd | sudo -S make check
  echo $pswd | sudo -S make install
fi

# FLINT
if [ -f $prefix/lib/libflint.so ] && [ -f $prefix/include/flint/flint.h ] ; then
  echo -e "\e[32m \n   flint is installed \e[0m"  
else
  if [ -d $srcdir/flint/flint-2.9.0 ]; then
    :
  else
    echo -e "\e[32m \n   Download flint \e[0m"
    echo $pswd | sudo -S mkdir $srcdir/flint
    cd $srcdir/flint
    echo $pswd | sudo -S wget https://www.flintlib.org/flint-2.9.0.tar.gz
    echo $pswd | sudo -S tar -xvf ./flint-2.9.0.tar.gz
  fi

  echo -e "\e[32m \n   Install flint \e[0m"
  cd $srcdir/flint/flint-2.9.0
  echo $pswd | sudo -S ./configure
  echo $pswd | sudo -S make
  echo $pswd | sudo -S make check
  echo $pswd | sudo -S make install
fi

# FFLOW
if [ -f $prefix/lib/libfflow.so ] && [ -f $prefix/include/fflow/graph.hh ] && [ -f $srcdir/fflow/finiteflow-master/fflowmlink.so ] ; then
  echo -e "\e[32m \n   fflow is installed \e[0m"
else  
  if [ -d $srcdir/fflow/finiteflow-master ]; then
    :
  else
    echo -e "\e[32m \n   Download fflow \e[0m"
    echo $pswd | sudo -S mkdir -p $srcdir/fflow
    cd $srcdir/fflow
    echo $pswd | sudo -S wget -c -O finiteflow-master.zip https://github.com/peraro/finiteflow/archive/refs/heads/master.zip 
    echo $pswd | sudo -S unzip -o ./finiteflow-master.zip
  fi
  
  echo -e "\e[32m \n   Install fflow \e[0m"
  cd $srcdir/fflow/finiteflow-master
  echo $pswd | sudo -S cmake . 
  echo $pswd | sudo -S make install
fi

# Blade
echo -e "\e[32m \n   Download blade \e[0m"
mkdir -p $bladedir/blade/blade-main
cd $bladedir/blade/blade-main
git init -q
str=$(git remote -v |grep origin)
if [ "${str}" ]; then
        :
else
        git remote add -t main origin https://gitlab.com/multiloop-pku/blade.git
fi
git pull

echo -e "\e[32m \n   Install blade \e[0m"
cd $bladedir/blade/blade-main
fflowmlink=$(echo $srcdir/fflow/finiteflow-master |sed 's/\//\\\//g')
sed -i "s/\(DFFLOWMLINK_DIR=\).*/\1\"${fflowmlink}\"/g" install.in.txt
cmake .
make install

if [ -f "install_manifest.txt" ]; then
  echo -e "\e[32m \n Congratulations! Blade is installed successfuly. \n refer to \e[35m $bladedir/blade/blade-main \e[0m"
else
  echo -e "\e[31m \n Error! install failed.\e[0m"
fi
