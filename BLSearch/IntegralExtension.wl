(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLGenerateScheme::usage = "BLGenerateScheme[target_] extend integral set that is suitable for constructing block-triangular system and reduce target. Their are two ways, called 'OperatorExtendG' and 'MinimalSchemRank'. 
'OperatorExtendG': We define \!\(\*FractionBox[\(r\), SubscriptBox[\(p\), \(+d\)]]\)-type Feynman integrals with rank equal to 'r', number of denominators equal to 'p' and dots equal to 'd'. After applying 'OperatorExtendG' at the left hand side, we will get a set consists of integrals no more complicated than the right hand side. \!\(\*FractionBox[\(r\), SubscriptBox[\(p\), \(+d\)]]\)\[Rule]{\!\(\*FractionBox[\(r\), SubscriptBox[\(p\), \(+d\)]]\),\!\(\*FractionBox[\(r\), SubscriptBox[\(p\), \(+\((d - 1)\)\)]]\),...,\!\(\*FractionBox[\(r\), \(p\)]\), \!\(\*FractionBox[\(r - 1\), SubscriptBox[\((p - 1)\), \(+d\)]]\),\!\(\*FractionBox[\(r - 1\), SubscriptBox[\((p - 1)\), \(+\((d - 1)\)\)]]\),...,\!\(\*FractionBox[\(r - 1\), \((p - 1)\)]\),...}. For instance, \!\(\*FractionBox[\(3\), SubscriptBox[\(8\), \(+1\)]]\) -> {\!\(\*FractionBox[\(3\), SubscriptBox[\(8\), \(+1\)]]\),\!\(\*FractionBox[\(2\), SubscriptBox[\(7\), \(+1\)]]\),\!\(\*FractionBox[\(2\), \(7\)]\),\!\(\*FractionBox[\(1\), SubscriptBox[\(6\), \(+1\)]]\),\!\(\*FractionBox[\(1\), \(6\)]\),\!\(\*FractionBox[\(0\), \(5\)]\)}, \!\(\*FractionBox[\(3\), \(8\)]\)\[Rule]{\!\(\*FractionBox[\(3\), \(8\)]\),\!\(\*FractionBox[\(2\), \(7\)]\),\!\(\*FractionBox[\(1\), \(6\)]\),\!\(\*FractionBox[\(0\), \(5\)]\)}. we respect the position of double propagators.
'MinimalSchemeRank': add (sub-)sector integrals whose rank is less than or equal than 'MinimalSchemeRank' and power of denominators are less than or equal than that of input integral. 'MinimalSchemeRank'->-1 means no integral is added.
One could call function BLSetReducerOptions[] to modify options for BLGenerateScheme";


Begin["`Private`"];
sectorDownCollect;
divideSubFamilyBasic;
divideSubFamily;
getTopSector;
End[];


Begin["`IntegralExtension`"];


(* ::Section:: *)
(*IntegralExtension*)


(* ::Subsection::Closed:: *)
(*Basic*)


(*degree n integrals in sect *)
(*sect: no irreducible scalar product, but dot is allowed*)
sectorDown[sect_,n_]:=Module[{name,power,posi,sample,tmp},
name=sect[[1]];
power=sect[[2]];
posi=Position[power,0,Heads->False]//Flatten;
sample=-frobeniusSolve2[ConstantArray[1,Length@posi],n];
Table[tmp=power;tmp[[posi]]=sample[[i]];BL[name,tmp],{i,Length@sample}]];
(*20240205: in cases where the integrand is general*)
sectorDownCollect[sect_,n_]:=With[{ints=Join@@(sectorDown[sect,#]&/@Reverse[Range[0,n]])},
If[Length[BLExtraIntDerivDen]<=1,ints,
Flatten[ints/.BL[a_,b_]:>(BL[a,b,#]&/@Range[Length[BLExtraIntDerivDen]])]]];


(* ::Subsection:: *)
(*globalExtendG*)


globalExtendG[tar_]:=Module[{target,gpsec,getrank,basis,scheme,full},
target=tar;
gpsec=GroupBy[target,BLSector];
getrank[bas_]:=Max[$MinimalSchemeRank,BLIntRank/@If[KeyExistsQ[gpsec,BLSector[bas]], gpsec[BLSector[bas]],{}]];
basis=Join@@(BLLowerDenom/@Union[BLDenominator/@target])//DeleteDuplicates;
scheme=Select[{#,getrank[#]}&/@basis,#[[2]]>=0&]
];


(* ::Subsection:: *)
(*OperatorExtendG*)


OperatorExtendG[target_]:=Module[{gpden, getrank,ncircleddash,unionscheme,basis,scheme},
gpden=GroupBy[target,BLDenominator];
getrank[bas_]:=Max[-1,BLIntRank/@If[KeyExistsQ[gpden,BLDenominator[bas]], gpden[BLDenominator[bas]],{}]];
ncircleddash[sect_,n_]:={#,BLIntPropagators[#]-(BLIntPropagators[sect]-n)}&/@BLLowerDenom[sect];
unionscheme[{},a_]:=a;
unionscheme[s1_,s2_]:=Map[SortBy[#,{-#[[2]]}&]&,GatherBy[Join[s1,s2]//DeleteDuplicates,#[[1]]&]][[All,1]];

basis=Union[BLDenominator/@target];
scheme={};
Do[
scheme=unionscheme[scheme,ncircleddash[basis[[i]],getrank[basis[[i]]]]];
,{i,1,Length@basis}];
scheme=Select[scheme,#[[2]]>=0&]
];


(* ::Subsection:: *)
(*BLGenerateScheme*)


(*combine results from globalExtendG and OperatorExtendG*)
BLGenerateScheme[tar_BL]:=BLGenerateScheme[{tar}];
BLGenerateScheme[tar_List]:=Module[{target,getrank,basis,scheme,full},
scheme=Join[If[$OperatorExtendGQ,OperatorExtendG[tar],{}],globalExtendG[tar]];
full=Join@@(sectorDownCollect@@@scheme)//DeleteDuplicates;
If[!SubsetQ[full,tar],ErrorPrint["error: current scheme is not sufficient to reduce target"];Abort[]];
full
]


(* ::Section:: *)
(*divideSubFamilyBasic*)


getTopSector[ints_]:=If[Max[#]>0, 1, 0]&/@Transpose[ints[[All,2]]];


(*target: no constraints, but 'tarinfo'*)
(*ZeroSectors must exist, target include at least one non-zero element*)
(*Pick the most complex integral to define a family and extend the family integral set by GenerateScheme *)
(*Define sub families recursively until all target is included*)
(*Sub-family information is stored in tarinfo*)
(*Distinguish by BLSector&&BLIntDots. e.g. (1 /Subscript[10, +1]) and 3/10 define different sub-families, 
(1 /Subscript[10, +1]) and (1/Subscript[10,+1']) define the same sub-family *)
(*TODO: divide by BLIntPropagators(Kira)*)
(*\"ReverseTargetQ\" \[Rule] False: target integrals are reduced in the biggest sub-family. natural
\"ReverseTargetQ\" \[Rule] True: target integrals are reduced in the smallest sub-family. may reduce cost of the biggest sub-family*)
Options[divideSubFamilyBasic]={"ReverseTargetQ"->False};
divideSubFamilyBasic[target_,tarinfo_,GenerateScheme_,dividelevel_,OptionsPattern[]]:=Module[
{sysid,ints,toplevel,reduced,tmp,ext},
sysid=1;
ints=BLSortIntegrals[BLIntegralsIn[BLEliminateZeroSectors[target]]];
toplevel = BLIntPropagators[ints[[1]]];
While[Length[ints]>0 && toplevel - BLIntPropagators[ints[[1]]] < dividelevel,
tarinfo[sysid,"top"]=BLSector@ints[[1]];
(*GenerateScheme may distinguish BLDenominator, but IBP system only cares about BLSector&&BLIntRank&&BLIntDots*)
ext=GenerateScheme[Select[ints,BLSector[#]===tarinfo[sysid,"top"]&&BLIntRank[#]<=BLIntRank[ints[[1]]]&&BLIntDots[#]<=BLIntDots[ints[[1]]]&]]//BLEliminateZeroSectors//BLIntegralsIn;
tarinfo[sysid,"target"]=ext;
ints=BLSortIntegrals[Complement[ints,ext]];
sysid++];

If[Length[ints]>0,
	ext = GenerateScheme[ints]//BLEliminateZeroSectors//BLIntegralsIn;
	tarinfo[sysid,"target"]=ext;
	tarinfo[sysid,"top"]=BL[ints[[1,1]],getTopSector[ints]];
	tarinfo["Counter"]=sysid;
	,
	tarinfo["Counter"]=sysid-1;
];

If[OptionValue["ReverseTargetQ"],
sysid=tarinfo["Counter"];
reduced={};
While[sysid>0,
tarinfo[sysid,"target"]=Complement[tarinfo[sysid,"target"],reduced];
reduced=Join[reduced,tarinfo[sysid,"target"]];
sysid--];]
];


(* ::Section:: *)
(*divideSubFamily*)


(*20231221: GenerateScheme["USFilter"] is introduced to utilize LC&LI identities*)
(*Divide sub-families*)
(*target: {us[1],BL{xxx],...}*)
(*If JoinFamilyQ is set to True, subfam[sysid,"set"] is used to generate IBP system of sub-families,
otherwise, subfam[sysid,"target"] and subfam[sysid,"usints"] is called by BLBlackBoxReduce*)
divideSubFamily[target_,preferred_,userdefined_,subfam_,GenerateScheme_,dividelevel_:Infinity]:=Module[
{allints,tarinfo,sysid,usints,id},

allints =  BLIntegralsIn[BLEliminateZeroSectors[{target,preferred,userdefined}]];

(*Decomposition of BLInts *)
divideSubFamilyBasic[allints,tarinfo,GenerateScheme,dividelevel];

(*Distribute 'target' into 'tarinfo'*)
tarinfo[___]:={};

sysid=1;
While[sysid<=tarinfo["Counter"],
	subfam[sysid,"top"]=tarinfo[sysid,"top"];
	(*20230219: pre is necessary, otherwise 'pre' may be droped in trimmed IBP system and leads to another set of masters*)
	subfam[sysid,"prefer"]=Select[preferred, AllTrue[Union[BLSector/@BLIntegralsIn[#]],BLSectorOrSubsectorQ[#,subfam[sysid,"top"]]&]&];
	usints=Select[userdefined,ContainsAll[tarinfo[sysid,"target"],BLIntegralsIn[#[[2]]]]&][[All,1]];
	subfam[sysid,"usints"]=FilterRules[userdefined,usints];
	subfam[sysid,"target"]=Join[usints,Intersection[target,tarinfo[sysid,"target"]]];
	subfam[sysid,"set"]=tarinfo[sysid,"target"];
	sysid++
];

If[Complement[target,Flatten[subfam[#,"target"]&/@Range[1,sysid-1]]]=!={},
	subfam[sysid,"target"]=Complement[target,Flatten[subfam[#,"target"]&/@Range[1,sysid-1]]];
	subfam[sysid,"usints"]=FilterRules[userdefined,subfam[sysid,"target"]];
	subfam[sysid,"top"]= BL[allints[[1,1]],getTopSector[BLIntegralsIn[{subfam[sysid,"target"],subfam[sysid,"usints"]}]]];
	subfam[sysid,"prefer"]=Select[preferred, AllTrue[Union[BLSector/@BLIntegralsIn[#]],BLSectorOrSubsectorQ[#,subfam[sysid,"top"]]&]&];
	subfam[sysid,"set"]=GenerateScheme[BLIntegralsIn[{subfam[sysid,"target"],subfam[sysid,"usints"]}]]//BLEliminateZeroSectors//BLIntegralsIn;
	subfam["Counter"]=sysid;
	,
	subfam["Counter"]=sysid-1
];

(*remove sub-families whose target is empty*)
While[AnyTrue[subfam[#,"target"]&/@Range[subfam["Counter"]],#==={}&],
id=SelectFirst[Range[subfam["Counter"]],subfam[#,"target"]==={}&];
Do[subfam[ii,key]=subfam[ii+1,key],{ii,id,subfam["Counter"]-1},{key,{"top","target","usints","set","prefer"}}];
Do[subfam[subfam["Counter"],key]=.,{key,{"top","target","usints","set","prefer"}}];
subfam["Counter"]=subfam["Counter"]-1;
];

(*Inspired by LC&LI identities, a better integral set for constructing DE is: *)
(*when USInts exist and other BLInts satisfy the filter function: *)
If[ValueQ@GenerateScheme["USFilter"],
	Do[
		If[subfam[ii,"usints"]=!={}&&AllTrue[BLIntegralsIn[{subfam[ii,"target"],subfam[ii,"prefer"]}],And@@Table[GenerateScheme["USFilter"][[i]][#1],{i,Length[GenerateScheme["USFilter"]]}]&],
			subfam[ii,"set"]=Select[subfam[ii,"set"],And@@Table[GenerateScheme["USFilter"][[i]][#1],{i,Length[GenerateScheme["USFilter"]]}]&];
		];
	,{ii,subfam["Counter"]}];
];

];


(* ::Section:: *)
(*End*)


End[];
