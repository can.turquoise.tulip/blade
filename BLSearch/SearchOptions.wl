(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLSetReducerOptions::usage ="BLSetReducerOptions[rules___], set options related to IBP system and reconstruction. Possible rules are: 
\"IBPParameter\": a list of ibp parameters invloved in IBPSystem, like {eps, s, t}.
\"IntegralOrdering\": an integer represents integral ordering. 1: BLIntPropagators>BLIntDots>BLIntRank, 2: BLIntPropagators>BLIntRank>BLIntDots, 3: BLIntPropagators>BLIntRank+BLIntDots>BLIntDots>BLIntRank, 4: BLIntPropagators>BLIntRank+BLIntDots>BLIntRank>BLIntDots\:ff0c where '>' means the left hand side has higher priority than the right hand side.  
\"CheckMastersQ\": whether to check masters with maximal-cut.
\"FilterLevel\": filter seed. see ?BLIBPSystem
\"MaxIncrement\": maximal increment of rank&dots for automatic generating ibp system.
\"CloseSymmetry\": whether to close symmetry  in IBP system.
\"CutNoDot\": whether to close symmetry when generate IBP system.
\"BlackBoxRank\": minimal rank of ibp seeds.
\"BlackBoxDot\": minimal dots of ibp seeds.
\"MaxPrime\": maximal number of primes to perform rational reconstruction.
\"MaxDegree\": maximal degree when reconstruct rational functions.
";
BLSetSchemeOptions::usage = "BLSetSchemeOptions[rules___], set scheme options. Possible rules are:
\"OperatorExtendGQ\": whether to use n circled dash to extend integral set.
\"MinimalSchemeRank\": minimal value of rank of integrals for all sectors.
\"PartitionDot\": whether to partition integrals in a sector into sub-blocks with different dots.
\"PartitionRankThreshold\" when the number of integrals in a block reaches the 'PartitionRankThreshold', the block will be divided into two sub-blocks with the rank equal to s_{max} and less than s_{max} correspondently.
\"UniqueSubsectorQ\": whether to map the simpler subsector integrals to UniqueSectors.
";
BLSetSearchOptions::usage = "BLSetSearchOptions[nana_,rules___], set search options. 'nana' means number of analytical parameters. Possible rules are:
\"SearchVariable\": a list of groups of variables, looks like {{x1,x2},{x3,x4}}. 
\"VariableWeight\": a list that has the same length as \"SearchVariable\", each element could be \"uniform\",\"adaptive\" or an Integer.
	\"uniform\" weights of variables in the same group are 1.
	\"adaptive\" weights of variables in the same groups are determined by their complexity. Complexity of variable is determined by maximal-power of single-variable-analytic block-triangular relations.
	Alternatively, exact number is allowed.
\"IntegralWeight\": a list that has the same length of \"SearchVariable\", each element could be \"uniform\" or \"weight\".
	\"uniform\" weights of integrals are 0.
	\"weight\" weights of integrals equal to their mass dimension minus the minimal mass dimension in the block.
\"CutIncrement\": a list of Integer whose length is less than that of \"SearchVariable\" than 1. An integer 'c' would be treated as {c,c,...}. This list plus maximal-power of single-group-analytic block-triangular relations serves as cut-off for searching along more than one direction. 
\"DatanamePrefix\": name of database.
\"Jobname\": name of job.
";
BLSetDefaultSearchOptions::usage ="BLSetDefaultSearchOptions[], set default search options. Can be modified by function BLSetSearchOptions";


Begin["`Private`"];
$IBPParameter; 
$IntegralOrdering;
$FilterLevel;
$MaxIncrement::usage ="max round in BlackBoxIBP. Both rank and dots increase by 1 is called one round. 1 by default";
$CloseSymmetry;
$CutNoDot;
$BlackBoxRank;
$BlackBoxDot;
$CheckMastersQ;
$MaxPrime;
$MaxDegree; 
$OperatorExtendGQ;
$MinimalSchemeRank; 
$PartitionDot;
$PartitionRankThreshold;
$UniqueSubsectorQ;
SearchOptions;
CheckSearchOptions;
CTX=$Context;
End[];


Begin["`SearchOptions`"];


(* ::Section:: *)
(*BLSetReducerOptions*)


Options[BLSetReducerOptions] = {"IBPParameter"->Automatic, "IntegralOrdering"->1, "CheckMastersQ"->True, "FilterLevel"->3, "MaxIncrement"->2, "CloseSymmetry"->False, "CutNoDot"->False, "BlackBoxRank"->2,"BlackBoxDot"->0,"MaxPrime" -> 200, "MaxDegree" -> 1000};
BLSetReducerOptions[opt___]:=Block[{},
(*ibp parameters invloved in IBPSystem , order is important.*)
(*Make sure BLSearchParameter = BLIBPParameter before any reduction*)
If[MemberQ[Keys[{opt}], "IBPParameter"], $IBPParameter = "IBPParameter"/.{opt};
	If[$IBPParameter=!=Automatic, BLSearchParameter = BLIBPParameter]];
(*integral ordering. refer to Blade`Integrals`DefaultWeight*)
If[MemberQ[Keys[{opt}], "IntegralOrdering"], $IntegralOrdering = "IntegralOrdering"/.{opt}];
(*whether to check masters with maximal-cut*)
If[MemberQ[Keys[{opt}], "CheckMastersQ"], $CheckMastersQ = "CheckMastersQ"/.{opt}];
(*filter seed*)
If[MemberQ[Keys[{opt}], "FilterLevel"], $FilterLevel = "FilterLevel"/.{opt};
	SetOptions[BLIBPSystem, "FilterLevel"->$FilterLevel]];
(*maximal increment of rank&dots for automatic generating ibp system*)
(*BLIBPSystem is controlled by $MaxIncrement directly*)
If[MemberQ[Keys[{opt}], "MaxIncrement"], $MaxIncrement="MaxIncrement"/.{opt};
	SetOptions[BLMaximalCutMasters, "MaxIncrement"->$MaxIncrement]];
(*do not generate symmetry relations in IBP system*)
If[MemberQ[Keys[{opt}],"CloseSymmetry"], $CloseSymmetry = "CloseSymmetry"/.{opt};
	SetOptions[BLIBPSystem, "CloseSymmetry"->$CloseSymmetry];
	SetOptions[BLMaximalCutMasters, "CloseSymmetry"->$CloseSymmetry]];
(*do not generate seed with dots on cut propagators*)
If[MemberQ[Keys[{opt}],"CutNoDot"], $CutNoDot = "CutNoDot"/.{opt};
	SetOptions[BLIBPSystem, "CutNoDot"->$CutNoDot];
	SetOptions[BLMaximalCutMasters, "CutNoDot"->$CutNoDot]];
(*minimal rank of ibp seeds*)
If[MemberQ[Keys[{opt}], "BlackBoxRank"], $BlackBoxRank = "BlackBoxRank"/.{opt}];
(*minimal dots of ibp seeds*)
If[MemberQ[Keys[{opt}], "BlackBoxDot"], $BlackBoxDot = "BlackBoxDot"/.{opt}];
(*maximal number of primes to perform rational reconstruct*)
If[MemberQ[Keys[{opt}], "MaxPrime"], $MaxPrime = "MaxPrime"/.{opt}];
(*maximal degree when reconstruct rational functions*)
If[MemberQ[Keys[{opt}], "MaxDegree"], $MaxDegree = "MaxDegree"/.{opt}];

PrintOptions[BLSetReducerOptions];
];


(* ::Section:: *)
(*BLSetSchemeOptions*)


Options[BLSetSchemeOptions] = {"OperatorExtendGQ"->True,"MinimalSchemeRank" -> 1, "PartitionDot"->False, "PartitionRankThreshold"-> 400, "UniqueSubsectorQ"->True};
BLSetSchemeOptions[opt___]:=Block[{},
(*whether to use n circled dash to extend integral set*)
If[MemberQ[Keys[{opt}], "OperatorExtendGQ"], $OperatorExtendGQ = "OperatorExtendGQ"/.{opt}];
(*minimal value of rank for integrals in all sectors*)
If[MemberQ[Keys[{opt}], "MinimalSchemeRank"], $MinimalSchemeRank = "MinimalSchemeRank"/.{opt}];
(*partition integrals in a sector into sub-blocks with different dots*)
If[MemberQ[Keys[{opt}], "PartitionDot"], $PartitionDot = "PartitionDot"/.{opt}];
(*partition integrals in a sector into sub-blocks with different rank(==s_{max} & <=s_{max})*)
If[MemberQ[Keys[{opt}], "PartitionRankThreshold"], $PartitionRankThreshold = "PartitionRankThreshold"/.{opt}];
(*whether the simpler subsector integrals belongs to UniqueSectors only*)
If[MemberQ[Keys[{opt}],"UniqueSubsectorQ"], $UniqueSubsectorQ = "UniqueSubsectorQ"/.{opt}];

PrintOptions[BLSetSchemeOptions];
];


(* ::Section:: *)
(*BLSetSearchOptions*)


ClearAll[SearchOptions]
SearchOptions=Association[{}];


Attributes[BLSetSearchOptions]={Orderless};
BLSetSearchOptions[]:=SearchOptions;
BLSetSearchOptions[nana_Integer,rules___Rule]:=BLSetSearchOptions["NVariables"->nana,rules];
BLSetSearchOptions["NVariables"->nana_,rules___Rule]:=Module[
	{temp},
	If[!KeyExistsQ[SearchOptions,nana],SearchOptions[nana]=Association[]];
	(*cache*)
	temp=SearchOptions[nana];
	Table[SearchOptions[nana][term[[1]]]=term[[2]],{term,{rules}}];
	
	If[!CheckSearchOptions[nana],WriteMessage["SearchOptions are not modified."];SearchOptions[nana]=temp];
	SearchOptions[nana]
];


CheckSearchOptions[nana_]:=Module[{vars, varweight, intmode,cut,test},
{vars,varweight,intmode,cut}={"SearchVariable","VariableWeight","IntegralWeight","CutIncrement"}/.SearchOptions[nana];
(*vars*)
If[(!ContainsExactly[BLSearchParameter[[-nana;;-1]],Flatten[vars]]),
WriteMessage["Caution: \"SearchVariable\" must contain ",ToString[BLSearchParameter[[-nana;;-1]]]," exactly at level ", nana];Return[False]];
If[Complement[Head/@vars,{List}]=!={},WriteMessage["Caution: wrong format of \"SearchVariable\"."];Return[False]];
(*varweight*)
If[Length[varweight]=!=Length[vars],WriteMessage["Caution: wrong format of \"VariableWeight\"."];Return[False]];
test[exp_]:=If[AllTrue[exp,IntegerQ[#]&&Positive[#] &] || MemberQ[{"uniform","adaptive"},exp],True,False];
If[!AllTrue[varweight,test],WriteMessage["Caution: unknown \"VariableWeight\"."];Return[False]];
(*intmode*)
If[Length[intmode]=!=Length[vars],WriteMessage["Caution: wrong format of \"IntegralWeight\"."];Return[False]];
If[!ContainsAll[{"uniform","weight"},intmode],WriteMessage["Caution: unknown \"IntegralWeight\"."];Return[False]];
(*cut*)
If[IntegerQ[cut]&&NonNegative[cut],Return[True]];
If[Length[cut]===Length[vars]-1 &&AllTrue[cut,IntegerQ[#]&&NonNegative[#]&],Return[True]];
WriteMessage["Caution: \"CutIncrement\" should be an Integer or a List with length equal to ",Length[vars]-1,"at level ",nana];Return[False]];


(* ::Section:: *)
(*BLSetDefaultSearchOptions*)


BLSetDefaultSearchOptions[]:=Module[{datasize,maxpower,mode},
(*0\[Rule] traditional IBP. No block-triangular at all*)
BLSetSearchOptions[
"NVariables"->0,
"SearchVariable"->{},
"VariableWeight"->{},
"IntegralWeight"->{},
"CutIncrement"->2,
"DatanamePrefix"->"0",
"Jobname"->"0"];

Do[
(*respect user-defined search options unless it contradicts with the adaptive search strategy.*)
If[KeyExistsQ[SearchOptions,nana]&&ContainsExactly[Flatten@SearchOptions[nana]["SearchVariable"],BLSearchParameter[[-nana;;]]],
{},
BLSetSearchOptions[
"NVariables"->nana,
"SearchVariable"->{BLSearchParameter[[-nana;;]]},
"VariableWeight"->{"adaptive"},
"IntegralWeight"->{"uniform"},
"CutIncrement"->2,
"DatanamePrefix"->(*StringJoin[ToString/@BLSearchParameter[[-nana;;]]]*)ToString[nana],
"Jobname"->(*StringJoin[ToString/@BLSearchParameter[[-nana;;]]]*)ToString[nana]]
];
,{nana,1,Length[BLSearchParameter]}]
];


(* ::Section:: *)
(*I/O Options*)


PrintOptions[func_]:=WriteMessage[{func,"->", Riffle[Thread[Keys[Options[func]] -> ToExpression[CTX<>"$"<>#&/@Keys[Options[func]]]],", "]}];
SaveOptions[func_,filename_]:=(CreateDir[filename//DirectoryName];Put[Thread[Keys[Options[func]] -> ToExpression[CTX<>"$"<>#&/@Keys[Options[func]]]],filename]);
LoadOptions[func_,filename_]:=func[Sequence@@Get[filename]];
CompareOptions[func_,filename_]:=If[FileExistsQ[filename]&&Get[filename]===(Thread[Keys[ATMOptions[func]] -> ToExpression["$"<>#&/@Keys[ATMOptions[func]]]]),True,False];


(* ::Section:: *)
(*End*)


End[];
