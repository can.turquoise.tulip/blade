(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLDataInfoInit::usage = "BLDataInfoInit[] load informations needed for search";
BLGenerateJob::usage = "BLGenerateJob[job_] Generate job to construct block-triangular form. Each block consists of a set of G1 and G2, where G1 is more complicated than G2 and block-triangular relations are used to express G1 interms of G2.
Block is divided by sector. One can call function BLSetSchemeOptions[] to modify the structure of blocks slightly.
\"PartitionDot\" would divide integrals in a sector with different dots into different blocks while keeping their G2 independence the same.
\"PartitionRankThreshold\" when the number of integrals in a block reaches the 'PartitionRankThreshold', the block will be divided into two sub-blocks with the rank equal to s_{max} and less than s_{max} correspondently. The original G2 remains the same.
\"UniqueSubsectorQ\" if this options is set to be true, integrals in G2 are replaced by unique-sectors. This could improve the efficiency of constructing block-triangular form
";


Begin["`Private`"];
G1G2::usage = "G1G2[G_, mode_] Determine dependent and independent integrals for a given set";
NonMIs::usage = "non-master integrals";
MIs::usage = "master integrals";
USInts::usage = "list of Rules, definition of user-defined integrals";
EXInts::usage = "list of Rules, definition of extra integrals(preferred master integrals)";
AllIntegrals::usage = "Join[NonMIs,MIs]";
IntegralID::usage = "Range from 1 to Length[AllIntegrals]";
End[];


Begin["`Job`"]


(* ::Section:: *)
(*Job*)


(* ::Subsection::Closed:: *)
(*BLDataInfoInit*)


BLDataInfoInit[]:=Module[{mapped, unique, posiindex, posim, posiu,equalsectors, mastersectors, ss},
ClearAll[NonMIs, MIs, AllIntegrals, IntegralID, MappedS, UniqueS, TestPrime, RedTable, SymMap];
(*integrals*)
USInts = GetFile[FileNameJoin[{$ReductionDirectory, "results/usints"}]];
EXInts = GetFile[FileNameJoin[{$ReductionDirectory, "results/exints"}]];
{NonMIs, MIs, MappedS, UniqueS} = GetFile[FileNameJoin[{$ReductionDirectory, "results/datainfo"}]][[1;;4]];
AllIntegrals = Join[NonMIs, MIs];
IntegralID[_]:=0;
Table[IntegralID[AllIntegrals[[i]]] = i, {i, Length[AllIntegrals]}];
{MappedS, UniqueS} = {MappedS, UniqueS};

(*numerical reduction table*)
{TestPrime, RedTable} = GetFile[FileNameJoin[{$ReductionDirectory, "results/data"}]];

(*symmetries. BLDenominator(e.g BL[topo,{1,2,1}]) level*)
mapped = Select[BLIntegralsIn@AllIntegrals, BLIntRank[#]===0 && MemberQ[MappedS, BLSector[#]]&];
unique = Select[BLIntegralsIn@AllIntegrals, BLIntRank[#]===0 && MemberQ[UniqueS, BLSector[#]]&];

posiindex = PositionIndex[AllIntegrals];
posim = Flatten[mapped/.posiindex];
posiu = Flatten[unique/.posiindex];

SymMap = {};
Table[If[RedTable[[posim[[i]]]]===RedTable[[posiu[[j]]]], AppendTo[SymMap, AllIntegrals[[posim[[i]]]] -> AllIntegrals[[posiu[[j]]]]]], {i, Length[posim]}, {j, Length[posiu]}];
If[ContainsAll[Keys[SymMap],mapped], SymFlag = True, SymFlag = False];
];


(* ::Subsection::Closed:: *)
(*G1G2*)


(*G should be sort *)
G1G2[G_,mode_]:=Module[{g,sol,solved,G1,G2},
g=G;
If[g==={}, Return[{{},{}}]];
sol = Select[evaluateSol[IntegralID/@g],AnyTrue[#,#=!=0&]&];
solved = g[[Flatten[FirstPosition[#,1]&/@sol]]];

Which[
mode === "sector",
G1 = Select[solved,BLSector@#===BLSector@g[[1]]&],

mode === "rank",
G1 = Select[solved,BLSector@#===BLSector@g[[1]]&&BLIntRank@#===BLIntRank@g[[1]]&],

mode === "dot",
G1 = Select[solved,BLSector@#===BLSector@g[[1]]&&BLIntDots@#===BLIntDots@g[[1]]&],

mode === "all",
G1 = solved,

True,
ErrorPrint["error: undefined mode for G1G2."];Abort[]
];

G2 = Select[g,!MemberQ[G1,#]&];
{G1,G2}
];


reverseColumn[{}]={};
reverseColumn[matrix_]:=Transpose@Reverse@Transpose@matrix;
evaluateSol[intid_]:=reverseColumn@NullSpace[reverseColumn@Transpose@RedTable[[intid]],Modulus->TestPrime];


(* ::Subsection:: *)
(*newG1G2*)


(*if s1\[Rule]s2\[Rule]s1(form a loop), then s1 and s2 should be reduced at the same block*)
(*ordering is necessary*)
newG1G2[g1g20_,loop_]:=Module[{g1g2=g1g20,ind,ints,g1,part},
Do[
ind=loop[[i]]//Flatten;
ints=g1g2[[ind]]//Flatten//DeleteDuplicates;
(*20230301: BLInternalEX[]*)
ints=Join[BLSortIntegrals@BLIntegralsIn[ints],Select[ints,Head[#]=!=BL&]];
g1=g1g2[[ind,1]]//Flatten//DeleteDuplicates//BLSortIntegrals;
g1g2[[First[ind]]]={g1,Select[ints,!MemberQ[g1,#]&]};
,{i,1,Length@loop}];
(*20220714: fixbug: loop structrue may be complex, e.g. {{{{891},{984}},{995}},{996}}*)
(*part=Complement[Range[Length@g1g2],loop[[All,2;;]]//Flatten]//Sort;*)
part=Complement[Range[Length@g1g2],(Flatten/@loop)[[All,2;;]]//Flatten]//Sort;
g1g2[[part]]
];


(*find all loops in the map*)
(*map0: {1\[Rule]{2,3},2\[Rule]{1,3,4},...} each point points to other points with arrayhead*)
(*O(N^3) scaling behavior*)
(*trick: define a root point that connect to every point in the map0, than call backTrack function to start from root and seach all path*)
collectMap[map0_]:=Block[{map=map0,flag,res},
AppendTo[map,"root"->Keys[map0]];
flag=True;
While[flag,
res=backTrack[map,{"root"},"root"];
If[res=!={},map=pinchMap[map,res],flag=False]];
map
]


(*start from one point, search all path. if one loop is found, stop search and return loop, if no loop is found, return {}*)
(*{12} denote a point, {12,19} denote a combined point,where {12} and {19} form a loop*)
backTrack[map_,path_,point_]:=Module[{values,res},
res={};
Do[
values=ii/.map;
If[values=!={},
	If[IntersectingQ[path,values],res=Join[path,{ii},Intersection[path,values][[1;;1]]];Break[],
		res=backTrack[map,Join[path,{ii}],ii];If[res=!={},Break[]]]]
,{ii, (point/.map)}];
Return[res];
]


(*combine a list of point that form a loop(res) as one point*)
pinchMap[map_,res_]:=Module[{com,rule,in,out},
com=res[[FirstPosition[res,res[[-1]]][[1]];;]]//DeleteDuplicates;
rule=Table[i->com,{i,com}];
in=Map[DeleteDuplicates,(Select[map,!MemberQ[com,#[[1]]]&]//Association)/.rule];
out=Join@@(Values/@Select[map,MemberQ[com,#[[1]]]&]);
Join[in,<|com->Complement[out,com]|>]//Normal
]


(*delete point from map*)
dropMap[map_,point_]:=Module[{newmap},
newmap=DeleteCases[map,point->a__];
newmap=DeleteCases[newmap,point,{3}];
newmap
]


(* ::Subsection:: *)
(*userDefinedG1G2*)


(*1. Userdefined integrals(usints) are collected in one block if they have the same top-sectors.*)
(*2. usints are always set to be G1. They can always be reduced by definition*)
(*3. G2 is consisted of Sectors that appears in the definition of usints and all their sub-sectors*)
(*userdefined: definition of userdefined integrals*)
(*scheme: all integrals in current IBP system. refer to Blade`Job`intsToScheme*)
userDefinedG1G2[userdefined_, scheme_]:=Module[{us,usgb,g1g2,projsec, sch,G2},
us=Rule[#[[1]],BLSector/@BLIntegralsIn[#[[2]]]//DeleteDuplicates]&/@userdefined;
usgb = GatherBy[us,BLIntegralsIn[MaximalBy[#[[2]],BLIntPropagators]]&];

If[$KernelCount==0,LaunchKernels[BLNthreads]];
g1g2 = ParallelTable[

projsec=DeleteDuplicates@Flatten@uss[[All,2]];

sch=Join@@(selectSubScheme[scheme,#]&/@projsec)//DeleteDuplicates;

G2 = (sectorDownCollect@@@sch)//Flatten//FamilyIntegrals;
G2 = Join[Select[G2, !MemberQ[MIs, #]&], MIs];
{uss[[All,1]],G2}
,{uss,usgb},DistributedContexts->All]
];


(* ::Subsection:: *)
(*selectSubScheme*)


(*2022.0620: use unique subsectors as G2 to avoid finding too many irrelavant relations during search. e.g mapped relations.*)
(*2022.1203: lower sectors are replaced while top sectors are invariant*)
(*top level sectors are not restricted*)
(*scheme & sec : BL[nm, {1,2,0}] is allowed. SymMap includes BL[nm, {1,2,0}] as well*)
selectSubScheme[scheme_,sec_]:=Module[{sub,sub2},
sub=BLLowerDenom[sec];
sub2=Select[sub,BLIntPropagators[#]<BLIntPropagators[sec]&];
If[$UniqueSubsectorQ&&SymFlag,sub2=sub2/.SymMap//DeleteDuplicates];
Select[scheme,MemberQ[Join[Complement[sub,sub2],sub2],BLSector[#[[1]]]]&]
]


intsToScheme[ints_]:={BLDenominator[#],BLIntRank[#]}&/@((GatherBy[ints//BLIntegralsIn//FamilyIntegrals,BLDenominator])[[All,1]]);


(* ::Subsection::Closed:: *)
(*writeScheme*)


(*G1 and G2: sets of integrals*)
writeScheme[{G1_,G2_}, job_, workid_]:=Module[{dir, fp},
dir = FileNameJoin[{$ReductionDirectory, "search", job, toStringInput[workid]}];
CreateDir[dir];
Put[Length[G1], FileNameJoin[{dir, "sch_g1"}]];
Put[Length[G1]+Length[G2], FileNameJoin[{dir, "sch_nint"}]];
writeObject[FileNameJoin@{dir, "sch_intid"}, IntegralID/@Join[G1, G2]-1,OpenWrite];
];


(* ::Subsection:: *)
(*BLGenerateJob*)


(*For a given sector S, its block is constructed through following steps:
1. find related sectors E that have the same number of Propagators as S, by looking up target sector integrals' projection to master integrals(consider EXInts),
2. G21 = E , G22 = subsectors of S&E,
3. If symmetry is open, G22 = remove mapped sectors from G22,
4. G2 = Join[G21, G22, master integrals] (If G21 and G22 involve masters ,drop them)*)

(*We should deal with S\[Rule]E, E\[Rule]S carefully, otherwise we can not search enough relations to reduce both of them. It may arise from:
1. more than one magic relations, inevitable, one must combine S&E as one block, see newG1G2,
2. a bad choice of exints(preferred masters). One choose a mapped integral as exint, while other masters in this sector are mapped to unique sector. Or one choose a combination of different sectors.
This will not happen if one consider exints sectorwise. For instance, UT basis, D-factorised basis and maximal-cut basis.
Anyway, newG1G2 could address this problem at the price of increased block-size and decreased numerical efficiency*)
Options[BLGenerateJob]={};
BLGenerateJob[job_,OptionsPattern[]]:=Module[
{scheme,g1g2,allsec,ints,sec,proj,projsec,sch,G,all,reduced,map,pindex,loop},

BLDataInfoInit[];
scheme=intsToScheme[Join[NonMIs,MIs]];

(*scheme*)
(*parallel among sector instead of denom*)
allsec=BLSector/@scheme[[All,1]]//DeleteDuplicates;
If[$KernelCount==0,LaunchKernels[BLNthreads]];
{g1g2, map}= Transpose@ParallelTable[

(*in case magic relations or symmetry relations*)
ints=Join@@sectorDownCollect@@@Select[scheme,BLSector[#[[1]]]===sec&];
proj=Position[RedTable[[IntegralID[#]]],Except[0],1,Heads->False]&/@ints//Flatten//Union;
projsec=Select[BLSector/@BLIntegralsIn[MIs[[proj]]/.EXInts]//DeleteDuplicates,BLIntPropagators[#]===BLIntPropagators[sec]&];
sch=Join@@(selectSubScheme[scheme,#]&/@Union[{sec},projsec])//DeleteDuplicates;

G = (sectorDownCollect@@@sch)//Flatten//FamilyIntegrals;
G = Join[Select[G, sec===BLSector[#]&],Select[G, sec=!=BLSector[#]&]];
G = Join[Select[G, !MemberQ[MIs, #]&], MIs];
{If[MemberQ[MIs,G[[1]]] || BLSector[G[[1]]]=!=sec,{},G1G2[G,"sector"]],Complement[projsec,{sec}]}
,{sec,allsec},DistributedContexts->All];

(*2022.0224: if there exists more than two magic relations and some sectors mix like s1\[Rule]s2\[Rule]s1(form a loop), 
then s1 and s2 should be reduced at the same block*)
pindex=PositionIndex[allsec];
map=Thread[allsec->map]/.pindex;
map=collectMap[map];
loop=Select[Keys[map],Length[#]>1&];
g1g2=newG1G2[g1g2,loop]/.{}->Nothing;

(*2022.0228: partitions integrals in a sector into subblocks of different dots*)
If[$PartitionDot,
g1g2=Join@@Table[
If[Select[g1g2[[ii,1]],BLIntDots[#]==jj&]==={},Nothing,
ints=Select[g1g2[[ii,1]],BLIntDots[#]>=jj&];
{Select[g1g2[[ii,1]],BLIntDots[#]==jj&],Select[Flatten[g1g2[[ii]]],!MemberQ[ints,#]&]}]
,{ii,1,Length@g1g2},{jj,0,Max[BLIntDots/@g1g2[[ii,1]]]}];];

(*2024.0212: partition integrals in a sector into two subblocks, with the rank equal to s_{max} and less than s_{max} correspondently.*)
g1g2=Join@@Table[
If[Length[g1g2[[ii,1]]]<=$PartitionRankThreshold,{g1g2[[ii]]},
ints=GroupBy[g1g2[[ii,1]],BLIntRank]//ReverseSort;
If[Length[ints]===1,{g1g2[[ii]]},
{{First[ints],Join[Flatten[Values[Rest[ints]]],g1g2[[ii,2]]]},
{Flatten[Values[Rest[ints]]],g1g2[[ii,2]]}}]]
,{ii,1,Length@g1g2}];

(*add user-defined integral block*)
g1g2=Join[g1g2,userDefinedG1G2[USInts, scheme]];

all = Length[DeleteDuplicates[Flatten[g1g2]]];
reduced = Length[Join@@g1g2[[All,1]]];

WriteMessage@StringTemplate["block number: `1`, all integrals: `2`, reduced integrals: `3`, masters: `4`"][
toStringInput[Length[g1g2]], toStringInput[all], toStringInput[reduced], toStringInput[all-reduced]];
WriteMessage[{"size of blocks ->", Riffle[Length/@g1g2[[All,1]]," "]}];
(*fix bug: all==={}, means target is a subset of (rawmis)MIs*)
Which[
all==0,
	Nothing,
True,
	If[all-reduced=!=Length[MIs], ErrorPrint["error: block-triangular masters are not the same as database masters."]; Abort[]]
];

ParallelTable[writeScheme[g1g2[[i]], job, i-1], {i,Length[g1g2]}, DistributedContexts -> All];
]


FamilyIntegrals[ints_]:=BLSortIntegrals[Select[ints,IntegralID@#=!=0&]];


(* ::Section:: *)
(*End*)


End[];
