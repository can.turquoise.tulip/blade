(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLGenerateAnsatzForJob::usage = "BLGenerateAnsatzForJob[job_,n_,vars_,varweight_,intmode_,cut_,OptionsPattern[]] generate polynomial ansatz of block-triangular relations.
'job': jobname.
'n': maximal power of the block-triangular relation. Power of a relation is computed by adding normalized weight of Feynman integral and weight of its coefficient. 
'vars': a list of groups of variables, looks like {{x1,x2},{x3,x4}}.
'varweight': has the same structure as 'vars', like {{1,2},{1,1}} Each term is a Integer specify the weight of the corresponding variable.
'intmode': a list that has the same length of 'vars', like {\"uniform\",\"weight\"}.
'cut': a list of integer whose length is less than that of 'vars' than 1. This serves as cut-off for searching along more than one direction.
";


Begin["`Private`"];
fromDigits;
workNumberGeneral;
workNumber;
templateNumber;
End[];


Begin["`Polynomial`"]


(* ::Section:: *)
(*PolynomialAnsatz*)


(* ::Subsection:: *)
(*Basic*)


(* ::Subsubsection:: *)
(*polymomialAnsatz*)


(*return part number of ansatz from explicit powers*)
(*if variables are divided into more than one group, all but the last group are truncated by 'cut'*)
(*negative power is mapped to -1*)
ClearAll[fromDigits];
fromDigits[powers_,cut_]/;AllTrue[powers,NonNegative]:=FromDigits[Reverse[powers],MixedRadix[Reverse[cut]+1]]
fromDigits[powers_,cut_]/;AnyTrue[powers,Negative]:=-1;


integerDigits[n_,cut_]:=Reverse[IntegerDigits[n,MixedRadix[Reverse[cut]+1]]]//PadRight[#,Length[cut]+1]&


(*example: polynomialAnsatz[{{x1,x2},{x3,x4}},{{1,1},{1,2}},{3},100]*)
polynomialAnsatz[vars_,weights_,cut_,n_]:=Module[{powers},
powers=integerDigits[n,cut];
Flatten[Outer[Join,Sequence@@Table[FrobeniusSolve[weights[[i]],powers[[i]]],{i,Length@vars}],1],Length@vars-1]
];


(* ::Subsubsection:: *)
(*toInternalAnsatz*)


(*BLSearchParameter that does not appear in vars is assumed to be conservative*)
(*sort by BLSearchParameter*)
toInternalAnsatz[vars_,weights_,cut_,n_]:=Module[{posi,monos,ans,tmp},
monos=polynomialAnsatz[vars,weights,cut,#]&/@Range[0,n];

posi=FirstPosition[BLSearchParameter,#]&/@Flatten[vars]//Flatten;
If[!FreeQ[posi,Missing],ErrorPrint["error: toInternalAnsatz unknown pattern"];Abort[]];
monos=Table[
ans=polynomialAnsatz[vars,weights,cut,ii];
tmp=ConstantArray[0,{Length[ans],Length[BLSearchParameter]}];
tmp[[All,posi]]=ans;
tmp
,{ii,0,n}];

ClearAll[leadingTerm];
Do[leadingTerm[i]=monos[[i+1]],{i,0,n}];

ClearAll[KinTable,KinTableID];
KinTable = Join@@monos;
Table[KinTableID[KinTable[[i]]]=i,{i,Length@KinTable}];

Do[leadingTerm[i]=KinTableID/@leadingTerm[i],{i,0,n}];
];


(* ::Subsubsection:: *)
(*integralDimension*)


integralDimension[expr_,vars_,varweight_]:=Module[{countdim,int,coe,rep,z},
If[Head[expr]===BL,Return[-(Plus@@expr[[2]])]];
int=FirstCase[expr,_BL,{},Infinity];
(*zero integral*)
If[int==={},Return[0]];
coe=Coefficient[expr,int]//Together;
rep=Thread[Flatten[vars]->(Flatten[vars]*Power[z,Flatten[varweight]])];
Exponent[coe/.rep,z]-(Plus@@int[[2]])];


(* ::Subsubsection:: *)
(*distributeAnsatz*)


(*n is the maximal power of the expression*)
(*intsweight is weight (mass dimension) of integrals*)
(*both polynomial ansatz and integrals would contribute to power.*)
(*return configs from 0 to n*)
(*each config looks like: {localIntID,KinTableID}*)
ClearAll[distributeAnsatz];
distributeAnsatz[ints_,intsweight_,intmode_,cut_,n_]:=Module[{configs,ansatz,weight,min,pos,power,index,lt,sub},

If[AllTrue[intmode,#==="uniform"&],
configs[-1]={};
Do[
configs[i]=Join[Join@@Table[Thread[List[kk,leadingTerm[i]]],{kk,0,Length[ints]-1}], configs[i-1]]
,{i,0,n}];
Return[configs/@Range[0,n]]];

(*normalized mass dimension of integrals*)
weight=intsweight;
min=Min[weight];
weight=weight-min;

pos=Position[intmode,"weight"]//Flatten;
power=ConstantArray[0,{Length@ints,Length[cut]+1}];
power[[All,pos]]=weight;

configs[-1]={};
Do[

(*explicit power of each group of variables*)
ansatz=integerDigits[i,cut];
index=Map[(ansatz-#)&,power,{1}];

(*ordering*)
index=Map[fromDigits[#,cut]&,index,{1}];

(*include local integralID*)
index=Transpose[{Range[0,Length[ints]-1],index}];

(*remove unneeded ansatz*)
index=Select[index,#[[2]]>=0&];

(*transform ordering to KinTableID*)
index=Join@@Map[Thread[List[#[[1]],leadingTerm[#[[2]]]]]&,index,{1}];

(*cache*)
lt[i]=index;

(*include sub-leading-term, whose power is lessequal ansatz*)
sub=Flatten[Array[List,ansatz+1,ConstantArray[0,Length@ansatz]],Length[cut]];
sub=Map[fromDigits[#,cut]&,sub,{1}]//Sort[#,Greater]&;
configs[i]=Join@@(lt/@sub);

,{i,0,n}];

configs/@Range[0,n]]


(* ::Subsubsection:: *)
(*checkAnsatzForJob*)


workNumberGeneral[dir_, testQ_]:=Block[{i = 0}, While[testQ[FileNameJoin[{dir, ToString[i]}]], i++]; i];
workNumber[job_]:=workNumberGeneral[FileNameJoin[{$SearchDirectory, job}], DirectoryQ];
templateNumber[job_, workid_, dataname_]:=workNumberGeneral[FileNameJoin[{$SearchDirectory, job, ToString[workid], "fit", dataname}], FileExistsQ];


(*after toInternalAnsatz*)
checkAnsatzForJob[job_,part_,n_,vars_,varweight_,intmode_,cut_]:=Module[{dir,k,intid,ints,intsweight,ansatzs,cache},
dir=FileNameJoin[{$ReductionDirectory,"search",ToString[job],ToString[part]}];
k=workNumberGeneral[FileNameJoin[{dir,"config"}], DirectoryQ];
If[k===0,Return[True]];

intid=Import[FileNameJoin[{dir,"sch_intid"}],"List"]+1;
ints=AllIntegrals[[intid]];
intsweight = integralDimension[#,vars,varweight]&/@(ints/.USInts/.EXInts);
ansatzs=distributeAnsatz[ints,intsweight,intmode,cut,n];
k=Min[k,n+1];
cache=Import[FileNameJoin[{dir,"config",ToString[k-1],"in_var"}],"Table"];
If[validateAnsatz[ansatzs[[k]]]===cache,Return[True],Return[False]]
];


(* ::Subsubsection:: *)
(*writeAnsatzForJob*)


writeAnsatzForJob[job_,part_,n_,vars_,varweight_,intmode_,cut_]:=Module[{dir,file,intid,ints,intsweight,k,ansatzs},
(*break point*)
dir=FileNameJoin[{$ReductionDirectory,"search",ToString[job],ToString[part]}];
file=FileNameJoin[{dir,"flag"}];
If[FileExistsQ[file]&&Get[file]===1,Return[{}]];

(*prepare ansatz*)
intid=Import[FileNameJoin[{dir,"sch_intid"}],"List"]+1;
ints=AllIntegrals[[intid]];
intsweight = integralDimension[#,vars,varweight]&/@(ints/.USInts/.EXInts);
toInternalAnsatz[vars,varweight,cut,n];
ansatzs=distributeAnsatz[ints,intsweight,intmode,cut,n];
k=workNumberGeneral[FileNameJoin[{dir,"config"}], DirectoryQ];

(*write*)
Do[
writeAnsatz[validateAnsatz[ansatzs[[ii+1]]],FileNameJoin[{dir,"config",ToString[ii]}]]
,{ii,k,n}]
];


(*dir: search/jobname/part/*)
(*ansatz: a list of sorted kinematics in correspondence with localid*)
validateAnsatz[ansatz_]:=Map[#-{0,1}&,ansatz,{1}];
writeAnsatz[ansatz_,dir_]:=Module[{},
CreateDir[dir];
Put[Length[ansatz], FileNameJoin[{dir, "in_nvar"}]];
fastwriteObject[FileNameJoin[{dir, "in_var"}], ansatz,OpenWrite];
];


(* ::Subsubsection::Closed:: *)
(*writeKinematics*)


writeKinematics[job_]:=Module[{},
CreateDir[FileNameJoin[{$ReductionDirectory, "search", "kinematics",ToString[job]}]];
writeObject[FileNameJoin[{$ReductionDirectory, "search","kinematics",ToString[job], "kin_common"}],{Dimensions[KinTable]},OpenWrite];
writeObject[FileNameJoin[{$ReductionDirectory, "search", "kinematics",ToString[job], "kin_table"}],KinTable,OpenWrite];
];


(* ::Subsection:: *)
(*BLGenerateAnsatzForJob*)


Options[BLGenerateAnsatzForJob]={"OverWrite"->True};
BLGenerateAnsatzForJob[job_,n_,vars_,varweight_,intmode_,cut_,OptionsPattern[]]:=Module[{dir},
dir=FileNameJoin[{$ReductionDirectory,"search",ToString[job]}];
If[OptionValue["OverWrite"],DeleteDir/@FileNames[FileNameJoin[{dir,"*/config"}]]];

toInternalAnsatz[vars,varweight,cut,n];
If[!checkAnsatzForJob[job,0,n,vars,varweight,intmode,cut],ErrorPrint["error: unmatched ansatz"];Abort[]];
writeKinematics[job];
If[$KernelCount==0,LaunchKernels[BLNthreads]];
ParallelTable[
writeAnsatzForJob[job,part,n,vars,varweight,intmode,cut],
{part,0,workNumber[job]-1},DistributedContexts->All,Method->"FinestGrained"];
];


(* ::Section:: *)
(*End*)


End[];
