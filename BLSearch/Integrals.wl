(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLIntRank::usage ="BLIntRank[int] return rank of int. the negative sum of negative powers of propagators, e.g. BLIntRank[BL[topo,{1,1,2,-1,-1}] = 2";
BLIntDots::usage ="BLIntDots[int] return dots of int. the sum of positive powers of propagators minus the number of propagators with positive power,e.g. BLIntDots[BL[topo,{1,1,2,-1,-1}] = 1";
BLIntPropagators::usage ="BLIntPropagators[int] return the number of propagators with positive power, e.g. BLIntPropagators[BL[topo,{1,1,2,-1,-1}] = 3.";
BLSector::usage = "BLSector[int] return sector, e.g. BLSector[BL[topo,{1,1,2,-1,-1}]] = BL[topo,{1,1,1,0,0}].";
BLDenominator::usage = "BLDenominator[int] return generalized sector, e.g. BLDenominator[BL[topo,{1,1,2,-1,-1}]] = BL[topo,{1,1,2,0,0}].";
BLSectorOrSubsectorQ::usage = "BLSectorOrSubsectorQ[sect1, sect2] return True if sect1 is same to sect2 or sect1 is a sub-sector of sect2, otherwise, return False";
BLSortIntegrals::usage ="BLSortIntegrals[ints] sort ints according to IntegralOrdering defined by BLSetReducerOptions[]";
BLIntegralsIn::usage = "BLIntegralsIn[expr] collect BL integrals in 'expr' without sorting";
BLEliminateZeroSectors::usage = "BLEliminateZeroSectors[expr] replace zero BL integrals in 'expr' by 0";


Begin["`Private`"];
BLLowerDenom::usage = "BLLowerDenom[sect] return generalized sector and generalized sub sectors of 'sect'.";
BLLowerSector::usage = "BLLowerSector[sect] return sector and sub sectors of 'sect'.";
End[];


Begin["`Integrals`"];


(* ::Section:: *)
(*Integrals*)


BLIntPropagators[int_]:=Length@Select[int[[2]],#>0&];
BLIntDots[int_]:=Total@Select[int[[2]]-1,#>0&];
BLIntRank[int_]:=-Total@Select[int[[2]],#<0&];


BLSectorOrSubsectorQ[sect1_,sect2_]:=If[AnyTrue[sect1[[2]]-sect2[[2]],Positive],False,True];
BLSubSectorQ[sect1_,sect2_]:=BLSectorOrSubsectorQ[sect1,sect2]&&(!SameQ[sect1,sect2]);


BLSector[int_]:=BL[int[[1]],If[#<=0,0,1]&/@int[[2]]];
BLDenominator[int_]:=BL[int[[1]],If[#<=0,0,#]&/@int[[2]]];


BLLowerDenom[int_]:=BL[int[[1]],#]&/@(Tuples[Range[0,Max[0,#]]&/@int[[2]]]);
BLLowerSector[int_]:=BL[int[[1]],#]&/@(Tuples[If[#<=0,{0},{0,1}]&/@int[[2]]]);


(*$IntegralOrdering = 1 by default*)
DefaultWeight:=IntegralWeight[$IntegralOrdering];


(*sectors are ordered by the number of lines*)
(*1: ISP are as simpler as dots *)
(*2: dots are as simpler as ISP*)
(*3: The first complexity is the sum of dots and ISP. If the sum is equal, ISP are regarded as simpler *)
(*4: Like 3, but if the sum is equal, dots are regarded as simpler*)
IntegralWeight[order_]:=Switch[order,
1, Function[{int},{-BLIntPropagators[int], -BLIntDots[int], -BLIntRank[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
2, Function[{int},{-BLIntPropagators[int], -BLIntRank[int], -BLIntDots[int], !ExtMappedQ[int],!MappedQ[int], Min[int[[2]]], int}],
3, Function[{int},{-BLIntPropagators[int], -BLIntDots[int] -BLIntRank[int], -BLIntDots[int], -BLIntRank[int], !ExtMappedQ[int], !MappedQ[int], Min[int[[2]]], int}],
4, Function[{int},{-BLIntPropagators[int], -BLIntDots[int] -BLIntRank[int], -BLIntRank[int], -BLIntDots[int], !ExtMappedQ[int], !MappedQ[int], Min[int[[2]]], int}],
_, Print["error: unknown integral ordering", Abort[]]]


(*This weight definition is the same as BladeIBP except that ExtMappedQ and MappedQ is useless*)
BLSortIntegrals[ints_]:=SortBy[ints, DefaultWeight];
BLIntegralsIn[expr_]:=Cases[{expr},_BL,Infinity]//DeleteDuplicates;


BLEliminateZeroSectors[expr_]:=Module[{zeroq},
AllZeroSectors=GetFile@FileNameJoin[{$MaximalCutDirectory,"results/zerosectors"}];
zeroq[int_]:=MemberQ[AllZeroSectors,BLSector@int];
expr/.Dispatch[(#->0)&/@Select[BLIntegralsIn[expr],zeroq[#]&]]];


(* ::Section:: *)
(*End*)


End[];
