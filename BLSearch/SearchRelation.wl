(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLSearchRelationsBasic::usage="BLSearchRelationsBasic[job_,dataname_,vars_,varweight_,intmode_,cut_,timelimit_] generate polynomial ansatz according arguments and search block-triangular relations automatically. 
There is no constraint on search modes.
'varweight': integer specify power of 'vars'.
'cut': a list of integer specify degree bound for the elements in 'vars' except the last one.
\"OverWrite\": whether to delete all polynomial ansatz and existing block-triangular relations.
\"SearchOnly\": whether to fit template and find minimal # database.
\"DataSize\": starting # database, varies according to # 'vars'.
\"AnsatzSize\": starting # power, varies according to 'cut'. By default, the last direction starts from 7.  
\"FitDataPercent\": It use bisection strategy to determine minimal # database. If the difference between two step is smaller than 'FitDataPercent', stop, otherwise, continue.
\"PrimeNo\": specify prime field FFPrimeNo['PrimeNo'].
\"Break\": searching relations under fixed degree bound and # database is called one round. If relations are not enough, increase degree bound or # database or both. Function finished untill relations are enough or reach round-limit 'Break'.
";
BLSearchRelations::usage="BLSearchRelations[job_,dataname_,vars_,varmode_,intmode_, cutincre_:2, timelimit_:Infinity] search block-triangular relations for 'job'. Return {fitnumber,tfit,tbt}. where 'fitnumber' is the number of numerical IBP used, 'tfit' is time for fit block-triangular relations, 'tbt' is time for solving block-triangular relations
'job': name of the block-triangular system.
'dataname': name of database( numerical IBP is used for determining block-triangular form).
'vars': a list of groups of variables, looks like {{x1,x2},{x3,x4}}.
'varmode': a list that has the same length with 'vars'. Possible values are \"uniform\", \"adaptive\" or exact number.
'intmode': a list that has the same length of 'vars', Possible values are \"uniform\" and \"weight\".
'cutincre' used if number of groups in 'vars' is larger than 1. This is because when we search block-triangular form in more than one direction(e.g. {x1,x2} and {x3,x4} forms two direction), we must introduce cut-off for all except one direction. possible values are one integer or a list of integer whose length is less than that of 'vars' by 1.
'timelimit' time-constraints in the unit of second(BLDatabase is not counted).
This function will call BLSearchRelationsBasic internally to transform 'varmode' to explicit integer 'varweight' and sort 'vars' by complexity . For options, see ?BLSearchRelationsBasic";
BLFitRelations::usage="BLFitRelations[dataname, job] construct block-triangular relations for 'job' at another prime field or numerical phase point, named by 'dataname'. It is much faster than BLSearchRelations because redudant polynomial ansatzs during BLSearchRelations have been removed";


Begin["`Private`"];
BLSetDataNumber;
BLDataNumber;
BLClearDatabase;
jobFitQ;
jobCreatedQ;
jobFinishedQ;
End[];


Begin["`SearchRelation`"]


(* ::Section:: *)
(*BLDataNumber*)


BLSetDataNumber[dataname_, number_]:=Block[{dir, common},
dir = FileNameJoin[{$SearchDirectory, "database", dataname}];
common = Import[FileNameJoin[{dir, "red_common"}], "Table"];
common[[1,-1]] = number;
writeObject[FileNameJoin[{dir, "red_common"}], common, OpenWrite];
];


BLDataNumber[dataname_]:=If[!FileExistsQ[#], 0, Import[#, "Table"][[1,-1]]]&[FileNameJoin[{$SearchDirectory, "database", dataname, "red_common"}]];


BLClearDatabase[dataname_]:=DeleteDir[FileNameJoin[{$SearchDirectory, "database", dataname}]];


(* ::Section:: *)
(*jobMonitor*)


(*all configs are evaluated correctly, but not enough to reduce g1*)
workTerminatedQ[job_, workid_]:=Block[{dir,flag,ins,tmps},
dir = FileNameJoin[{$SearchDirectory, job, ToString[workid]}];
flag = FileNameJoin[{dir, "flag"}];
ins = FileNames[FileNameJoin[{dir, "config", "*", "in_nvar"}]];
tmps = FileNames[FileNameJoin[{dir, "config", "*", "tmp_nvar"}]];
Length[ins] === Length[tmps] && GetFile[flag] === 0
];
jobTerminatedQ[job_]:=AnyTrue[workTerminatedQ[job, #]&/@Range[0, workNumber[job]-1], TrueQ];


(*configs are partially evaluated, but not enough to reduce g1*)
workMissingDatabaseQ[job_, workid_]:=Block[{dir,flag,ins,tmps},
dir = FileNameJoin[{$SearchDirectory, job, ToString[workid]}];
flag = FileNameJoin[{dir, "flag"}];
ins = FileNames[FileNameJoin[{dir, "config", "*", "in_nvar"}]];
tmps = FileNames[FileNameJoin[{dir, "config", "*", "tmp_nvar"}]];
Length[ins] =!= Length[tmps] && GetFile[flag] === 0
];
jobMissingDatabaseQ[job_]:=AnyTrue[workMissingDatabaseQ[job, #]&/@Range[0, workNumber[job]-1], TrueQ];


jobCreatedQ[job_] :=Block[{},Length[FileNames[FileNameJoin[{$SearchDirectory, job, "*", "sch_g1"}]]]>0]


jobFinishedQ[job_] :=Block[{sches, flags},
sches = FileNames[FileNameJoin[{$SearchDirectory, job, "*", "sch_g1"}]];
flags = FileNames[FileNameJoin[{$SearchDirectory, job, "*", "flag"}]];
Length[sches] === Length[flags] && AllTrue[GetFile/@flags, #===1&]
];


jobFitQ[dataname_, job_]:=Block[{sches, flags},
sches = FileNames[FileNameJoin[{$SearchDirectory, job, "*", "sch_g1"}]];
flags = FileNames[FileNameJoin[{$SearchDirectory, job, "*", "fit", dataname, "flag"}]];
Length[sches] === Length[flags] && AllTrue[GetFile/@flags, #===1&]
];


jobMatchQ[btfiles_,name1_,name2_]:=Module[{count,workdir,partname,work,part,file1,file2,flag,wc1,wc2},
Print["jobMatchQ for: ",name1," and ",name2,"..."];
count=1;
workdir[i_,j_]:=FileNameJoin[{btfiles[[i]],ToString[j]}];
partname[i_,j_,k_,name_]:=FileNameJoin[{btfiles[[i]],ToString[j],"fit",name,ToString[k]}];
(*obtain all file names*)
Do[
work=0;
While[DirectoryQ@workdir[i,work],
part=0;
While[FileExistsQ@partname[i,work,part,name1],
file1[count]=partname[i,work,part,name1];
file2[count]=partname[i,work,part,name2];
count++;
part++];
work++];
,{i,1,Length@btfiles}];
(*parallel check *)
flag=AbsoluteTiming@ParallelTable[
wc1=Import[file1[i],"Table"]//Dimensions;
wc2=If[FileExistsQ@file2[i],Import[file2[i],"Table"],{}]//Dimensions;
If[wc1=!=wc2,file1[i],Nothing]
,{i,1,count-1},DistributedContexts->All];
If[flag[[2]]==={},Print["jobMatchQ: ",True, " ,in: ", flag[[1]],"s."];Return[True],
					Print["jobMatchQ: ",False, " ,in: ", flag[[1]],"s."];
					Print["unmatched families: ",StringCases[flag[[2]],fam__~~"/reduction/search/"~~Shortest[job__]~~"/"~~c__:>StringRiffle[{fam,job},"_"]]//DeleteDuplicates];
					Return[False]];
]


(* ::Section:: *)
(*BLSearchRelationsBasic*)


dataSize[paras_]:=(Quotient[Switch[Length[Flatten[paras]],1,10,2,30,3,100,4,300,_,1000]-1,BLDatabaseNthreads]+1)*BLDatabaseNthreads;
ansatzSize[cut_]:=fromDigits[Append[cut,7],cut];


getLowerLimit[job_]:=Module[{max,part},
max=0;
Do[
part=0;
While[FileExistsQ[FileNameJoin[{$SearchDirectory,job,ToString[jobid],"config",ToString[part],"tmp_nsol"}]],
part++];
If[part>max,max=part-1],{jobid,0,workNumber[job]-1}];
max];


Options[BLSearchRelationsBasic]={"OverWrite"->True,"SearchOnly"->False,"DataSize"->dataSize,"AnsatzSize"->ansatzSize,"FitDataPercent"->1/5,"PrimeNo"->0,"Break"->10};
BLSearchRelationsBasic[job_,dataname_,vars_,varweight_,intmode_,cut_,timelimit_,OptionsPattern[]]:=Block[
{iniansatz,k,break,pid,datasize,searchnumber,range,fitnumber, percent, time1,time2,tfit,tfitlimit,tbt,count},

(*DeleteDirectory[FileNameJoin[{$SearchDirectory,job}],DeleteContents->True];*)
(*generate job*)
BLGenerateJob[job]//MyTiming;

(*initialize ansatz*)
(*iniansatz=ansatzSize[cut];*)
iniansatz=OptionValue["AnsatzSize"][cut];
BLGenerateAnsatzForJob[job,iniansatz,vars,varweight,intmode,cut,"OverWrite"->OptionValue["OverWrite"]];

(*initialize*)
k=0;
break=OptionValue["Break"];
pid=OptionValue["PrimeNo"];
(*datasize=dataSize[vars];*)
datasize=OptionValue["DataSize"][vars];

(*initialize database*)
$Conservative=Select[BLSearchParameter,!MemberQ[Flatten[vars],#]&];
If[OptionValue["OverWrite"],BLClearDatabase[dataname]];
If[BLDataNumber[dataname] < datasize, BLDatabase[dataname, datasize-BLDataNumber[dataname],pid]//MyTiming];

(*search iterativly*)
(*increase ansatz by fromDigits[UnitVector[Length[cut]+1,Length[cut]+1],cut], i.e increase the power of the last group variable by 1*)
(*increase datasize by a factor of 2*)
MyTiming[Catch[While[k < break,
WriteMessage[{"search round ->" , k}];
WriteMessage[{"current data number ->", BLDataNumber[dataname]}];
BLRedG1[dataname, job, timelimit];
If[jobFinishedQ[job],Throw[1],k++];
Which[
jobTerminatedQ[job]&&jobMissingDatabaseQ[job],
	BLGenerateAnsatzForJob[job,iniansatz+k*fromDigits[UnitVector[Length[cut]+1,Length[cut]+1],cut],vars,varweight,intmode,cut,"OverWrite"->False];
	(*If[BLDataNumber[dataname] < Power[2,k]*datasize, BLDatabase[dataname, Power[2,k]*datasize-BLDataNumber[dataname],pid]]*)
	BLDatabase[dataname, BLDataNumber[dataname],pid],
jobTerminatedQ[job],
	BLGenerateAnsatzForJob[job,iniansatz+k*fromDigits[UnitVector[Length[cut]+1,Length[cut]+1],cut],vars,varweight,intmode,cut,"OverWrite"->False],
jobMissingDatabaseQ[job],
	(*If[BLDataNumber[dataname] < Power[2,k]*datasize, BLDatabase[dataname, Power[2,k]*datasize-BLDataNumber[dataname],pid]]*)
	BLDatabase[dataname, BLDataNumber[dataname],pid],
_,  ErrorPrint["error: BLSearchRelationsBasic encounter unknown pattern."]];
]];,"SearchTemplate"];

(*search failed if reach round-limit or time-limit*)
Which[jobFinishedQ[job], Nothing,
k>=break, WriteMessage["Caution: search failed, reach round-limit -> ",break]; Return[$Failed],
True, WriteMessage["Caution: search failed, reach time-limit -> ",timelimit]; Return[$Failed]];

(*used in search learning*)
If[OptionValue["SearchOnly"],Return[1]];

(*fit template and find the minimum number of database*)
DeleteDir[FileNameJoin[{$SearchDirectory, job, ToString[#], "fit", dataname}]]&/@Range[0, workNumber[job]-1];
searchnumber = BLDataNumber[dataname];
percent=OptionValue["FitDataPercent"];
time1 = AbsoluteTime[];
BLFitRelations[dataname, job];
time2 = AbsoluteTime[];
tfit = time2-time1;
(*backup*)
Do[RenameDirectory[FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname}],FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname<>"_bak"}]]
,{ii,0,workNumber[job]-1}];

(*bisection method*)
range={0,searchnumber};
MyTiming[While[(range[[2]]-Ceiling[Mean[range]])/Ceiling[Mean[range]] > percent && Quotient[range[[2]]-1,BLDatabaseNthreads]>Quotient[Ceiling[Mean[range]]-1,BLDatabaseNthreads],
BLSetDataNumber[dataname, Ceiling[Mean[range]]];
WriteMessage[{"current data number ->" , BLDataNumber[dataname]}];
(*time limit for BLFitRelations, CPU efficiency is estimated to be 0.8*)
tfitlimit= ( range[[2]]- BLDataNumber[dataname])*BLTimeIBP/BLDatabaseNthreads/0.8 + tfit; 
DeleteFile[FileNames[FileNameJoin[{$SearchDirectory, job, "*", "fit", dataname, "flag"}]]];
time1 = AbsoluteTime[];
BLFitRelations[dataname, job,tfitlimit];
time2 = AbsoluteTime[];
If[jobFitQ[dataname, job],range[[2]]=BLDataNumber[dataname];tfit=time2-time1,range[[1]]=BLDataNumber[dataname]];
];,"FitTemplate"];
(*recover flag, no need to Fit again*)
If[!jobFitQ[dataname, job],
	Do[
		count=0;
		While[count<3,
		DeleteDir[FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname}]];
		Pause[.1];
		If[DirectoryQ[FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname}]],count++,Break[]]];
		If[count===3,ErrorPrint["error: DeleteDirectory failed."];Abort[]];
		RenameDirectory[FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname<>"_bak"}],FileNameJoin[{$SearchDirectory, job, ToString[ii], "fit", dataname}]]
	,{ii,0,workNumber[job]-1}];
];

fitnumber = range[[2]];
Put[fitnumber,FileNameJoin[{$SearchDirectory,job,"fitnumber"}]];

(*evarage time for solving block-triangular relations(single core) unit: second*)
blSolveLearning[job,dataname,pid];
tbt=GetFile[FileNameJoin[{$SearchDirectory,job,"average_time"}]];

BLSetDataNumber[dataname, searchnumber];

{fitnumber,tfit,tbt}

];


(*search relations with the given scheme*)
BLRedG1[dataname_, job_, timelimit_:Infinity]:=Block[{prop, fp},
fp = FileNameJoin[{$SearchDirectory, job, "redg1"}];
prop = TimeConstrained[BLRunCommand[{$BLRedG1Path, $SearchDirectory, dataname, job, ToString[0], ToString[workNumber[job]], ToString[BLNthreads]},"log"->fp],timelimit,TIMELIMIT];
If[prop===TIMELIMIT,WriteMessage["Caution: search reach time-limit -> "<>ToString[timelimit]];Throw[$Failed]];
];


(* ::Section:: *)
(*BLSearchRelations*)


defaultSearchName[expr__]:=StringJoin[ToString/@Flatten[{expr}]];


(* *1000 in case limits0 contains 0 *)
powerlimit2weight[limits0_]:=Module[{temp},temp=limits0*1000/.(0->1);temp=LCM[Sequence@@temp]/temp;Floor[temp/Min[temp]]];


Options[BLSearchRelations]=Options[BLSearchRelationsBasic];
BLSearchRelations[job_,dataname_,vars0_,varmode0_,intmode0_,cutincre_:2,timelimit_:Infinity,OptionsPattern[]]:=Module[
{vars,varmode,intmode,varweights, name,weight,cut,ord,powerlimit,flag, opt},

vars=vars0;
varmode=varmode0;
intmode=intmode0;

(*determine varweights(exact number)*)
WriteMessage["Begin determine variable weights..."];
varweights=Table[
Which[
	varmode[[i]]==="adaptive" && Length[vars[[i]]]>1,
	powerlimit=Table[
		name=defaultSearchName[{{vars[[i,kk]]}},{{1}},{intmode[[i]]},{}];
		opt = FilterRules[#->OptionValue[#]&/@Keys[Options[BLSearchRelationsBasic]],Except["SearchOnly"]];
		If[!(jobCreatedQ[name]&&jobFinishedQ[name]),BLSearchRelationsBasic[name, name, {{vars[[i,kk]]}},{{1}},{intmode[[i]]},{},timelimit,"SearchOnly"->True,Sequence@@opt]//MyTiming];
		getLowerLimit[name],{kk,Length@vars[[i]]}];
	weight=powerlimit2weight[powerlimit],
	
	varmode[[i]]==="uniform" || Length[vars[[i]]]===1,
	weight=ConstantArray[1,Length[vars[[i]]]],
	
	True,
	weight=varmode[[i]]
]
,{i,Length@vars}];
WriteMessage["Variables: ", ToString[vars], ", Weights: ",ToString[varweights]];

(*cut *)
(*Suppose we are going to search relations w.r.t x&y. We divide them into two group {{x},{y}}, which could increase powers individualy. 
It is necessary to set an upper-bound for one group, e.g {x}, otherwise we can not explore relations along {y}-dimension. Similarly, 
if we are going to search among three group of variables,  we should set upper-bound for two groups. This explains the meaning of \"cut\"*)
WriteMessage["Begin determine cut..."];
If[Length[vars]===1, cut={},
	powerlimit=Table[
		name=defaultSearchName[{vars[[i]]},{varweights[[i]]},{intmode[[i]]}];
		opt = FilterRules[#->OptionValue[#]&/@Keys[Options[BLSearchRelationsBasic]],Except["SearchOnly"]];
		If[!(jobCreatedQ[name]&&jobFinishedQ[name]),BLSearchRelationsBasic[name,name, {vars[[i]]},{varweights[[i]]},{intmode[[i]]},{},timelimit,"SearchOnly"->True,Sequence@@opt]//MyTiming];
		getLowerLimit[name]
		,{i,Length[vars]}];
	WriteMessage["Lower limit: ",ToString@powerlimit];
	ord=Ordering[powerlimit];	
	(*sort SearchVariables from simple to complex*)
	vars=vars[[ord]];
	varweights=varweights[[ord]];
	intmode=intmode[[ord]];
	cut = powerlimit[[ord]][[;;-2]] + cutincre;
];
WriteMessage["Sorted vars: ",ToString@vars,", Weights: ",ToString@varweights,", IntMode: ", ToString@intmode, ", Cut: ",ToString@cut];

(*search*)
opt = #->OptionValue[#]&/@Keys[Options[BLSearchRelationsBasic]];
flag=BLSearchRelationsBasic[job,dataname,vars,varweights,intmode,cut,timelimit,Sequence@@opt]//MyTiming;
If[flag=!=$Failed,WriteMessage["# database -> ",flag[[1]], ", time for fitting -> ", Round[flag[[2]],0.0001], "s, time for solving -> ", Round[flag[[3]],0.0001],"s."]]; 
flag
];


(* ::Section:: *)
(*BLFitRelations*)


(*search relations with the given scheme*)
BLFitRelations[dataname_, job_, timelimit_:Infinity]:=Block[{prop, fp},
fp = FileNameJoin[{$SearchDirectory, job, "fitrel_"<>dataname}];
prop = TimeConstrained[BLRunCommand[{$BLFitRelPath, $SearchDirectory, dataname, job, ToString[0], ToString[workNumber[job]], ToString[BLNthreads]},"log"->fp],timelimit,TIMELIMIT];
If[prop===TIMELIMIT,WriteMessage["Caution: fit reach time-limit -> "<>ToString[timelimit]];Return[$Failed]];
];


(* ::Section:: *)
(*End*)


End[];
