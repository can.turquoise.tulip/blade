(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLAdaptiveSearch::usage = "BLAdaptiveSearch[points_] This function weighs the benefits of sovling block-triangular system more efficiently against the risk of searching block-triangular system failed and return `nana`, the optimal number of analytic parameters in Block-triangular system.
\"SearchTimeFactor\" allow users to adjust their acceptable time-limit for searching BL(normalized to estimated TotalTime for reduction).
For search options, please see BLSetSearchOptions[] and ?BLSearchRelations";


Begin["`Private`"];
End[];


Begin["`AdaptiveSearch`"]


(* ::Section:: *)
(*BLAdaptiveSearch*)


Options[BLAdaptiveSearch]={"SearchTimeFactor"->1/2};
BLAdaptiveSearch[points_,OptionsPattern[]]:=Module[
{timefactor, nana, timelimit, vars,varmode, intmode, cutincre, varweights, powerlimit, name, weight,
powerlimit2weight,ord,cut, job, dataname, flag},

(*ReconstructionFunction requirement*)
If[!AllTrue[CheckSearchOptions/@Range[Length@BLSearchParameter],TrueQ],WriteMessage["error: please check search options"];Return[$Failed]];
timefactor=OptionValue["SearchTimeFactor"];

Clear[TotalTime];
TotalTime[_]:=Infinity;
TotalTime[0]=timeEstimate[0,Infinity,Infinity,Infinity,points];
(*20230427: if # points \[Equal]0(weird! M1\[Rule]M1,I1\[Rule]0,I2\[Rule]0, where Ii are target), TimeConstraint would fail*)
If[TotalTime[0]==0,WriteMessage["Caution: 0 sample points encountered"];TotalTime[0]=0.001];

(*increase nana one by one*)
nana=1;
While[nana<=Length[BLSearchParameter],
timelimit = timefactor * Min[TotalTime/@Range[0,nana-1]];
WriteMessage["current options:  ",ToString[SearchOptions[nana]]];

(*initialze value*)
vars = SearchOptions[nana]["SearchVariable"];
varmode = SearchOptions[nana]["VariableWeight"];
intmode = SearchOptions[nana]["IntegralWeight"];
cutincre = SearchOptions[nana]["CutIncrement"];
job=SearchOptions[nana]["Jobname"];
dataname=SearchOptions[nana]["DatanamePrefix"]; 
If[TrueQ[nana==Length[BLSearchParameter]],dataname=dataname~~"_0"]; (*reduce one prime field*)

flag=BLSearchRelations[job,dataname,vars,varmode,intmode,cutincre,timelimit]//MyTiming;
If[flag===$Failed,Break[]];
TotalTime[nana]=timeEstimate[nana,Sequence@@flag,points];
WriteMessage["Estimated wall time(BL FiniteField) -> ", Round[TotalTime[nana],0.001], " s."];
nana++
];

(*select the optimal strategy of sampling, i.e. `nana` *)
nana=Ordering[Round[TotalTime/@Range[0,Length[BLSearchParameter]],1],1][[1]]-1;
WriteMessage["The optimal choice is NVariables -> ",nana];
nana
]


(*estimate real time for sampling under prime field*)
(*t1: real time for traditional IBP(in the unit of #samples),  t2: real tiem for Block-triangular relations(in the unit of #samples*)
timeEstimate[nana_,ndata_,tfit_,tbt_,points_]:=Module[{pts,t1,t2},
pts=GatherBy[points,#[[1;;Length[BLSearchParameter]-nana]]&];
t1=BLTimeIBP/BLDatabaseNthreads;
t2=tbt/BLNthreads;
Return[Sum[Function[{pt},If[Length[pt]<=ndata,Length[pt]*t1,ndata*t1+tfit+Length[pt]*t2]][pts[[i]]],{i,1,Length@pts}]]
];


(* ::Section:: *)
(*End*)


End[];
