struct database
{
	//prime and inverse
	unsigned long long prime;
	unsigned long long invp;
	
	//number of parameters
	int npara;

	//number of integrals, number of master integrals
	int nint;
	int nmaster;

	//number of nonzero elements in a single reduction table
	int nentry;

	//number of phase space points
	int nps;

	//partition information
	struct list_int part0;
	struct list_int * part;

	//integrals that have nonzero projection on each master
	struct list_int posi0;
	struct list_int * posi;

	//phase space points configurations
	struct table_llu ps0;
	struct table_llu * ps;

	//database
	struct table_llu data0;
	struct table_llu * data;
};

void database_init(struct database * db, char * dir);
void database_clear(struct database * db);