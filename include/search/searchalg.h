void subtract_list(struct list_llu * list, struct list_llu * list0, int first, unsigned long long prime, unsigned long long invp);
void normalize_list(struct list_llu * list, int first, unsigned long long prime, unsigned long long invp);
unsigned long long inner(struct list_llu * list1, struct list_llu * list2, unsigned long long prime, unsigned long long invp);
void generate_constraint(struct database * db, struct kinematics * kn, struct template * tp, int m, int n, struct list_llu * list);
void simplify_rel_sparse(struct sparse * sp, unsigned long long prime, unsigned long long invp, struct list_llu * list);
void solve_master(struct database * db, struct kinematics * kn, struct template * tp, int n, struct sparse * sp);
void nullspace(struct sparse * sp, unsigned long long prime, unsigned long long invp, struct table_llu * null);
int self_check(struct database * db, struct template * tp);
int search_relation(struct database * db, struct kinematics * kn, struct template * tp);
int matrix_rank(struct table_llu * mat, unsigned long long prime);
int determine_indep_relations(struct template * tp, struct table_llu * matrix, struct list_int * indep, int * rank, int * rowid, unsigned long long prime);
void determine_needed_variables(struct template * tp, struct list_int * indep, struct list_int * needQ);
void write_template_g1(struct template * tp, struct list_int * indep, char * dir);
int key_existsQ(char * dir, int configid, char * key);
void reduce_g1(struct database * db, struct kinematics * kn, char * dir);
int search_relation_from_source(struct database * db, struct kinematics * kn, struct template * tp, char * dir, char * key, char * path);
void fit_relations(struct database * db, struct kinematics * kn, char * dir, char * writedir);