struct block_info
{
	int g1;
	int nint;
	struct list_int intid0;
	struct list_int * intid;
};

struct tmp_info
{
	int nvar;
	int nsol;

	struct table_int var0;
	struct table_int * var;
};

struct system
{
	//number of blocks
	int nbi;
	struct block_info ** bilist;

	//partition info of tmplist
	struct list_int part0;
	struct list_int * part;

	//number of template
	int nti;
	struct tmp_info ** tilist;
	struct table_llu ** sollist;

	//info read from red_common
	unsigned long long prime;
	unsigned long long invp;
	int npara;
	int nint;
	int nmaster;

	//info read from kinematics
	int nmono;
	struct table_int kincfg0;
	struct table_int * kincfg;

	//solving order of blocks
	struct list_int order0;
	struct list_int * order;
};

void block_info_init(char * dir, struct block_info * bi);
void block_info_clear(struct block_info * bi);
void template_info_init(char * dir, struct tmp_info * ti);
void template_info_clear(struct tmp_info * ti);
int block_number_count(char * dir);
int tmp_number_count(char * dir);
void system_info_init(char * dir, struct system * sys);
void system_null_init(char * dir, char * dataname, struct system * sys);
void system_common_init(char * dir, char * dataname, struct system * sys);
int block_solvableQ(struct system * sys, int n, struct list_int * solved);
void append_solved(struct system * sys, int n, struct list_int * solved);
int first_block(struct system * sys, struct list_int * block, struct list_int * solved);
void system_order_init(struct system * sys);
void system_init(char * dir, char * dataname, struct system * sys);
void system_clear(struct system * sys);
int system_relation_count(struct system * sys);

//solution of each integral
struct integral_sol
{
	//number of nonzero projections on master
	int nmas;
	
	//index of nonzero projections
	struct list_int index0;
	struct list_int * index;

	//projections
	struct list_llu sol0;
	struct list_llu * sol;
};

struct system_sol
{
	//kinematics evaluated at fixed phase space points
	struct list_llu kinlist0;
	struct list_llu * kinlist;

	int nint;
	struct integral_sol ** islist;
};

//block
struct block_sol
{
	int nmas;

	//index of nonzero projections on masters
	struct list_int index0;
	struct list_int * index;

	//numerical form
	struct table_llu block0;
	struct table_llu * block;

	//after substitute non-homogeneous terms
	nmod_mat_t mat;
};

void system_sol_kinematics_init(struct system_sol * ss, struct system * sys, struct list_llu * ps);
void integral_sol_init(struct integral_sol * is, int leng);
void integral_sol_clear(struct integral_sol * is);
void system_sol_ints_init(struct system_sol * ss, struct system * sys);
void system_sol_init(struct system_sol * ss, struct system * sys, struct list_llu * ps);
void system_sol_clear(struct system_sol * ss);
void block_sol_index_init(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs);
void block_sol_assign(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs);
void block_sol_substitute(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs);
void block_sol_trans(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs);
void block_sol_clear(struct block_sol * bs);
void block_solve_learn(struct system_sol * ss, struct system * sys, int n, struct block_sol ** bslist);
void system_solve_learn(struct system_sol * ss, struct system * sys, struct block_sol ** bslist);
void block_solve(struct system_sol * ss, struct system * sys, int n, struct block_sol ** bslist);
void system_solve(struct system_sol * ss, struct system * sys, struct block_sol ** bslist);
int system_sol_entry_count(struct system_sol * ss);
void sparse_system_sol(struct system_sol * ss, struct list_llu * list);
void sparse_system_sol_info(struct system_sol * ss, struct list_int * index, struct list_int * part);