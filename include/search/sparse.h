struct sparse
{
	//number of row, number of col, number of nonzero elements
	int nrow;
	int ncol;
	int nentry;

	//list of partition info
	struct list_int part0;
	struct list_int * part;

	//list of nonzero elements
	struct list_llu entry0;
	struct list_llu * entry; 
	
	//list of column id
	struct list_int posi0;
	struct list_int * posi;
};

void sparse_init(struct sparse * sp);
void sparsify(struct sparse * sp, struct table_llu * tab);
void sparse_clear(struct sparse * sp);
void sparse_index_replace(struct sparse * sp, struct list_int * index);
void sparse_append(struct sparse * sp, struct sparse * sp0);