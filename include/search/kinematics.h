struct kinematics
{
	//number of monomials
	int nmono;

	//number of parameters
	int npara;

	//monomials list
	struct table_int cfg0;
	struct table_int * cfg;

	//numerical evaluations
	//size: db->nps * kn->nmono
	struct table_llu num0;
	struct table_llu * num;
};

void kinematics_config_init(struct kinematics * kn, char * dir);
void kinematics_num_init(struct kinematics * kn, struct database * db);
void kinematics_init(struct kinematics * kn, struct database * db, char * dir);
void kinematics_clear(struct kinematics * kn);