struct list_int
{
	int size;
	int * entry;
};

void list_init_int(struct list_int * list, int size);
void list_clear_int(struct list_int * list);
void list_realloc_int(struct list_int * list, int size);
void list_read_int(char * path, struct list_int * list);
void list_write_int(char * path, struct list_int * list);
void list_reverse_int(struct list_int * list);
int list_countab_int(struct list_int * list, int a, int b);
int list_count_int(struct list_int * list, int key);
int list_agcount_int(struct list_int * list, int key);
int list_first_position_int(struct list_int * list, int key);
int list_first_agposition_int(struct list_int * list, int key);
void list_position_int(struct list_int * list, struct list_int * posi, int key);
void list_agposition_int(struct list_int * list, struct list_int * posi, int key);
void list_picksub_int(struct list_int * list, struct list_int * sublist, struct list_int * posi);
void list_copy_int(struct list_int * a, struct list_int * b);


struct table_int
{
	int nrow;
	int ncol;
	int * entry;
};

void table_init_int(struct table_int * tab, int nrow, int ncol);
void table_clear_int(struct table_int * tab);
int * table_pointer_int(struct table_int * tab, int rowid, int colid);
void table_read_int(char * path, struct table_int * tab);
void table_write_int(char * path, struct table_int * tab);
void table_pick_int(struct list_int * list, struct table_int * tab, int rowid);
void table_picksub_int(struct table_int * tab, struct table_int * subtab, struct list_int * posi);


struct list_llu
{
	int size;
	unsigned long long * entry;
};

void list_init_llu(struct list_llu * list, int size);
void list_clear_llu(struct list_llu * list);
void list_realloc_llu(struct list_llu * list, int size);
void list_read_llu(char * path, struct list_llu * list);
void list_write_llu(char * path, struct list_llu * list);
void list_reverse_llu(struct list_llu * list);
int list_countab_llu(struct list_llu * list, unsigned long long a, unsigned long long b);
int list_count_llu(struct list_llu * list, unsigned long long key);
int list_agcount_llu(struct list_llu * list, unsigned long long key);
int list_first_position_llu(struct list_llu * list, unsigned long long key);
int list_first_agposition_llu(struct list_llu * list, unsigned long long key);
void list_position_llu(struct list_llu * list, struct list_int * posi, unsigned long long key);
void list_agposition_llu(struct list_llu * list, struct list_int * posi, unsigned long long key);
void list_picksub_llu(struct list_llu * list, struct list_llu * sublist, struct list_int * posi);
void list_copy_llu(struct list_llu * a, struct list_llu * b);


struct table_llu
{
	int nrow;
	int ncol;
	unsigned long long * entry;
};

void table_init_llu(struct table_llu * tab, int nrow, int ncol);
void table_clear_llu(struct table_llu * tab);
unsigned long long * table_pointer_llu(struct table_llu * tab, int rowid, int colid);
void table_read_llu(char * path, struct table_llu * tab);
void table_write_llu(char * path, struct table_llu * tab);
void table_pick_llu(struct list_llu * list, struct table_llu * tab, int rowid);
void table_picksub_llu(struct table_llu * tab, struct table_llu * subtab, struct list_int * posi);