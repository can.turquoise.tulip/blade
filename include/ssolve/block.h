#include <flint/nmod_mat.h>
#include "file.h"
#include "common.h"

#pragma once
#ifdef __cplusplus
extern "C" {
#endif
//struct symbol0, *symbol = &symbol  initialize
struct template_info {
    
    int nvar;
    int nsol;
    struct table_int var0;
    struct table_int *var;
};

struct block {
    int g1; //number of integrals to reduce
    int nint; //total number of integrals in the block
    struct list_int intid0;
    struct list_int * intid; //integral id
    
    int ntp;//number of template
    struct template_info ** tp_info; //double pointer
    struct table_ulong ** sol;
};

struct system {
    //number of blocks
    int nblocks;
    struct block ** bllist;
    
    //database info
    //masters should put at the end of integral_list. 
    unsigned long long prime;
    unsigned long long invp;
    int nints;
    int nmis;
    unsigned long long nparsin; //equal to kin_row
    unsigned long long nparsout; //after learning
    
    //kinematics
    int kin_col;
    int kin_row;
    struct table_int kintable0; 
    struct table_int * kintable; //config
 
    //remove redudant blocks, when magic relation appears
    struct list_int *intid2blockid;
   
    //target integrals
    struct list_int tarid0;
    struct list_int * tarid;
    
    //registerd blocks. determined by target integrals
    struct list_int regisbl0;
    struct list_int *regisbl;
    
    //solve ordering
    struct list_int ordering0; 
    struct list_int *ordering; //solve ordering of blocks
    
    int trimmingQ;
    struct list_int output_flag0; //not identical to ConstBitArrayView. If some integrals are zero, its flag==True but no output either.
    struct list_int *output_flag; //length of flag equal to length of tarid. if 0, set zero and output. if 1, get value from system_sol
    
    //target integrals solution infomation
    struct integral_sol **islist;
    
    //block solution information
    struct block_sol **bslist;
    
};

struct system_sol {
    struct list_ulong kinlist0;
    struct list_ulong * kinlist; //after substitution. do once for one point
    
    int nints;
    struct integral_sol **islist;
};

struct block_sol {
    struct list_int nzindex0;
    struct list_int *nzindex; // nonzero projection positions 
    
    struct table_ulong block_numeric0;
    struct table_ulong *block_numeric;//numerical relations of integrals
    
    // flint rowreduce
    nmod_mat_t mat;
};

struct integral_sol {
    int nmis;
    
    struct list_int index0; //nonzero position
    struct list_int *index;
    
    struct list_ulong elem0;
    struct list_ulong * elem; //projection
};

void template_info_init(struct template_info *tpif, char *dir);
void template_assign(struct system *sys, struct system_sol *ss,struct template_info *tpif, struct table_ulong *sol, struct table_ulong *sub);
void integral_sol_init(struct integral_sol *is, int size);
void block_init(struct block *bl, char *dir, char *db);
void block_sol_index_init(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs);
void block_sol_assign(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs);
void block_sol_substitute(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs);
void block_sol_rowreduce(struct system *sys, struct block_sol *bs);
void block_sol_trans(struct system * sys, struct system_sol * ss, struct block *bl, struct block_sol * bs);
void block_sol_clear(struct block_sol *bs);
void block_learn(struct system *sys, struct system_sol *ss, int n);
void block_solve(struct system *sys, struct system_sol *ss, int n);

void system_database_init(struct system *sys, char * dir);
void system_kinematic_init(struct system *sys, char * dir);
void system_block_init(struct system *sys, char * dir, char *db);
void system_intid2blockid_init(struct system *sys);
void system_register_needed_init(struct system *sys, struct list_int *need);
void system_solve_ordering_init(struct system *sys);
void system_init(struct system *sys, char *dir, char * db, char *job);
void system_init_from_txt(struct system *sys, char *filename);
void system_init_from_txt_parallel(struct system *sys, char *filename, unsigned nthreads);
void system_sol_kinematic_init(struct system *sys, struct system_sol *ss, struct list_ulong *pt);
void system_sol_integral_init(struct system *sys, struct system_sol *ss);
void system_sol_init(struct system *sys, struct system_sol *ss, struct list_ulong *pt);
void system_sol_cache_info(struct system *sys, struct system_sol *ss);
void system_sol_clear(struct system_sol *ss);
void system_learn(struct system *sys);
void system_evaluate(struct system *sys, struct list_ulong *pts, struct list_ulong *data);
//void system_solve(struct system *sys, struct table_ulong *pts, struct table_ulong *data);
void system_needed_clear(struct system *sys);
void system_sol_dump_info(char * dir,struct system *sys);
void system_average_sample_time(struct system *sys, char * dir);


#ifdef __cplusplus
}
#endif
