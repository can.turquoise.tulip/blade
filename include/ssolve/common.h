#ifndef COMMON_H
#define COMMON_H

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

struct list_int {
    int size;
    int * entry;
};

struct list_ulong {
    int size;
    unsigned long long * entry;
};


struct table_int {
    int row;//row
    int col;//collum
    int *entry;
};

struct table_ulong {
    int row;//row
    int col;//collum
    unsigned long long *entry;
};

void directory_count_with_filename(char *prefix, char *file, int *a);

void list_int_init(struct list_int * ls, int size);
void list_int_read(struct list_int * ls, char *filename);
void list_int_print(struct list_int * ls);
void list_int_copy(struct list_int * src, struct list_int *tar);
void list_int_quik_union(struct list_int *a, struct list_int *b, struct list_int *ls);
void list_int_clear(struct list_int * ls);
int list_int_count(struct list_int *a, int key);
int list_int_first_position(struct list_int * ls, int key);
int list_int_contains_all(struct list_int *a, struct list_int *b);
//void list_int_pick_sublist(struct list_int * a, struct list_int *posi, struct list_int *sub);
void list_ulong_init(struct list_ulong * ls, int size);
void list_ulong_read(struct list_ulong * ls, char *filename);
void list_ulong_write(struct list_ulong * ls, char* filename);
void list_ulong_nonzero_position(struct list_ulong * ls, struct list_int *posi);
int list_ulong_agcount(struct list_ulong *ls , unsigned long long key);
int list_ulong_first_position(struct list_ulong * ls, unsigned long long key);
void list_ulong_print(struct list_ulong * ls);
void list_ulong_random(struct list_ulong * ls, unsigned long long prime);
void list_ulong_clear(struct list_ulong *ls);

void table_int_init(struct table_int * tb, int r, int c);
void table_int_read(struct table_int * tb, char *filename);
void table_int_picklist(struct table_int * tb, int r, struct list_int *ls);
int table_int_point(struct table_int *tb, int r, int c);
void table_ulong_init(struct table_ulong * tb, int r, int c);
void table_ulong_read(struct table_ulong * tb, char *filename);
void table_ulong_pick(struct table_ulong * tb, int r, struct list_ulong *ls);
void table_ulong_subtable(struct table_ulong *tb, int r, struct table_ulong *sub);
unsigned long long table_ulong_entry(struct table_ulong *tb, int r, int c);
void table_ulong_print(struct table_ulong *tb);
void table_ulong_clear(struct table_ulong *tb);

#ifdef __cplusplus
}
#endif

#endif