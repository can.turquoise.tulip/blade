#include <fflow/common.hh>
#include <fflow/alg_reconstruction.hh>
#include <algorithm>
#include <fstream>
#include "common.h"
#include "block.h"

#pragma once
#ifdef __cplusplus
extern "C" {
#endif
//using namespace fflow;
void system_reset_needed_from_points(struct system *sys, fflow::SamplePointsVector &vec);
void system_trimming_from_points(struct system *sys, fflow::SamplePointsVector &vec);
void system_evaluate_and_sparse_cache(struct system *sys,const std::unique_ptr<fflow::UInt[]> * start,const std::unique_ptr<fflow::UInt[]> * end, unsigned ptsord, struct table_ulong *cache);
void system_sample_from_points(struct system *sys, int nthreads_, unsigned ptsord, fflow::SamplePointsVector &vec, struct table_ulong *Cache);
void system_dump_evaluations(char * filename, unsigned nparsin, unsigned nparsout, struct table_ulong *cache);

#ifdef __cplusplus
}
#endif