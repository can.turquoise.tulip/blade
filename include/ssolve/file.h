#ifndef FILE_H
#define FILE_H

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

int file_existsQ(char * filename);
void file_name_join(char *file , char *dir, char * name);
void file_read_int(char * filename, int *a);
void file_read_ulong(char * filename, unsigned long long *a);

#ifdef __cplusplus
}
#endif

#endif