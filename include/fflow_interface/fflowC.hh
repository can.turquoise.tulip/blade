//to include fflow::MPRational
#include <fflow/mp_gcd.hh>
#include <fflow/gcd.hh>
#include <fflow/primes.hh>

unsigned fflowml_new_graph(unsigned nvars);
unsigned fflowml_graph_dummy(unsigned nin, unsigned nout);
void fflowml_graph_delete(unsigned id);
unsigned fflowml_alg_json_sparse_system(unsigned graphid, int *nodes,  char * filename);
void fflowml_alg_system_only_homogeneous(unsigned id, unsigned nodeid);
void fflowml_graph_set_out_node(unsigned graphid, unsigned nodeid);
void fflowml_alg_set_learning_options(unsigned id, unsigned nodeid, unsigned primeno );
void fflowml_alg_learn(unsigned id);
int fflowml_graph_evaluate(unsigned gid, unsigned long long * xin, unsigned  n_xin, unsigned prime_no );
unsigned fflowml_alg_mark_and_sweep_eqs(unsigned id, unsigned nodeid);
void fflowml_alg_delete_unneeded_eqs(unsigned id, unsigned nodeid);
unsigned fflowml_alg_nonzero(unsigned graphid, int *nodes);
void fflowml_alg_all_degrees(unsigned id, unsigned nthreads, unsigned * opt_data);
void fflowml_dump_degrees(unsigned gid, char * filename);
void fflowml_load_degrees(unsigned gid, char * filename);
void fflowml_dump_sample_points(unsigned gid, char * filename, unsigned * opt_data);
unsigned  fflowml_samples_file_size(char * filename);
void fflowml_alg_sample_from_points(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename, unsigned samples_start, unsigned samples_size);
void fflowml_dump_evaluations(unsigned gid, char *filename);
void fflowml_load_evaluations(unsigned gid, char * list_of_filename );
void fflowml_alg_reconstruct(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename);
void fflowml_alg_reconstruct_mod(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename);


struct ReconstructionOptions get_rec_opt(unsigned * opt_data, ReconstructionOptions opt);
struct ReconstructionOptions get_rec_opt(unsigned * opt_data);

void put_sparse_poly_mono(FILE * fp , const class MPReconstructedPoly & p);
void put_sparse_ratfun_mono(FILE * fp, const class MPReconstructedRatFun & f);
void put_sparse_poly_coeff(FILE * fp , const class MPReconstructedPoly & p);
void put_sparse_ratfun_coeff(FILE * fp, const class MPReconstructedRatFun & f);
void put_sparse_ratfun_part(FILE * fp, const class MPReconstructedRatFun & f);
void put_mprat(FILE * fp, const class fflow::MPRational & q, void (*gmpfreefunc) (void *, size_t));
int load_recmod_coeff(char *filename, fflow::UInt * xout, unsigned start, unsigned nparsout);














