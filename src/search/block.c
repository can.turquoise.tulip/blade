#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <flint/nmod_mat.h>
#include "list.h"
#include "file.h"
#include "block.h"

//initialize information of a block
//dir: where sch_* lives
void block_info_init(char * dir, struct block_info * bi)
{
	char * string = (char *)malloc(200);

	file_name_join(string, dir, "sch_g1");
	file_read_int(string, &bi->g1);

	file_name_join(string, dir, "sch_nint");
	file_read_int(string, &bi->nint);

	file_name_join(string, dir, "sch_intid");
	bi->intid = &bi->intid0;
	list_init_int(bi->intid, bi->nint);
	list_read_int(string, bi->intid);
}

void block_info_clear(struct block_info * bi)
{
	bi->g1 = 0;
	bi->nint = 0;
	list_clear_int(bi->intid);
}

//dir: where tmp_* lives
void template_info_init(char * dir, struct tmp_info * ti)
{
	char * string = (char *)malloc(200);

	file_name_join(string, dir, "tmp_nvar");
	file_read_int(string, &ti->nvar);

	file_name_join(string, dir, "tmp_nsol");
	file_read_int(string, &ti->nsol);

	file_name_join(string, dir, "tmp_var");
	ti->var = &ti->var0;
	table_init_int(ti->var, ti->nvar, 2);
	table_read_int(string, ti->var);

	free(string);
}

void template_info_clear(struct tmp_info * ti)
{
	ti->nvar = 0;
	ti->nsol = 0;
	table_clear_int(ti->var);
}

//count the number of blocks
//dir: where works live
int block_number_count(char * dir)
{
	char * string = (char *)malloc(200);
	int i = 0;

	while(1)
	{
		sprintf(string, "%s/%d/sch_g1", dir, i);
		if(!file_existsQ(string))
			break;
		i++;
	}

	free(string);
	return i;
}

//dir: where sch_* lives
//count the number of configs in a single work
int tmp_number_count(char * dir)
{
	char * string = (char*)malloc(200);

	int i = 0;
	while(1)
	{
		sprintf(string, "%s/config/%d/tmp_nvar", dir, i);
		if(!file_existsQ(string))
			break;
		i++;
	}

	free(string);
	return i;
}

//dir: where works live
void system_info_init(char * dir, struct system * sys)
{
	char * string = (char *)malloc(200);
	int i, j, counter = 0;

	sys->nbi = block_number_count(dir);
	sys->bilist = (struct block_info **)malloc(sys->nbi*sizeof(struct block_info *));
	sys->part = &sys->part0;
	list_init_int(sys->part, sys->nbi+1);

	for(i = 0; i<sys->nbi; i++)
	{
		sprintf(string, "%s/%d", dir, i);

		*(sys->bilist+i) = (struct block_info *)malloc(sizeof(struct block_info));
		block_info_init(string, *(sys->bilist+i));

		counter = counter+tmp_number_count(string);
		*(sys->part->entry+i+1) = counter;
	}

	sys->nti = *(sys->part->entry+sys->nbi);
	sys->tilist = (struct tmp_info **)malloc(sys->nti*sizeof(struct tmp_info *));

	for(i = 0; i<sys->nbi; i++)
	{
		int start = *(sys->part->entry+i), end = *(sys->part->entry+i+1);
		for(j = 0; j<end-start; j++)
		{
			sprintf(string, "%s/%d/config/%d", dir, i, j);
			*(sys->tilist+start+j) = (struct tmp_info *)malloc(sizeof(struct tmp_info));
			template_info_init(string, *(sys->tilist+start+j));
		}
	}

	free(string);
}

void system_null_init(char * dir, char * dataname, struct system * sys)
{
	char * string = (char *)malloc(200);
	int i, j;

	sys->sollist = (struct table_llu **)malloc(sys->nti*sizeof(struct table_llu *));

	for(i = 0; i<sys->nbi; i++)
	{
		int start = *(sys->part->entry+i), end = *(sys->part->entry+i+1);
		for(j = 0; j<end-start; j++)
		{
			sprintf(string, "%s/%d/fit/%s/%d", dir, i, dataname, j);
			
			*(sys->sollist+start+j) = (struct table_llu *)malloc(sizeof(struct table_llu));
			struct tmp_info * ti = *(sys->tilist+start+j);
			table_init_llu(*(sys->sollist+start+j), ti->nsol, ti->nvar);
			table_read_llu(string, *(sys->sollist+start+j));
		}
	}

	free(string);
}

//dir: where works live
//read from file the information of database and kinematcs
void system_common_init(char * dir, char * dataname, struct system * sys)
{
	char * string = (char *)malloc(200);
	sprintf(string, "%s/../database/%s/red_common", dir, dataname);

	FILE * fp;
	fp = fopen(string, "r");
	fscanf(fp, "%llu %d %d %d", &sys->prime, &sys->npara, &sys->nint, &sys->nmaster);
	fclose(fp);

	sys->invp = n_preinvert_limb(sys->prime);

	sprintf(string, "%s/../kinematics/kin_common", dir);
	file_read_int(string, &sys->nmono);

	sprintf(string, "%s/../kinematics/kin_table", dir);
	sys->kincfg = &sys->kincfg0;
	table_init_int(sys->kincfg, sys->nmono, sys->npara);
	table_read_int(string, sys->kincfg);

	free(string);
}

//solved: 1 means solved integrals, 0 means unsolved
//check whether g2 in the n-th block have been solved already
int block_solvableQ(struct system * sys, int n, struct list_int * solved)
{
	if(n==-1)
		return 0;

	struct block_info * bi = *(sys->bilist+n);

	int i;
	for(i = bi->g1; i<bi->nint; i++)
	{
		int integral = *(bi->intid->entry+i);
		if(*(solved->entry+integral)!=1)
			return 0;
	}

	return 1;
}

//n: non-negative
//set g1 of the n-th block to solved integrals
void append_solved(struct system * sys, int n, struct list_int * solved)
{
	struct block_info * bi = *(sys->bilist+n);

	int i;
	for(i = 0; i<bi->g1; i++)
	{
		int integral = *(bi->intid->entry+i);
		*(solved->entry+integral) = 1;
	}
}

//there may be some -1 in block
//find the first solvable one in block
//the value of block will be changed by this function
int first_block(struct system * sys, struct list_int * block, struct list_int * solved)
{
	struct list_int solvable0, * solvable = &solvable0;
	list_init_int(solvable, sys->nbi);

	int i;
	for(i = 0; i<sys->nbi; i++)
		*(solvable->entry+i) = block_solvableQ(sys, *(block->entry+i), solved);

	int first = list_first_position_int(solvable, 1);

	list_clear_int(solvable);

	int id;
	if(first!=-1)
	{
		id = *(block->entry+first);
		append_solved(sys, id, solved);
		*(block->entry+first) = -1;
	}
	else
		id = -1;

	return id;
}

//determine the order to solve the system
void system_order_init(struct system * sys)
{
	int i;
	
	//note that masters are appended to the last
	struct list_int solved0, * solved = &solved0;
	list_init_int(solved, sys->nint);
	for(i = sys->nint-sys->nmaster; i<sys->nint; i++)
		*(solved->entry+i) = 1;

	struct list_int block0, * block = &block0;
	list_init_int(block, sys->nbi);
	for(i = 0; i<sys->nbi; i++)
		*(block->entry+i) = i;

	sys->order = &sys->order0;
	list_init_int(sys->order, sys->nbi);
	for(i = 0; i<sys->nbi; i++)
		*(sys->order->entry+i) = first_block(sys, block, solved);

	if(list_count_int(sys->order, -1)>0)
		puts("system_order_init: cannot assign a correct order to solve the system.");

	list_clear_int(solved);
	list_clear_int(block);
}

void system_init(char * dir, char * dataname, struct system * sys)
{
	system_info_init(dir, sys);
	system_null_init(dir, dataname, sys);
	system_common_init(dir, dataname, sys);
	system_order_init(sys);
}

void system_clear(struct system * sys)
{
	int i;
	for(i = 0; i<sys->nbi; i++)
		block_info_clear(*(sys->bilist+i));

	for(i = 0; i<sys->nti; i++)
	{
		template_info_clear(*(sys->tilist+i));
		table_clear_llu(*(sys->sollist+i));
	}

	list_clear_int(sys->part);

	free(sys->bilist);
	free(sys->tilist);
	sys->nbi = 0;
	sys->nti = 0;

	sys->prime = 0;
	sys->invp = 0;
	sys->npara = 0;
	sys->nint = 0;
	sys->nmaster = 0;
	sys->nmono = 0;

	table_clear_int(sys->kincfg);
	list_clear_int(sys->order);
}

int system_relation_count(struct system * sys)
{
	int i, counter = 0;
	for(i = 0; i<sys->nbi; i++)
	{
		struct block_info * a = *(sys->bilist+i);
		counter = counter+a->g1;
	}
	
	return counter;
}

//evaluate all the kinematics at a single phase space point
void system_sol_kinematics_init(struct system_sol * ss, struct system * sys, struct list_llu * ps)
{
	ss->kinlist = &ss->kinlist0;
	list_init_llu(ss->kinlist, sys->nmono);

	struct list_int kincfg0, * kincfg = &kincfg0;

	int i, j;
	unsigned long long prod;
	for(i = 0; i<sys->nmono; i++)
	{
		table_pick_int(kincfg, sys->kincfg, i);
		prod = 1;
		for(j = 0; j<sys->npara; j++)
			prod = n_mulmod2_preinv(prod, n_powmod2_preinv(*(ps->entry+j), *(kincfg->entry+j), sys->prime, sys->invp), sys->prime, sys->invp);
		*(ss->kinlist->entry+i) = prod;
	}
}

void integral_sol_init(struct integral_sol * is, int leng)
{
	is->nmas = leng;
	is->index = &is->index0;
	list_init_int(is->index, is->nmas);
	is->sol = &is->sol0;
	list_init_llu(is->sol, is->nmas);
}

void integral_sol_clear(struct integral_sol * is)
{
	is->nmas = 0;
	list_clear_int(is->index);
	list_clear_llu(is->sol);
}

//initialize the solutions
void system_sol_ints_init(struct system_sol * ss, struct system * sys)
{
	ss->nint = sys->nint;
	ss->islist = (struct integral_sol **)malloc(ss->nint*sizeof(struct integral_sol *));
	
	int i;
	for(i = 0; i<sys->nint; i++)
	{
		*(ss->islist+i) = (struct integral_sol *)malloc(sizeof(struct integral_sol));
		struct integral_sol * is = *(ss->islist+i);
		
		if(i<sys->nint-sys->nmaster)
			integral_sol_init(is, 0);
		else
		{
			integral_sol_init(is, 1);
			*(is->index->entry) = i;
			*(is->sol->entry) = 1;
		}
	}
}

void system_sol_init(struct system_sol * ss, struct system * sys, struct list_llu * ps)
{
	system_sol_kinematics_init(ss, sys, ps);
	system_sol_ints_init(ss, sys);
}

void system_sol_clear(struct system_sol * ss)
{
	list_clear_llu(ss->kinlist);
	int i;
	for(i = 0; i<ss->nint; i++)
		integral_sol_clear(*(ss->islist+i));

	free(ss->islist);
}

//initialize the n-th block
void block_sol_index_init(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs)
{
	struct block_info * bi = *(sys->bilist+n);
	int i, j;
	
	//determine the nonzero projections
	struct list_int flag0, * flag = &flag0;
	list_init_int(flag, sys->nint);

	for(i = bi->g1; i<bi->nint; i++)
	{
		int globalid = *(bi->intid->entry+i);
		struct integral_sol * is = *(ss->islist+globalid);
		for(j = 0; j<is->nmas; j++)
			*(flag->entry+*(is->index->entry+j)) = 1;
	}

	bs->nmas = list_count_int(flag, 1);
	bs->index = &bs->index0;
	list_init_int(bs->index, bs->nmas);
	list_position_int(flag, bs->index, 1);

	list_clear_int(flag);
}

//assign the n-th block
void block_sol_assign(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs)
{
	struct block_info * bi = *(sys->bilist+n);
	bs->block = &bs->block0;
	table_init_llu(bs->block, bi->g1, bi->nint);

	int start = *(sys->part->entry+n), end = *(sys->part->entry+n+1);

	int i, j, k;
	int relid = 0;
	struct list_llu list0, * list = &list0;

	for(i = start; i<end; i++)
	{
		struct tmp_info * ti = *(sys->tilist+i);
		struct table_llu * sol = *(sys->sollist+i);

		//scan the nullspace
		for(j = 0; j<ti->nsol; j++)
		{
			table_pick_llu(list, sol, j);
			for(k = 0; k<ti->nvar; k++)
			{
				int localid = *table_pointer_int(ti->var, k, 0);
				int kinid = *table_pointer_int(ti->var, k, 1);
				unsigned long long mono = *(ss->kinlist->entry+kinid);
				unsigned long long prod = n_mulmod2_preinv(mono, *(list->entry+k), sys->prime, sys->invp);
				*table_pointer_llu(bs->block, relid, localid) = n_addmod(*table_pointer_llu(bs->block, relid, localid), prod, sys->prime);
			}
			relid++;
		}
	}
}

//subsititute the non-homogeneous terms
void block_sol_substitute(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs)
{
	struct block_info * bi = *(sys->bilist+n);
	int i, j, k;

	nmod_mat_init(bs->mat, bi->g1, bi->g1+bs->nmas, sys->prime);

	//copy the homogeneous terms
	for(i = 0; i<bi->g1; i++)
		for(j = 0; j<bi->g1; j++)
			nmod_mat_entry(bs->mat, i, j) = *table_pointer_llu(bs->block, i, j);

	//substitute
	//column first, to minimize the number of evaluations
	for(j = bi->g1; j<bi->nint; j++)
	{
		int globalid = *(bi->intid->entry+j);
		struct integral_sol * is = *(ss->islist+globalid);

		struct list_int posilist0, * posilist = &posilist0;
		list_init_int(posilist, is->nmas);
		for(i = 0; i<is->nmas; i++)
			*(posilist->entry+i) = list_first_position_int(bs->index, *(is->index->entry+i));
	
		for(i = 0; i<bi->g1; i++)
		{
			unsigned long long coe = *table_pointer_llu(bs->block, i, j);
			if(coe!=0)
			{
				for(k = 0; k<is->nmas; k++)
				{
					unsigned long long proj = *(is->sol->entry+k);
					unsigned long long prod = n_mulmod2_preinv(coe, proj, sys->prime, sys->invp);
					int posi = *(posilist->entry+k);
					nmod_mat_entry(bs->mat, i, posi+bi->g1) = n_addmod(nmod_mat_entry(bs->mat, i, posi+bi->g1), prod, sys->prime);
				}
			}
		}
		list_clear_int(posilist);
	}
}


//generate integral_sol from block_sol
void block_sol_trans(struct system_sol * ss, struct system * sys, int n, struct block_sol * bs)
{
	struct block_info * bi = *(sys->bilist+n);
	int i, j, k;

	for(i = 0; i<bi->g1; i++)
	{
		int globalid = *(bi->intid->entry+i);
		struct integral_sol * is = *(ss->islist+globalid);

		struct list_llu list0, * list = &list0;
		list_init_llu(list, bs->nmas);
		for(j = 0; j<bs->nmas; j++)
		{
			unsigned long long entry = nmod_mat_entry(bs->mat, i, j+bi->g1);
			if(entry!=0)
				*(list->entry+j) = sys->prime-entry;
		}

		is->nmas = list_agcount_llu(list, 0);
		integral_sol_init(is, is->nmas);

		int counter = 0;
		for(j = 0; j<bs->nmas; j++)
		{
			if(*(list->entry+j)!=0)
			{
				*(is->sol->entry+counter) = *(list->entry+j);
				*(is->index->entry+counter) = *(bs->index->entry+j);
				counter++;
			}
		}

		list_clear_llu(list);
	}
}

void block_sol_clear(struct block_sol * bs)
{
	bs->nmas = 0;
	list_clear_int(bs->index);
	table_clear_llu(bs->block);
	nmod_mat_clear(bs->mat);
}

//solve the n-th block
void block_solve_learn(struct system_sol * ss, struct system * sys, int n, struct block_sol ** bslist)
{
	struct block_sol * bs = (struct block_sol *)malloc(sizeof(struct block_sol));
	block_sol_index_init(ss, sys, n, bs);
	block_sol_assign(ss, sys, n, bs);
	block_sol_substitute(ss, sys, n, bs);
	nmod_mat_rref(bs->mat);
	block_sol_trans(ss, sys, n, bs);
	*(bslist + n) = bs;
}

void system_solve_learn(struct system_sol * ss, struct system * sys, struct block_sol ** bslist)
{
	int i;
	for(i = 0; i<sys->nbi; i++)
		block_solve_learn(ss, sys, *(sys->order->entry+i), bslist);
}

//solve the n-th block
//after learning phase
void block_solve(struct system_sol * ss, struct system * sys, int n, struct block_sol ** bslist)
{
	struct block_sol bs0, * bs = &bs0;
	//block_sol_index_init(ss, sys, n, bs);
	bs->nmas = (*(bslist+n)) -> nmas;
	bs->index = &bs->index0;
	list_init_int(bs->index, bs->nmas);
	list_copy_int(bs->index, (*(bslist+n))->index);

	block_sol_assign(ss, sys, n, bs);
	block_sol_substitute(ss, sys, n, bs);
	nmod_mat_rref(bs->mat);
	block_sol_trans(ss, sys, n, bs);
	block_sol_clear(bs);
}

void system_solve(struct system_sol * ss, struct system * sys, struct block_sol ** bslist)
{
	int i;
	for(i = 0; i<sys->nbi; i++)
		block_solve(ss, sys, *(sys->order->entry+i), bslist);
}

int system_sol_entry_count(struct system_sol * ss)
{
	int i, counter = 0;
	for(i = 0; i<ss->nint; i++)
	{
		struct integral_sol * is = *(ss->islist+i);
		counter = counter+is->nmas;
	}

	return counter;
}

void sparse_system_sol(struct system_sol * ss, struct list_llu * list)
{
	int i, j, counter = 0;
	for(i = 0; i<ss->nint; i++)
	{
		struct integral_sol * is = *(ss->islist+i);
		for(j = 0; j<is->nmas; j++)
		{
			*(list->entry+counter) = *(is->sol->entry+j);
			counter++;
		}
	}
}

void sparse_system_sol_info(struct system_sol * ss, struct list_int * index, struct list_int * part)
{
	int i, j, counter = 0;
	for(i = 0; i<ss->nint; i++)
	{
		struct integral_sol * is = *(ss->islist+i);
		for(j = 0; j<is->nmas; j++)
		{
			*(index->entry+counter) = *(is->index->entry+j);
			counter++;
		}
		*(part->entry+i+1) = counter;
	}
}