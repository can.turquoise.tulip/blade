#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <flint/nmod_mat.h>
#include "list.h"
#include "file.h"
#include "sparse.h"
#include "database.h"
#include "kinematics.h"
#include "template.h"
#include "searchalg.h"

//input: working_directory database_name work_name workid_ini workid_fin nthreads
int main(int argc, char* argv[])
{
	printf("redg1: working directory -> %s, database -> %s, work -> %s, from %s to %s.\n", argv[1], argv[2], argv[3], argv[4], argv[5]);
	
	char * dir = (char*)malloc(200);
	time_t start, end;
	struct database db0, * db = &db0;
	struct kinematics kn0, * kn = &kn0;

	//initialize database
	sprintf(dir, "%s/database/%s", argv[1], argv[2]);
	start = time(NULL);
	puts("----------");
	puts("redg1: loading database.");
	database_init(db, dir);
	end = time(NULL);
	printf("redg1: database loaded in %fs.\n", difftime(end, start));
	printf("redg1: prime -> %llu, parameter -> %d, integral -> %d, master -> %d, size -> %d * %d.\n", db->prime, db->npara, db->nint, db->nmaster, db->nps, db->nentry);

	//initialize kinematics
	sprintf(dir, "%s/kinematics/%s", argv[1], argv[3]);
	start = time(NULL);
	puts("----------");
	puts("redg1: loading kinematics.");
	kinematics_init(kn, db, dir);
	end = time(NULL);
	printf("redg1: kinematics loaded in %fs.\n", difftime(end, start));
	printf("redg1: size -> %d * %d.\n", kn->nmono, kn->npara);

	//works
	start = time(NULL);
	puts("----------");
	printf("redg1: reducing works using %s threads.\n", argv[6]);
	
	int i;
	int work_ini = atoi(argv[4]);
	int work_fin = atoi(argv[5]);

	omp_set_num_threads(atoi(argv[6]));
	#pragma omp parallel for schedule(dynamic)
	for(i = work_ini; i<work_fin; i++)
	{
		char * string = (char*)malloc(200);
		sprintf(string, "%s/%s/%d", argv[1], argv[3], i);
		reduce_g1(db, kn, string);
		free(string);
	}

	end = time(NULL);
	printf("redg1: works complete in %fs.\n", difftime(end, start));

	free(dir);
	database_clear(db);
	kinematics_clear(kn);

    return 0;
}