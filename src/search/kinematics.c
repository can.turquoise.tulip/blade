#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <flint/nmod_mat.h>
#include "file.h"
#include "list.h"
#include "database.h"
#include "kinematics.h"

//common monomials list used in searching procedure
//kin_common: number of monomials, number of parameters
void kinematics_config_init(struct kinematics * kn, char * dir)
{
	char * string = (char *)malloc(200);
	FILE * fp;
	file_name_join(string, dir, "kin_common");
	fp = fopen(string, "r");
	fscanf(fp, "%d %d", &kn->nmono, &kn->npara);
	fclose(fp);

	file_name_join(string, dir, "kin_table");
	kn->cfg = &kn->cfg0;
	table_init_int(kn->cfg, kn->nmono, kn->npara);
	table_read_int(string, kn->cfg);

	free(string);
}

//evaluate a single monomial
void kinematics_num_init(struct kinematics * kn, struct database * db)
{
	kn->num = &kn->num0;
	table_init_llu(kn->num, db->nps, kn->nmono);

	struct list_llu pscfg0, * pscfg = &pscfg0;
	struct list_int kincfg0, * kincfg = &kincfg0;

	int i, j, k;
	unsigned long long prod;
	for(i = 0; i<db->nps; i++)
	{
		table_pick_llu(pscfg, db->ps, i);
		for(j = 0; j<kn->nmono; j++)
		{
			table_pick_int(kincfg, kn->cfg, j);
			prod = 1;
			for(k = 0; k<db->npara; k++)
				prod = n_mulmod2_preinv(prod, n_powmod2_preinv(*(pscfg->entry+k), *(kincfg->entry+k), db->prime, db->invp), db->prime, db->invp);
			*table_pointer_llu(kn->num, i, j) = prod;
		}
	}
}

void kinematics_init(struct kinematics * kn, struct database * db, char * dir)
{
	kinematics_config_init(kn, dir);
	kinematics_num_init(kn, db);
}

void kinematics_clear(struct kinematics * kn)
{
	kn->nmono = 0;
	kn->npara = 0;
	table_clear_int(kn->cfg);
	table_clear_llu(kn->num);
}