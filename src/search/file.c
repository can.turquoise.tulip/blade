#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "file.h"

//string = dir/name
void file_name_join(char * string, char * dir, char * name)
{
	strcpy(string, dir);
	strcat(string, "/");
	strcat(string, name);
}

//whether the file exists or not
int file_existsQ(char * path)
{
	FILE * fp = fopen(path, "r");
	if(fp==NULL)
		return 0;
	else
	{
		fclose(fp);
		return 1;
	}
}

//read an integer from file
void file_read_int(char * path, int * a)
{
	FILE * fp = fopen(path, "r");
	fscanf(fp, "%d", a);
	fclose(fp);
}

//write an integer to file
void file_write_int(char * path, int * a)
{
	FILE * fp = fopen(path, "w");
	fprintf(fp, "%d", *a);
	fclose(fp);
}