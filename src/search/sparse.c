#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "list.h"
#include "sparse.h"

//sparse matrix operation
void sparse_init(struct sparse * sp)
{
	sp->nrow = 0;
	sp->ncol = 0;
	sp->nentry = 0;
	sp->part = &sp->part0;
	sp->entry = &sp->entry0;
	sp->posi = &sp->posi0;
	list_init_int(sp->part, 1);
	list_init_llu(sp->entry, 0);
	list_init_int(sp->posi, 0);
}

//tab: a dense matrix
void sparsify(struct sparse * sp, struct table_llu * tab)
{
	sp->nrow = tab->nrow;
	sp->ncol = tab->ncol;
	sp->nentry = 0;

	sp->part = &sp->part0;
	list_init_int(sp->part, sp->nrow+1);

	int i, j;
	struct list_llu list0, * list = &list0;
	for(i = 0; i<tab->nrow; i++)
	{
		table_pick_llu(list, tab, i);
		sp->nentry = sp->nentry+list_agcount_llu(list, 0);
		*(sp->part->entry+i+1) = sp->nentry;
	}

	sp->entry = &sp->entry0;
	list_init_llu(sp->entry, sp->nentry);
	sp->posi = &sp->posi0;
	list_init_int(sp->posi, sp->nentry);
	
	int k = 0;
	unsigned long long element;
	for(i = 0; i<tab->nrow; i++)
	{
		for(j = 0; j<tab->ncol; j++)
		{
			element = *table_pointer_llu(tab, i, j);
			if(element!=0)
			{
				*(sp->entry->entry+k) = element;
				*(sp->posi->entry+k) = j;
				k++;
			}
		}
	}
}

void sparse_clear(struct sparse * sp)
{
	sp->nrow = 0;
	sp->ncol = 0;
	sp->nentry = 0;
	list_clear_int(sp->part);
	list_clear_llu(sp->entry);
	list_clear_int(sp->posi);
}

//replace the column id in sparse with index
void sparse_index_replace(struct sparse * sp, struct list_int * index)
{
	int i, tmp;
	for(i = 0; i<sp->nentry; i++)
	{
		tmp = *(sp->posi->entry+i);
		*(sp->posi->entry+i) = *(index->entry+tmp);
	}
}

//append the content of sp0 to sp
void sparse_append(struct sparse * sp, struct sparse * sp0)
{
	int oldnentry = sp->nentry;
	int oldnrow = sp->nrow;
	
	sp->nrow = sp->nrow + sp0->nrow;
	sp->ncol = 0;//this term is not important, we will assign a correct value to it later
	sp->nentry = sp->nentry+sp0->nentry;

	list_realloc_int(sp->part, sp->nrow+1);
	list_realloc_llu(sp->entry, sp->nentry);
	list_realloc_int(sp->posi, sp->nentry);

	int i;
	for(i = 0; i<sp0->nentry; i++)
	{
		*(sp->entry->entry+oldnentry+i) = *(sp0->entry->entry+i);
		*(sp->posi->entry+oldnentry+i) = *(sp0->posi->entry+i);
	}

	for(i = 0; i<sp0->nrow; i++)
		*(sp->part->entry+oldnrow+1+i) = *(sp0->part->entry+i+1)+oldnentry;
}