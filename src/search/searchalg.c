#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <flint/nmod_mat.h>
#include "list.h"
#include "file.h"
#include "sparse.h"
#include "database.h"
#include "kinematics.h"
#include "template.h"
#include "searchalg.h"

//first: the leading variable in list0, which has been normalized to 1
//subtract list by list0: list - list0 * list[[first]]
void subtract_list(struct list_llu * list, struct list_llu * list0, int first, unsigned long long prime, unsigned long long invp)
{
	unsigned long long element = *(list->entry+first);
	if(element!=0)
	{
		int i;
		for(i = first; i<list->size; i++)
		{
			unsigned long long sub, list0ele = *(list0->entry+i);
			if(list0ele!=0)
			{
				sub = n_mulmod2_preinv(list0ele, element, prime, invp);
				*(list->entry+i) = n_submod(*(list->entry+i), sub, prime);
			}
		}
	}
}

//normalize a list
void normalize_list(struct list_llu * list, int first, unsigned long long prime, unsigned long long invp)
{
	unsigned long long inverse = n_invmod(*(list->entry+first), prime);

	int i;
	for(i = first; i<list->size; i++)
	{
		unsigned long long element = *(list->entry+i);
		if(element!=0)
			*(list->entry+i) = n_mulmod2_preinv(element, inverse, prime, invp);
	}
}

//inner product
unsigned long long inner(struct list_llu * list1, struct list_llu * list2, unsigned long long prime, unsigned long long invp)
{
	int i;
	unsigned long long sum = 0, prod;
	for(i = 0; i<list1->size; i++)
	{
		if(*(list1->entry+i)!=0 && *(list2->entry+i)!=0)
		{
			prod = n_mulmod2_preinv(*(list1->entry+i), *(list2->entry+i), prime, invp);
			sum = n_addmod(sum, prod, prime);
		}		
	}
	return sum;
}

//generate a constraint form m-th ps and n-th MI, m: [0,db->nps), n: [0,db->nmaster)
//list has been initialized to be 0,...,0 of length tp->nvar
void generate_constraint(struct database * db, struct kinematics * kn, struct template * tp, int m, int n, struct list_llu * list)
{
	int i;
	unsigned long long proj, mono;
	for(i = 0; i<tp->nvar; i++)
	{
		int localid = *table_pointer_int(tp->varlist, i, 0);
		int monoid = *table_pointer_int(tp->varlist, i, 1);
		int posi = *table_pointer_int(tp->posi, n, localid);
		if(posi!=-1)
		{
			proj = *table_pointer_llu(db->data, m, *(db->part->entry+n)+posi);
			mono = *table_pointer_llu(kn->num, m, monoid);
			*(list->entry+i) = n_mulmod2_preinv(proj, mono, db->prime, db->invp);
		}
	}

    list_reverse_llu(list);
}

//simplify a relation from a known sparse matrix; list is of original length (dense)
void simplify_rel_sparse(struct sparse * sp, unsigned long long prime, unsigned long long invp, struct list_llu * list)
{
	int i, j;
	for(i = 0; i<sp->nrow; i++)
   	{	
        int start = *(sp->part->entry+i), end = *(sp->part->entry+i+1);
   		int first = *(sp->posi->entry+start);
        unsigned long long element = *(list->entry+first);

    	if(element!=0)
    	{
    		//subtract
    		unsigned long long sub;
    		for(j = start; j<end; j++)
    		{
    			int posi = *(sp->posi->entry+j);
    			sub = n_mulmod2_preinv(*(sp->entry->entry+j), element, prime, invp);
    			*(list->entry+posi) = n_submod(*(list->entry+posi), sub, prime);//mod p
    		}
    	}
    }
}

//sp: obtained sparse matrix previous
//n: the index of master integrals [0,db->nmaster)
//reduce the constraints from n-th MI and append to result to sp
void solve_master(struct database * db, struct kinematics * kn, struct template * tp, int n, struct sparse * sp)
{
	struct list_llu list0, * list = &list0;
	struct list_llu sublist0, * sublist = &sublist0;
	struct list_int posi0, * posi = &posi0;
	struct list_int first0, * first = &first0;
	struct table_llu mat0, * mat = &mat0;

	struct list_llu tmplist0, * tmplist = &tmplist0;
	struct list_llu tmplist02, * tmplist2 = &tmplist02;

	list_init_llu(list, tp->nvar);
	list_init_int(first, db->nps-1);

	int solvedQ = 0;
	int initQ = 0;
	int i, j, psid = 0;
	//note that we preserve the final point for self-consistence check
	while(solvedQ==0 && psid<db->nps-1)
	{	
		//generate a constraint
		memset(list->entry, 0, list->size*sizeof(unsigned long long));
		generate_constraint(db, kn, tp, psid, n, list);

		//simplify it by previous relations
		simplify_rel_sparse(sp, db->prime, db->invp, list);

		//find nonzero position and pick up sublist
		if(initQ==0)
    	{
    		int size = list_agcount_llu(list, 0);
    		list_init_llu(sublist, size);
    		list_init_int(posi, size);
    		list_agposition_llu(list, posi, 0);
    		table_init_llu(mat, db->nps-1, size);
   			initQ = 1;
    	}

    	//pick nonzero elements only
    	list_picksub_llu(list, sublist, posi);

    	//forward elimination in small matrix
    	for(i = 0; i<psid; i++)
    	{
    		table_pick_llu(tmplist, mat, i);
    		subtract_list(sublist, tmplist, *(first->entry+i), db->prime, db->invp);
    	}

		//judge
    	if(list_count_llu(sublist, 0)<sublist->size)
    	{
    		//add the new relation into matrix
    		*(first->entry+psid) = list_first_agposition_llu(sublist, 0);
    		normalize_list(sublist, *(first->entry+psid), db->prime, db->invp);
    		table_pick_llu(tmplist, mat, psid);
    		list_copy_llu(tmplist, sublist);
    		psid++;
    	}
    	else
    		solvedQ = 1;
	}

	//if(solvedQ==0)
	//	printf("from thread %d: solve_master: database is insufficient for master: %d\n", omp_get_thread_num(), n);

	//backward substitution
	for(i = psid-1; i>=0; i--)
	{
		table_pick_llu(tmplist, mat, i);
		for(j = psid-1; j>i; j--)
		{
			table_pick_llu(tmplist2, mat, j);
			subtract_list(tmplist, tmplist2, *(first->entry+j), db->prime, db->invp);
		}
	}

	//to sparse
	if(initQ==1)
	{
		struct sparse sp00, * sp0 = &sp00;
		mat->nrow = psid;
		sparsify(sp0, mat);
		sparse_index_replace(sp0, posi);
		sparse_append(sp, sp0);
		sparse_clear(sp0);
		
		list_clear_llu(sublist);
		list_clear_int(posi);
		table_clear_llu(mat);
	}

	list_clear_llu(list);
	list_clear_int(first);
}

//null: has been initialized correctly
//find the nullspace of a sparse matrix
void nullspace(struct sparse * sp, unsigned long long prime, unsigned long long invp, struct table_llu * null)
{
	// what relation to reduce i-th variable
	struct list_int red0, * red = &red0;
	list_init_int(red, sp->ncol);
	memset(red->entry, -1, red->size*sizeof(int));
	
	int i;
	for(i = 0; i<sp->nrow; i++)
		*(red->entry+*(sp->posi->entry+*(sp->part->entry+i))) = i;
	int nsol = list_count_int(red, -1);

	if(nsol!=null->nrow)
		printf("from thread %d: nullspace: cannot determine the dimensions of nullspace.\n", omp_get_thread_num());

	int j = 0, k, l;
	for(i = sp->ncol-1; i>=0; i--)
	{
		int relid = *(red->entry+i);
		if(relid == -1)
		{
			*table_pointer_llu(null, j, i) = 1;
			j++;
		}
		else
		{
			int start = *(sp->part->entry+relid)+1;
			int end = *(sp->part->entry+relid+1);
			for(k = start; k<end; k++)
			{
				unsigned long long coe = *(sp->entry->entry+k);
				int id = *(sp->posi->entry+k);
				for(l = 0; l<nsol; l++)
				{
					unsigned long long proj = *table_pointer_llu(null, l, id);
					if(proj!=0)
					{
						unsigned long long sub = n_mulmod2_preinv(coe, proj, prime, invp);
						*table_pointer_llu(null, l, i) = n_submod(*table_pointer_llu(null, l, i), sub, prime);
					}
				}
			}
		}
	}

	struct list_llu tmp0, * tmp = &tmp0;
	for(i = 0; i<nsol; i++)
	{
		table_pick_llu(tmp, null, i);
		list_reverse_llu(tmp);
	}

	list_clear_int(red);
}

//self check with the last phase points at integrals relation level after calling template_relation_create
int self_check(struct database * db, struct template * tp)
{
	struct list_llu list0, * list = &list0;
	list_init_llu(list, tp->nint);

	int i, j;
	struct list_llu tmp0, * tmp = &tmp0;
	for(j = 0; j<db->nmaster; j++)
	{
		memset(list->entry, 0, list->size*sizeof(unsigned long long));
		for(i = 0; i<tp->nint; i++)
		{
			int posi = *table_pointer_int(tp->posi, j, i);
			if(posi!=-1)
				*(list->entry+i) = *table_pointer_llu(db->data, db->nps-1, *(db->part->entry+j)+posi);
		}

		for(i = 0; i<tp->nsol; i++)
		{
			table_pick_llu(tmp, tp->rel, i);	
			unsigned long long test = inner(list, tmp, db->prime, db->invp);
			if(test!=0)
			{
				list_clear_llu(list);
				return 0;
			}
		}
	}

	list_clear_llu(list);
	return 1;
}

//find the final solutions
int search_relation(struct database * db, struct kinematics * kn, struct template * tp)
{
	time_t t1, t2;
	t1 = time(NULL);

	struct sparse sp0, * sp = &sp0;
	sparse_init(sp);
	int i;
	for(i = 0; i<db->nmaster; i++)
		solve_master(db, kn, tp, i, sp);	

	//printf("sp_size: %d %d %d\n", sp->nrow, sp->ncol, sp->nentry);

	//determine the nullspace from reduced sparse matrix sp
	sp->ncol = tp->nvar;
	tp->nsol = sp->ncol-sp->nrow;
	tp->sol = &tp->sol0;
	table_init_llu(tp->sol, tp->nsol, tp->nvar);
	nullspace(sp, db->prime, db->invp, tp->sol);
	sparse_clear(sp);

	t2 = time(NULL);
	printf("from thread %d: search_relation: nvar -> %d, nsol -> %d, time -> %fs.\n", omp_get_thread_num(), tp->nvar, tp->nsol, difftime(t2, t1));

	template_relation_create(tp, db, kn);
	int check = self_check(db, tp);
	if(check==0)
		printf("from thread %d: search_relation: check failed, please try to generate more database.\n", omp_get_thread_num());

	return check;
}

int matrix_rank(struct table_llu * mat, unsigned long long prime)
{
	nmod_mat_t mat2;
	nmod_mat_init(mat2, mat->nrow, mat->ncol, prime);
	int i, j;
	for(i = 0; i<mat->nrow; i++)
		for(j = 0; j<mat->ncol; j++)
			nmod_mat_entry(mat2, i, j) = *table_pointer_llu(mat, i, j);

	int rank = nmod_mat_rank(mat2);
	nmod_mat_clear(mat2);

	return rank;
}

//determine independent relations for g1
//return whether the matrix has been filled
int determine_indep_relations(struct template * tp, struct table_llu * matrix, struct list_int * indep, int * rank, int * rowid, unsigned long long prime)
{
	struct list_llu list01, * list1 = &list01;
	struct list_llu list02, * list2 = &list02;

	int i;
	for(i = 0; i<tp->nsol; i++)
	{
		table_pick_llu(list1, tp->rel, i);
		int first = list_first_agposition_llu(list1, 0);
		if(first<matrix->ncol)
		{
			table_pick_llu(list2, matrix, *rowid);
			list_copy_llu(list2, list1);
			int rank0 = matrix_rank(matrix, prime);

			if(rank0>*rank)
			{
				*rank = rank0;
				*rowid = *rowid+1;
				*(indep->entry+i) = 1;
				if(*rank==matrix->nrow)
					return 1;
			}
		}
	}

	return 0;
}

//determine needed variables for template by scanning the independent nullspace
void determine_needed_variables(struct template * tp, struct list_int * indep, struct list_int * needQ)
{
	int i, j;
	for(j = 0; j<tp->nvar; j++)
		for(i = 0; i<tp->nsol; i++)
			if(*(indep->entry+i)==1)
			{
				//only independent relations are concerned
				unsigned long long element = *table_pointer_llu(tp->sol, i, j);
				if(element!=0)
				{
					*(needQ->entry+j) = 1;
					break;
				}
			}
}

//write tmp_indep, tmp_nvar and tmp_var
void write_template_g1(struct template * tp, struct list_int * indep, char * dir)
{
	struct list_int needQ0, * needQ = &needQ0;
	list_init_int(needQ, tp->nvar);
	determine_needed_variables(tp, indep, needQ);

	int nvar = list_count_int(needQ, 1);
	int nsol = list_count_int(indep, 1);

	struct list_int posi0, * posi = &posi0;
	list_init_int(posi, nvar);
	list_position_int(needQ, posi, 1);

	struct table_int var0, * var = &var0;
	table_init_int(var, nvar, 2);
	table_picksub_int(tp->varlist, var, posi);

	char * string = (char*)malloc(200);

	file_name_join(string, dir, "tmp_indep");
	list_write_int(string, indep);

	file_name_join(string, dir, "tmp_var");
	table_write_int(string, var);

	file_name_join(string, dir, "tmp_nvar");
	file_write_int(string, &nvar);

	file_name_join(string, dir, "tmp_nsol");
	file_write_int(string, &nsol);

	list_clear_int(needQ);
	list_clear_int(posi);
	table_clear_int(var);
	free(string);
}

//judge whether key_* exists
int key_existsQ(char * dir, int configid, char * key)
{
	char * string = (char*)malloc(200);
	sprintf(string, "%s/config/%d/%s_nvar", dir, configid, key);

	int a = 0;
	if(file_existsQ(string))
		a = 1;

	free(string);
	return a;
}

//reduce g1 to g2
//dir: where you store sch_g1, sch_nint, sch_intid and config/
//it provides the basic usage of functions defined previously, pedagogically useful
void reduce_g1(struct database * db, struct kinematics * kn, char * dir)
{
	time_t start, end;
	start = time(NULL);

	int g1;
	char * string = (char*)malloc(200);
	file_name_join(string, dir, "sch_g1");
	if(!file_existsQ(string))
	{
		free(string);
		return;
	}

	file_read_int(string, &g1);

	struct template tp0, * tp = &tp0;
	//by default, we can absorb maximally 500 monomials for each integral
	template_init(tp, db, dir, 500);

	int reducedQ = 0;
	int configid = 0;
	struct table_llu matrix0, * matrix = &matrix0;
	table_init_llu(matrix, g1, g1);
	struct list_int indep0, * indep = &indep0;
	int rank = 0;
	int rowid = 0;

	printf("from thread %d: reduce_g1: work in the folder %s, to reduce %d integrals.\n", omp_get_thread_num(), dir, g1);
	
	while(reducedQ==0 && key_existsQ(dir, configid, "in"))
	{
		printf("from thread %d: reduce_g1: config %d\n", omp_get_thread_num(), configid);
		sprintf(string, "%s/config/%d", dir, configid);

		if(key_existsQ(dir, configid, "out"))
		{
			//read solution already existed
			printf("from thread %d: reduce_g1: read solutions from out_*.\n", omp_get_thread_num());
			template_solution_read(tp, string);
			table_clear_llu(tp->rel);
			template_relation_create(tp,db,kn);
		}
		else
		{
			//initialize variables info from in_nvar and in_var
			template_config_init(tp, kn, string, "in");
		
			//search relations
			int check = search_relation(db, kn, tp);
			if(check==0)
			{
				template_config_clear(tp);
				break;
			}

			//write template info
			template_solution_write(tp, string);
		}
			
		//append absorbed table
		template_absorb_append(tp);
		
		//judge the independence and write template info
		list_init_int(indep, tp->nsol);
		reducedQ = determine_indep_relations(tp, matrix, indep, &rank, &rowid, db->prime);
		printf("from thread %d: reduce_g1: %d independent relations searched.\n", omp_get_thread_num(), list_count_int(indep, 1));
		write_template_g1(tp, indep, string);
		list_clear_int(indep);

		//clear configs
		template_config_clear(tp);

		configid++;
	}

	file_name_join(string, dir, "flag");
	file_write_int(string, &reducedQ);

	table_clear_llu(matrix);
	template_clear(tp);
	free(string);

	end = time(NULL);

	if(reducedQ==1)
		printf("from thread %d: reduce_g1: g1 reduced in %fs.\n", omp_get_thread_num(), difftime(end, start));
	else
		printf("from thread %d: reduce_g1: g1 not reduced in %fs.\n", omp_get_thread_num(), difftime(end, start));
}

//search relation with inputs read from dir/key_nvar and dir/key_var and write the nullspace to path if check is successful
//key: "in", "out", "tmp" or any other user defined strings
int search_relation_from_source(struct database * db, struct kinematics * kn, struct template * tp, char * dir, char * key, char * path)
{
	template_config_init(tp, kn, dir, key);
	int check = search_relation(db, kn, tp);
	if(check!=0)
		table_write_llu(path, tp->sol);
	
	template_config_clear(tp);
	return check;
}

//dir: /directory/to/work which is the same as reduce_g1
//write to: writedir/0, writedir/1, ...
void fit_relations(struct database * db, struct kinematics * kn, char * dir, char * writedir)
{
	time_t start, end;
	start = time(NULL);

	char * string = (char*)malloc(200);
	file_name_join(string, dir, "sch_g1");
	if(!file_existsQ(string))
	{
		free(string);
		return;
	}

	sprintf(string, "mkdir -p %s", writedir);
	system(string);

	char * path = (char*)malloc(200);
	struct template tp0, * tp = &tp0;
	template_init(tp, db, dir, 0);

	int configid = 0;
	int check = 1;

	printf("from thread %d: fit_relations: read from %s, and write to %s.\n", omp_get_thread_num(), dir, writedir);

	while(key_existsQ(dir, configid, "tmp"))
	{
		printf("from thread %d: fit_relations: config %d\n", omp_get_thread_num(), configid);
		sprintf(string, "%s/config/%d", dir, configid);
		sprintf(path, "%s/%d", writedir, configid);
		
		check = search_relation_from_source(db, kn, tp, string, "tmp", path);
		if(!check)
			break;

		configid++;
	}

	file_name_join(string, writedir, "flag");
	file_write_int(string, &check);

	template_clear(tp);
	free(string);
	free(path);

	end = time(NULL);
	
	if(check==1)
		printf("from thread %d: fit_relations: relations fit in %fs.\n", omp_get_thread_num(), difftime(end, start));
	else
		printf("from thread %d: fit_relations: relations not fit in %fs.\n", omp_get_thread_num(), difftime(end, start));
}