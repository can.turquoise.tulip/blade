#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <flint/nmod_mat.h>
#include "file.h"
#include "list.h"
#include "database.h"
#include "kinematics.h"
#include "template.h"

//TEMPLATE OPERATIONS
//read integrals form external files
void template_integrals_init(struct template * tp, char * dir)
{
	char * string = (char*)malloc(200);
	file_name_join(string, dir, "sch_nint");
	file_read_int(string, &tp->nint);

	file_name_join(string, dir, "sch_intid");
	tp->intid = &tp->intid0;
	list_init_int(tp->intid, tp->nint);
	list_read_int(string, tp->intid);
}

//find integral position for a template, should be performed once G are determined
void template_position_init(struct template * tp, struct database * db)
{
	tp->posi = &tp->posi0;
	table_init_int(tp->posi, db->nmaster, tp->nint);

	int i, j;
	struct list_int list0, * list = &list0;
	for(i = 0; i<db->nmaster; i++)
	{
		int start = *(db->part->entry+i);
		int end = *(db->part->entry+i+1);
		list->size = end-start;	
		list->entry = db->posi->entry+start;
		
		for(j = 0; j<tp->nint; j++)
		{
			int key = *(tp->intid->entry+j);
			*table_pointer_int(tp->posi, i, j) = list_first_position_int(list, key);
		}
	}
}

//should be performed before any searching procedure
//localid is valid
void template_absorb_init(struct template * tp, int size)
{
	tp->abs = &tp->abs0;
	table_init_int(tp->abs, tp->nint, size);
	memset(tp->abs->entry, -1, tp->nint*size*sizeof(int));
}

//initialize the template
void template_init(struct template * tp, struct database * db, char * dir, int size)
{
	template_integrals_init(tp, dir);
	template_position_init(tp, db);
	template_absorb_init(tp, size);
}

// return 1 if each entry in a is larger(or equal to) than corresponding one in b
int absorb_larger(struct list_int * a, struct list_int * b)
{
	int i;
	for(i = 0; i<a->size; i++)
		if(*(a->entry+i)<*(b->entry+i))
			return 0;
	return 1;
}

//test variable {localid, monoid}
//retrun 1 if the variable should be absorbed
int absorb_test(struct template * tp, struct kinematics * kn, int localid, int monoid)
{
	struct list_int mono0, tmp0, * mono = &mono0, * tmp = &tmp0;
	table_pick_int(mono, kn->cfg, monoid);

	int i, testid;
	for(i = 0; i<tp->abs->ncol; i++)
	{
		testid = *table_pointer_int(tp->abs, localid, i);
		if(testid==-1)
			return 0;
		
		table_pick_int(tmp, kn->cfg, testid);
		if(absorb_larger(mono, tmp)==1)
			return 1;
	}

	//in case that the loop is not interrupted
	return 0;
}

//initialize variables used in searching procedure
//before each searching
//key: in, out or tmp
void template_config_init(struct template * tp, struct kinematics * kn, char * dir, char * key)
{
	int nvar_tmp;
	struct table_int varlist_tmp0, * varlist_tmp = &varlist_tmp0;

	char * string = (char*)malloc(200);
	sprintf(string, "%s/%s_nvar", dir, key);
	file_read_int(string, &nvar_tmp);

	//file_name_join(string, dir, "in_var");
	sprintf(string, "%s/%s_var", dir, key);
	table_init_int(varlist_tmp, nvar_tmp, 2);
	table_read_int(string, varlist_tmp);

	//perform absorb
	//need_absorb: 0 and 1, 0: unabsorb, 1: absorb
	struct list_int need_absorb0, * need_absorb = &need_absorb0;
	list_init_int(need_absorb, nvar_tmp);

	int i, testintid, testmonoid;
	for(i = 0; i<nvar_tmp; i++)
	{
		testintid = *table_pointer_int(varlist_tmp,i,0);
		testmonoid = *table_pointer_int(varlist_tmp,i,1);
		*(need_absorb->entry+i) = absorb_test(tp, kn, testintid, testmonoid);
	}

	int remain = list_count_int(need_absorb, 0);
	struct list_int posi0, * posi = &posi0;
	list_init_int(posi, remain);
	list_position_int(need_absorb, posi, 0);
	
	tp->nvar = remain;
	tp->varlist = &tp->varlist0;
	table_init_int(tp->varlist, tp->nvar, 2);
	table_picksub_int(varlist_tmp, tp->varlist, posi);

	table_clear_int(varlist_tmp);
	list_clear_int(need_absorb);
	list_clear_int(posi);
}

//append absorbed variables after a searching
void template_absorb_append(struct template * tp)
{
	struct list_int solvedQ0, * solvedQ = &solvedQ0;
	list_init_int(solvedQ, tp->nvar);

	//var_position <= abs_cut correspond to Leading Term 
	int i;
	int abs_cut=tp->nvar-2;
	for(i=0;i<tp->nvar-1;i++)
	{
		int intid = *table_pointer_int(tp->varlist,i,0);
		int next_intid= *table_pointer_int(tp->varlist,i+1,0);

		if(intid>next_intid)
		{
			abs_cut=i;
			break;
		}
	}

	int first;
	struct list_int list0, * list = &list0;
	struct list_llu tmp0, * tmp = &tmp0;
	for(i = 0; i<tp->nsol; i++)
	{
		table_pick_llu(tmp, tp->sol, i);
		first = list_first_position_llu(tmp, 1);
		if(first<=abs_cut)
		{
			*(solvedQ->entry+first) = 1;
		}
	}

	for(i = 0; i<tp->nvar; i++)
	{
		if(*(solvedQ->entry+i)==1)
		{
			int intid = *table_pointer_int(tp->varlist, i, 0);
			int monoid = *table_pointer_int(tp->varlist, i, 1);

			table_pick_int(list, tp->abs, intid);
			first = list_first_position_int(list, -1);
			*(list->entry+first) = monoid;
		}
	}
}

//generate relation among integrals
void template_relation_create(struct template * tp, struct database * db, struct kinematics * kn)
{
	tp->rel = &tp->rel0;
	table_init_llu(tp->rel, tp->nsol, tp->nint);

	struct list_llu kin0, * kin = &kin0;
	list_init_llu(kin, tp->nvar);

	int i, j;
	for(i = 0; i<tp->nvar; i++)
	{
		int monoid = *table_pointer_int(tp->varlist, i, 1);
		*(kin->entry+i) = *table_pointer_llu(kn->num, db->nps-1, monoid);
	}

	struct list_llu sol0, * sol = &sol0;
	for(i = 0; i<tp->nsol; i++)
	{
		table_pick_llu(sol, tp->sol, i);
		for(j = 0; j<tp->nvar; j++)
		{
			int intid = *table_pointer_int(tp->varlist, j, 0);
			unsigned long long prod = n_mulmod2_preinv(*(kin->entry+j), *(sol->entry+j), db->prime, db->invp);
			*table_pointer_llu(tp->rel, i, intid) = n_addmod(*table_pointer_llu(tp->rel, i, intid), prod, db->prime);
		}
	}

	list_clear_llu(kin);
}

//write information to file
//dir: recommended to be the same as input dir
void template_solution_write(struct template * tp, char * dir)
{
	char * string = (char*)malloc(200);

	file_name_join(string, dir, "out_nsol");
	file_write_int(string, &tp->nsol);

	file_name_join(string, dir, "out_nvar");
	file_write_int(string, &tp->nvar);

	file_name_join(string, dir, "out_sol");
	table_write_llu(string, tp->sol);

	file_name_join(string, dir, "out_rel");
	table_write_llu(string, tp->rel);

	file_name_join(string, dir, "out_var");
	table_write_int(string, tp->varlist);

	free(string);
}

//read solution information from file
void template_solution_read(struct template * tp, char * dir)
{
	char * string = (char*)malloc(200);

	file_name_join(string, dir, "out_nsol");
	file_read_int(string, &tp->nsol);

	file_name_join(string, dir, "out_nvar");
	file_read_int(string, &tp->nvar);

	file_name_join(string, dir, "out_sol");
	tp->sol = &tp->sol0;
	table_init_llu(tp->sol, tp->nsol, tp->nvar);
	table_read_llu(string, tp->sol);

	file_name_join(string, dir, "out_rel");
	tp->rel = &tp->rel0;
	table_init_llu(tp->rel, tp->nsol, tp->nint);
	table_read_llu(string, tp->rel);

	file_name_join(string, dir, "out_var");
	tp->varlist = &tp->varlist0;
	table_init_int(tp->varlist, tp->nvar, 2);
	table_read_int(string, tp->varlist);

	free(string);
}

void template_config_clear(struct template * tp)
{
	tp->nvar = 0;
	tp->nsol = 0;
	table_clear_int(tp->varlist);
	table_clear_llu(tp->sol);
	table_clear_llu(tp->rel);
}

void template_clear(struct template * tp)
{
	tp->nint = 0;
	list_clear_int(tp->intid);
	table_clear_int(tp->posi);
	table_clear_int(tp->abs);
}
