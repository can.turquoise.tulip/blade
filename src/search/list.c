#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "list.h"

//list_int
//initialize a integer list with each element 0
void list_init_int(struct list_int * list, int size)
{
	list->size = size;
	if(size==0)
		list->entry = NULL;
	else
	{
		list->entry = (int *)malloc(list->size*sizeof(int));
		memset(list->entry, 0, list->size*sizeof(int));
	}
}

void list_clear_int(struct list_int * list)
{
	free(list->entry);
	list_init_int(list, 0);
}

void list_realloc_int(struct list_int * list, int size)
{
	list->size = size;
	if(size==0)
		list_clear_int(list);
	else
		list->entry = (int *)realloc(list->entry, list->size*sizeof(int));
}

//read list from file specified by path
void list_read_int(char * path, struct list_int * list)
{
	FILE * fp = fopen(path, "r");
	int i;
	for(i = 0; i<list->size; i++)
		fscanf(fp, "%d", list->entry+i);
	fclose(fp);
}

//write list to file
void list_write_int(char * path, struct list_int * list)
{
	FILE * fp = fopen(path, "w");
	int i;
	for(i = 0; i<list->size; i++)
		fprintf(fp, "%d\n", *(list->entry+i));
	fclose(fp);
}

//reverse a list
void list_reverse_int(struct list_int * list)
{
    int i, tmp;
    for(i = 0; i<list->size/2; i++)
    {
        tmp = *(list->entry+i);
        *(list->entry+i) = *(list->entry+list->size-1-i);
        *(list->entry+list->size-1-i) = tmp;
    }
}

//count the number of entries that in [a,b)
int list_countab_int(struct list_int * list, int a, int b)
{
	int i, number = 0;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)>=a && *(list->entry+i)<b)
			number++;
	return number;
}

//count the number of elements equal to key in the list
int list_count_int(struct list_int * list, int key)
{
	return list_countab_int(list, key, key+1);
}

//count the number of elements not equal to key in the list
int list_agcount_int(struct list_int * list, int key)
{
	return list->size-list_count_int(list, key);
}

//find the first position of key in a list
int list_first_position_int(struct list_int * list, int key)
{
	int i;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)==key)
			return i;
	return -1;
}

//find the first position of !key in a list
int list_first_agposition_int(struct list_int * list, int key)
{
	int i;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)!=key)
			return i;
	return -1;
}

//positions of all entries that match key
//posi must be initialized previously with appropriate size
void list_position_int(struct list_int * list, struct list_int * posi, int key)
{
    int i, j = 0;
    for(i = 0; i<list->size; i++)
        if(*(list->entry+i)==key)
        {
            *(posi->entry+j) = i;
            j++;
        }
}

//positions of all entries that do not match key
void list_agposition_int(struct list_int * list, struct list_int * posi, int key)
{
	int i, j = 0;
    for(i = 0; i<list->size; i++)
        if(*(list->entry+i)!=key)
        {
            *(posi->entry+j) = i;
            j++;
        }
}

//pick a sublist whose size is the same as posi
//sublist must be initialized previously with appropriate size
void list_picksub_int(struct list_int * list, struct list_int * sublist, struct list_int * posi)
{	
	int i;
	for(i = 0; i<posi->size; i++)
		*(sublist->entry+i) = *(list->entry+*(posi->entry+i));
}

//copy b to a, a must be initialized previously
//a can be shorter than b
void list_copy_int(struct list_int * a, struct list_int * b)
{
	int i;
	for(i = 0; i<a->size; i++)
		*(a->entry+i) = *(b->entry+i);
}

//table_int
//initialize a integer matrix with each element 0
void table_init_int(struct table_int * tab, int nrow, int ncol)
{
	tab->nrow = nrow;
	tab->ncol = ncol;
	if(nrow==0 || ncol==0)
		tab->entry = NULL;
	else
	{
		tab->entry = (int *)malloc(tab->nrow*tab->ncol*sizeof(int));
		memset(tab->entry, 0, tab->nrow*tab->ncol*sizeof(int));
	}
}

void table_clear_int(struct table_int * tab)
{
	free(tab->entry);
	table_init_int(tab, 0, 0);
}

//return the pointer that points the element (rowid, colid)
int * table_pointer_int(struct table_int * tab, int rowid, int colid)
{
	return tab->entry+rowid*tab->ncol+colid;
}

//read list from file specified by path
void table_read_int(char * path, struct table_int * tab)
{
	FILE * fp = fopen(path, "r");
	int i;
	for(i = 0; i<tab->nrow*tab->ncol; i++)
		fscanf(fp, "%d", tab->entry+i);
	fclose(fp);
}

void table_write_int(char * path, struct table_int * tab)
{
	FILE * fp = fopen(path, "w");
	int i, j;
	for(i = 0; i<tab->nrow; i++)
	{
		for(j = 0; j<tab->ncol; j++)
			fprintf(fp, "%d ", *table_pointer_int(tab, i, j));
		fprintf(fp,"\n");
	}
	fclose(fp);
}

//define a list by a row in matrix
void table_pick_int(struct list_int * list, struct table_int * tab, int rowid)
{
	list->size = tab->ncol;
	if(tab->ncol>0)
		list->entry = table_pointer_int(tab, rowid, 0);
	else
		list->entry = NULL;
}

//pick a subtable whose size is the same as posi
void table_picksub_int(struct table_int * tab, struct table_int * subtab, struct list_int * posi)
{
	int i;
	struct list_int a0, b0, * a = &a0, * b = &b0;
	for(i = 0; i<posi->size; i++)
	{
		table_pick_int(a, subtab, i);
		table_pick_int(b, tab, *(posi->entry+i));
		list_copy_int(a, b);
	}
}


//list_llu
//initialize a integer list with each element 0
void list_init_llu(struct list_llu * list, int size)
{
	list->size = size;
	if(size==0)
		list->entry = NULL;
	else
	{
		list->entry = (unsigned long long *)malloc(list->size*sizeof(unsigned long long));
		memset(list->entry, 0, list->size*sizeof(unsigned long long));
	}
}

void list_clear_llu(struct list_llu * list)
{
	free(list->entry);
	list_init_llu(list, 0);
}

void list_realloc_llu(struct list_llu * list, int size)
{
	list->size = size;
	if(size==0)
		list_clear_llu(list);
	else
		list->entry = (unsigned long long *)realloc(list->entry, list->size*sizeof(unsigned long long));
}

//read list from file specified by path
void list_read_llu(char * path, struct list_llu * list)
{
	FILE * fp = fopen(path, "r");
	int i;
	for(i = 0; i<list->size; i++)
		fscanf(fp, "%llu", list->entry+i);
	fclose(fp);
}

//write list to file
void list_write_llu(char * path, struct list_llu * list)
{
	FILE * fp = fopen(path, "w");
	int i;
	for(i = 0; i<list->size; i++)
		fprintf(fp, "%llu\n", *(list->entry+i));
	fclose(fp);
}

//reverse a list
void list_reverse_llu(struct list_llu * list)
{
    unsigned long long tmp;
    int i;
    for(i = 0; i<list->size/2; i++)
    {
        tmp = *(list->entry+i);
        *(list->entry+i) = *(list->entry+list->size-1-i);
        *(list->entry+list->size-1-i) = tmp;
    }
}

//count the number of entries that in [a,b)
int list_countab_llu(struct list_llu * list, unsigned long long a, unsigned long long b)
{
	int i, number = 0;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)>=a && *(list->entry+i)<b)
			number++;
	return number;
}

//count the number of elements equal to key in the list
int list_count_llu(struct list_llu * list, unsigned long long key)
{
	return list_countab_llu(list, key, key+1);
}

//count the number of elements not equal to key in the list
int list_agcount_llu(struct list_llu * list, unsigned long long key)
{
	return list->size-list_count_llu(list, key);
}

//find the first position of key in a list
int list_first_position_llu(struct list_llu * list, unsigned long long key)
{
	int i;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)==key)
			return i;
	return -1;
}

//find the first position of !key in a list
int list_first_agposition_llu(struct list_llu * list, unsigned long long key)
{
	int i;
	for(i = 0; i<list->size; i++)
		if(*(list->entry+i)!=key)
			return i;
	return -1;
}

//positions of all entries that match key
void list_position_llu(struct list_llu * list, struct list_int * posi, unsigned long long key)
{
    int i, j = 0;
    for(i = 0; i<list->size; i++)
        if(*(list->entry+i)==key)
        {
            *(posi->entry+j) = i;
            j++;
        }
}

//positions of all entries that do not match key
void list_agposition_llu(struct list_llu * list, struct list_int * posi, unsigned long long key)
{
    int i, j = 0;
    for(i = 0; i<list->size; i++)
        if(*(list->entry+i)!=key)
        {
            *(posi->entry+j) = i;
            j++;
        }
}

//pick a sublist whose size is the same as posi
void list_picksub_llu(struct list_llu * list, struct list_llu * sublist, struct list_int * posi)
{
	int i;
	for(i = 0; i<posi->size; i++)
		*(sublist->entry+i) = *(list->entry+*(posi->entry+i));
}

//copy b to a, a must be initialized previously
//a can be shorter than b
void list_copy_llu(struct list_llu * a, struct list_llu * b)
{
	int i;
	for(i = 0; i<a->size; i++)
		*(a->entry+i) = *(b->entry+i);
}


//table_llu
//initialize a integer matrix with each element 0
void table_init_llu(struct table_llu * tab, int nrow, int ncol)
{
	tab->nrow = nrow;
	tab->ncol = ncol;
	if(nrow==0 || ncol==0)
		tab->entry = NULL;
	else
	{
		tab->entry = (unsigned long long *)malloc(tab->nrow*tab->ncol*sizeof(unsigned long long));
		memset(tab->entry, 0, tab->nrow*tab->ncol*sizeof(unsigned long long));
	}
}

void table_clear_llu(struct table_llu * tab)
{
	free(tab->entry);
	table_init_llu(tab, 0, 0);
}

//return the pointer that points the element (rowid, colid)
unsigned long long * table_pointer_llu(struct table_llu * tab, int rowid, int colid)
{
	return tab->entry+rowid*tab->ncol+colid;
}

//read list from file specified by path
void table_read_llu(char * path, struct table_llu * tab)
{
	FILE * fp = fopen(path, "r");
	int i;
	for(i = 0; i<tab->nrow*tab->ncol; i++)
		fscanf(fp, "%llu", tab->entry+i);
	fclose(fp);
}

void table_write_llu(char * path, struct table_llu * tab)
{
	FILE * fp = fopen(path, "w");
	int i, j;
	for(i = 0; i<tab->nrow; i++)
	{
		for(j = 0; j<tab->ncol; j++)
			fprintf(fp, "%llu ", *table_pointer_llu(tab, i, j));
		fprintf(fp,"\n");
	}
	fclose(fp);
}

//define a list by a row in matrix
void table_pick_llu(struct list_llu * list, struct table_llu * tab, int rowid)
{
	list->size = tab->ncol;
	if(tab->ncol>0)
		list->entry = table_pointer_llu(tab, rowid, 0);
	else
		list->entry = NULL;
}

//pick a subtable whose size is the same as posi
void table_picksub_llu(struct table_llu * tab, struct table_llu * subtab, struct list_int * posi)
{
	int i;
	struct list_llu a0, b0, * a = &a0, * b = &b0;
	for(i = 0; i<posi->size; i++)
	{
		table_pick_llu(a, subtab, i);
		table_pick_llu(b, tab, *(posi->entry+i));
		list_copy_llu(a, b);
	}
}