#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <flint/nmod_mat.h>
#include "file.h"
#include "list.h"
#include "database.h"

//database
//dir: directory where you store the files
//red_common: prime npara nint nmaster nentry nps
void database_init(struct database * db, char * dir)
{
	char * string = (char*)malloc(200);
	FILE * fp;
	file_name_join(string, dir, "red_common");
	fp = fopen(string, "r");
	fscanf(fp, "%llu %d %d %d %d %d", &db->prime, &db->npara, &db->nint, &db->nmaster, &db->nentry, &db->nps);
	fclose(fp);

	db->invp = n_preinvert_limb(db->prime);
	
	file_name_join(string, dir, "red_part");
	db->part = &db->part0;
	list_init_int(db->part, db->nmaster+1);
	list_read_int(string, db->part);

	file_name_join(string, dir, "red_posi");
	db->posi = &db->posi0;
	list_init_int(db->posi, db->nentry);	
	list_read_int(string, db->posi);

    file_name_join(string, dir, "red_ps");
    db->ps = &db->ps0;
    table_init_llu(db->ps, db->nps, db->npara);	
	table_read_llu(string, db->ps);

	file_name_join(string, dir, "red_data");
	db->data = &db->data0;
    table_init_llu(db->data, db->nps, db->nentry);	
	table_read_llu(string, db->data);

	free(string);
}

void database_clear(struct database * db)
{
	db->prime = 0;
	db->invp = 0;
	db->npara = 0;
	db->nint = 0;
	db->nmaster = 0;
	db->nentry = 0;
	db->nps = 0;
	list_clear_int(db->part);
	list_clear_int(db->posi);
	table_clear_llu(db->ps);
	table_clear_llu(db->data);
}