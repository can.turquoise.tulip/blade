#include <algorithm>
#include <fstream>
#include <fflow/gcd.hh>
#include <fflow/mp_functions.hh>
#include <fflow/polynomial.hh>
#include <fflow/mp_multivariate_reconstruction.hh>
#include <fflow/alg_mp_reconstruction.hh>
#include <fflow/mp_gcd.hh>
#include <fflow/primes.hh>
#include <fflow/ostream.h>
#include <fflow/json.hh>
#include <fflow/graph.hh>
#include <fflow/subgraph.hh>
#include <fflow/alg_lists.hh>
#include <fflow/alg_functions.hh>
#include <fflow/analytic_solver.hh>
#include <fflow/analytic_fit.hh>
#include <fflow/numeric_solver.hh>
#include <fflow/numeric_fit.hh>
#include <fflow/alg_laurent.hh>
#include <fflow/subgraph_fit.hh>
#include <fflow/subgraph_reconstruct.hh>
#include <fflow/node_solver.hh>
#include <fflow/cached_subgraph.hh>

//Taken from finiteflow, modified by X.G

using namespace fflow;

//namespace  {

  // global data

  //const UInt DEFAULT_MOD = BIG_UINT_PRIMES[BIG_UINT_PRIMES_SIZE-1];

  // mathematica global context
  Session session;

  // internal classes, functions and methods
  struct ReconstructionOptions get_rec_opt(unsigned * opt_data, ReconstructionOptions opt)
  {

    if (opt_data[0] >= 0)
      opt.n_checks = opt_data[0];
    if (opt_data[1] >= 0)
      opt.n_uchecks = opt_data[1];
    if (opt_data[2] >= 0)
      opt.n_singular = opt_data[2];
    if (opt_data[3] >= 0)
      opt.start_mod = opt_data[3];
    if (opt_data[4] >= 0)
      opt.max_primes = opt_data[4];
    if (opt_data[5] >= 0)
      opt.max_deg = opt_data[5];
    if (opt_data[6] >= 0)
      opt.dbginfo = opt_data[6];

    return opt;
  }

  struct ReconstructionOptions get_rec_opt(unsigned * opt_data)
  {
    return get_rec_opt (opt_data, ReconstructionOptions());
  }


  void put_sparse_poly_mono(FILE * fp , const MPReconstructedPoly & p)
  {
    const std::size_t psize = p.size();
    const std::size_t nvars = p.nvars();

    void (*gmpfreefunc) (void *, size_t);
    mp_get_memory_functions (0, 0, &gmpfreefunc);

    if (psize == 0) {
      Monomial mono(nvars);
      const Monomial::VarExponent * exp = mono.exponents();
      for(unsigned i =0; i<nvars; ++i)
      {
      	fprintf(fp, "%u ", exp[i]);
      }
      return;
    }

    for (unsigned i=0; i<psize; ++i) {
      const Monomial::VarExponent * exp = p.monomial(i).exponents();
      for(unsigned j =0; j<nvars; ++j)
      {
      	fprintf(fp, "%u ", exp[j]);
      }
      //char * coeff = mpq_get_str(0, 10, p.coeff(i).get());
      //MLPutString(mlp, coeff);
      //(*gmpfreefunc)(coeff, std::strlen(coeff)+1);
    }
  }

  void put_sparse_poly_coeff(FILE * fp , const MPReconstructedPoly & p)
  {
    const std::size_t psize = p.size();
    const std::size_t nvars = p.nvars();

    void (*gmpfreefunc) (void *, size_t);
    mp_get_memory_functions (0, 0, &gmpfreefunc);

    if (psize == 0) {
      fprintf(fp, "%s ", "0");
      return;
    }

    for (unsigned i=0; i<psize; ++i) {
      char * coeff = mpq_get_str(0, 10, p.coeff(i).get());
      fprintf(fp, "%s ", coeff);
      (*gmpfreefunc)(coeff, std::strlen(coeff)+1);
    }
  }



  void put_sparse_ratfun_mono(FILE * fp, const MPReconstructedRatFun & f)
  {
    put_sparse_poly_mono(fp, f.numerator());
    put_sparse_poly_mono(fp, f.denominator());
  }

  void put_sparse_ratfun_coeff(FILE * fp, const MPReconstructedRatFun & f)
  {
    put_sparse_poly_coeff(fp, f.numerator());
    put_sparse_poly_coeff(fp, f.denominator());
  }

  void put_sparse_ratfun_part(FILE * fp, const MPReconstructedRatFun & f)
  {
    const MPReconstructedPoly & p = f.numerator();
    std::size_t psize = p.size();

    if(psize==0){
    	fprintf(fp, "%u ", 1);
    }
    else{
    	fprintf(fp, "%u ", (int)psize);
    }

	const MPReconstructedPoly & q = f.denominator();
	std::size_t qsize = q.size();
    qsize = q.size();

    if(qsize==0){
    	fprintf(fp, "%u ", 1);
    }
    else{
    	fprintf(fp, "%u ", (int)qsize);
    }    
  }

  void put_mprat(FILE * fp, const MPRational & q,
                 void (*gmpfreefunc) (void *, size_t))
  {
    if (q.sign() == 0) {
      fprintf(fp, "%s ", "0");
    } else {
      char * coeff = mpq_get_str(0, 10, q.get());
      fprintf(fp, "%s ", coeff);
      (*gmpfreefunc)(coeff, std::strlen(coeff)+1);
    }
  }
  
//return 0 if success
//delimiter is ' '
//this requirement is satisfied by recmod.exe, which call put_sparse_ratfun_coeff
int load_recmod_coeff(char *filename, UInt * xout, unsigned start, unsigned nparsout)
{
    std::ifstream file;
    file.open(filename, std::ios::in);
    if(file.fail())
        return 2;
    
    if (start)
    {
        for(unsigned j =0; j <start; ++j){
            file.ignore(256,' ');
        }
    }
    
    for(unsigned j =0; j<nparsout;++j){
        file>>xout[j];
        if (file.fail())
            return 2;
    }
    
    return 0;
}
/*  according to this function, refer how to use Monomial 
UInt eval(const Monomial & m, const UInt x[], Mod mod) const
    {
      const Monomial::VarExponent * exp = m.exponents();
      UInt res = m.coeff();
      for (unsigned i=0; i<n_vars; ++i)
        res = mul_mod(res, power(x[i],exp[i],mod), mod);
      return res;
    }
*/

//}//namespace

//extern "C" {
unsigned fflowml_new_graph(unsigned nvars)
{
	//new graph
	unsigned graphid = session.new_graph();
	Graph * graph = session.graph(graphid);
	//set input variables
	//inputid in fact is 0
	unsigned inputid = graph->set_input_vars(nvars);
	return graphid;
}

unsigned fflowml_graph_dummy(unsigned nin, unsigned nout)
  {
    unsigned id = session.new_dummy_graph(nin, nout);
    return id;
  }


void fflowml_graph_delete(unsigned id)
  {
    session.delete_graph(id);
    return;
  }

//supose each node only invloves one input node
unsigned fflowml_alg_json_sparse_system(unsigned graphid, int * nodes, char * filename0)
{
  	//inputs
    std::vector<unsigned> inputnodes;
    inputnodes.resize(1);
    std::copy(nodes, nodes+1, inputnodes.data());

    //algorithm
    typedef AnalyticSparseSolverData Data;
    std::unique_ptr<AnalyticSparseSolver> algptr(new AnalyticSparseSolver());
    std::unique_ptr<Data> data(new Data());
    auto & alg = *algptr;
    const char *filename = filename0;
    unsigned needed_workspace;
    Ret ret = json_sparse_system(filename, alg, *data, needed_workspace);

    Graph * graph = session.graph(graphid);
    unsigned id = graph->new_node(std::move(algptr), std::move(data),
                                  inputnodes.data());

    session.main_context()->ww.ensure_size(needed_workspace);
    return id;
}

void fflowml_alg_system_only_homogeneous(unsigned id, unsigned nodeid)
{
    bool okay = true;

    Algorithm * alg = session.algorithm(id, nodeid);
    if (!alg || !alg->is_mutable())
      okay = false;

    if (okay) {
      if (dynamic_cast<DenseLinearSolver *>(alg)) {
          DenseLinearSolver & ls = *static_cast<DenseLinearSolver *>(alg);
          ls.only_homogeneous();
          session.invalidate_subctxt_alg_data(id, nodeid);

      } else if (dynamic_cast<SparseLinearSolver *>(alg)) {
          SparseLinearSolver & ls = *static_cast<SparseLinearSolver *>(alg);
          ls.only_homogeneous();
          session.invalidate_subctxt_alg_data(id, nodeid);

      } else {
        okay = false;
      }
    }
}

void fflowml_graph_set_out_node(unsigned graphid, unsigned nodeid)
{
    session.set_output_node(graphid, nodeid);

}


//"PrimeNo", "MaxSingularPoints", Automatic->-1
void fflowml_alg_set_learning_options(unsigned id, unsigned nodeid, unsigned primeno )
{

    int maxsingular=-1;

    if (!session.node_exists(id, nodeid)) {
      std::cout<<"node not exists"<<std::endl;
      exit(0);
    }


    LearningOptions & opt = session.node(id, nodeid)->learn_opt;

    if (primeno >= 0)
      opt.prime_no = primeno;
    if (maxsingular >= 0)
      opt.n_singular = maxsingular;
}

void fflowml_alg_learn(unsigned id)
{
    Ret ret = session.learn(id);

    if (ret != SUCCESS) {
      std::cout<<"learn failed"<<std::endl;
      exit(0);
    }

    Graph * g = session.graph(id);
    unsigned nparsout = g->nparsout;

}


int fflowml_graph_evaluate(unsigned gid, unsigned long long * xin, unsigned  n_xin, unsigned prime_no )
{


    std::unique_ptr<UInt[]> x;
    x.reset(new UInt[n_xin]);
    std::copy(xin, xin+n_xin, x.get());


    Graph * g = session.graph(gid);
    if (!session.graph_can_be_evaluated(gid)) {
       std::cout<<"failed"<<std::endl;
       return 0;
    }


    unsigned nparsin = g->nparsin[0];


    Mod mod(BIG_UINT_PRIMES[prime_no]);
    unsigned nparsout = g->nparsout;
    std::unique_ptr<UInt[]> xout;
    xout.reset(new UInt[nparsout]);

    {
      const UInt * xxin = x.get();
      Context * ctxt = session.main_context();
      Ret ret = g->evaluate(ctxt, &xxin, mod,
                            ctxt->graph_data(gid),
                            xout.get());
      if (ret != SUCCESS)  {
        std::cout<<"evaluate failed"<<std::endl;
        return 0;      
      }
    }

    std::cout<<"nparseout->"<<nparsout<<std::endl;
    const UInt * xxout = xout.get();
    std::cout<<*xxout<<std::endl;
    
	for(unsigned i=0;i<nparsout;++i)
    {
		printf("%llu ", (unsigned long long)xout[i]);
    }

	return 0;

}

//return indep_eqs
unsigned fflowml_alg_mark_and_sweep_eqs(unsigned id, unsigned nodeid)
  {
    // note: The algorithm doesn't need to be mutable, because
    // input/output are unchanged.  AlgorithmData is however
    // invalidated in subcontexts
    Algorithm * alg = session.algorithm(id, nodeid);

    if (!alg) {
      std::cout<<"failed"<<std::endl;
    } else if (dynamic_cast<SparseLinearSolver *>(alg)) {
      SparseLinearSolver & ls = *static_cast<SparseLinearSolver *>(alg);
      if (ls.marked_and_sweeped()) {
        std::cout<<"failed"<<std::endl;
      } else {
        ls.mark_and_sweep_eqs(session.alg_data(id, nodeid));
        session.invalidate_subctxt_alg_data(id, nodeid);
        return ls.n_indep_eqs();
      }
    } else {
      std::cout<<"failed"<<std::endl;
    }

	std::cout<<"failed"<<std::endl; 
	return 1;  
  }


void fflowml_alg_delete_unneeded_eqs(unsigned id, unsigned nodeid)
  {

    Algorithm * alg = session.algorithm(id, nodeid);
    if (!alg) {
      std::cout<<"failed"<<std::endl;

    } else if (dynamic_cast<AnalyticSparseSolver *>(alg)) {
      AnalyticSparseSolver & ls = *static_cast<AnalyticSparseSolver *>(alg);
      ls.delete_unneeded_eqs(session.alg_data(id, nodeid));
      session.invalidate_subctxt_alg_data(id, nodeid);

    } else if (dynamic_cast<NumericSparseSolver *>(alg)) {
      NumericSparseSolver & ls = *static_cast<NumericSparseSolver *>(alg);
      ls.delete_unneeded_eqs(session.alg_data(id, nodeid));
      session.invalidate_subctxt_alg_data(id, nodeid);
      

    } else {
      std::cout<<"failed"<<std::endl;
    }

  }



unsigned fflowml_alg_nonzero(unsigned graphid, int *nodes)
{

    std::vector<unsigned> inputnodes;
    inputnodes.resize(1);
    std::copy(nodes, nodes+1, inputnodes.data());
 
    if (!session.graph_exists(graphid) || inputnodes.size() != 1) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    }

    Graph * graph = session.graph(graphid);

    std::unique_ptr<NonZeroes> algptr(new NonZeroes());
    auto & alg = *algptr;

    Node * innode = session.node(graphid, inputnodes[0]);
    if (!innode) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    }

    alg.init(innode->algorithm()->nparsout);
    unsigned id = graph->new_node(std::move(algptr), nullptr,
                                  inputnodes.data());

    if (id == ALG_NO_ID) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    } 

    return id;
}


void fflowml_alg_all_degrees(unsigned id, unsigned nthreads, unsigned * opt_data)
{
    
    if (nthreads < 0)
      nthreads = 0;
    ReconstructionOptions opt = get_rec_opt(opt_data);

    Ret ret = session.parallel_all_degrees(id, nthreads, opt);

    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    }

}

//} // extern "C"  

void fflowml_dump_degrees(unsigned gid, char * filename)
{
    Ret ret = session.dump_degrees(gid, filename);
    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
    } 

}


void fflowml_load_degrees(unsigned gid, char * filename)
  {
    Ret ret = session.load_degrees(gid, filename);
    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
    } 
  }


void fflowml_dump_sample_points(unsigned gid, char * filename, unsigned * opt_data)
  {
  	ReconstructionOptions opt = get_rec_opt(opt_data);
    Ret ret = session.dump_sample_points(gid, opt, filename);
    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
    } 
    return ;
  }

unsigned  fflowml_samples_file_size(char * filename)
  {
    UInt size = samples_file_size(filename);
    if (size == FAILED) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    } 
    return (unsigned)size;
  }

void fflowml_dump_evaluations(unsigned gid, char *filename)
 {
    Ret ret = session.dump_evaluations(gid, filename);
    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
    }
 }


//evaluations may be distributed in many files
//evals is a file that list all evaluation'filenames
//maximal file size ->1MB
void fflowml_load_evaluations(unsigned gid, char * list_of_filename )
  {

  	//read list of filenames
  	char * string = (char *)malloc(1000000);
  	FILE *fp =fopen(list_of_filename, "r");
  	while(!feof(fp)){
  		fscanf(fp,"%s", string);
  	}
  	fclose(fp);

  	//to filename
    std::vector<const char *> filename;
    char * name = (char *)malloc(200);
    name = strsep(&string,",");
    while(name!=NULL)
    {
    	filename.push_back(name);
    	name = strsep(&string,",");
    }
    
    Ret ret = session.load_evaluations(gid, filename.data(), filename.size());

 	free(string);
 	free(name);

    if (ret != SUCCESS) {
      std::cout<<"failed"<<std::endl;
    }
  }


void fflowml_alg_sample_from_points(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename, unsigned samples_start, unsigned samples_size)
  {
 
    if (nthreads < 0)
      nthreads = 0;
    ReconstructionOptions opt = get_rec_opt(opt_data);

    std::string file;
    file = std::string(filename);

    SamplePointsFromFile pts(file.c_str(), samples_start, samples_size);
    session.parallel_sample(id, nthreads, opt, &pts);
  }

void fflowml_alg_reconstruct(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename)
  {
    ReconstructionOptions opt = get_rec_opt(opt_data);

    Graph * g = session.graph(id);

    if (!g) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    }

    typedef MPReconstructedRatFun ResT;
    const unsigned nparsout = g->nparsout;
    std::unique_ptr<ResT[]> res (new ResT[nparsout]);

    Ret ret = session.parallel_reconstruct(id, res.get(), nthreads, opt);

    if (ret != SUCCESS) {

      if (ret == MISSING_SAMPLES) {
        std::cout<<"FiniteFlow`FFMissingPoints"<<std::endl;
        exit(0);
      }

      if (ret == MISSING_PRIMES) {
      	std::cout<<"FiniteFlow`FFMissingPrimes"<<std::endl;
        exit(0);
      }

      std::cout<<"failed"<<std::endl;
      exit(0);
    }

    //output
    char * string = (char *)malloc(200);
    strcpy(string,filename);
    strcat(string,"_coeff");
    FILE *fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_coeff( fp , res[i]);
  	fclose(fp);
    
    strcpy(string,filename);
    strcat(string,"_mono");
    fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_mono( fp , res[i]);
  	fclose(fp);

  	strcpy(string,filename);
  	strcat(string,"_part");
    fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_part( fp , res[i]);
  	fclose(fp);

  	return;
  }

void fflowml_alg_reconstruct_mod(unsigned id, unsigned nthreads, unsigned * opt_data, char * filename)
  {
    ReconstructionOptions opt = get_rec_opt(opt_data);

    Graph * g = session.graph(id);

    if (!g) {
      std::cout<<"failed"<<std::endl;
      exit(0);
    }

    typedef MPReconstructedRatFun ResT;
    const unsigned nparsout = g->nparsout;
    std::unique_ptr<ResT[]> res (new ResT[nparsout]);

    Ret ret = session.parallel_reconstruct_mod(id, res.get(), nthreads, opt);

    if (ret != SUCCESS) {

      if (ret == MISSING_SAMPLES) {
        std::cout<<"FiniteFlow`FFMissingPoints"<<std::endl;
        exit(0);
      }

      if (ret == MISSING_PRIMES) {
      	std::cout<<"FiniteFlow`FFMissingPrimes"<<std::endl;
        exit(0);
      }

      std::cout<<"failed"<<std::endl;
      exit(0);
    }

  	//output
    char * string = (char *)malloc(200);
    strcpy(string,filename);
    strcat(string,"_coeff");
    FILE *fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_coeff( fp , res[i]);
  	fclose(fp);
    
    strcpy(string,filename);
    strcat(string,"_mono");
    fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_mono( fp , res[i]);
  	fclose(fp);

  	strcpy(string,filename);
  	strcat(string,"_part");
    fp = fopen(string,"w");
    for (unsigned i=0; i<nparsout; ++i)
      put_sparse_ratfun_part( fp , res[i]);
  	fclose(fp);

  	return;
  }
