#include <algorithm>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <time.h>
#include <fflowC.hh>
#include <omp.h>

//Taken from finiteflow, modified by X.G

using namespace fflow;

//coeff_start nparsout nthreads file_name_prefix out_file;
//file_name_prefix_pid_coeff represent evaluation under FFPrimeNo[pid]
int main(int argc, char *argv[])
{

	std::cout<<"rational reconstruct: start: "<<argv[1]<<", nparsout: "<<argv[2]<<", nthreads: "<<argv[3]<<", read files from: "<<argv[4]<<"_*_coeff"<<", out_file: "<<argv[5]<<std::endl;

	time_t start, end, time1;
	double time_load,time_mod,time_rat,time_tag;
	char * ptr;
	unsigned coeff_start = (unsigned)std::strtoul(argv[1],&ptr, 10);
	unsigned nparsout = (unsigned)std::strtoul(argv[2],&ptr, 10);
	unsigned nthreads = atoi(argv[3]);
	char * out_file = argv[5];

	std::string file;
	char * file_name[200];
	std::ostringstream oss;
	start = time(NULL);
	time_load =0;
	time_mod= 0;
	time_rat=0;
	time_tag=0;
    
    unsigned nthreads_ ;
    nthreads_ = (nthreads<nparsout)? nthreads: nparsout;
    
    MPInt totmod, c1, c2;
    typedef  MPRational ResT;
    UInt * xout = (UInt  *)malloc(sizeof(UInt) * nparsout);
	MPInt * zout = (MPInt *)malloc(sizeof(MPInt)* nparsout);
	ResT * res = (ResT *)malloc(sizeof(ResT)*nparsout);
	int * res_success = (int *)malloc(sizeof(int)*nparsout);
	memset(res_success, 0, sizeof(int)*nparsout);


    int max_primes = -1;
    FILE * fp;
    int state;
  //loop until rational reconstrut success
  while(1){
    
    //increase prime_no one by one
	max_primes++;
	oss << max_primes;
	file.assign(argv[4]);
	file.push_back('_');
	file.append(oss.str());
	file.append("_coeff");
	oss.clear();
	oss.str("");
	file_name[max_primes]=(char *)malloc(200);
	snprintf(file_name[max_primes],200,"%s_%u_coeff",argv[4],max_primes);

	//state: 0-waiting, 1-running, 2-finished
	//start at state '0'. waiting for mathematic to prepare next prime and modify the state to '1' 
	//when that prime was finished, update state to be '0' and wait for next-next prime again. 
	fp = fopen((char*)"state","w");
    fprintf(fp, "%d", 0);
    fclose(fp);

	while(1)
	{
		fp = fopen((char*)"state","r");
		fscanf(fp,"%d",&state);
		fclose(fp);

		if(state){
			break;
		}
		else{
			sleep(1);
		}
	}


	if(::access(file.c_str(),R_OK)!=0)
	{
		printf("error: %s not exists\n",file_name[max_primes]);
		fflush(stdout);
		exit(2);
	}

	for(std::size_t ip=max_primes; ip<=max_primes; ++ip){

		Mod mod(BIG_UINT_PRIMES[ip]);
		time1 = time(NULL);

        if(load_recmod_coeff(file_name[ip],xout,coeff_start,nparsout))
            return 2;
        
		time_load = time_load + difftime(time(NULL),time1);

		if(ip == 0){
			totmod = mod.n();
            
            time1 = time(NULL);

			#pragma omp parallel for num_threads(nthreads_) \
			schedule(static,1) default(none) shared(xout,zout,res,mod,nparsout)
            for(unsigned i =0; i<nparsout; i++){
    			if(xout[i]){
    			//	zout[i]=xout[i];
    			//	res[i]=rat_rec(xout[i],mod.n());
    			    zout[i]=(MPInt )xout[i];    		
    		    	res[i]=(ResT )rat_rec(xout[i],mod.n());
    			} else{
    			//	zout[i]=UInt(0);
    			//	res[i]=Int(0);
    			    zout[i]=(MPInt)UInt(0);
    				Rational zerorat={0,1};
    				res[i]=(ResT )zerorat;
    			}
            }
   
		} else {

			//check 
			MPInt this_mod(mod.n()), xmp;
			bool test_success = true;
			unsigned to_do_count,pos,idx;
            unsigned * to_do_element;

            //select res_failed elements to do 
            to_do_count = 0;
            for(unsigned j=0;j<nparsout;j++){
                if (!res_success[j])
                    to_do_count++;
            };
            
            to_do_element = (unsigned *)malloc(to_do_count * sizeof(unsigned));
            idx =0;
            for(unsigned j=0; j<nparsout;j++){
                if (!res_success[j]){
                    to_do_element[idx]=j;
                    idx++;
                }
            }
			
			//check 
			time1=time(NULL);
			#pragma omp parallel for num_threads(nthreads_) \
			schedule(static,1) default(none) private(xmp,pos) shared(to_do_count,to_do_element,res,xout,res_success,this_mod,std::cout) reduction(&&:test_success)
			for(unsigned j =0; j<to_do_count; j++){	
				pos = to_do_element[j];			
                if(!rat_mod(res[pos], this_mod, xmp)){
                    std::cout<<"function rat_mod fail"<<std::endl;
                    exit(1);
                }

                if (xout[pos] != xmp.to_uint()){
                    test_success = false;
                } else{
                    res_success[pos]=1;
                }

			}
			free(to_do_element);
			time_mod=time_mod+difftime(time(NULL),time1);

			//dump
			if(test_success)
			{
				std::cout<<"rational reconstruct success with primes: "<<ip<<" + 1, ";
				
				end = time(NULL);
				printf("total use %fs. load use %fs. ratrec use %fs.", difftime(end, start),time_load,time_rat);

				time1=time(NULL);
				//output
				fp = fopen(out_file,"w");
				void (*gmpfreefunc) (void *, size_t);
    			mp_get_memory_functions(0, 0, &gmpfreefunc);
    			for (unsigned j=0; j<nparsout; ++j){
    			  put_mprat(fp, res[j], gmpfreefunc);
    			}
    			fclose(fp);

    			printf(" dump in %fs.",difftime(time(NULL),time1));

    			//a flag indicate rr success
    			fp = fopen((char*)"state","w");
    			fprintf(fp, "%d", 2);
    			fclose(fp);

    			return 0;
			}

			//indicate 
			//select res_failed elements to do 
            to_do_count = 0;
            for(unsigned j=0;j<nparsout;j++){
                if (!res_success[j])
                    to_do_count++;
            };
            std::cout<<"used primes-> "<<ip<<", rest elements -> "<<to_do_count<<std::endl;
            
            to_do_element = (unsigned *)malloc(to_do_count * sizeof(unsigned));
            idx =0;
            for(unsigned j=0; j<nparsout;j++){
                if (!res_success[j]){
                    to_do_element[idx]=j;
                    idx++;
                }
            }

    		//merge
			time1=time(NULL);
			chinese_remainder_coeffs(totmod, mod.n(), c1, c2, totmod);
			#pragma omp parallel for num_threads(nthreads_) \
			schedule(static,1) default(none) private(pos) shared(to_do_count,to_do_element,res,zout,xout,res_success,c1,c2,totmod)
			for(unsigned j =0; j<to_do_count ;++j){
                pos = to_do_element[j];
                chinese_remainder_from_coeffs(zout[pos], xout[pos], c1, c2,totmod,
												zout[pos]);
                rat_rec(zout[pos], totmod, res[pos]);	
			}	
			free(to_do_element);
			time_rat=time_rat+difftime(time(NULL),time1);
		}
	}

  }

	std::cout<<"missing primes"<<std::endl;
	end = time(NULL);
	printf("ratrec: works failed in %fs. load files in %fs. ratrec in %fs.\n", difftime(end, start),time_load,time_rat);
	return 1;


}
