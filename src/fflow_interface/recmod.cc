#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fflowC.hh>

//Taken from finiteflow, modified by X.G

//nparsin nparsout prime nthreads degree_file eval_list_file out_file_prefix
int main(int argc,char *argv[])
{
	unsigned nparsin = atoi(argv[1]);
	unsigned nparsout = atoi(argv[2]);
	unsigned start_mod= atoi(argv[3]);
	unsigned nthreads = atoi(argv[4]);
	time_t start, end;

	char * cwd = (char *)malloc(200);
	getcwd(cwd,200);
	char * deg_file = (char *)malloc(200);
	deg_file = argv[5];
	char * eval_list_file = (char *)malloc(200);
	eval_list_file = argv[6];
	char * out_file_prefix = (char *)malloc(200);
	out_file_prefix = argv[7];


	unsigned gid = fflowml_graph_dummy( nparsin, nparsout );
	fflowml_load_degrees(gid, deg_file);

	start = time(NULL);
    fflowml_load_evaluations( gid, eval_list_file);
    end = time(NULL);
	printf("load evaluations in %fs.\n", difftime(end,start));

    unsigned opt_data[7]={0,0,0,start_mod,1,1000,0};
    start = time(NULL);
    fflowml_alg_reconstruct_mod( gid,  nthreads, opt_data,  out_file_prefix);
    end = time(NULL);
	printf("reconstruction mod in %fs.\n", difftime(end,start));

    fflowml_graph_delete( gid);

    return 0;
}
