#include <stdio.h>
#include <unistd.h>
#include <gmp.h>
#include <flint/nmod_mat.h>
#include <algorithm>
#include <fflow/common.hh>
#include <fflow/gcd.hh>
#include <fflow/mp_functions.hh>
#include <fflow/polynomial.hh>
#include <fflow/mp_multivariate_reconstruction.hh>
#include <fflow/mp_gcd.hh>
#include <fflow/primes.hh>
#include <fflow/ostream.h>
#include <fflow/json.hh>
#include <fflow/graph.hh>
#include <fflow/subgraph.hh>
#include <fflow/alg_lists.hh>
#include <fflow/alg_functions.hh>
#include <fflow/analytic_solver.hh>
#include <fflow/analytic_fit.hh>
#include <fflow/numeric_solver.hh>
#include <fflow/numeric_fit.hh>
#include <fflow/alg_laurent.hh>
#include <fflow/subgraph_fit.hh>
#include <fflow/subgraph_reconstruct.hh>
#include <fflow/node_solver.hh>
#include <fflow/cached_subgraph.hh>
#include <fstream>
#include <fflow/alg_reconstruction.hh>
#include <omp.h>
#include "file.h"
#include "common.h"
#include "block.h"
#include "iofflow.h"

//nparsin primeid start size nthreads system_file points_order points_file out_file
int main(int argc, char **argv)
{

	unsigned nparsin = atoi(argv[1]);
    unsigned prime_id = atoi(argv[2]);
    unsigned samples_start = atoi(argv[3]);
	unsigned samples_size = atoi(argv[4]);
	unsigned nthreads = atoi(argv[5]);
	char * system_file = (char *)malloc(200);
	system_file = argv[6];
    unsigned pts_order = atoi(argv[7]);
	char * pts_file = (char *)malloc(200);
	pts_file = argv[8];
	char * out_file = (char *)malloc(200);
	out_file = argv[9];

    char * cwd = (char *)malloc(200);
    if(getcwd(cwd,200)==NULL)
        exit(1);
    std::cout<<"ssolve: \ncurrent working directory: "<<cwd <<std::endl;
	std::cout<<"samples_start: "<<samples_start<<", samples_size: "<<samples_size<<", nthreads: "<<nthreads<<std::endl;
	std::cout<<"system_file from: "<<system_file<<", points_ordering: "<<pts_order<<", points_file from: "<<pts_file<<", out_file to: "<<out_file<<std::endl;
    //load system. 
    
    //system_init_from_txt is a multi-family version
    //system_init_from_txt_parallel is a parallel version
    time_t tstart, tend;
    double tcost;
    
    tstart = time(NULL);
    struct system sys0, *sys= &sys0;
    //system_init_from_txt(sys,system_file);
    system_init_from_txt_parallel(sys,system_file,nthreads);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    printf("system loaded in %f s. \nwith %d blocks, %d integrals, %d masters.\n",tcost,sys->nblocks,sys->nints,sys->nmis);
 
    //learn
    tstart = time(NULL);
    system_solve_ordering_init(sys);
    system_learn(sys);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    printf("system learn in %f s.\n",tcost);   

    //learn average sample time only. write to cwd/average_time. unit: second
    if(samples_size==0)
    {
        system_average_sample_time(sys, cwd);
        exit(0);
    }
    
    tstart = time(NULL);
    fflow::SamplePointsVector samples;
    unsigned flags_size = fflow::bit_array_u64size(sys->nparsout);
    fflow::Ret ret = fflow::load_samples(pts_file,sys->nparsin+flags_size,samples,samples_start,samples_size);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    if(ret){
        printf("load_samples failed!");
        exit(1);
    } else{
        printf("load points in %f s.\n",tcost);
    }
    
    tstart = time(NULL);
    system_trimming_from_points(sys,samples);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    printf("trimming from points done in %f s.\n, with %d blocks to evaluate\n",tcost,sys->ordering->size);

    tstart = time(NULL);
    struct table_ulong Cache0 , *Cache=&Cache0;
    system_sample_from_points(sys, nthreads, pts_order, samples, Cache);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    printf("system solve in %f s.\n",tcost);
    
    tstart = time(NULL);
    system_dump_evaluations(out_file,sys->nparsin,sys->nparsout+flags_size,Cache);
    tend = time(NULL);
    tcost = difftime(tend,tstart);
    printf("dump evaluations in %f s.\n",tcost);
    
    return 0;
    
}
