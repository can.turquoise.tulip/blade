#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "file.h"
#include "common.h"
#include <flint/nmod_mat.h>

void list_int_init(struct list_int *ls, int size)
{
    ls->size =  size;
    
    if(size ==0){
        ls->entry = NULL;
    }
    else{
        ls->entry = (int *)malloc(sizeof(int) * size);
        if (ls->entry == NULL) {
            printf("Memory allocation failed.\n");
            exit(1);
        }
        memset(ls->entry,0, sizeof(int) * size);
    }
}


void list_int_read(struct list_int *ls, char *filename)
{
    FILE *fp = fopen(filename,"r");
    int i;
    for(i=0;i<ls->size;i++){
        if(! fscanf(fp,"%d", ls->entry+i)){
        	exit(1);
        }
    }
    
    fclose(fp);
}


void list_int_print(struct list_int * ls)
{    
    int i;
    for(i =0 ;i<ls->size; i++){
        printf("%d ", *(ls->entry+i));
    }
}

//lscp should be malloc. usually it has a MAXIMAL_SIZE
void list_int_copy(struct list_int * src, struct list_int *tar)
{
    int i;
    for(i =0; i<src->size; i++){
        *(tar->entry+i) = *(src->entry+i);
    }
    tar->size = src->size;
}


int list_int_count(struct list_int *a, int key)
{
    int cnt = 0;
    int i;
    for(i=0;i<a->size;i++)
    {
        if(*(a->entry+i) ==key)
            cnt++;
    }
    return cnt;
}


//ls should be sorted from small to large. this is the case for index
void list_int_quik_union(struct list_int *a, struct list_int *b, struct list_int *ls)
{
    int i, j, cnt;
    i = j = cnt = 0;
    while (i < a->size && j < b->size){
        if (*(a->entry+i) > *(b->entry+j))
        {
            *(ls->entry+cnt)=(*(b->entry+j));
            j++;
            cnt++;
        }
        else if (*(a->entry+i) < *(b->entry+j))
        {
            *(ls->entry+cnt)=(*(a->entry+i));
            i++;
            cnt++;
        }
        else
        {
            *(ls->entry+cnt)=(*(a->entry+i));
            i++;
            j++;
            cnt++;
        }
    }

    while (i < a->size)
    {
        *(ls->entry+cnt)=(*(a->entry+i));
        i++;
        cnt++;
    }
    while (j < b->size)
    {
        *(ls->entry+cnt)=(*(b->entry+j));
        j++;
        cnt++;
    }
    
    ls->size = cnt;
}


void list_int_clear(struct list_int * ls)
{
    free(ls->entry);
    ls->entry=NULL;
    ls->size=0;
}


//-1 not found
int list_int_first_position(struct list_int * ls, int key)
{
    int i=0;
    int flag = 1;
    while(flag&&i<ls->size){
        if(*(ls->entry+i) ==key){
            flag = 0;
            i--;
        }
        i++;
    }
    if(flag ==0){
        return i;
    }
    else{
        return -1;
    }
}

//1: True 
//0: False
int list_int_contains_all(struct list_int *a, struct list_int *b)
{
    int flag;
    int i;
    if(a->size < b->size){
        return 0;
    }
    
    for(i=0;i<b->size;i++)
    {
        flag = list_int_first_position(a, *(b->entry + i));
        if(flag<0){
            return 0;
        }
    }
    return 1;
}



void list_ulong_init(struct list_ulong *ls, int size)
{
    ls->size = size;
    
    if(size ==0){
        ls->entry = NULL;
    }
    else{
        ls->entry = (unsigned long long *)malloc(sizeof(unsigned long long) * size);
        if (ls->entry == NULL) {
            printf("Memory allocation failed.\n");
            exit(1);
        }
        memset(ls->entry,0, sizeof(unsigned long long) * size);
    }
}


void list_ulong_read(struct list_ulong *ls, char *filename)
{
    int i;
    FILE *fp = fopen(filename,"r");
    for(i=0;i<ls->size;i++){
        if(!fscanf(fp,"%llu", ls->entry+i)){
        	exit(1);
       	}
    }
    fclose(fp);
}

void list_ulong_write(struct list_ulong * ls, char* filename)
{
    int i;
    FILE *fp =fopen(filename,"w");
    for(i=0; i<ls->size; i++){
        fprintf(fp,"%llu ",*(ls->entry+i));
    }
    fclose(fp);
}

//posi not initialize
void list_ulong_nonzero_position(struct list_ulong * ls, struct list_int *posi)
{
    int * a =(int *)malloc(sizeof(int)*ls->size);
    if (a == NULL) {
            printf("Memory allocation failed.\n");
            exit(1);
    }
    int cnt=0;
    int i;
    for(i=0;i<ls->size;i++){
        if(*(ls->entry+i) != 0){
            *(a+cnt)=i;
            cnt++;
        }
    }
    
    list_int_init(posi,cnt);
    for(i=0; i< cnt;i++){
        *(posi->entry+i)=*(a+i);
    }
    
    free(a);
}

int list_ulong_agcount(struct list_ulong *ls , unsigned long long key)
{
    int cnt=0;
    int i;
    unsigned long long entry;
    for(i=0;i<ls->size;i++){
        entry = *(ls->entry+i);
        if(entry != key){
            cnt++;
        }
    }
    return cnt;
}

int list_ulong_first_position(struct list_ulong * ls, unsigned long long key)
{
    int i;
    for(i =0;i<ls->size;i++)
    {
        if(*(ls->entry+i)==key)
        {
            return i;
        }
    }
    return -1;
}


void list_ulong_clear(struct list_ulong *ls)
{
    free(ls->entry);
    ls->size=0;
    ls->entry=NULL;
}


void list_ulong_print(struct list_ulong * ls)
{
    int i;
    for(i = 0; i<ls->size; i++){
        printf("%llu ",*(ls->entry + i));
    }
    printf("\n");
}

void table_int_init(struct table_int * tb, int r, int c){
    
    tb->row = r;
    tb->col = c;
    
    if(r==0||c==0){
        tb->entry = NULL;
    }
    else{
        tb->entry = (int*)malloc(sizeof(int)* r* c);
        if (tb->entry == NULL) {
            printf("Memory allocation failed.\n");
            exit(1);
        }
        memset(tb->entry,0,sizeof(int)* r* c);
    }
}

void table_int_read(struct table_int * tb, char *filename){
    FILE *fp = fopen(filename,"r");
    unsigned long long n = (unsigned long long)(tb->row) * (unsigned long long)(tb->col);
    unsigned long long i;
    for(i=0; i<n; i++){
        if(!fscanf(fp,"%d",tb->entry+i)){
        	exit(1);
        }
    }
    fclose(fp);
}

// ls should be initialed
/*inline*/ void table_int_picklist(struct table_int * tb, int r, struct list_int *ls)
{
    ls ->size = tb->col;
    unsigned long long product = (unsigned long long)r * (unsigned long long)(tb->col);
    ls->entry = tb->entry + product;
}

/*inline*/ int table_int_point(struct table_int *tb, int r, int c)
{
    unsigned long long product = (unsigned long long)r * (unsigned long long)(tb->col);
    return(*(tb->entry + product + c));
}

void table_ulong_init(struct table_ulong * tb, int r, int c){
    
    tb->row = r;
    tb->col = c;
    
    if(r==0||c==0){
        tb->entry = NULL;
    }
    else{
        tb->entry = (unsigned long long*)malloc(sizeof(unsigned long long)* r* c);
        if (tb->entry == NULL) {
            printf("Memory allocation failed.\n");
            exit(1);
        }
        memset(tb->entry,0,sizeof(unsigned long long)* r* c);
    }
}

void table_ulong_read(struct table_ulong * tb, char *filename){
    FILE *fp = fopen(filename,"r");
    unsigned long long n = (unsigned long long)(tb->row) * (unsigned long long)(tb->col);
    unsigned long long i;
    for(i=0; i<n; i++){
        if(!fscanf(fp,"%llu",tb->entry+i)){
        	exit(1);
        }
    }
    fclose(fp);
}


/*inline*/void table_ulong_pick(struct table_ulong * tb, int r, struct list_ulong *ls)
{
    ls->size = tb->col;
    unsigned long long product = (unsigned long long)r * (unsigned long long)(tb->col);
    ls->entry = tb->entry + product;
}


//sub should be initialize
/*inline*/ void table_ulong_subtable(struct table_ulong *tb, int r, struct table_ulong *sub)
{
    unsigned long long product = (unsigned long long)r * (unsigned long long)(tb->col);
    sub->entry = tb->entry + product; 
}

/*inline*/ unsigned long long table_ulong_entry(struct table_ulong *tb, int r, int c)
{
    unsigned long long product = (unsigned long long)r * (unsigned long long)(tb->col);
    return(*(tb->entry + product + c));
}


void table_ulong_print(struct table_ulong *tb)
{
    int i,j;
    for(i=0; i<tb->row; i++){
        for(j =0; j<tb->col; j++){
            printf("%llu ", table_ulong_entry(tb,i,j));
        }
        
        printf("\n");
    }
}



void directory_count_with_filename(char *prefix, char *file, int *a)
{
    char *string = (char *)malloc(200);
    int b=0;
    sprintf(string,"%s/%d/%s",prefix,b,file);
    while(file_existsQ(string)){
        b++;
        sprintf(string,"%s/%d/%s",prefix,b,file);
    };
    
    *a =b;
    free(string);
}

void list_ulong_random(struct list_ulong * ls, unsigned long long prime)
{
    srand((unsigned)time(NULL));
    unsigned long long invp=n_preinvert_limb(prime);
    unsigned long long shift = 1381455948467955846;
    int i;
    for(i=0;i<ls->size;i++)
    {
        *(ls->entry +i ) =n_mulmod2_preinv(rand()+shift,rand(),prime,invp);
    }
}

void table_ulong_clear(struct table_ulong *tb)
{
    tb->row=0;
    tb->col=0;
    
    free(tb->entry);
    tb->entry=NULL;
}
