#include <fflow/alg_mp_reconstruction.hh>
#include <omp.h>
#include "iofflow.h"

//Taken from finiteflow, modified by X.G

//determine sys->output_flag 
void system_reset_needed_from_points(struct system *sys, fflow::SamplePointsVector &vec)
{
    int i,j;
    sys->trimmingQ =1;
    sys->output_flag = &sys->output_flag0;
    list_int_init(sys->output_flag, sys->tarid->size);
    
    //part information for tarid. 
    struct list_int part0, *part=&part0;
    list_int_init(part,sys->tarid->size+1);
    *(part->entry)=0;
    for(i =0;i<sys->tarid->size;i++)
    {
        int globalid = *(sys->tarid->entry + i);
        int len = (*(sys->islist+globalid))->nmis;
        *(part->entry+i+1) = *(part->entry+i)+ len; 
    }
    
    unsigned tot_samples = vec.size();
    const std::unique_ptr<fflow::UInt[]> * start = vec.data();
    const std::unique_ptr<fflow::UInt[]> * end = vec.data() + tot_samples;
    for(; start !=end; ++start){
        
        fflow::ConstBitArrayView needed((*start).get()+sys->nparsin+1);
        
        
        
        for(j =0; j< part->size-1; j++)
        {
            int jstart = *(part->entry+j);
            int jend = *(part->entry+j+1);
            
            if(*(sys->output_flag->entry + j)){
            
            }
            else{    
                for(; jstart !=jend; jstart++){
                    if(needed.get(jstart)){
                        *(sys->output_flag->entry + j) = 1;
                        break;
                    }
                }
            }
        }   
    }
    
}

void system_trimming_from_points(struct system *sys, fflow::SamplePointsVector &vec)
{
   
    system_reset_needed_from_points(sys,vec);
    
    struct list_int taridcp0, *taridcp = &taridcp0;
    struct list_int needed0, *needed = &needed0;
    list_int_init(taridcp, sys->tarid->size);
    list_int_copy(sys->tarid,taridcp);
    system_needed_clear(sys);//clear sys->tarid, sys->regisbl,sys->ordering
    
    //store new needed
    int size = list_int_count(sys->output_flag,1);
    int i;
    list_int_init(needed,size);
    int idx =0;
    for(i =0;i<taridcp->size;i++)
    {
        if(*(sys->output_flag->entry + i)){
            *(needed->entry+idx) = *(taridcp->entry+i);
            idx++;
        }
    }
    
    //determing solve-ordering according to new needed integrals. analogous to trimming
    system_register_needed_init(sys,needed);
    system_solve_ordering_init(sys);
    
    //restore inital tarid.
    // we do not care about the new needed integral otherwise we could set it at the first stage.
    //we just want to evaluate less blocks since they are not used in the final reconstruction.
    //so we keep the output structure as before, except that set those unneeded integrals to be zero(according to output_flag).
    list_int_clear(sys->tarid);
    list_int_init(sys->tarid,taridcp->size);
    list_int_copy(taridcp,sys->tarid);
    
}


void system_evaluate_and_sparse_cache(struct system *sys,
                                        const std::unique_ptr<fflow::UInt[]> * start,
                                        const std::unique_ptr<fflow::UInt[]> * end, 
                                        unsigned ptsord,
                                        struct table_ulong *cache)
{
    unsigned nparsin = sys->nparsin;
    unsigned nparsout = sys->nparsout;
    int cnt =0;
    
    //extract ordlist e.g{2,1,3} from ptsord e.g 213
    struct list_int ordlist0, *ordlist = &ordlist0;
    list_int_init(ordlist,nparsin);
    unsigned ptsordcp = ptsord;
    for(unsigned i=nparsin; i>0; i--){
        *(ordlist->entry + i-1) = ptsordcp % 10;
        ptsordcp = ptsordcp/10;
    }


    unsigned flags_size = fflow::bit_array_u64size(nparsout);
    //malloc output
    struct list_ulong allxout0, *allxout=&allxout0;
    list_ulong_init(allxout,nparsout);
        
    for (; start != end; ++start) {

        fflow::ConstBitArrayView needed((*start).get()+nparsin+1);
    
        //unsigned this_nout = fflow::bit_array_nonzeroes(needed, nparsout);
        
        //get input
        struct list_ulong xin0, *xin = &xin0;
        list_ulong_init(xin,nparsin);
        for(unsigned i=0;i<nparsin;i++){
            *(xin->entry+ *(ordlist->entry+i)-1) = *((*start).get() + i); 
        }

        if (sys->prime != *((*start).get() + nparsin)){  
            printf("error primefield");
            exit(1);
        }
      
        system_evaluate(sys, xin, allxout);
    
        //sparse output. xout does not malloc memory
        struct list_ulong xout0,*xout = &xout0;
        xout->size = cache->col;
        table_ulong_pick(cache,cnt,xout);
        //copy from points.fflow, the ordering of sample should not change
        for(unsigned i=0;i<nparsin+1+flags_size;i++){
          *(xout->entry+i) = *((*start).get() + i); 
        }
      
        unsigned xidx = nparsin+1+flags_size;
        for (unsigned j=0; j<nparsout; ++j){
          if (needed.get(j)){
            *(xout->entry+xidx) = *(allxout->entry+j);
            xidx++;
          }  
        }
        cnt++;
        
        list_ulong_clear(xin);
    }
    
    list_ulong_clear(allxout);
}



//this function would alloc memory for Cache
//block-triangular form has ordering kin_table , e.g. {z1,z2,z3}, points.fflow has ordering, e.g. {z3,z1,z2}, 
//then ptsord = 312; ordlist={3,1,2}; xin[[ordlist[[i]]]]=points[[i]]
void system_sample_from_points(struct system *sys, int nthreads_, unsigned ptsord, fflow::SamplePointsVector &samples, struct table_ulong *Cache)
{   
    
    unsigned flags_size = fflow::bit_array_u64size(sys->nparsout);
    unsigned long  tot_samples = samples.size();
    table_ulong_init(Cache,tot_samples,sys->nparsin+1+flags_size+sys->nparsout);
    
    unsigned nthreads = nthreads_;
    nthreads = (nthreads<tot_samples)? nthreads: tot_samples;
    
    
    omp_set_num_threads(nthreads);
    
    #pragma omp parallel for schedule(static)
    for(unsigned i =0; i<nthreads;i++){
        
        unsigned done_samples = 0;
        for(unsigned j =0; j<i;j++){
            done_samples += tot_samples/nthreads + (j < (tot_samples % nthreads));
        }
        
        std::unique_ptr<fflow::UInt[]> * start = samples.data() + done_samples;
        const unsigned n = tot_samples/nthreads + (i < (tot_samples % nthreads));
        
        struct table_ulong subcache0, *subcache = &subcache0;
        subcache->row=n;
        subcache->col=Cache->col;
        table_ulong_subtable(Cache,done_samples,subcache);
        //printf("threadid: %d, sampleid: %d\n",omp_get_thread_num(), i);
        system_evaluate_and_sparse_cache(sys,start,start+n,ptsord,subcache);
    }
    
}

//npar_in = sys->nparsin
//npars_out =  flag_size + sys->nparsout 
void system_dump_evaluations(char * filename, unsigned npars_in, unsigned npars_out, struct table_ulong *cache)
{
    if(npars_in + npars_out + 1 !=(unsigned)cache->col){
        printf("error: dump_evaluations, not matched Cache");
        exit(1);
    }
    
    std::ofstream file;
    file.open(filename, std::ios::binary);
    if (file.fail())
      exit(2);

    auto dump = [&file](unsigned long long z)
      {
        file.write(reinterpret_cast<char*>(&z), sizeof(unsigned long long));
      };

	dump(npars_in);
    dump(npars_out);
    dump(cache->row);
    dump(0);

    for (int i = 0; i< cache->row; i++) {
      for (int j=0; j< cache->col; j++)
        dump(table_ulong_entry(cache,i,j));
    }
    
}
