#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"

//1 : True, OK
//0 : False, NO
//-1: Error
int file_existsQ(char * filename)
{
    FILE *fp = fopen(filename,"r");
    if(fp==NULL){
        return 0;
    }
    fclose(fp);
    return 1;
   
}

//file has been malloc
void file_name_join(char *file , char *dir, char * name)
{
    sprintf(file,"%s/%s",dir,name);
}


void file_read_int(char * filename, int * a){
    FILE *fp = fopen(filename,"r");
    if(! fscanf(fp,"%d", a)){
        exit(1);
    }
    fclose(fp);
}

void file_read_ulong(char * filename, unsigned long long *a){
    FILE *fp = fopen(filename,"r");
    if(!fscanf(fp,"%llu",a)){
        exit(1); 
    };
    fclose(fp);
}
