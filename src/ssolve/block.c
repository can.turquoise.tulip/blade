#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "block.h"

void template_info_init(struct template_info *tpif, char *dir){
    char *file = (char *)malloc(200);
    
    char *cha = (char*)"tmp_nvar";
    file_name_join(file,dir,cha);
    file_read_int(file,&tpif->nvar);
    
    cha = (char*)"tmp_nsol";
    file_name_join(file,dir,cha);
    file_read_int(file,&tpif->nsol);
    
    tpif->var = &tpif->var0;
    table_int_init(tpif->var, tpif->nvar, 2);
    cha = (char*)"tmp_var";
    file_name_join(file,dir,cha);
    table_int_read(tpif->var,file);
    
    free(file);
    
    
}

//sub should be initalized. 0,0,0,....
void template_assign(struct system *sys, struct system_sol *ss,struct template_info *tpif, struct table_ulong *sol, struct table_ulong *sub)
{
    int i, j;
    for(i =0; i<tpif->nsol; i++){
        
        struct list_ulong ssol0, *ssol =&ssol0;
        table_ulong_pick(sol, i, ssol);
        struct list_ulong ssub0, *ssub=&ssub0;
        table_ulong_pick(sub, i, ssub);
        
        unsigned long long prod;
        int intid;
        int kineid;
        
        for(j=0; j<ssol->size; j++)
        {
            kineid = table_int_point(tpif->var,j,1);
            intid = table_int_point(tpif->var,j,0);
            prod = n_mulmod2_preinv(*(ssol->entry+j), *(ss->kinlist->entry + kineid), sys->prime, sys->invp);
            *(ssub->entry+ intid) = n_addmod(*(ssub->entry+ intid),prod,sys->prime) ;
        }
    
    }
    
}

//ls should be initialize (memset(0))
//abandom since block_sol_trans is more efficient
void integral_sol_to_normal(struct integral_sol * is, struct list_int * index, struct list_ulong * ls)
{
    int i, j;
    i = j =0;
    while(i<is->nmis && j<index->size){
    
        if((*(is->index->entry+i)) < (*(index->entry+j))){  // index do not collect all projection of integral
            i++;
        }
        else if((*(is->index->entry+i)) == *((index->entry+j))){
            *(ls->entry+j) = *(is->elem->entry+i);
            i++;
            j++;
        }
        else{
            j++;
        }
    }
    
}


void block_init(struct block *bl, char *dir, char *db){
    char *file = (char *)malloc(200);
    char *cha;
    
    cha = (char*)"sch_g1";
    file_name_join(file,dir,cha);
    file_read_int(file, &(bl->g1));
    
    cha = (char*)"sch_nint";
    file_name_join(file,dir,cha);
    file_read_int(file,&(bl->nint));
    
    cha =(char*)"multi_sch_intid";
    file_name_join(file,dir,cha);
    bl->intid = &bl->intid0;
    list_int_init(bl->intid, bl->nint);
    list_int_read(bl->intid, file);
    
    //count number of templates
    cha = (char*)"config";
    file_name_join(file,dir,cha);
    cha = (char*)"tmp_nvar";
    directory_count_with_filename(file,cha,&bl->ntp);
    
    //load scheme
    //load sol under certain database
    bl->tp_info = (struct template_info **)malloc(sizeof(struct template_info *) * bl->ntp);
    bl->sol = (struct table_ulong **)malloc(sizeof(struct table_ulong *) *bl->ntp);
    
    int i;
    for(i =0; i<bl->ntp; i++){
        sprintf(file,"%s/config/%d",dir,i);
        
        *(bl->tp_info+i) = (struct template_info *)malloc(sizeof(struct template_info));
        memset(*(bl->tp_info+i),0,sizeof(struct template_info));
        
        template_info_init(*(bl->tp_info+i),file);
        
        
        *(bl->sol+i) = (struct table_ulong *)malloc(sizeof(struct table_ulong));
        memset(*(bl->sol+i),0, sizeof(struct table_ulong));
        
        sprintf(file,"%s/fit/%s/%d",dir,db, i);
        table_ulong_init(*(bl->sol+i), (*(bl->tp_info+i))->nsol, (*(bl->tp_info+i))->nvar);
        table_ulong_read(*(bl->sol+i), file);
    }
    
    
    
    
    free(file);
}

void block_sol_index_init(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs)
{
/*    struct list_int index0, *index = &index0;
    list_int_init(index,sys->nmis);  //maximal_size -> number of masters
    index->size=0;*/
    bs->nzindex = &bs->nzindex0;
    list_int_init(bs->nzindex,sys->nmis);  //maximal_size -> number of masters
    bs->nzindex->size=0;
    
    struct list_int swap0, *swap = &swap0;
    list_int_init(swap,sys->nmis);
    swap->size=0;
    
    //union all nonzero projection position
    int i;
    for(i = bl->g1; i< bl->nint; i++){
        int id = *(bl->intid->entry + i);    
        list_int_copy(bs->nzindex,swap);
        list_int_quik_union(swap,(*(ss->islist + id))->index, bs->nzindex);
    }
}


void block_sol_assign(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs)
{
    bs->block_numeric = &bs->block_numeric0;
    table_ulong_init(bs->block_numeric, bl->g1, bl->nint);
    struct table_ulong *sub = (struct table_ulong*)malloc(sizeof(struct table_ulong));
    sub->col=bl->nint;
    
    int sol_count=0;
    int i;
    for(i =0; i< bl->ntp; i++){
        if((*(bl->tp_info+i))->nsol > 0){
            
            table_ulong_subtable(bs->block_numeric, sol_count,sub);
            template_assign(sys,ss,*(bl->tp_info+i),*(bl->sol+i),sub);
            sol_count += (*(bl->tp_info+i))->nsol;
        }
    }
    
    if(sol_count != bl->g1){
        printf("wrong relations\n");
        exit(1);
    }
    
    free(sub);
}

void block_sol_substitute(struct system *sys, struct system_sol *ss, struct block * bl, struct block_sol *bs)
{
    nmod_mat_init(bs->mat,bl->g1,bl->g1+bs->nzindex->size,sys->prime);
    
    //copy homogenous part into mat
    int i, j, k;
    for(i =0; i < bl->g1; i++){
        for(j =0; j< bl->g1; j++){
            nmod_mat_entry(bs->mat,i,j) = table_ulong_entry(bs->block_numeric,i,j);
        }
    }
    
    //calculate nonhomogenous part
    //sparse matrix multiplication, less number of evaluation 
    for(j = bl->g1; j<bl->nint; j++){
        int globalid = *(bl->intid->entry+ j );
        struct integral_sol * is = *(ss->islist+globalid);
        int * posilist = (int *)malloc(sizeof(int)* is->nmis);
        
        for(k=0;k<is->nmis; k++){
            *(posilist+k)= list_int_first_position(bs->nzindex, *(is->index->entry+k));
        }
        
        for(i=0;i<bl->g1;i++){
            unsigned long long coe = table_ulong_entry(bs->block_numeric, i, j);
            if(coe != 0 ){
                for(k =0; k<is->nmis; k++)
                {
                    unsigned long long proj;
                    unsigned long long prod;
                    proj = *(is->elem->entry+k);
                    prod = n_mulmod2_preinv(coe,proj,sys->prime,sys->invp);
                    int posi = *(posilist+k);
                    nmod_mat_entry(bs->mat,i,bl->g1+posi) = n_addmod(nmod_mat_entry(bs->mat,i,bl->g1+posi) , prod,sys->prime);
                }
                
            }
        }
        free(posilist);
    }
    
}


//if an integral is solved in more than one blocks, then the solution should be checked.
//is is the solve integral solution, list is a new solution, refer to block_sol_trans
//1: PASS, 0: FAIL
int integral_sol_check(struct integral_sol *is, struct list_ulong *list)
{
    //check size, avoid segmentation fault
    if(is->nmis > list->size){
        return 0;
    }
    
    int counter = 0;
    int j;
    for(j = 0; j<list->size; j++)
    {
        if(*(list->entry+j)!=0)
        {
            if(*(is->elem->entry+counter) != *(list->entry+j)){
                return 0;
            }
            counter++;
        }
    }
    return 1;
}


//transform nmod_mat_rref to integral_sol
//generate integral_sol from block_sol
void block_sol_trans(struct system * sys, struct system_sol * ss, struct block *bl, struct block_sol * bs)
{
	int i, j;

	for(i = 0; i<bl->g1; i++)
	{
		int globalid = *(bl->intid->entry+i);
		struct integral_sol * is = *(ss->islist+globalid);

		struct list_ulong list0, * list = &list0;
		list_ulong_init(list, bs->nzindex->size);
		for(j = 0; j<bs->nzindex->size; j++)
		{
			unsigned long long entry = nmod_mat_entry(bs->mat, i, j+bl->g1);
			if(entry!=0)
				*(list->entry+j) = sys->prime-entry;
		}
        
        //if integral solved, check, otherwise, register
        if(is->nmis > 0){
            int check = integral_sol_check(is,list);
            if(!check){
                printf("error: solution for integral: %d is not self-consistent!\n",globalid);
                exit(1);
            } else{
                
            }
        } else{
            is->nmis = list_ulong_agcount(list, 0);
            integral_sol_init(is, is->nmis);

            int counter = 0;
            for(j = 0; j<bs->nzindex->size; j++)
            {
                if(*(list->entry+j)!=0)
                {
                    *(is->elem->entry+counter) = *(list->entry+j);
                    *(is->index->entry+counter) = *(bs->nzindex->entry+j);
                    counter++;
                }
            }
        }

		list_ulong_clear(list);
	}
}


void block_sol_clear_partial(struct block_sol *bs)
{
    table_ulong_clear(bs->block_numeric);
    nmod_mat_clear(bs->mat);
}


void block_sol_clear(struct block_sol *bs)
{
    
    list_int_clear(bs->nzindex);
    table_ulong_clear(bs->block_numeric);
    nmod_mat_clear(bs->mat);
}

void block_learn(struct system *sys, struct system_sol *ss, int n)
{
    struct block *bl = *(sys->bllist+n);
    *(sys->bslist+n) = (struct block_sol *)malloc(sizeof(struct block_sol));
    struct block_sol *bs = *(sys->bslist+n);
    block_sol_assign(sys,ss,bl,bs);
    block_sol_index_init(sys,ss,bl,bs);
    block_sol_substitute(sys,ss,bl,bs);
    nmod_mat_rref(bs->mat);
    block_sol_trans(sys,ss,bl,bs);

    //do not free since system cache
    block_sol_clear_partial(bs);
}


void block_solve(struct system *sys, struct system_sol *ss, int n)
{
    struct block *bl = *(sys->bllist+n);
    struct block_sol bs0, *bs=&bs0;

    struct block_sol *bslearn = *(sys->bslist+n);
    bs->nzindex = &bs->nzindex0;
    list_int_init(bs->nzindex,bslearn->nzindex->size);
    list_int_copy(bslearn->nzindex, bs->nzindex);
    
    block_sol_assign(sys,ss,bl,bs);
    //block_sol_index_init(sys,ss,bl,bs);
    block_sol_substitute(sys,ss,bl,bs);
    nmod_mat_rref(bs->mat);
    block_sol_trans(sys,ss,bl,bs);
 
    block_sol_clear(bs);
}


void system_database_init(struct system *sys, char * dir)
{
    char *file =(char* )malloc(200);
    char *cha;
    cha = (char*)"red_common";
    file_name_join(file,dir,cha);    
    if(!file_existsQ(file)){
        printf("error: database not found!\n");
        exit(2);
    }
    
    FILE *fp = fopen(file,"r");
    if(fscanf(fp,"%llu %d %d %d", &sys->prime, &sys->kin_row, &sys->nints, &sys->nmis)!=4)
        exit(1);
    fclose(fp);
    
    sys->invp=n_preinvert_limb(sys->prime);
    
    free(file);
}

void system_kinematic_init(struct system *sys, char * dir)
{
    char *file =(char* )malloc(200);
    file_name_join(file,dir,(char*)"kin_common");
    if(!file_existsQ(file)){
        printf("error: kinematics not found!\n");
        exit(2);
    }
    
    FILE *fp = fopen(file,"r");
    if(fscanf(fp,"%d %d", &sys->kin_row, &sys->kin_col) !=2)
        exit(1);
    fclose(fp);
    
    sys->nparsin = sys->kin_col;
    
    file_name_join(file,dir,(char*)"kin_table");
    sys->kintable = &sys->kintable0;
    table_int_init(sys->kintable, sys->kin_row,sys->kin_col);
    table_int_read(sys->kintable, file);
    
    free(file);
    
}

void system_block_init(struct system *sys, char * dir, char *db)
{
    char *file =(char* )malloc(200);
    int i;
    
    directory_count_with_filename(dir,(char*)"sch_g1",&sys->nblocks);
    
    sys->bllist = (struct block **)malloc(sizeof(struct block *) * sys->nblocks);
    for(i =0; i< sys->nblocks; i++){
        *(sys->bllist + i) = (struct block *)malloc(sizeof(struct block));
        memset(*(sys->bllist + i), 0, sizeof(struct block));
        
        sprintf(file,"%s/%d",dir,i);
        block_init(*(sys->bllist + i), file, db);
        
    }
    
    free(file);
}

void system_intid2blockid_init(struct system *sys)
{
    int i,j;
    sys->intid2blockid = (struct list_int *)malloc(sizeof(struct list_int));
    struct list_int * id2bl = sys->intid2blockid;
    
    id2bl->size = sys->nints;
    id2bl->entry = (int *)malloc(sizeof(int) * id2bl->size);
    memset(id2bl->entry,-1, sizeof(int) * id2bl->size);
        
    //int * block_register =  (int*)malloc(sizeof(int)*sys->nblocks);
    for(i=0;i<sys->nblocks;i++){
        struct block *bl = *(sys->bllist+i);
        int globalid = *(bl->intid->entry);
        int regid = *(id2bl->entry + globalid);
        if(regid <0){   //not registered yet
            for(j=0;j<bl->g1;j++){
                globalid = *(bl->intid->entry +j);
                *(id2bl->entry+globalid) = i;
            }
        }
        else{ //if an integral belongs to more than one block, choose the bigger block, omit the smaller one
            struct list_int *intidnew = (struct list_int*)malloc(sizeof(struct list_int));
            list_int_init(intidnew,bl->g1);
            for(j=0;j<bl->g1;j++){
                *(intidnew->entry+j) = *(bl->intid->entry+j);
            }
            
            struct block *blold = *(sys->bllist+regid); 
            struct list_int *intidold = (struct list_int*)malloc(sizeof(struct list_int));
            list_int_init(intidold,blold->g1);
            for(j=0;j<blold->g1;j++){
                *(intidold->entry+j) = *(blold->intid->entry+j);
            }
            
            if(list_int_contains_all(intidold,intidnew)){
                
            }
            else if(list_int_contains_all(intidnew,intidold)){
                for(j=0;j<bl->g1;j++){
                    globalid = *(bl->intid->entry+j);
                    *(id2bl->entry+globalid) = i;
                }
            }
            else{ //otherwise, let unregisterd integrals  point to this block
                for(j = 0;j<intidnew->size;j++)
                {
                    globalid = *(intidnew->entry +j);
                    if(list_int_first_position(intidold,globalid)==-1)
                    {
                        *(id2bl->entry +globalid) = i;
                    }
                }
            }
            free(intidnew);
            free(intidold);
        }   
    }

    //more powerful to detect error (avoid segmentation fault)
    int posi = list_int_first_position(sys->intid2blockid,-1);
    if(posi < (sys->nints - sys->nmis))
    {
        printf("error: integral %d is not reduced",posi);
        exit(1);
    }
}

//input tarid, determine solve-blocks and solve-ordering
void system_register_needed_init(struct system *sys, struct list_int *need)
{
    sys->tarid = &sys->tarid0;
    list_int_init(sys->tarid,sys->nints);
    list_int_copy(need,sys->tarid);
    
    int i,j;
    struct list_int intneedQ0, *intneedQ= &intneedQ0;
    list_int_init(intneedQ,sys->nints);
    struct list_int blregisQ0, *blregisQ= &blregisQ0;
    list_int_init(blregisQ,sys->nblocks);

    //for an target integral, register its blockID(if the blockID is not registerd), append its dependent integralID into solveID
    //register integrals recursively until no integral is appened at all, which means the integral set is closed.
    for(i =0;i<need->size;i++){
        *(intneedQ->entry + *(need->entry+i) ) =1;
    }
    
    int continue_flag = 0;
    do{
        int cache_flag = 0;
        for(i =0; i<(sys->nints-sys->nmis);i++){//masters are excluded
            if(*(intneedQ->entry + i)){ //needed integral
                int blid = *(sys->intid2blockid->entry+ i);
                
                if(!(*(blregisQ->entry + blid))){ //registed once
                    struct block *bl = *(sys->bllist+blid);
                    int iid;
                    for(j=0;j<bl->nint;j++){
                        iid =*(bl->intid->entry+j);
                        *(intneedQ->entry + iid) = 1;
                    }
                
                    *(blregisQ->entry+blid) =1;
                    cache_flag =1;
                }
            }
        }
        if(cache_flag){
            continue_flag = 1;
        }
        else{
            continue_flag =0;
        }
        
    }while(continue_flag);
    
    //get registerd blocks
    int nblregis = list_int_count(blregisQ,1);
    sys->regisbl = &sys->regisbl0;
    list_int_init(sys->regisbl,nblregis);
    int counter=0;
    for(i=0;i<blregisQ->size;i++){
        if(*(blregisQ->entry + i)){
            *(sys->regisbl->entry+counter) = i;
            counter++;
        }
    }
    
    list_int_clear(blregisQ);
    list_int_clear(intneedQ);
}

int block_solvableQ(struct block *bl, struct list_int *intsolveQ)
{
    int i;
    for(i =bl->g1;i<bl->nint;i++)
    {
        int id = *(bl->intid->entry+i);
        if(!(*(intsolveQ->entry+id))){
            return 0;
        }
    }
    return 1;
}

void append_solve_block(struct block *bl, struct list_int *intsolveQ)
{
    int i;
    for(i =0; i<bl->g1;i++)
    {
        int id = *(bl->intid->entry+i);
        *(intsolveQ->entry+id)=1;
    }
}

void system_solve_ordering_init(struct system *sys)
{
    sys->ordering = &sys->ordering0;
    list_int_init(sys->ordering,sys->regisbl->size);
    
    struct list_int * intsolveQ =(struct list_int*)malloc(sizeof(struct list_int));
    list_int_init(intsolveQ,sys->nints);
    //initialize masters
    int i,j;
    for(i=sys->nints-sys->nmis; i <sys->nints; i++){
        *(intsolveQ->entry+i)=1;
    }
    
    //0: unsolved, 1: solvable but not registerd, -1: registered
    struct list_int *flag = (struct list_int *)malloc(sizeof(struct list_int));
    list_int_init(flag,sys->regisbl->size);
    struct block *bl; //here, bl is only a pointer, do not need malloc;
    
    for(i=0;i<sys->regisbl->size;i++){
        
        //detect solvable
        for(j =0; j<sys->regisbl->size;j++)
        {
            switch(*(flag->entry+j)){
                case -1:
                  break;
                case 1:
                  break;
                default :
                  bl = *(sys->bllist + *(sys->regisbl->entry+j));
                  *(flag->entry+j) = block_solvableQ(bl,intsolveQ);
                  break;
            }
        }
        
        int posi = list_int_first_position(flag,1);
        if(posi == -1){
            printf("error: the registerd block is unsolvable!");
            exit(1);
        }
        
        *(sys->ordering->entry+i)=*(sys->regisbl->entry+posi);
        bl = *(sys->bllist + *(sys->regisbl->entry+posi));
        append_solve_block(bl,intsolveQ);
        *(flag->entry+posi)=-1;
    }
    
    list_int_clear(intsolveQ);
    list_int_clear(flag);
}


//load block-triangular system 
//dir: ..../search
void system_init(struct system *sys, char *dir, char * db, char *job)
{
    char *file =(char* )malloc(200);
    //database
    sprintf(file,"%s/database/%s",dir,db);
    system_database_init(sys,file);
    
    //kintable
    sprintf(file,"%s/kinematics/%s",dir,job);
    system_kinematic_init(sys,file);
    
    //block-triangular-system
    sprintf(file,"%s/%s",dir,job);
    system_block_init(sys,file, db);
    
    system_intid2blockid_init(sys);

    
    free(file);
}

int compute_blocks_number(char **files, int nfiles)
{
    int i;
    int cnt=0;
    for(i =0; i<nfiles; i++)
    {
        int tmp;
        directory_count_with_filename(files[i],(char*)"sch_g1",&tmp);
        cnt +=tmp;
    }
    
    return cnt;
}


//load block-triangular system from txt file.
//  elements in txt is : prime nparsin n_integrals n_masters n_needed_integrals needed n_block_dirs block_dirs dataname
void system_init_from_txt(struct system *sys, char *filename)
{

    if(!file_existsQ(filename)){
        printf("error: txt file not found!\n");
        exit(2);
    }
    
    //database info
    FILE *fp = fopen(filename,"r");
    if(fscanf(fp, "%llu %d %d %d",&sys->prime, &sys->kin_row, &sys->nints, &sys->nmis) != 4)
        exit(1);
//    fscanf(fp,"%llu", &sys->prime);
//    fscanf(fp,"%d", &sys->kin_row);
//    fscanf(fp,"%d", &sys->nints);
//    fscanf(fp,"%d", &sys->nmis);
    sys->invp=n_preinvert_limb(sys->prime);
    
    
    //load needed integrals info 
    struct list_int *needid = (struct list_int *)malloc(sizeof(struct list_int));
    int ntar;
    int i,j;
    if(!fscanf(fp,"%d", &ntar))
        exit(1);    
    list_int_init(needid,ntar);
    for(i=0;i<needid->size;i++){
        if(!fscanf(fp,"%d", needid->entry+i))
            exit(1);
    }

    
    //block-triangular system directories
    int nfiles;
    if(!fscanf(fp,"%d", &nfiles))
        exit(1);
    char * string = (char *)malloc(1000000);
  	while(!feof(fp)){
  		if(!fscanf(fp,"%s", string)){
            exit(1);
        }
  	}
  	fclose(fp);

  	//directories are divided by ','
    char * files[nfiles];
    char * db = (char*)malloc(200);
    char * name; 
    name = strsep(&string,",");
    for(i =0; i<nfiles;i++)
    {
    	files[i]=name;
    	name = strsep(&string,",");
    }
    strcpy(db,name);

    //extract job name from the first job, used in system_kinematic_init
    char * job = (char*)malloc(200);
    char * qqq = (char*)malloc(200);
    strcpy(qqq, files[0]);
    while((name = strsep(&qqq,"/"))){
         strcpy(job,name);
    }
    free(qqq);
    name = (char *)malloc(200);
    
    //read kintable from the first file
    sprintf(name,"%s/../kinematics/%s",files[0],job);
    system_kinematic_init(sys,name);
    
    //load blocktriangular system
    sys->nblocks= compute_blocks_number(files,nfiles);
    
    sys->bllist = (struct block **)malloc(sizeof(struct block *) * sys->nblocks);
    int blid =0;
    for(i=0; i<nfiles; i++){
        int tmp;
        directory_count_with_filename(files[i],(char*)"sch_g1",&tmp);
        
        for(j=0;j<tmp;j++){
            *(sys->bllist + blid) = (struct block *)malloc(sizeof(struct block));
            memset(*(sys->bllist + blid), 0, sizeof(struct block));
        
            sprintf(name,"%s/%d",files[i],j);
            block_init(*(sys->bllist + blid), name, db);
            blid++;
        }
    }
    
    //register all integrals
    system_intid2blockid_init(sys);
    
    //register needed 
    system_register_needed_init(sys,needid);
    
    
    
    free(string);
    free(db);
    free(job);
    free(name);
    list_int_clear(needid);
    
}

//analogous to system_init_from_txt except this function would parallel load blocks
//load block-triangular system from txt file.
//  elements in txt is : prime nparsin n_integrals n_masters n_needed_integrals needed n_block_dirs block_dirs dataname
void system_init_from_txt_parallel(struct system *sys, char *filename, unsigned nthreads)
{

    if(!file_existsQ(filename)){
        printf("error: txt file not found!\n");
        exit(2);
    }
    
    //database info
    FILE *fp = fopen(filename,"r");
    if(fscanf(fp, "%llu %d %d %d",&sys->prime, &sys->kin_row, &sys->nints, &sys->nmis) != 4)
        exit(1);
//    fscanf(fp,"%llu", &sys->prime);
//    fscanf(fp,"%d", &sys->kin_row);
//    fscanf(fp,"%d", &sys->nints);
//    fscanf(fp,"%d", &sys->nmis);
    sys->invp=n_preinvert_limb(sys->prime);
    
    
    //load needed integrals info 
    struct list_int *needid = (struct list_int *)malloc(sizeof(struct list_int));
    int ntar;
    int i,j;
    if(!fscanf(fp,"%d", &ntar))
        exit(1);    
    list_int_init(needid,ntar);
    for(i=0;i<needid->size;i++){
        if(!fscanf(fp,"%d", needid->entry+i))
            exit(1);
    }

    
    //block-triangular system directories
    int nfiles;
    if(!fscanf(fp,"%d", &nfiles))
        exit(1);
    char * string = (char *)malloc(1000000);
    while(!feof(fp)){
        if(!fscanf(fp,"%s", string)){
            exit(1);
        }
    }
    fclose(fp);

    //directories are divided by ','
    char * files[nfiles];
    char * db = (char*)malloc(200);
    char * name; 
    name = strsep(&string,",");
    for(i =0; i<nfiles;i++)
    {
        files[i]=name;
        name = strsep(&string,",");
    }
    strcpy(db,name);

    //extract job name from the first job, used in system_kinematic_init
    char * job = (char*)malloc(200);
    char * qqq = (char*)malloc(200);
    strcpy(qqq, files[0]);
    while((name = strsep(&qqq,"/"))){
         strcpy(job,name);
    }
    free(qqq);
    name = (char *)malloc(200);
    
    //read kintable from the first file
    sprintf(name,"%s/../kinematics/%s",files[0],job);
    system_kinematic_init(sys,name);
    
    //load blocktriangular system
    sys->nblocks= compute_blocks_number(files,nfiles);
    
    sys->bllist = (struct block **)malloc(sizeof(struct block *) * sys->nblocks);
    
    //set block file name
    char** blname=(char **)malloc(sizeof(char*) * sys->nblocks);
    int blid =0;
    for(i=0; i<nfiles; i++){
        int tmp;
        directory_count_with_filename(files[i],(char*)"sch_g1",&tmp);
        for(j=0;j<tmp;j++){
            *(blname+blid)=(char *)malloc(200);
            sprintf(*(blname+blid),"%s/%d",files[i],j);
            blid++;
        }
        
    }
    
    //alloc memory
    for(i=0; i<sys->nblocks;i++){
        *(sys->bllist + i) = (struct block *)malloc(sizeof(struct block));
        memset(*(sys->bllist + i), 0, sizeof(struct block));
    }
    
    //parallel load blocks
    #pragma omp parallel for num_threads(nthreads) \
    schedule(static,1) default(none) shared(sys,blname,db,stdout)
    for(i=0; i<sys->nblocks;i++){
        block_init(*(sys->bllist + i), *(blname+i), db);
    }
    
    //register all integrals
    system_intid2blockid_init(sys);
    
    //register needed 
    system_register_needed_init(sys,needid);
    
    
    
    free(string);
    free(db);
    free(job);
    free(name);
    list_int_clear(needid);
    
}


//at one phase point. 
void system_sol_kinematic_init(struct system *sys, struct system_sol *ss, struct list_ulong *pt)
{
    ss->kinlist = &ss->kinlist0;
    list_ulong_init(ss->kinlist,sys->kin_row);
    struct list_int kincfg0, *kincfg = &kincfg0;
    int i, j;

    for(i =0; i< sys->kin_row; i ++)
    {
        unsigned long long prod=1;
        table_int_picklist(sys->kintable,i,kincfg);
        for(j=0;j<sys->kin_col; j ++){
            prod = n_mulmod2_preinv(prod, n_powmod2_preinv(*(pt->entry+j),*(kincfg->entry+j),sys->prime,sys->invp),sys->prime,sys->invp);
        }
        *(ss->kinlist->entry + i) =prod;
    }
}

//if siize ==0, is->nmis, is->index->size, is->elem->size would be set to be 0
//but no extra memory would alloc
void integral_sol_init(struct integral_sol *is, int size)
{
    is->nmis=size;
    is->index = &is->index0;
    list_int_init(is->index,size);
    
    is->elem = &is->elem0;
    list_ulong_init(is->elem,size);
    
}


void integral_sol_clear(struct integral_sol *is)
{
    if(is->nmis ==0){
        
    }
    else{
        list_int_clear(is->index);
        list_ulong_clear(is->elem);
        is->nmis=0;
    }
    //important!!!
    free(is);
}

//malloc illist
void system_sol_integral_init(struct system *sys, struct system_sol *ss)
{
    ss->nints = sys->nints;
    ss->islist = (struct integral_sol**)malloc(sizeof(struct integral_sol *) * sys->nints);
    int i;

    for(i=0; i<sys->nints; i++){
        *(ss->islist + i) = (struct integral_sol *)malloc(sizeof(struct integral_sol));
        //memset(*(ss->islist+i),0,sizeof(struct integral_sol));
        integral_sol_init(*(ss->islist+i), 0);
    }
    
    //masters project to themself
    for(i =sys->nints - sys->nmis; i< sys->nints; i++){
        struct integral_sol * is = *(ss->islist +i);
        integral_sol_init(is,1);
        *(is->index->entry) = i;
        *(is->elem->entry) = 1;
    }
}

void system_sol_cache_info(struct system *sys, struct system_sol *ss)
{
    sys->islist = (struct integral_sol **)malloc(sizeof(struct integral_sol *)*sys->nints);
    sys->nparsout = 0;
    int i;

    for(i =0; i<sys->tarid->size; i++){
        int globalid = *(sys->tarid->entry+ i);
        struct  integral_sol *ssis = *(ss->islist+globalid);
        
        *(sys->islist+globalid) = (struct integral_sol *)malloc(sizeof(struct integral_sol));
        integral_sol_init(*(sys->islist+globalid),ssis->nmis);
        list_int_copy(ssis->index,(*(sys->islist+globalid))->index);     
        list_ulong_clear((*(sys->islist+globalid))->elem);

        sys->nparsout += ssis->nmis;
    }
}


void system_sol_clear( struct system_sol *ss)
{
    list_ulong_clear(ss->kinlist);
    int i;

    for(i=0; i<ss->nints; i++){
        integral_sol_clear(*(ss->islist+i));
    }
    
    free(ss->islist);
}

//learn once for all.(no matter whether trimming)
void system_learn(struct system *sys)
{   
    //cache 
    sys->bslist = (struct block_sol **)malloc(sizeof(struct block_sol *) * sys->nblocks);
    
    struct system_sol ss0, *ss=&ss0;
    struct list_ulong pts0, * pts=&pts0;
    int i;
    
    list_ulong_init(pts,sys->kin_row);
    list_ulong_random(pts,sys->prime);
    
    system_sol_kinematic_init(sys,ss,pts);
    system_sol_integral_init(sys, ss);

    for(i =0;i<sys->ordering->size;i++){
        int blid = *(sys->ordering->entry +i);
        block_learn(sys,ss,blid);
    }    
    
    sys->trimmingQ = 0; //full output;
    
    system_sol_cache_info(sys,ss);
    system_sol_clear(ss);
    
    list_ulong_clear(pts);
    

}

//this function transform system_sol to data. data's structure is the same as FFGraphEvaluate[graph,ps]//DeleteCases[#,0]&
//FFGraphEvaluate[graph,ps] == Length[sys->tarid] * Length[sys->nmis]
//however, zero elements may appear if system_trimming_from_points is applied.
//data should be initialied
void cache_eval(struct system *sys, struct system_sol *ss, struct list_ulong *data)
{
    int idx =0; 
    int i,j;
    int globalid; 
    struct integral_sol *is;
    
    if(sys->trimmingQ){
        for(i=0;i<sys->tarid->size;i++){
    
            globalid = *(sys->tarid->entry+ i);
            
            if(*(sys->output_flag->entry+i)){
                is = *(ss->islist+globalid);
                for(j=0;j<is->nmis;j++){
                    *(data->entry+idx)=*(is->elem->entry+j);
                    idx++;
                }
            }
            else{
                is = *(sys->islist+globalid); //learned
                //for(j=0;j<is->nmis;j++){
                //    *(data->entry+idx)=0;
                //    idx++;
                //}
                idx += is->nmis;
            }   
        }
    }
    else{
        for(i=0;i<sys->tarid->size;i++){
            
            int globalid = *(sys->tarid->entry+ i);
            is = *(ss->islist+globalid);
            for(j=0;j<is->nmis;j++){
                *(data->entry+idx)=*(is->elem->entry+j);
                idx++;
            }
        }
    }
}

void system_evaluate(struct system *sys, struct list_ulong *pts, struct list_ulong *data)
{
    int i;
    struct system_sol ss0, *ss=&ss0;
    //compute kinematic terms once
    system_sol_kinematic_init(sys,ss,pts);
    //set master integrals to unit-vector
    system_sol_integral_init(sys, ss);
    //solve block-by-block
    for(i =0;i<sys->ordering->size;i++){
        int blid = *(sys->ordering->entry +i);
        block_solve(sys,ss,blid);
    }


    //reorganize system_sol to expected form (reduction table in sparsform)
    cache_eval(sys,ss,data);     
    //clear 
    system_sol_clear(ss);    
}

void system_needed_clear(struct system *sys)
{
    list_int_clear(sys->tarid);
    list_int_clear(sys->regisbl);
    list_int_clear(sys->ordering);
}



void system_sol_dump_info(char * dir,struct system *sys)
{
    char * string =(char *)malloc(200);
    struct integral_sol *is;
    int i,j;
    
    file_name_join(string,dir,(char*)"index");
    FILE *fp2 =fopen(string,"w");
    
    file_name_join(string,dir,(char*)"length");
    FILE *fp3 =fopen(string,"w");
    
    for(i=0;i<sys->tarid->size;i++){
        int globalid = *(sys->tarid->entry+i);
        is = *(sys->islist +globalid);
        for(j =0; j<is->nmis;j++){
            fprintf(fp2,"%d ", *(is->index->entry+j));
        }
        fprintf(fp3,"%d ",is->nmis);
    }
    
    fclose(fp2);
    fclose(fp3);
    
    free(string);
}

//after system_learn. solve at random numerical point and write average sample time.
void system_average_sample_time(struct system *sys, char * dir)
{    
    char * string =(char *)malloc(200);
    file_name_join(string,dir,(char*)"average_time");
    clock_t sta, fin;
    int i;
    
    
    struct list_ulong pts0, * pts=&pts0;
    struct list_ulong data0, *data = &data0;
    
    list_ulong_init(pts,sys->kin_row);
    list_ulong_random(pts,sys->prime);
    list_ulong_init(data,sys->nparsout);
    
    sta=clock();
    for(i=0;i<3;i++){
        system_evaluate(sys,pts,data);
    }
    fin=clock();
    
    FILE *fp =fopen(string,"w");
    fprintf(fp,"%f\n",(double)(fin-sta)/CLOCKS_PER_SEC/3);
    fclose(fp);
    
    free(string);
    list_ulong_clear(pts);
    list_ulong_clear(data);
}
