(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLIBPSystem::usage = "BLIBPSystem[top_,rank_,dot_,exints_:{},userdefined_:{}] generate IBP system. 
\"top\": topsector, e.g. {1, 1, 1, 1, 0, 0}.
\"rank\": the maximal negative sum of negative propagator powers in the seed.
\"dot\": the maximal number of dots. (dots equal to the sum of positive propagator powers minus the number of propagators with positive integer exponents. 
\"exints\": a list of preferred master integrals, can be linear combination of Feynman integrals, e.g. {ex[1]->BL[topo,...]+BL[topo,...],ex[2]->...}
\"userdefined\": a list of target expression, can be linear combination of Feynman integrals, e.g. {us[1]->BL[topo,...]+BL[topo,...], us[2]->BL[topo,...]...}

\"CutNoDot\": do no generate ibp seeds with dots on cut propagators.
\"FilterLevel\": filter ibp seeds with lower rank of subsectors: FilterLevel euqal to 3 means top-sector\[Rule]{d,r}, sub-sector\[Rule]{d,r}, sub-sub-sector\[Rule]{d,r}, sub-sub-sub(...)-sectors\[Rule]{d,Max[r-1,LowerLimit]}.
LowerLimit is determined by the maximal value of BLSetReducerOptions[\"BlackBoxRank\"->r1] and BLSetSchemeOptions[\"MinimalSchemeRank\"->r2]. \
It is advisable to set \"FilterLevel\"  when \"DivideLevel\" is greater than 0. For \"DivideLevel\", refer to BLReduce.
\"IBPForMapSector\": generate IBP&LI identities for MappedSectors.
\"CloseSymmetry\": do not generate symmetry identities( IBPForMapSectors would turn on automatically).
\"OverWritten\": overwrite old files";


Begin["`Private`"];
TopologyTemplate;
End[];


Begin["`IBP`"];


(* ::Section:: *)
(*topology*)


Options[TopologyTemplate]={"CloseSymmetry"->False};
TopologyTemplate[dir_,OptionsPattern[]]:=Module[{template,rule},
template = StringTemplate[StringJoin@Riffle[{
"SetDim[`spacetimedim`];",
"Internal = `loop`;",
"External = `leg`;",
"MomentumConservation = `momcon`;",
"Replacements = `rep`;",
"Propagators = `prop`/. MomentumConservation//Expand;",
"NewBasis[`fam`,Propagators,Internal, External, MomentumConservation, Replacements,\"ExtraIntDeriv\"->`extraintderiv`];",
"AnalyzeSectors[`fam`,`topsec`,\"CutDs\"->`cut`,\"Prescription\"->`prescription`,\"CloseSyms\"->`closesyms`];"
},"\n"]];

rule =<|
"spacetimedim"->toStringInput[BLDimension],
"loop"->toStringInput[BLLoop],
"leg"->toStringInput[BLLeg],
"momcon"->toStringInput[BLMomCons],
"rep"->toStringInput[BLSPToSTU],
"fam"->toStringInput[BLFamily],
"topsec"->toStringInput[BLTopSector],
"cut" -> toStringInput[BLCut],
"prescription" ->toStringInput[BLPrescription],
"closesyms" ->toStringInput[OptionValue["CloseSymmetry"]],
"prop" -> toStringInput[BLPropagators],
"extraintderiv" -> toStringInput[BLExtraIntDerivDen]
|>;

FileTemplateApply[template,rule,FileNameJoin[{dir,"topology.wl"}]];
];


(* ::Section:: *)
(*ibp*)


Options[FastIBPTemplate]={"CutNoDot"->False,"FilterLevel"->Infinity,"IBPForMapSector"->False,"LIForLowerRank"->True,"CloseSymmetry"->False,"SectorSeeds"->None,"JoinFamilyQ"->False};
FastIBPTemplate[exints_,userdefined_,dir_,OptionsPattern[]]:=Module[{template,usints,ibpvars,rule,filterlevel,learn},
template = StringTemplate[StringJoin@Riffle[{
"AppendTo[$Path,\"`bladeibp`\"];",
"AppendTo[$Path,\"`ff`\"];",
"AppendTo[$LibraryPath,\"`fflib`\"];",
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;",
"<<BladeIBP.m;",
"<<FiniteFlow.m",
"<<topology.wl;",

"IntegralOrdering = `integralordering`;",
"CreateDirectory[\"results\"];
Put[ZeroSectors[`fam`],\"results/zerosectors\"];",
"If[NonZeroSectors[`fam`]==={}, Print[\"this family is trivial.\"];Quit[]];",

"\nModule[{rank,maxdots,GetSectors,GetSeeds,cutposi,learn,maplearn,filters,prefix,famcount,selectedfiles,lirankmin},\n",
"rank=`ibprank`;",
"maxdots=`ibpdot`;",

Switch[{OptionValue["IBPForMapSector"],OptionValue["CloseSymmetry"]},
{_,True},
"GetSectors[\"IBP\"]:=NonZeroSectors[`fam`];
GetSectors[\"LI\"]=NonZeroSectors[`fam`];
GetSectors[\"SR\"]={};
GetSectors[\"Map\"]={};
GetSectors[\"ExtMap\"]={};",
{True,False},
"GetSectors[\"IBP\"]:=NonZeroSectors[`fam`];
GetSectors[\"LI\"]=NonZeroSectors[`fam`];
GetSectors[\"SR\"]=UniqueSectors[`fam`];
GetSectors[\"Map\"]=MappedSectors[`fam`];
GetSectors[\"ExtMap\"]=If[TrueQ[#[[0]]==List],#,{}]&[ExtMappedSectors[`fam`]];",
{False,True},
"GetSectors[\"IBP\"]:=UniqueSectors[`fam`];
GetSectors[\"LI\"]=UniqueSectors[`fam`];
GetSectors[\"SR\"]={};
GetSectors[\"Map\"]={};
GetSectors[\"ExtMap\"]={};",
{False,False},
"GetSectors=Automatic;"],

"cutposi=Position[`cut`,1]//Flatten;",
"learn=`learn`;",
"maplearn=Map[{1,0}+#&,learn//Association]//Normal;",
"filters[learn_]:=Join[If[learn=!={},{If[-Plus@@Select[#[[2]],#<0&]>(Length@Select[#[[2]],#1>0&]/.learn)[[2]],False,True]&,
If[(Plus@@Select[#[[2]],#>0&]-Length@Select[#[[2]],#>0&])>(Length@Select[#[[2]],#1>0&]/.learn)[[1]],False,True]&},{}],
If[`cutnodot`,{If[(#[[2]][[cutposi]]=!=ConstantArray[1,Length[cutposi]]),False,True]&},{}]];",

Switch[OptionValue["LIForLowerRank"],
True,"lirankmin[_]:=0;",
_, "lirankmin[sector_]:=Min[If[learn=!={},(Length@Select[sector[[2]],#1>0&]/.learn)[[2]],rank],rank];"],

Switch[OptionValue["SectorSeeds"],
None,
"GetSeeds[\"IBP\"][sector_]:=GenSeeds[sector,{0,maxdots},0,rank,filters[learn]];
GetSeeds[\"LI\"][sector_]:=GenSeeds[sector,{0,maxdots},lirankmin[sector],rank,filters[learn]];
GetSeeds[\"SR\"][sector_]:=GenSeeds[sector,{0,0},0,rank,filters[learn]];
GetSeeds[\"Map\"][sector_]:=GenSeeds[sector,{0,maxdots+1},0,rank,filters[maplearn]];
GetSeeds[\"ExtMap\"][sector_]:=GenSeeds[sector,{0,maxdots+1},0,rank];",
_,
"JSectorOrSubsector[jsec_]:=Select[NonZeroSectors[jsec[[1]]],SectorOrSubsectorQ[#,jsec]&];
secseeds=`sectorseeds`;
secseeds=Thread[Rule[JSectorOrSubsector[IntSector[#[[1]]]],#[[2]]],List,{1}]&/@secseeds//Flatten;
secseeds=GroupBy[secseeds,First\[Rule]Last,Max/@Transpose[#]&]//Normal;
symmap=Thread[MappedSectors[`fam`]\[Rule]SectorRelations[MappedSectors[`fam`]]];
secseeds=GroupBy[Join[secseeds/.symmap,secseeds],First->Last,Max/@Transpose[#]&];
GetSeeds[\"IBP\"][sector_]:=GenSeeds[sector,{0,(sector/.secseeds)[[2]]},0,(sector/.secseeds)[[1]]];
GetSeeds[\"LI\"][sector_]:=GenSeeds[sector,{0,(sector/.secseeds)[[2]]},(sector/.secseeds)[[1]],(sector/.secseeds)[[1]]];
GetSeeds[\"SR\"][sector_]:=GenSeeds[sector,{0,0},0,(sector/.secseeds)[[1]]];
GetSeeds[\"Map\"][sector_]:=GenSeeds[sector,{0,(sector/.secseeds)[[2]]+1},0,(sector/.secseeds)[[1]]];
GetSeeds[\"ExtMap\"][sector_]:=GenSeeds[sector,{0,(sector/.secseeds)[[2]]+1},0,(sector/.secseeds)[[1]]];"],

(*join independent equations in sub-families*)
Switch[OptionValue["JoinFamilyQ"],
False,
"FastGenIds[`fam`,GetSeeds,\"Directory\"->\"ibps\",\"LaunchKernels\"->`ibpkernel`,\"GetSectors\"->GetSectors];",
_,
"prefix=Directory[];
If[!DirectoryQ[#],CreateDirectory[#]]&@FileNameJoin[{prefix,\"ibps\"}];
famcount=1;
While[DirectoryQ[prefix<>\"_\"<>ToString[famcount]],famcount++];
famcount--;                                                                    
Do[
SetDirectory[prefix<>\"_\"<>ToString[sys]];

(*select needed equations ID*)
Block[{files,nids,indep,bins,localindep},
files=FileNames@\"ibps/sids_*.json\";
nids=Get/@StringReplace[files,{\"sids_\"\[Rule]\"nids_\",\".json\"\[Rule]\".m\"}];
files=Pick[files,nids,_?Positive];
nids=DeleteCases[nids,_?NonPositive];
indep = Get[FileNameJoin[{Directory[],\"results/indep\"}]];
bins=Prepend[Accumulate[nids],0];
localindep=BinLists[indep,{bins+1}] - Most[bins];
(*indep_*.m*)
selectedfiles=Table[If[Length@localindep[[i]]>0,Export[StringReplace[files[[i]],{\"sids_\"->\"indep_\",\".json\"->\".mx\"}],localindep[[i]],\"MX\"];files[[i]],Nothing]
,{i,Length@files}];
];

(*reset directory. for SerializeFastIds*)
SetDirectory[prefix];

(*ids_*_system*.mx, ints_*_system*.mx*)
ParallelDo[
	SetDirectory[prefix<>\"_\"<>ToString[sys]];
	Print[\"System: \", sys, \" File: \",file];
	Print[\"System: \", sys, \" File: \", file, \" done in: \",AbsoluteTiming[Module[{eqs,ppindep},
		eqs=Import[StringReplace[file,{\"sids_\"\[Rule]\"ids_\",\".json\"\[Rule]\".mx\"}]];
		ppindep=Import[StringReplace[file,{\"sids_\"->\"indep_\",\".json\"->\".mx\"}]];
		eqs=eqs[[ppindep]];
		Export[FileNameJoin[{prefix,StringReplace[file,{\"sids_\"\[Rule]\"ids_\",\".json\"\[Rule]\"_SYS\"<>ToString[sys]<>\".mx\"}]}],eqs,\"MX\"];
		Export[FileNameJoin[{prefix,StringReplace[file,{\"sids_\"\[Rule]\"ints_\",\".json\"\[Rule]\"_SYS\"<>ToString[sys]<>\".mx\"}]}],IntegralsIn[eqs],\"MX\"];]]
	];
	SetDirectory[prefix];
,{file,selectedfiles},DistributedContexts->All,Method\[Rule]\"FinestGrained\"];

,{sys,1,famcount}];"
],
(*module*)
"];",

"\nModule[{exints,usints},\n",
"exints = `exints`;
Export[\"ibps/exints_def.m\",exints];
Export[\"ibps/ids_`fam`_exints.mx\",(-#[[1]]+#[[2]])&/@exints,\"MX\"];
Export[\"ibps/ints_`fam`_exints.mx\",IntegralsIn[exints],\"MX\"];",

"usints = `usints`;
Export[\"ibps/usints_def.m\",usints];
Export[\"ibps/ids_`fam`_usints.mx\",(-#[[1]]+#[[2]])&/@usints,\"MX\"];
Export[\"ibps/ints_`fam`_usints.mx\",IntegralsIn[usints],\"MX\"];",

"
If[!(`ibpvars`==={}),SerializeFastIds[FileNames[\"ibps/ids_`fam`_*.mx\"], `ibprule`, `ibpvars`, \"UserDefinedInts\"->(First/@usints),\"ExtraInts\"->(First/@exints)]];",
(*module*)
"];",
"CloseKernels[];Pause[0.1];",
"Quit[];"
},"\n"]];

filterlevel=OptionValue["FilterLevel"];
learn=If[filterlevel===Infinity,{},
Table[If[count<filterlevel,Rule[Count[BLTopSector,Except[0]]-count,{IBPDot,IBPRank}],Rule[_,{IBPDot,Max[IBPRank-1, $BlackBoxRank, $MinimalSchemeRank]}]],{count,0,filterlevel}]];

rule = <|
"bladeibp"->$BLBladeIBPPath,
"ff"->$BLFiniteFlowPath,
"fflib"->$BLFiniteFlowLibraryPath,
"integralordering"->toStringInput[$IntegralOrdering],
"ibprank"->toStringInput[IBPRank],
"ibpdot"->toStringInput[IBPDot],
"fam"->toStringInput[BLFamily],
"cut"->toStringInput[BLCut],
"learn"->toStringInput[learn],
"cutnodot"->OptionValue["CutNoDot"],
"ibpkernel"->toStringInput[BLNthreads],
"usints" ->toStringInput[userdefined],
"exints" -> toStringInput[exints],
"ibprule"->toStringInput[BLRules],
"ibpvars"->toStringInput[BLIBPParameter],
"sectorseeds"->toStringInput[OptionValue["SectorSeeds"]]
|>;

FileTemplateApply[template,rule,FileNameJoin[{dir,"ibp.wl"}]];
];


(* ::Section:: *)
(*BLIBPSystem*)


(*"CutNoDot": do no generate ibp seeds with dots for cut propagators*)
(*"FilterLevel": filter ibp seeds, decrease rank of subsectors: FilterLevel euqal to 3 means top-sector\[Rule]{d,r}, sub-sector\[Rule]{d,r}, sub-sub-sector\[Rule]{d,r}, sub-sub-sub(...)-sectors\[Rule]{d,Max[r-1,LowerLimit]}*)
(*"IBPForMapSector" generate IBP&LI identities for MappedSectors*)
(*"LIForLowerRank": generate LI identities for lower rank seeds*)
(*"CloseSymmetry": do not generate Map&SR identities( IBPForMapSectors would turn on automatically*)
(*"SectorSeeds": ibps seeds for individual sectors*)
Options[BLIBPSystem]=Join[Options[FastIBPTemplate],{"OverWritten"->True}];
BLIBPSystem[top_,rank_,dot_,exints_:{},userdefined_:{},OptionsPattern[]]:=Module[{dir,time,opt},
opt=Rule[#,OptionValue[BLIBPSystem,#]]&/@Keys[Options[BLIBPSystem]];
dir=$ReductionDirectory;
If[OptionValue["OverWritten"],DeleteDir[dir]];
CreateDir[dir];
BLTopSector = top ;
IBPRank = rank;
IBPDot = dot;
WriteMessage[{"Rank: ",IBPRank, ", Dots: ",IBPDot}];
TopologyTemplate[dir,Sequence@@FilterRules[opt,Options[TopologyTemplate]]];
FastIBPTemplate[exints,userdefined,dir,Sequence@@FilterRules[opt,Options[FastIBPTemplate]]];
BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"ibp.wl"}]}, "log"->FileNameJoin[{dir,"ibp.log"}]];
];



(* ::Section:: *)
(*End*)


End[];
