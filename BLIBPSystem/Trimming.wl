(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLDatabaseInfo::usage = "BLDatabaseInfo[target_,OptionsPattern[]] reduce target and trimming the IBP system.
\"SaveMemoryQ\": determine independent equations and collect independent equations separately could save a half memory";


Begin["`Private`"];
BasicCode;
LearnCode;
QuitCode;
DataCode;
DataSupCode;
End[];


Begin["`Trimming`"];


(* ::Section:: *)
(*trimming*)


(* ::Subsection:: *)
(*basic*)


BasicCode[]:={
"AppendTo[$Path,\"`bladeibp`\"];
AppendTo[$Path,\"`ff`\"];
AppendTo[$LibraryPath,\"`fflib`\"];
If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
If[!DirectoryQ[\"ibps\"], Put[{{}, {}}, \"results/table\"]; Abort[]];",
"<<BladeIBP.m;
<<FiniteFlow.m
<<topology.wl;",

"IntegralOrdering = `integralordering`;",
"
Module[
{resdir,usints,exints,all,target,eqsfiles,vars,eqs,ibplearn,mis,nonmis,indep,graph,in,ibps,depvarsorder,
time,pid,ps,eva,nonzero,sparseinfo, alleqs, fflearn, sol, rule,mem1,mem2},

resdir = \"results\";
If[!DirectoryQ@resdir,CreateDirectory[resdir]];",

"usints = Import[\"ibps/usints_def.m\"];
exints = Import[\"ibps/exints_def.m\"];
all = Join[First/@usints,GetAllInts[FileNames[\"ibps/ints_`fam`_*.mx\"]], First/@exints];
target = Get[\"target\"];
If[target === 0,
target = Select[all, IntRank[#]<=`s` && IntDots[#]<=`d` &],
target = Select[EliminateZeroSectors[target], #=!=0&]];
If[target === {}, Print[\"target is empty.\"];Put[{{},{}}, \"results/table\"];Abort[]];",

"eqsfiles = FileNames[\"ibps/sids_*.json\"];
vars = `ibpvars`;"
};


LearnCode[]:={
"If[!(vars==={}),WriteSystemJSON[eqsfiles, all, target, vars, \"FileName\"->\"system.json\"]];",
"mem1=MemoryAvailable[];",
"FFNewGraph[graph,in,vars];
If[!(vars==={}),
FFAlgJSONSparseSolver[graph,ibps,{in},\"system.json\"],
eqsfiles = FileNames[\"ibps/ids_*.mx\"];
eqs=Join@@((Import[#]/.`ibprule`)&/@eqsfiles);
eqs=Thread[eqs\[Equal]0];
FFAlgSparseSolver[graph,ibps,{in},{},eqs,all,\"NeededVars\"\[Rule]target]];
FFSolverOnlyHomogeneous[graph,ibps];
FFGraphOutput[graph,ibps];
ibplearn = FFSparseSolverLearn[graph,all];
{nonmis, mis} = {\"DepVars\", \"IndepVars\"}/.ibplearn;
Print[\"number of integrals\" -> Length/@{nonmis,mis}];
If[Length[mis]==0,Print[\"target is zero\"];
Put[If[!(vars==={}),Range[Total[Import/@(StringReplace[#1,{\"sids_\"->\"nids_\",\".json\"->\".m\"}]&)/@eqsfiles]],Length[eqs]],FileNameJoin@{resdir, \"indep\"}];
Put[{nonmis,mis,MappedSectors[`fam`],UniqueSectors[`fam`],ConstantArray[{},Length[nonmis]]}, FileNameJoin@{resdir, \"datainfo\"}];
Abort[]];"}


QuitCode[]:={
"FFDeleteGraph[graph];];
Quit[];"
};


(* ::Subsection:: *)
(*trimming*)


DataCode[] := {
"FFSparseSolverMarkAndSweepEqs[graph,ibps];
mem2=MemoryAvailable[];
Print[\"memory used -> \",(mem1-mem2)/Power[1024,3]//N,\"GB\"];
FFSparseSolverDeleteUnneededEqs[graph,ibps];
Print[\"number of independent eqs.\"->FFSolverNIndepEqs[graph,ibps]];
indep = FFSolverIndepEqs[graph,ibps];
Put[indep,FileNameJoin@{resdir, \"indep\"}];",

(*fix bug: ordering of depvars may differ before trimming and after trimming*)
"depvarsorder=FFNonZeroesGetInfo[graph,ibps];
Put[depvarsorder,FileNameJoin@{resdir, \"depvarsorder\"}];",

"time=AbsoluteTiming[FFGraphEvaluateMany[graph,RandomInteger[{1,FFPrimeNo[200]-1},{3,Length@vars}],\"NThreads\"\[Rule]1];];
Print[\"average sample time for ibps (single core) ->\"<>ToString[time[[1]]/3//N]];",

"pid=`pid`;
ps = RandomInteger[{1, FFPrimeNo[pid]}, Length[vars]];
eva = FFGraphEvaluate[graph, ps, \"PrimeNo\"->pid];
eva = Join[Partition[eva, Length[mis]], IdentityMatrix[Length[mis]]];
nonzero[l0_]:=Position[l0, Except[0], Heads->False]//Flatten;
sparseinfo = nonzero/@eva;

Put[{FFPrimeNo[pid], eva}, FileNameJoin@{resdir, \"data\"}];
Put[{nonmis,mis,MappedSectors[`fam`],UniqueSectors[`fam`],sparseinfo}, FileNameJoin@{resdir, \"datainfo\"}];
Put[usints,FileNameJoin@{resdir, \"usints\"}];
Put[exints,FileNameJoin@{resdir,\"exints\"}];"
}


DataSupCode[] := {
"If[!FileExistsQ[FileNameJoin@{resdir, \"indep\"}],Print[\"file not found -> indep\"];Abort[]];
indep = Get@FileNameJoin@{resdir, \"indep\"};
alleqs = Join@@(Import/@eqsfiles)[[All,2]];
alleqs = alleqs[[indep]];
Export[\"ibps/all.json\",{Length[alleqs],alleqs},\"RawJSON\",\"Compact\"->True];
FFSparseSystemToJSON[\"systemC.json\",Length[alleqs],all,vars,{\"ibps/all.json\"},\"NeededVars\"->target];"(*,

(*fix bug: check dependent-variables-ordering before trimming and after. if differ, FFAlgTake is needed*)
"
FFNewGraph[$InternalGraph, in, vars];
FFAlgJSONSparseSolver[$InternalGraph, ibps, {in}, \"systemC.json\"];
FFSolverOnlyHomogeneous[$InternalGraph, ibps];
FFGraphOutput[$InternalGraph, ibps];
fflearn=FFLearn[$InternalGraph];

depvarsorder=Get@FileNameJoin@{resdir, \"depvarsorder\"};
Block[{pos,trimposi,trimelems,take2internal},
pos=Association[{}];
Do[pos[fflearn[[1,i]]]=i-1,{i,1,Length@fflearn[[1]]}];
trimposi=(depvarsorder[[1,2]]/.Dispatch[pos])+1;
If[trimposi=!=Range[Length@trimposi],
	trimelems={Range[Length@fflearn[[1]]*Length@fflearn[[2]]]}->Flatten[(Range[1+(#-1)*Length@fflearn[[2]],#*Length@fflearn[[2]]]&/@trimposi)];
	take2internal=Symbol[`algtake`];
	Export[\"systemCsup.json\",Flatten@take2internal[trimelems],\"RawJSON\",\"Compact\"->True];];];
"*)}


(* ::Subsection:: *)
(*template*)


Options[DatabaseInfoTemplate]={"SaveMemoryQ"->True,"PrimeNo"->0};
DatabaseInfoTemplate[dir_,OptionsPattern[]]:=Module[{extra,template,ibpvars,rule},

If[
!OptionValue["SaveMemoryQ"],
	template = StringTemplate@StringJoin@Riffle[Join[BasicCode[],LearnCode[],DataCode[],DataSupCode[],QuitCode[]],"\n\n"],
	template = StringTemplate@StringJoin@Riffle[Join[BasicCode[],LearnCode[],DataCode[],QuitCode[]],"\n\n"]];

rule = <|
"bladeibp"->$BLBladeIBPPath,
"ff"->$BLFiniteFlowPath,
"fflib"->$BLFiniteFlowLibraryPath,
"integralordering"->toStringInput[$IntegralOrdering],
"fam"->toStringInput[BLFamily],
"s" -> toStringInput[IBPRank],
"d" -> toStringInput[IBPDot],
"ibpvars"->toStringInput[BLIBPParameter],
"ibprule"->toStringInput[BLRules],
"pid" -> toStringInput[OptionValue["PrimeNo"]],
"algtake"->toStringInput["FiniteFlow`Private`TakeElemsToInternal"]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir,"reductionDataInfo.wl"}]];
];


DatabaseInfosupTemplate[dir_]:=Module[{extra,template,ibpvars,rule},

template = StringTemplate@StringJoin@Riffle[Join[BasicCode[],DataSupCode[],QuitCode[]],"\n\n"];
rule = <|
"bladeibp"->$BLBladeIBPPath,
"ff"->$BLFiniteFlowPath,
"fflib"->$BLFiniteFlowLibraryPath,
"integralordering"->toStringInput[$IntegralOrdering],
"fam"->toStringInput[BLFamily],
"s" -> toStringInput[IBPRank],
"d" -> toStringInput[IBPDot],
"ibpvars"->toStringInput[BLIBPParameter],
"ibprule"->toStringInput[BLRules],
"algtake"->toStringInput["FiniteFlow`Private`TakeElemsToInternal"]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir,"reductionDataInfosup.wl"}]];
];


(* ::Section:: *)
(*BLDatabaseInfo*)


(*ExportIndepEqsQ: whether pick independent equations for numeric sampling*)
(*SaveMemory: trimming and pick independent equations separately*)
Options[BLDatabaseInfo]=Join[{"ExportIndepEqsQ"->True}, Options[DatabaseInfoTemplate]];
BLDatabaseInfo[target_,OptionsPattern[]]:=Module[{name,dir,time,opt},
opt=Rule[#,OptionValue[BLDatabaseInfo,#]]&/@Keys[Options[BLDatabaseInfo]];
dir=$ReductionDirectory;
Put[target, FileNameJoin[{dir, "target"}]];
Which[
!TrueQ[OptionValue["ExportIndepEqsQ"]],
	DatabaseInfoTemplate[dir,"SaveMemoryQ"->True,Sequence@@FilterRules[opt,FilterRules[Options[DatabaseInfoTemplate],Except["SaveMemoryQ"]]]];
	BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"reductionDataInfo.wl"}]},"log"->FileNameJoin[{dir,"reductionDataInfo.log"}]];
	,
TrueQ@OptionValue["SaveMemoryQ"],
	DatabaseInfoTemplate[dir,Sequence@@FilterRules[opt,Options[DatabaseInfoTemplate]]];
	DatabaseInfosupTemplate[dir];
	BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"reductionDataInfo.wl"}]},"log"->FileNameJoin[{dir,"reductionDataInfo.log"}]];
	BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"reductionDataInfosup.wl"}]},"log"->FileNameJoin[{dir,"reductionDataInfosup.log"}]];
	,
True,
	DatabaseInfoTemplate[dir,Sequence@@FilterRules[opt,Options[DatabaseInfoTemplate]]];
	BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"reductionDataInfo.wl"}]},"log"->FileNameJoin[{dir,"reductionDataInfo.log"}]];
];
(*after DatabaseInfo, the trimmed system may be different*)
BLClearSystem[];
];


(* ::Section:: *)
(*End*)


End[];
