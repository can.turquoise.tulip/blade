(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLFamilyInf::usage = "BLFamilyInf: a symbol that store family informations defined by BLFamilyDefine";
BLIBPParameter::usage = "BLIBPParameter: a symbol represent parameters in ibp system";
BLSearchParameter::usage = "BLSearchParameter: a symbol represent parameters in block-triangular system. Note the ordering of parameters will effect the efficiency of reduction. Blade will put the most complicated parameter at the first position by default";
BLNthreads::usage = "BLNthreads: the number of threads used in parallel computation. If not specified, it is set to $ProcessorCount by default. Note that the function BLNthreadsLearning will modify BLNthreads according to the available memory";
BLFamilyDefine::usage = "BLFamilyDefine[family,dimension_,loopdenominators,loopmomenta,externalmomenta,momentumconservation,scalarproducts,top,rules,cut_:Automatic, prescription_:Automatic] define the topology of the family
 - family: symbol represent the name of the family.
 - dimension: space-time dimension, e.g, 4-2eps.
 - loopdenominators: a list of loop denominators. One should keep momenta explicitly.
 - loopmomenta: a list of loop momenta like {l1, l2}
 - externalmomenta: a list of external momenta {p1,p2,p3,p4}
 - momentumconservation: momentum conservation like {p4->-p1-p2-p3}
 - scalarproducts: a complete set of scalarproducts among externalmomenta, like {p1^2->0, p1 p2 ->s/2, p3^2 -> msq...}. here msq:=m^2, Note that mass parameter should be squared for numerical IBP, 
 - top: topsector information like {1, 1, 1, 0, 0, 0, 0}, where '1' means denominators and  '0' means irreducible scalar products.
 - rules: replacement rules for parameters, like {s->1}. 
 - cut: cut information like {1, 1, 1, 0, 0, 0, 0}, where '0' means uncut propagators and '1' means cut propagators.
 - prescription: prescription for loopmomenta, like {1,1,0,-1}. '1' means that propagators with this loopmomenta has +i0 Feynman prescription, '-1' means -i0, '0' means cut momenta to be integrated. 
\"ExtraIntDerivDen\": a SparseArray data that specifies dF_\[Rho]/dD_i = A_{\[Rho]i}^\[Beta] F_\[Beta], which is used to generate IBP identities for generailized integrand. 'F_\[Rho]' is introduced as an extra integrand factor, i.e the integrand looks like F_\[Rho] * {D_i}^{-v_i}. F=1 leads to the standard Feynman integral(by default). For example, if F_1=sin(x D_4) & F_2=cos(x D_4), then \"ExtraIntDerivDen\"->{{1,4,2}->x,{2,4,1}->-x}. The input may, but need not, be simplied when there is only one general integrand. For instance, if F={D_4}^x1 {D_5}^x2, then \"ExtraIntDerivDen\"->{4->x1/D_4, 5->x2/D_5} also works. D_i should be denoted by BL[...] explicitly.
\"ExtraIntDerivPara\": a list of rules that specifies dF_\[Rho]/ds = A_\[Rho]^\[Beta] F_\[Beta]. For example, if F_1=sin(x D_4) & F_2=cos(x D_4), \"ExtraIntDerivPara\"->{x->{{1,2}->D_4,{2,1}->-D_4}}. The input may, but need not, be simplied when there is only one general integrand. For instance, if F=Exp[x1 D_3 + x2 D_4], then \"ExtraIntDerivPara\"->{x1->D_3, x2->D_4}. D_i should be denoted by BL[...] explicitly.";


Begin["`Private`"];
BLFamily::usage = "return current family name, should be a symbol";
BLDimension::usage = "return space-time dimension";
BLLoop::usage = " return a list of loop momenta";
BLLeg::usage = "return a list of external momenta";
BLMomCons::usage = "return momentum conservation";
BLIndepLeg::usage = "return independent external momenta";
BLSPToSTU::usage =" return a list of scalarproducts among externalmomenta";
BLPropagators::usage = "loop denominators ";
BLTopSector::usage ="define topsector like {1, 1, 1, 0, 0, 0, 0}, where '1' means Denorminators and '0' means irreducible scalar products";
BLCut::usage = " cut information like {1, 1, 1, 0, 0, 0, 0}, where '0' means uncut propagators and '1' means cut propagators";
BLPrescription::usage = "Feynman prescription for propagators related to loopmomenta. one to one correspondent with loopmomenta. '1' means +i0+, '-1' means -i0+, '0' means cut momenta.";
BLExtraIntDerivDen::usage = "BLExtraIntDerivDen[[\[Rho],i,\[Beta]]], a 3-dimensional array that defined by dF_\[Rho]/dD_i = A_{\[Rho]i}^\[Beta] F_\[Beta], where F_\[Rho] is the \[Rho]-th general integrand, D_i is the i-th propagator.";
BLExtraIntDerivPara::usage="BLExtraIntDerivPara[s][[\[Rho],\[Beta]]], a 2-dimensional array that defined by dF_\[Rho]/ds = A_\[Rho]^\[Beta] F_\[Beta]";
BLMassScale::usage = " all variables present in the family except for dimensional regulator. like {s, msq,...}.";
BLRules::usage = "replacement rules for parameters like {s->1}";
BLReverseRule::usage = "replacement rule for blIm.";
End[];


Begin["`DefineFamily`"];


(* ::Section:: *)
(*BLFamilyDefine*)


(*vacuum*)
MaximalGroup[{}]:={};
MaximalGroup[matrix_]:= Flatten[FirstPosition[#,1]&/@Select[RowReduce[Transpose@matrix], !AllTrue[#, #===0&]&]];


BLFamilyInf=Association[];


Options[BLFamilyDefine]={"ExtraIntDerivDen"->{},"ExtraIntDerivPara"->{}};


(*Note: BLTopSector may be modified during compuation. BLFamilyInf[family]["TopSector"] is invariant*)
BLFamilyDefine[family_,dimension_,loopdenominators_,loopmomenta_,externalmomenta_,momentumconservation_,scalarproducts_,top_,rules_,cut_List:Automatic,prescription_List:Automatic,OptionsPattern[]]:=Module[
{sps,eqs,paras,complex,extraintderivden,dim},
BLFamily = family;
BLDimension = dimension;
BLPropagators = loopdenominators;
BLLoop = loopmomenta; 
BLLeg = externalmomenta;
BLMomCons = momentumconservation;
BLIndepLeg = Select[externalmomenta,FreeQ[Keys@momentumconservation,#]&];
BLSPToSTU = toStanRep[BLIndepLeg,scalarproducts/.momentumconservation];
BLTopSector = top;
BLCut = If[cut===Automatic,ConstantArray[0,Length@loopdenominators],cut];
BLPrescription = If[prescription===Automatic, ConstantArray[1,Length@loopmomenta], prescription];
(*20230519: generating function mode*)
extraintderivden=OptionValue["ExtraIntDerivDen"];
If[extraintderivden=!={},
	WriteMessage["ExtraIntDerivDen detected, Close symmetry by default."];
	BLSetReducerOptions["CloseSymmetry"->True];
	(*20240205: support multiple extra integrands under the condition that their derivatives w.r.t 'Den' are closed.
	BLExtraIntDerivDen is a 3-dimensional arrays*)
	BLExtraIntDerivDen=Which[
		MatchQ[extraintderivden,{({_Integer,_Integer,_Integer}->_)...}], dim=Max[Keys[extraintderivden][[All,{1,3}]]];Normal[SparseArray[extraintderivden,{dim,Length@loopdenominators,dim},0]],
		MatchQ[extraintderivden,{(_Integer->_)...}], {List/@Normal[SparseArray[OptionValue["ExtraIntDerivDen"],Length@loopdenominators]]},
		True, ErrorPrint["error: unknown pattern of ExtraIntDerivDen"];Abort[]];
	,
	BLExtraIntDerivDen = ConstantArray[0,{1,Length[loopdenominators],1}]
];
(*To generate differential equation w.r.t parameter in ExtraInt*)
BLExtraIntDerivPara = OptionValue["ExtraIntDerivPara"];

(*20230501: complex mode*)
complex=Cases[rules,_Complex,Infinity]//Union;
If[Length[complex]===0, BLRules=rules;BLReverseRule={},
  Which[
    Length[complex]===1, 
      BLRules=Thread[Keys[rules]->(If[Head[#]===Complex,Symbol["blIm"],#]&/@Values[rules])];
      BLReverseRule=Rule[Symbol["blIm"],complex[[1]]] ,
    Length[complex]>1, 
      BLRules=Thread[Keys[rules]->(Identity[Re[#]+Im[#]*Symbol["blIm"]]&/@Values[rules])];
      BLReverseRule=Rule[Symbol["blIm"],I]
    ];
   WriteMessage["Complex replacement rule detected, using: ", ToString[BLRules,InputForm]," and ",ToString[BLReverseRule,InputForm]," internally!"];
];


sps=Outer[Times,loopmomenta,Join[loopmomenta,BLIndepLeg]]//Flatten//DeleteDuplicates;
If[Length@BLPropagators=!=Length@sps,ErrorPrint["error: overdetermined basis"];Abort[]];
If[Length@MaximalGroup[Coefficient[loopdenominators/.momentumconservation,#]&/@sps]=!=Length@sps,ErrorPrint["error: not a valid basis"];Abort[]];
sps=Outer[Times,BLIndepLeg,BLIndepLeg]//Flatten//DeleteDuplicates;
If[Length@MaximalGroup[Coefficient[scalarproducts/.momentumconservation/.Rule[a00_,b00_]:>a00-b00,#]&/@sps]=!=Length@sps,Print["error: not a valid scalar products"];Abort[]];
If[!checkPrescription[BLPropagators/.BLMomCons, BLLoop, BLPrescription, BLTopSector, BLCut],ErrorPrint["error: inconsistent definition of propagatora and prescription"];Abort[]];

BLMassScale = Join[Complement[Variables[{loopdenominators,Values@scalarproducts,Values[BLRules]}],Join[loopmomenta,externalmomenta]],Cases[Variables[BLExtraIntDerivDen],Except[_BL]]]//Union; 

ComputeDerivatives[];

BLFamilyInf[family]=Association[];
Do[BLFamilyInf[family][key]=ToExpression[CTX<>"BL"<>key],{key,
{"Dimension","Propagators","Loop","Leg","MomCons","IndepLeg","SPToSTU","TopSector","Cut","Rules","Prescription"}}];

BLSearchParameter = BLIBPParameter;
BLSetDefaultSearchOptions[];

(*20230519: a convenient way to improve performance at 1-loop*)
If[Length[BLLoop]<=1,
WriteMessage["Modify default BLReducerOptions for one-loop reduction."];
BLSetReducerOptions["CheckMastersQ"->False];
SetOptions[BLReduce,"DivideLevel"->0,"BladeMode"->None];
SetOptions[BLDifferentialEquation,"DivideLevel"->0,"BladeMode"->None];
];
];


(*left loop and right loop decouple*)
(*cut propagators involves cut momenta only?*)
checkPrescription[props_List,loop_List,pres_List,top_List,cut_List]:=Module[{left,cc,right},
{left,cc,right}=Pick[loop,pres,#]&/@{1,0,-1};
If[AnyTrue[Pick[props,top,1],(!FreeQ[#,Alternatives@@left])&&(!FreeQ[#,Alternatives@@right])&],Return[False]];
If[AllTrue[Pick[props,cut,1], (!FreeQ[#,Alternatives@@cc])&&(FreeQ[#,Alternatives@@left])&&(FreeQ[#,Alternatives@@right])&], Return[True] ,Return[False]];
];


(*rep: only involve 'ext'*)
toStanRep[ext_,rep_]:=Module[{vars,sol},
vars=Table[Rule[ext[[i]] * ext[[j]],"SP"[ext[[i]],ext[[j]]]],{i,1,Length[ext]},{j,i,Length[ext]}]//Flatten;
Off[Solve::svars];
sol=Solve[Expand[rep]/.vars/.Rule->Equal,Values[vars]][[1]];
On[Solve::svars];
sol/.(Reverse/@vars)
];


(* ::Section:: *)
(*BLIBPParameter*)


(*for generate ibp system and reconstruction*)
(*in most cases, reduction coefficients of dimensional regulator is simpler than other invariants*)
BLIBPParameter := Module[{paras},
paras=Select[Join[BLMassScale,Variables[BLDimension]], !MemberQ[Keys[BLRules], #]&];
If[$IBPParameter===Automatic, paras, 
	If[ContainsExactly[$IBPParameter,paras],$IBPParameter,
		WriteMessage["Caution: please check IBP parameters -> ",$IBPParameter]]]];


(* ::Section:: *)
(*End*)


End[];
