(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLComputeDerivative::usage = "BLComputeDerivative[expr,x] compute derivatives of 'expr' w.r.t parameter 'x'.";


Begin["`Private`"];
ComputeDerivatives;
End[];


Begin["`Derivatives`"];


BLSTUToSP::usage = " inverse replacements like {s->2 p1 p2, msq -> p3^2}";


(* ::Section:: *)
(*BLComputeDerivative *)


ReplacedProps := BLPropagators/.BLMomCons//Expand;


(*20230602: support linear/quadratic kinematic variables, e.g. s,m^2*)
(*20230612: support overdetermined/underdetermined kinematic variables, e.g. p^2\[Rule]x+y*)
BLSTUToSP := Block[{vars,norvars,mat,pos,solved,sol},
If[BLSPToSTU==={},Return[{}]];  (*<---vacuum*)
vars=Variables[Values@BLSPToSTU];
If[vars==={},Return[{}]]; (*<--- numeric p^2\[Rule]1*)
norvars=Function[{var},With[{pow=Cases[Values[BLSPToSTU],Power[var,b_]:>b,Infinity]},
	Switch[Length[Union[pow]], 0,var,1,var^pow[[1]],_,Print["Caution: nonhomogenous kinematic variables encountered -> ",var,", BLComputeDerivatives may give wrong result."];$Failed]]]/@Variables[Values@BLSPToSTU];
If[!FreeQ[norvars,$Failed],Return[{}]];
mat=Coefficient[Values[BLSPToSTU],#]&/@norvars//Transpose;  (*<-- in case m^2*)
pos=maximalGroup[mat];
solved=Flatten[FirstPosition[#,1]&/@RowReduce[mat[[pos]]]];
sol=Solve[Thread[Keys[BLSPToSTU]==Values[BLSPToSTU]][[pos]],vars[[solved]]];
If[sol==={},ErrorPrint["error: can not determine SPToSTU."];Abort[],Join[sol[[1]],Thread[Complement[vars,vars[[solved]]]->Complement[vars,vars[[solved]]]]]]]


maximalGroup[mat_]:=Flatten[FirstPosition[#,1]&/@Select[RowReduce[Transpose[mat]],AnyTrue[#,#=!=0&]&]];


Attributes[sp]={Orderless};
spRepl := Outer[Rule[#1*#2,sp[#1,#2]]&,Join[BLLoop,BLIndepLeg],Join[BLLoop,BLIndepLeg]]//Flatten//DeleteDuplicates;
BL2sp[bl_BL]:=Times@@Power[ReplacedProps/.BLSTUToSP/.spRepl,-bl[[2]]];
sp2Den := Solve[Thread[(ReplacedProps/.BLSPToSTU/.spRepl)==(BL[BLFamily,-UnitVector[Length@ReplacedProps,#]]&/@Range[Length@ReplacedProps])],Select[Values[spRepl],!FreeQ[#,Alternatives@@BLLoop]&]]//Flatten;


sp[a_,b__Plus]:=sp[a,#]&/@b;
sp[a_?NumberQ*b_,c_]:=a*sp[b,c];
sp[a_,Times[b__,c_]]/;!FreeQ[c,sp]:=sp[a,b]*c;
sp[a_,0]:=0;


BL/:Times[BL[a_,b_],BL[a_,c_]]:=BL[a,b+c];
BL/:Power[BL[a_,b_],c_]:=BL[a,c*b];
BL/:Times[BL[a_,b_,f_],BL[a_,c_,f_]]:=BL[a,b+c,f];
BL/:Power[BL[a_,b_,f_],c_]:=BL[a,c*b,f];


(*refer to 1310.1145 eq.3*)
Dinv[bl_BL,sp[p_,q_]]:=Module[{nm,djdp},
nm=First@bl;
djdp=D[BL2sp[bl],p]/.{Derivative[0,1][sp][a_,b_]:>a,Derivative[1,0][sp][a_,b_]:>b};
((If[p===q,1/2,1]*sp[#,djdp]&/@BLIndepLeg) . Inverse[Outer[sp,BLIndepLeg,BLIndepLeg]] . D[BLIndepLeg,q])/.(BLSPToSTU/.spRepl)/.sp2Den
];


(*20240118: g(sij,mi) = f(pi.pj(sij,mi), wi(mi)) \[Rule] D[g,inv] = D[pi.pj,inv]*D[f,pi.pj] + D[wi,inv]*D[f,wi]
BLSPToSTU:  {pi.pj is linear combinations of sij and mi}
BLSTUToSP:  {sij is linear combinations of pi.pj and wi, mi\[Rule]wi}  (internally, wi===mi)
# sij = # independent (pi.pj)
# mi: remain freedom*)
Dinv[bl_BL,s_Symbol]:=Module[{sps,nzsps},
sps=DeleteDuplicates[Flatten@Outer[sp,BLIndepLeg,BLIndepLeg]];
nzsps=Select[sps,!FreeQ[#/.(BLSPToSTU/.spRepl),s]&];
(Dinv[bl,#]&/@nzsps) . D[nzsps/.(BLSPToSTU/.spRepl),s]+(D[BL2sp[bl],s]/.(BLSPToSTU/.spRepl)/.sp2Den)
];


(*derivatives of propagators are expressed as linear combinations of propagators*)
ComputeDerivatives[]:=Module[{dim},
ClearAll[DenomDiv];

Do[DenomDiv[s]=(Dinv[BL[BLFamily,#],s]&/@(-IdentityMatrix[Length@BLPropagators]))/.BLRules,{s,Keys[BLSTUToSP]}];
DenomDiv[s_]:=DenomDiv[s]=(Dinv[BL[BLFamily,#],s]&/@(-IdentityMatrix[Length@BLPropagators]))/.BLRules; 

(*20230520 ExtraIntDeriv*)
(*20240205: support multiple extra integrands under the condition that their derivatives w.r.t 'Den' are closed*)
Clear[ExtraIntDiv];
ExtraIntDiv[_]:=0;
(*Do[ExtraIntDiv[inv]=BLExtraIntDerivDen . DenomDiv[inv],{inv,Keys[BLSTUToSP]}]; (*assume no explicit dependence on 's'*)
Do[ExtraIntDiv[xxx[[1]]]=xxx[[2]],{xxx,BLExtraIntDerivPara}];*)
Do[
ExtraIntDiv[xxx[[1]]]=If[MatchQ[xxx[[2]],{({_Integer,_Integer}->_)...}], 
	dim=Max[Keys[xxx[[2]]]];Normal[SparseArray[xxx[[2]],{dim,dim},0]]
	,
	{{xxx[[2]]}}];
,{xxx,BLExtraIntDerivPara}]
];


Ftag/:Ftag[r_]*BL[a__]:=BL[a,r];
FtagSimplify::usage = "FtagSimplify[expr] drop the tag in BL[family,index,tag] when there is only one general integrand.";
FtagSimplify[exp_]:=If[Length[BLExtraIntDerivDen]<=1,exp/.BL[a_,b_,c_]:>BL[a,b],exp];


(* d(D_1^{-a_1}...D_N^{-a_N}F_\[Rho])/ds === F_\[Rho] * d D_i/ds * (-a_i)D_i^{-a_i-1} + dF_\[Rho]/ds *(D_1^{-a_1}...D_N^{-a_N}))
where  d D_i/ds -> DenomDiv[s], dF_\[Rho]/ds = A_\[Rho]\[Beta] F_\[Beta] -> ExtraIntDiv[s]*)
IntDeriv[BL[t_,a_,r_],s_]:=Ftag[r]*(DenomDiv[s] . ((-a[[#]] BL[t,(UnitVector[Length[a],#]+a)])&/@Range[Length[a]]))+Sum[ExtraIntDiv[s][[r,b]]*Ftag[b]*BL[t,a],{b,Length@ExtraIntDiv[s]}];


IntDeriv[BL[t_,a_],s_]:=IntDeriv[BL[t,a,1],s];


IntDeriv[a_Plus,s_]:=IntDeriv[#,s]&/@a;
IntDeriv[a_Times,s_]:=Plus@@(MapAt[IntDeriv[#,s]&,a,#]&/@Range[Length[a]]);
IntDeriv[expr_,s_]:=D[expr,s];


BLComputeDerivative[expr_List,x_List]:=BLComputeDerivative[expr,#]&/@x;
BLComputeDerivative[expr_List,x_]:=BLComputeDerivative[#,x]&/@expr;
BLComputeDerivative[expr_,x_List]:=BLComputeDerivative[expr,#]&/@x;
BLComputeDerivative[expr_,x_]:=FtagSimplify[Collect[Expand[IntDeriv[expr,x],Ftag[_]|BL[__]],_BL,Together]];


(* ::Section:: *)
(*End*)


End[];
