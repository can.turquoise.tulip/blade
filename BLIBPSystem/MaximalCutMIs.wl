(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLMaximalCutMasters::usage="BLMaximalCutMasters[] determine master integrals under maximal cut.
\"PreferredMI\". possible choices are: Automatic, \"Rank\", \"Dot\", \"MixedRank\", \"MixedDot\", \"None\". 	
	Automatic would adopt the ordering set by BLSetReducerOptions[\"IntegralOrdering\"].
	\"Rank\" would give maximal-cut masters with scalarproducts.
	\"Dot\" would give maximal-cut masters with double propagators.
	\"MixedRank\" is similar to \"Rank\" except that \"Rank\"+\"Dot\" has top priority.
	\"MixedDot\" is similar to \"Dot\" except that \"Rank\"+\"Dot\" has top priority.
	\"None\" if \"preferred\" constitutes a complete set of master integrals.
\"AnalyzeSectorsOnly\": analyze sectors without determine maximal-cut masters. 
\"GenCutIds\": type of identities,
\"LinearReduce\": type of linear solver. possible choice are MMALinearSolveLearn and FFLinearSolveLearn,
\"CutNoDot\": remove seeds that have dots on cut propagators,
\"CloseSymmetry\": whether to generate symmetry relations or not,
\"MaxIncrement\": maximal round of increasing seeds allowed,
\"StartingPower\": assumed lowerlimit of power of master integrals,
\"HighestPower\": maximal power of master integrals allowed,
\"Nthreads\": number of threads.";
BLRawMasters::usage= "MaximalCut master integrals. This is obtained by function BLMaximalCutMasters";


Begin["`Private`"];
End[];


Begin["`MaximalCutMIs`"];


(* ::Section:: *)
(*MaximalCutMastersTemplate*)


Options[MaximalCutMastersTemplate]={"GenCutIds"->"GenCutIds","LinearReduce"->"FFLinearSolveLearn","CutNoDot"->False,"CloseSymmetry"->False,"MaxIncrement"->2,"StartingPower"->2,"HighestPower"->5,"CheckSymmetry"->True, "Nthreads"->BLNthreads};
MaximalCutMastersTemplate[dir_,zeroonlyq_,OptionsPattern[]]:=Module[
{template,exints,ibpvars,rule,opt},
template = StringTemplate[StringJoin@Riffle[{
"AppendTo[$Path,\"`bladeibp`\"];
AppendTo[$Path,\"`ff`\"];
AppendTo[$LibraryPath,\"`fflib`\"];
If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
<<BladeIBP.m;
<<FiniteFlow.m;
<<topology.wl;",

"IntegralOrdering=`integralordering`;
family=`fam`;",

If[zeroonlyq,
"Block[{mapdir},
mapdir = \"results\";
If[!DirectoryQ@mapdir,CreateDirectory[mapdir]];
Put[MappedSectors[family],FileNameJoin[{mapdir,\"mappedsectors\"}]];
Put[UniqueSectors[family],FileNameJoin[{mapdir,\"uniquesectors\"}]];
Put[ZeroSectors[family],FileNameJoin[{mapdir,\"zerosectors\"}]];
];",

StringRiffle[{"FFLinearSolveLearn[eqs_,paras_,allvars_,neededvars_]:=Module[{graph,in,sys},
(*initialize fflow*)
`ffgraphid` = Association[{}];
`ffalgid` = Association[{}];
`ffgraphinputs` = Association[{}];
FFNThreads = Automatic;
(*solve*)
FFSparseSolve[eqs,allvars,\"Parameters\"\[Rule]paras,\"NeededVars\"\[Rule]neededvars,\"IndepVarsOnly\"\[Rule]True]
];",

(*2022/06/18: add options to control MaximalCutMastersTemplate[]*)
opt=(#->OptionValue[#])&/@Keys[FilterRules[Options[MaximalCutMastersTemplate],Except[{"GenCutIds","LinearReduce"}]]];
"SetOptions[AutoDetermine,"<>StringReplace[ToString[opt,InputForm],{"{"->"","}"->""}]<>"];",

(*20221123: In BladeIBP.wl, GenCutIds and LinearReduce are symbols. However, in Blade.wl, they are strings.*)
opt=(#->Symbol[ToString[OptionValue[#]]])&/@Keys[FilterRules[Options[MaximalCutMastersTemplate],{"GenCutIds","LinearReduce"}]];
"SetOptions[AutoDetermine,"<>StringReplace[ToString[opt,InputForm],{"{"->"","}"->""}]<>"];",

(*20230113: numeric replacement.*)
"SetOptions[AutoDetermine,\"Numeric\"\[Rule]`ibprule`];",

"Block[{mapdir,flag},
mapdir = \"results\";
If[!DirectoryQ@mapdir, CreateDirectory[mapdir]];
flag=AutoDetermine[family];
If[!FreeQ[flag,$Failed],Print[\"error: AutoDetermine failed\"];Abort[]];
Put[MappedSectors[family],FileNameJoin[{mapdir,\"mappedsectors\"}]];
Put[UniqueSectors[family],FileNameJoin[{mapdir,\"uniquesectors\"}]];
Put[ZeroSectors[family],FileNameJoin[{mapdir,\"zerosectors\"}]];
Put[FastMIs[#]&/@NonZeroSectors[family]//Flatten//SortIntegrals,FileNameJoin[{mapdir,\"rawmastersnomap\"}]];
Put[FastMIs[#]&/@UniqueSectors[family]//Flatten//SortIntegrals,FileNameJoin[{mapdir,\"rawmasters\"}]];
Print[\"# rawmasters(nomap): \", Length@(Get@FileNameJoin[{mapdir,\"rawmastersnomap\"}]),\", # rawmasters: \", Length@(Get@FileNameJoin[{mapdir,\"rawmasters\"}]) ];
];"},"\n\n\n"]]
},"\n\n\n"]];

rule = <|
"bladeibp"->$BLBladeIBPPath,
"ff"->$BLFiniteFlowPath,
"fflib"->$BLFiniteFlowLibraryPath,
"integralordering"->toStringInput[$IntegralOrdering],
"fam"->toStringInput[BLFamily],
"ibprule"->toStringInput[BLRules],
"ffgraphid"->"FiniteFlow`Private`FFGraphId",
"ffalgid"->"FiniteFlow`Private`FFAlgId",
"ffgraphinputs"->"FiniteFlow`Private`FFGraphInputs"
|>;

FileTemplateApply[template,rule,FileNameJoin[{dir,"maximalcutmasters.wl"}]];
];


(* ::Section:: *)
(*BLMaximalCutMasters*)


(*2022/06/18: add options to control FastDetermineMIs[]*)
(*\"PreferredMI\" : top priority! If user want to call this function solely.*)
(*For CheckMasters(BLReduce), we fix \"PreferredMI\"\[Rule]Automatic because user does not care about details of judgement.
Automatic is the best choice considering \"preferred\" and $IntegralOrdering*)
(*For completing masters(BLDifferentialEquation), we allow user to choose ordering they like*)
(*If no topsector is input, use BLFamilyInf["TopSector"]*)
Options[BLMaximalCutMasters]=Join[{"PreferredMI"->Automatic,"AnalyzeSectorsOnly"->False},Options[MaximalCutMastersTemplate]];
BLMaximalCutMasters[top0:{a__Integer}:Hold[BLFamilyInf[BLFamily]["TopSector"]],preferred_List:{},OptionsPattern[]]:=Module[
{top, ordering, zeroonlyq, r00,d00, opt,dir},

top = ReleaseHold[top0];
ordering = Switch[OptionValue["PreferredMI"],
	None, Return[{}],
	Automatic, If[preferred==={},$IntegralOrdering, 
					{r00,d00}={Max[BLIntRank/@BLIntegralsIn[preferred]],Max[BLIntDots/@BLIntegralsIn[preferred]]};
					Which[ d00<=0, 1, r00<=0, 2, d00<=r00, 3, d00>r00, 4]],
	"Rank", 1,
	"Dot", 2,
	"MixedRank", 3,
	"MixedDot", 4,
	_,ErrorPrint["error, unknown option value for PreferredMI."];Abort[]];
zeroonlyq = OptionValue["AnalyzeSectorsOnly"];
opt=(#->OptionValue[#])&/@Keys[FilterRules[FilterRules[Options[BLMaximalCutMasters],Options[MaximalCutMastersTemplate]],Except["Nthreads"]]];

dir=$MaximalCutDirectory;
CreateDir[dir];
BLTopSector = top;
TopologyTemplate[dir,Sequence@@FilterRules[opt,Options[TopologyTemplate]]];
Block[{$IntegralOrdering = ordering},MaximalCutMastersTemplate[dir,zeroonlyq,Sequence@@opt,"Nthreads"->BLNthreads]];
BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"maximalcutmasters.wl"}]}, "log"->FileNameJoin[{dir,"maximalcutmasters.log"}]];

Put[top, FileNameJoin[{dir,"config"}]];
BLFamilyInf[BLFamily]["MaximalCutMasters"]=If[$CheckMastersQ,GetFile[FileNameJoin[{dir,"results/rawmastersnomap"}]],{}];
If[FileExistsQ[#],Get[#],{}]&[FileNameJoin[{dir,"results/rawmastersnomap"}]] (*<--return rawmasters if needed*)
];


(* ::Section:: *)
(*End*)


End[];
