(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLNthreadsLearning::usage = "BLNthreadsLearning[] Load trimmed IBP system and learn available number of threads for Sampling";
BLDatabase::usage = "BLDatabase[dataname_,nps_,pid_, fixps_:Automatic] Evaluate IBP system at 'nps' numerical sample points under prime field FFPrimeNo['pid']. solutions are stored at $SearchDirectory/database/'dataname'. One specify numerical values by 'fixps'. It must be called after BLNthreadsLearning[]";


Begin["`Private`"];
$InternalGraph;
BLClearSystem;
BLDatabaseNthreads;
BLTimeIBP;
$Conservative;
End[];


Begin["`Database`"];


(* ::Section:: *)
(*LoadIBPSystem*)


(*the output of fflow may vary before and after mark&sweep. sort the latter if applicable*)
(*depvarsorder: learn from system.json
fflearn: learn from systemC.json, after*)
GenSystemCsup[depvarsorder_,fflearn_]:=Module[{pos,trimposi,trimelems},
pos=Association[{}];
Do[pos[fflearn[[1,i]]]=i-1,{i,1,Length@fflearn[[1]]}];
trimposi=(depvarsorder[[1,2]]/.Dispatch[pos])+1;
If[trimposi=!=Range[Length@trimposi],
	trimelems={Range[Length@fflearn[[1]]*Length@fflearn[[2]]]}->Flatten[(Range[1+(#-1)*Length@fflearn[[2]],#*Length@fflearn[[2]]]&/@trimposi)];
	Export["systemCsup.json",Flatten@FiniteFlow`Private`TakeElemsToInternal[trimelems],"RawJSON","Compact"->True];];	
];


FFAlgTakeFromJSON[graph_,node_,inputs_,patternfile_]:=Module[{idno,elems},
If[!FileExistsQ[patternfile],Return[$Failed]];
elems=Import[patternfile];
idno = FiniteFlow`Private`FFAlgTakeImplem[FiniteFlow`Private`FFGraphId[graph],FiniteFlow`Private`FFAlgId[{graph,#}]&/@inputs,elems];
     If[TrueQ[idno == $Failed], Return[$Failed]];
     FiniteFlow`Private`FFAlgId[{graph,node}] = idno;]


(*"system.C" & "results/depvarsorder" must be prepared by DatabaseInfo[target] previously*)
(*systemCsup.json is a take pattern. used if DepVarsOrdering is different before trimming and after*)
LoadIBPSystem[]:=Block[{ in, ibps, fflearn, ibpstake,ibpstakenz},
SetDirectory[$ReductionDirectory];
If[!FileExistsQ["systemC.json"],ErrorPrint["error: trimmed ibp system not found."];Abort[]];
FFNewGraph[$InternalGraph, in, BLIBPParameter];
FFAlgJSONSparseSolver[$InternalGraph, ibps, {in}, "systemC.json"];
FFSolverOnlyHomogeneous[$InternalGraph, ibps];
FFGraphOutput[$InternalGraph, ibps];
If[!FileExistsQ["systemCsupflag"],
	fflearn = FFLearn[$InternalGraph];
	GenSystemCsup[Get@"results/depvarsorder",fflearn];
	Put[1, "systemCsupflag"];
];

If[FileExistsQ["systemCsup.json"],
	FFLearn[$InternalGraph];
	FFAlgTakeFromJSON[$InternalGraph,ibpstake,{ibps},"systemCsup.json"];
	FFAlgNonZeroes[$InternalGraph,ibpstakenz,{ibpstake}];
	FFGraphOutput[$InternalGraph,ibpstakenz];
	FFLearn[$InternalGraph]
	,
	FFSolverSparseOutput[$InternalGraph, ibps];
	FFLearn[$InternalGraph];
	];
ResetDirectory[];
];


BLClearSystem[]:=FiniteFlow`FFDeleteGraph[$InternalGraph];


(* ::Section:: *)
(*Database*)


(*once and for all*)
FromDatabaseInfo[]:=Module[{datainfo, sortinfo, posimap, posi, part},
{NonMIs, MIs, datainfo} = GetFile[FileNameJoin[{$ReductionDirectory,"results/datainfo"}]][[{1,2,5}]];
AllIntegrals = Join[NonMIs, MIs];
datainfo = Join@@Table[{i, datainfo[[i,j]]},{i,Length[datainfo]},{j,Length[datainfo[[i]]]}];
sortinfo = Join@@Sort[GatherBy[datainfo, Last], #1[[1,2]]<=#2[[1,2]]&];
posimap = sortinfo/.PositionIndex[datainfo]//Flatten;
SparseTrans[list_]:=Join[list, ConstantArray[1, Length[MIs]]][[posimap]];

posi = sortinfo[[All,1]]-1;
part = Prepend[Accumulate[Length/@GatherBy[sortinfo, Last]],0];
$DataInfo = {Length[NonMIs]+Length[MIs], Length[MIs], posi, part};
];


(*after FromDatabaseInfo[]*)
DatabaseInit[dataname_, pid_]:=Module[{dir, file, datainfo, prime, common,posi},
dir = FileNameJoin[{$SearchDirectory, "database", dataname}];
CreateDir[dir];
file = FileNameJoin[{dir, "red_pid"}];
If[FileExistsQ[file], WriteMessage["datainfo has already been written. ingnore it."]; Return[]];

datainfo = $DataInfo;
prime = FFPrimeNo[pid];
common = {{prime, Length[BLIBPParameter], datainfo[[1]], datainfo[[2]], Length[datainfo[[3]]], 0}};
writeObject[FileNameJoin[{dir, "red_common"}], common, OpenWrite];
writeObject[FileNameJoin[{dir, "red_posi"}], datainfo[[3]], OpenWrite];
writeObject[FileNameJoin[{dir, "red_part"}], datainfo[[4]], OpenWrite];
Put[pid, FileNameJoin[{dir, "red_pid"}]];
(*kin_table and red_ps should keep the same ordering (BLSearchParameter)*)
posi=(FirstPosition[BLSearchParameter,#,{}]&/@$Conservative)//Flatten;
If[Length@posi=!=Length@$Conservative,Print["unknow conservative parameter"];Abort[]];
Put[posi,FileNameJoin[{dir,"red_cons"}]];
];


(*after DatabaseInit[dataname, pid] and LoadIBPSystem[]*)
(*ordering of 'fixps' and $Conservative must be the same*)
(*TODO: efficiently write data?*)
Database[dataname_, nps_, nthreads_, fixps_:Automatic]:=Block[{dir, pid, cons, prime, pslist, oldps, res, time1, time2, posi},
dir = FileNameJoin[{$SearchDirectory, "database", dataname}];
If[!FileExistsQ[FileNameJoin[{dir, "red_common"}]], ErrorPrint["error: database has not been intialized yet."]; Abort[]];
BLSetDataNumber[dataname, nps+BLDataNumber[dataname]];

pid = GetFile[FileNameJoin[{dir, "red_pid"}]];
cons = GetFile[FileNameJoin[{dir, "red_cons"}]];
prime = FFPrimeNo[pid];
pslist = RandomInteger[{1, prime-1}, {nps, Length[BLIBPParameter]}];
If[FileExistsQ[FileNameJoin[{dir, "red_ps"}]],
  oldps=Import[FileNameJoin[{dir, "red_ps"}],"Table"];
  If[Length[Union[Flatten[oldps[[All,cons]]]]]=!=Length@cons,Print["database is not correct"];Abort[]];
  If[fixps===Automatic,
  pslist[[All,cons]]=oldps[[1,cons]],
  If[oldps[[1,cons]]=!=fixps,Print["database is not correct"];Abort[],pslist[[All,cons]]=fixps]]
  ,
  If[fixps===Automatic,
  pslist[[All,cons]]=pslist[[1,cons]],
  pslist[[All,cons]]=fixps]];
  
writeObject[FileNameJoin[{dir, "red_ps"}], pslist, OpenAppend];
(*transform from BLSearchParameter to BLIBPParameter*)
posi = FirstPosition[BLSearchParameter,#]&/@BLIBPParameter//Flatten;
pslist = pslist[[All,posi]]; 
time1 = AbsoluteTime[];
res = FFGraphEvaluateMany[$InternalGraph, pslist, "PrimeNo" -> pid, "NThreads"->nthreads];
time2 = AbsoluteTime[];
WriteMessage[ToString[nps]<>" data generated in "<>ToString[Round[time2-time1,0.001]]<>"s. name: "<>dataname];
time1 = AbsoluteTime[];
writeObject[FileNameJoin[{dir, "red_data"}], SparseTrans/@res, OpenAppend];
time2 = AbsoluteTime[];
WriteMessage[ToString[nps]<>" data write in "<>ToString[Round[time2-time1,0.001]]<>"s. name: "<>dataname];
];


(* ::Section:: *)
(*BLDatabase*)


BLDatabase[dataname_,nps_,pid_, fixps_:Automatic]:=Module[{},
If[!FiniteFlow`Private`FFGraphQ[$InternalGraph],LoadIBPSystem[];FromDatabaseInfo[];];
DatabaseInit[dataname,pid];
Database[dataname,nps,BLDatabaseNthreads, fixps];
];


(* ::Section:: *)
(*BLNthreadsLearning*)


(*must before BLDatabase*)
BLNthreadsLearning[]:=Module[{mem1,mem2,aval,time},
BLDatabaseNthreads = BLNthreads;
If[!FiniteFlow`Private`FFGraphQ[$InternalGraph],LoadIBPSystem[];FromDatabaseInfo[];];
If[BLNthreads==1,WriteMessage["BLNthreads-> 1, there is no need to learn."], (*<-- also need to load IBPsystem*)
mem1=MemoryAvailable[];
FFGraphEvaluateMany[$InternalGraph,RandomInteger[{1,FFPrimeNo[0]-1},{2,Length[BLIBPParameter]}],"NThreads"->2];
mem2=MemoryAvailable[];
aval=If[mem1<=mem2+1,Infinity,2+Quotient[0.8*mem2,(mem1-mem2)]];
If[BLDatabaseNthreads>aval,BLDatabaseNthreads=aval;WriteMessage["Database Nthreads-> ",aval,", constraint by memory."],
WriteMessage["Database Nthreads-> ",BLDatabaseNthreads,", there is enough memory"]];];
time=AbsoluteTiming[FFGraphEvaluateMany[$InternalGraph,RandomInteger[{1,FFPrimeNo[200]-1},{3,Length@BLIBPParameter}],"NThreads"->1];];
BLTimeIBP=time[[1]]/3;
WriteMessage["Average sample time for ibps (single core) -> "<>ToString[Round[BLTimeIBP,0.0001]]<>"s."];
]


(* ::Section:: *)
(*End*)


End[];
