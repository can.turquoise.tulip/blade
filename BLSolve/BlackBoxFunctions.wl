(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLDifferentialEquation::usage = "BLDifferentialEquation[invs_List, preferred_List:{},key_String:\"diffeq\"] constructs differential equations for the master integrals w.r.t the variables \"invs\". 
The raw master integrals are first identified using the maximal-cut method. 'preferred' has priority over the maximal-cut masters. 
'preferred' looks like {BL[topo,...],BL[topo,..] + BL[topo,...]}, where linear combinations are allowed.
\"PreferredMI\" can take on the following values: Automatic, \"Rank\", \"Dot\", \"MixedRank\", \"MixedDot\", \"None\". 	
	Automatic will use an ordering that is more likely to give masters in \"preferred\" and BLSetReducerOptions[\"IntegralOrdering\"].
	\"Rank\" will give maximal-cut masters with scalarproducts.
	\"Dot\" will give maximal-cut masters with double propagators.
	\"MixedRank\" is similar to \"Rank\", but \"Rank\"+\"Dot\" has top priority.
	\"MixedDot\" is similar to \"Dot\", but \"Rank\"+\"Dot\" has top priority.
	\"None\" if \"preferred\" constitutes a complete set of master integrals.
The derivatives are reduced using the function BLReduce. For more options, see ?BLReduce.
";
BLReduce::usage = "BLReduce[target_List,preferred_List_:{},key_String:\"reduce\",OptionsPattern[]] reduce target integrals, referred to as 'target', by splitting them into sub-families and reducing them individually.
'target' looks like {BL[topo,...], BL[topo,...]+BL[topo,...],...}. Linear combinations of BL integrals are allowed.
'preferred' is a list of preferred master integrals, e.g. {BL[topo,...], BL[topo,..] + BL[topo,...]}, where linear combinations are allowed.
The 'key' parameter specifies the working directory.

BLReduce divides target integrals into several sub-families and reduces them separately. Roughly speaking, it starts by selecting the most complex integral in the target(F_1)
and defining an integral family to reduce all integrals in target that have a rank and dot that are both less than or equal to those of F_1. It then repeat this process for the remaining target integrals untill all integrals are reduced.
The results of each sub-family are then merged together.
This is useful as far as squared amplitudes are considered, where sub-sectors may have high dots while top-sector may not have double propagator integrals.It is benefical to generate IBP systems with different rank and dots for these different target structures rather than jumbling everything together. 

\"DivideLevel\" determines the degree to which 'target' is split into sub-families. \"DivideLevel\" -> 0: don't split sub-families. 
\"DivideLevel\" -> 'n'(Positive integer), sub-sectors whose number of propagators is greater than or equal to the number of propagators in the top-sector minus 'n' will be split, while sub-sectors with 'n' fewer propagators than the top-sector will be combined together.
\"DivideLevel\" -> Infinity: complete decomposition  
Caution: \"DivideLevel\" does not give expected result if \"userdefined\" contains integrals distributed in more than one sub-families.
\"GenerateScheme\": function that determines how the target integral set is extended during the search for block-triangular form and defines sub-families as demonstrated by \"DivideLevel\". By default, we adopt BLGenerateScheme(more details can be found by ?BLGenerateScheme). One could define their preferred scheme, too.
\"ReadCacheQ\": allows the user to resume a previous reduction job.
\"FilterIBP\": an alternative to \"DivideLevel\" that generates a delicate IBPSystem with seed parameters(rank and dot) that differ by sector.
\"BladeMode\": specify the mode of block-triangular form to use during the reduction. Possible choices are None, Full and Automatic. None: do reduction without block-triangular form, Full: search fully-analytic block-triangular form, Automatic: adopt adaptive-search strategy,see ?BLAdaptiveSearch for more details. 
\"DeleteDirectory\": whether to delete the cache directory after the reduction.

BLReduce is a collection of many basic functions. The options of these basic functions can be modified using BLSetReducerOptions[],BLSetSchemeOptions[] and BLSetSearchOptions[] without the need for detailed knowledge of the internal functions.
";


Begin["`Private`"];
BLBlackBoxReduce::usage ="BLBlackBoxReduce[target0_List,preferred_List,userdefined_:{},key_String:\"reduce\",OptionsPattern[]]";
sortIntegrals::usage ="applicable to Non-BL integrals";
End[];


Begin["`BlackBoxFunctions`"];


(* ::Section:: *)
(*BLBlackBoxReduce*)


(*ReadCacheQ only support the same reduction job, which means InitialSearchOptions,target0,preferred must be same*)
(*Selected options of internal functions is available by calling BLSetReducerOptions[]*)
(*For more advanced options, modify corresponding functions individually*)
Options[BLBlackBoxReduce]={"GenerateScheme"->BLGenerateScheme, "ReadCacheQ"->True, "JoinFamilyQ"->False, "DivideLevel"->Infinity, "BladeMode"->Automatic, "DeleteDirectory"->False};


(*Update SolveNoBL: keep DivideLevel but only reduce original target integrals rather than extended integrals.
i) FilterIBP: subfamily should reduce BLs that occur in USInts;
			merge family only reduce original target integrals.
ii) BlackBoxIBP: only reduce original target integrals.*)


(*A combination of IBP + search + solve.*)
(*DivideLevel will be ignored if JoinFamilyQ is set to False*)
(*preferred0: a replacement rule of prefered master integrals*)
(*userdefined0: a replacement rule in case target0 invloves linear combinations of Feynman integrals*)
(*return {masters,rules}, where the keys of 'rules' are target integrals, the values of the 'rules' are projections onto 'masters'*)
BLBlackBoxReduce[target0_List,preferred0_List:{},userdefined0_List:{},key_String:"reduce",OptionsPattern[]]:=Block[
{$BLWorkingDirectory=CacheDirectory[key], GenerateScheme, top, preferred, userdefined,full,masters,target,subfam, flag,cache,ptsfile, points,nana,args, res,
datainfo,zeroints,noblflag,original,blademode},
CreateDir[$BLWorkingDirectory];

If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$ReductionDirectory,"results/table"}]],Return[Get[FileNameJoin[{$ReductionDirectory,"results/table"}]]]];
(************extend integral set************)
GenerateScheme=OptionValue["GenerateScheme"];

(*flag of SolveNoBL*)
noblflag=(OptionValue["BladeMode"]===None||Length[BLIBPParameter]===0);

(***************Determine raw masters belong to this sub-family*************)
top = BL[BLFamily,getTopSector[BLIntegralsIn[{target0,preferred0,userdefined0}]]];
BLRawMasters=Select[BLFamilyInf[BLFamily]["MaximalCutMasters"],BLSectorOrSubsectorQ[BLSector[#],top]&];

(*target: collect nonzero ints from target0&preferred0&userdefined0 to extend integral set, also determine topsector, IBPRank and Dot*)
(*If CheckMastersQ \[Rule] True, reduce BLRawMasters as well*)
(*remove zero ints from preferred0 && userdefined0 otherwise they would appear as master integrals *)
preferred = preferred0//BLEliminateZeroSectors;
userdefined=userdefined0//BLEliminateZeroSectors;
target = BLIntegralsIn[BLEliminateZeroSectors[{target0,userdefined}]];
If[target==={},Return[{{},{}}]];
target = BLIntegralsIn[BLEliminateZeroSectors[{target0,preferred,userdefined,BLRawMasters}]];

(***********ibp**************)
If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$ReductionDirectory,"ibpflag"}]]&&ContainsAll[Get[FileNameJoin[{$ReductionDirectory,"target"}]],target],
	WriteMessage["IBP system already exists"]
	,
	(*original target integrals, in case SolveNoBL; Join BLRawMasters, in case CheckMasters*)
	original = Join[Select[BLEliminateZeroSectors[target0],#=!=0&],BLRawMasters];
	(*20231221: divideSubFamilyBasic\[Rule]divideSubFamily such that the structure remains consistent regardless JoinFamily\[Rule]True|False*)
	divideSubFamily[original,preferred,userdefined,subfam,GenerateScheme,OptionValue["DivideLevel"]];
	flag = MyTiming[
			If[OptionValue["JoinFamilyQ"]&&subfam["Counter"]>1,
			    If[noblflag,
			        (*20230609: improve SolveNoBL*)
					FilterIBP[preferred,userdefined, subfam,"ReadCacheQ"->OptionValue["ReadCacheQ"],"NeededVars"->original]
					,
					FilterIBP[preferred,userdefined, subfam,"ReadCacheQ"->OptionValue["ReadCacheQ"]]
				]
				,
				full = If[noblflag, BLIntegralsIn[original],DeleteDuplicates[Flatten[subfam[#,"set"]&/@Range[subfam["Counter"]]]]];
				BlackBoxIBP[full,preferred, userdefined,"LearnFromUSIntsQ"->noblflag]
			],"GenerateIBP"];
	If[flag===$Failed,Return[$Failed]];
];

(*********check masters**********)
target = Join[First/@userdefined,BLIntegralsIn[BLEliminateZeroSectors[{target0}]]];
(*cut propagator: identify zero sectors failed at some special kinematic points, integrals turn out to be zero*)
(*20230429: linear propagators may give zero integrals, too. A more general check*)
(*1. targets are zero; 2. targets are masters; 3. mixed *)
datainfo=Get[FileNameJoin[{$ReductionDirectory,"results/datainfo"}]];
masters=datainfo[[2]];
zeroints=Pick[datainfo[[1]],Length/@datainfo[[5,1;;Length[datainfo[[1]]]]],0];
If[ContainsAll[zeroints,target],Return[{{},{}}]];
If[ContainsAll[masters,target],Return[{target,Thread[target->IdentityMatrix[Length@target]]}]];
If[ContainsAll[Join[zeroints,masters],target],target=Complement[target,zeroints];Return[{target,Thread[target->IdentityMatrix[Length@target]]}]];
target=Join[target,masters]//DeleteDuplicates;


If[ContainsAll[masters,target],Return[{target,Thread[target->IdentityMatrix[Length@target]]}]];

(*********if user don't want use block-triangular form or there is no IBP parameter, direct solve IBP system****************)
If[noblflag, res = BLSolveNoBL[target]//MyTiming,

(*********learn BLDatabaseNthreads and BLTimeIBP**********)
BLNthreadsLearning[]//MyTiming;
BLDataInfoInit[];

(**********search*************)
blademode=OptionValue["BladeMode"];
If[blademode===Automatic&&Length[BLIBPParameter]===1,blademode=Full];
cache="temporary";
If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$SearchDirectory,"nana"}]]&&With[{nnnaaa=Get[FileNameJoin[{$SearchDirectory,"nana"}]]},nnnaaa==0||(jobCreatedQ[SearchOptions[nnnaaa]["Jobname"]]&&jobFinishedQ[SearchOptions[nnnaaa]["Jobname"]])],
WriteMessage["Block-triangular system already exists"];
nana = Get[FileNameJoin[{$SearchDirectory,"nana"}]];
blReconstructParameter=Get[FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
If[OptionValue["BladeMode"]===Automatic && $IBPParameter===Automatic, BLSearchParameter=blReconstructParameter];
,
Switch[blademode,
Automatic,  (*<---degrees.fflow first, adpative-search second*)
	(*generate degrees.fflow for further analysis*)
	(*sort parameters and degrees.fflow.original if needed*)
	If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow"}]],
		WriteMessage["degrees.fflow already exists."];
		blReconstructParameter=Get[FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
		BLSearchParameter=blReconstructParameter;
		BLSetDefaultSearchOptions[];
		,
		If[$IBPParameter===Automatic,
			BLDumpDegrees[FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow.original"}]]//MyTiming;
			blSortDegrees[BLIBPParameter, FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow.original"}],
					FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow"}],
					FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
			(*modify BLSearchParameter and search options*)
			blReconstructParameter=Get[FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
			BLSearchParameter=blReconstructParameter;
			BLSetDefaultSearchOptions[];
			,
			BLDumpDegrees[FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow"}]]//MyTiming;
			Put[BLSearchParameter,FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
			blReconstructParameter=BLSearchParameter;
		];
	];
	
	(*register target integrals*)
	BLRegisterNeeded[target,cache];

	(*generate and load sample points for further analysis*)
	SetDirectory[FileNameJoin[{$BLWorkingDirectory,cache}]];
	ptsfile=FileNameJoin[{"points","points_"<>ToString[0]<>".fflow"}];
	If[FileExistsQ[ptsfile],WriteMessage[ptsfile<>" already exists, nothing to do"],BLDumpSamplePoints[0,ptsfile]//MyTiming];
	points=BLLoadSamplePoints[ptsfile];
	WriteMessage["number of sample points -> ",Length[points]];
	WriteMessage["Estimated wall time(IBP FiniteField) -> ", Round[Length[points]*BLTimeIBP/BLDatabaseNthreads,0.001]," s."];
	ResetDirectory[];
	
	(*search the BL system with \"nana\" variables*)
	(*`nana` variables is the optimum choice weighing the benifit of sovle block-triangular-form against the time-consuming for searching block-triangular-form*)
	nana=BLAdaptiveSearch[points]//MyTiming;
	If[nana===$Failed,Return[$Failed]];
	Put[nana,FileNameJoin[{$SearchDirectory,"nana"}]],
	
Full, (*<---search first, degrees.fflow second*)
	nana=Length[BLSearchParameter];
	args = SearchOptions[nana][#]&/@{"Jobname","DatanamePrefix","SearchVariable","VariableWeight","IntegralWeight","CutIncrement"};
	args[[2]] = args[[2]]~~"_0"; (*<-- reduce one prime field*)
	If[OptionValue["ReadCacheQ"]&&jobCreatedQ[SearchOptions[nana]["Jobname"]]&&jobFinishedQ[SearchOptions[nana]["Jobname"]],
		WriteMessage["Block-triangular system already exists"],
		BLSearchRelations[Sequence@@args]//MyTiming;
	];
	
	(*generate degrees.fflow with the block-triangular form*)
	MyTiming[blDumpDegreesBT[SearchOptions[nana]["Jobname"],SearchOptions[nana]["DatanamePrefix"],"degrees.fflow",cache],"blDumpDegreesBT"];
	blReconstructParameter=Get[FileNameJoin[{$BLWorkingDirectory,cache,"parameters"}]];
		(*register target integrals*)
	BLRegisterNeeded[target,cache];

	(*print #samples and t_solve for further analysis*)
	SetDirectory[FileNameJoin[{$BLWorkingDirectory,cache}]];
	ptsfile=FileNameJoin[{"points","points_"<>ToString[0]<>".fflow"}];
	If[FileExistsQ[ptsfile],WriteMessage[ptsfile<>" already exists, nothing to do"],BLDumpSamplePoints[0,ptsfile]//MyTiming];
	WriteMessage["number of sample points -> ",BinaryReadList[ptsfile,"UnsignedInteger64",2][[2]]];
	blSolveLearning[args[[1]],args[[2]],0];
	WriteMessage["time for solving(single core) -> ",Round[GetFile[FileNameJoin[{$SearchDirectory,args[[1]],"average_time"}]],0.0001]," s."];
	ResetDirectory[];	
	Put[nana,FileNameJoin[{$SearchDirectory,"nana"}]],
_,
	ErrorPrint["error: unknown options for \"BladeMode\""];Abort[]
];
];

(**********solve BL*************)
If[nana===Length[BLSearchParameter]&&False,  (*<--- t_ssolve > t_fflow TBD*)
	(*SolveFullBL, using finiteflow as sparse solver*)
	res=BLSolveFullBL[SearchOptions[nana]["Jobname"],SearchOptions[nana]["DatanamePrefix"],target]//MyTiming
	,
	(*SolveSemiBL, target has been registered*)
	BLRegisterNeeded[target,cache];
	res=BLSolveSemiBL[nana,cache]//MyTiming
];
];

(*restore MIs*)
res[[2]]=Join[res[[2]],Thread[res[[1]]->IdentityMatrix[Length[res[[1]]]]]]//DeleteDuplicates;
(*restore zero integrals. target may contain zero integrals and BLSolveFullBL would drop them.*)
res[[2]]=Join[res[[2]], Replace[Complement[target0,Keys[res[[2]]]],a_:>(a->ConstantArray[0,Length@res[[1]]]),{1}]];

If[OptionValue["DeleteDirectory"],DeleteDir[$BLWorkingDirectory];DeleteDir[$MaximalCutDirectory],Put[res,FileNameJoin[{$ReductionDirectory,"results/table"}]]];
BLClearSystem[];
res
];



(* ::Section:: *)
(*BLReduce*)


(* ::Subsection:: *)
(*checkIntegralForm*)


checkIntegralForm[expr_]:=With[{ints=Complement[Variables[{expr,BLExtraIntDerivDen,BLExtraIntDerivPara}],Variables[{Keys[BLRules],BLIBPParameter}]], npd=Length[BLPropagators]},
If[AllTrue[ints,MatchQ[#,BL[BLFamily,{___Integer},_:Null]]&&Length[#[[2]]]===npd&],{},Print["error: wrong format of input integrals"];Abort[]]]


(* ::Subsection:: *)
(*tointernal*)


(*target: any structure, standard Feynman integral or linear combination*)
(*preferred: list, standad Feynman integral or linear combination*)
(*return a list of target integrals and a replacemnt rule for non-standard Feynman integrals*)
tointernalTarget[target_]:=Module[{rep,count,res,usints},
Attributes[rep]={Listable};
count=0;
usints={};
rep[a_]:=If[Head[a]===BL,a,count++;AppendTo[usints,Symbol["BLInternalUS"][count]->a];Symbol["BLInternalUS"][count]];
res=rep[target];
{res,Union[Flatten[res]],usints}
];


(*prefer: a list of preferred master integrals*)
(*return a list of rule, like {BLInternalEX[1]\[Rule]BL[box,{1,1}],BLInternalEX[2]\[Rule]BL[box,{1,-1}]+BL[box,{1,2}]}*)
tointernalPrefer[prefer_]:=Table[Symbol["BLInternalEX"][ii]->prefer[[ii]],{ii,1,Length@prefer}];


(* ::Subsection:: *)
(*BLReduce*)


(*DivideLevel: define sub-families according to BLIntPropagators*)
(*JoinFamilyQ: join trimmed equations in sub-families*)
(*It is recommended to set "JoinFamilyQ" to True if userdefined0 contains integrals distributed in more than one sub-families.*)
Options[BLReduce]=Join[{},Options[BLBlackBoxReduce]];
BLReduce[target00_List,preferred00_List:{},key_String:"reduce",OptionsPattern[]]:=Block[
{$BLWorkingDirectory=CacheDirectory[key], target0, preferred0, userdefined0, origin, opt, preferred, userdefined, allints,
target,masters,top,tar,pre,table,sysid,subfam,res,joinfamilyQ,zerofy},

checkIntegralForm[{target00,preferred00}];

(*cache*)
If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$ReductionDirectory,"results/table"}]],Return[Get[FileNameJoin[{$ReductionDirectory,"results/table"}]]]];
If[target00==={},Return[{{},{}}]];

WriteMessage["DivideLevel -> ",OptionValue["DivideLevel"]];
MyTiming[

(*to internal format, for the convenience of user*)
{origin,target0,userdefined0}=tointernalTarget[target00];
preferred0=tointernalPrefer[preferred00];
preferred0=SortBy[preferred0,-Max[BLIntPropagators/@BLIntegralsIn[#]]&];(*sort preferred integrals, such that sub-sectors are never reduced to topsector.*)

(***************Determine zero sectors(and raw masters)*************)
(*top = getTopSector[BLIntegralsIn[{target0,preferred0,userdefined0}]];*)
If[$CheckMastersQ, 
	If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$MaximalCutDirectory,"results/rawmastersnomap"}]]&&Get[FileNameJoin[{$MaximalCutDirectory,"config"}]]===BLFamilyInf[BLFamily]["TopSector"],
		WriteMessage["MaximalCutMasters already exist."];
		BLFamilyInf[BLFamily]["MaximalCutMasters"]=GetFile[FileNameJoin[{$MaximalCutDirectory,"results/rawmastersnomap"}]];
		,
		BLMaximalCutMasters[BLFamilyInf[BLFamily]["TopSector"],preferred0,"AnalyzeSectorsOnly"->False,"PreferredMI"->Automatic]//MyTiming
	];
	,
	If[OptionValue["ReadCacheQ"]&&FileExistsQ[FileNameJoin[{$MaximalCutDirectory,"results/zerosectors"}]]&&Get[FileNameJoin[{$MaximalCutDirectory,"config"}]]===BLFamilyInf[BLFamily]["TopSector"],
		WriteMessage["ZeroSectors already exist."];
		BLFamilyInf[BLFamily]["MaximalCutMasters"]={};
		,
		BLMaximalCutMasters[BLFamilyInf[BLFamily]["TopSector"],preferred0,"AnalyzeSectorsOnly"->True];
	]
];

(*remove zero sectors,necessary*)
(*target : to be reduced nonzero integrals, including user-defined integrals*)
(*allints: all BL integrals to generate scheme*)
preferred = Select[preferred0//BLEliminateZeroSectors,#[[2]]=!=0&];
userdefined = Select[userdefined0//BLEliminateZeroSectors,#[[2]]=!=0&];
target = Join[First/@userdefined,BLIntegralsIn@BLEliminateZeroSectors[target0]];
If[target==={},
	WriteMessage["      BLReduce done(target integrals are trival)."];
	BLFamilyInf[BLFamily]["Masters"]={};
	BLFamilyInf[BLFamily]["MastersRules"]={};
	Attributes[zerofy]={Listable};
	zerofy[expr_]:=0;
	Return[zerofy[origin]];
];
allints =  BLIntegralsIn[BLEliminateZeroSectors[{target0,preferred,userdefined}]];

(*********Perform reduction jointly or individually ******************)
If[AnyTrue[Values@Join[userdefined,preferred],Length[BLIntegralsIn[#]]>1&],WriteMessage["Linear combinations of Feynman integrals detected, \"JoinFamilyQ\" is set to True forcibly."];joinfamilyQ=True];
If[TrueQ[OptionValue["JoinFamilyQ"]]||TrueQ[joinfamilyQ],
	subfam["Counter"]=1;
	opt=FilterRules[(#->OptionValue[BLReduce,#])&/@Keys[Options[BLReduce]],Except[{"JoinFamilyQ","DeleteDirectory"}]];
	MyTiming[table[1]=BLBlackBoxReduce[target,preferred, userdefined, key,"JoinFamilyQ"->True,"DeleteDirectory"->False,Sequence@@opt],"BLReduce"];
	,
	
	(*Define sub-families according to the 'GenerateScheme'*)
	divideSubFamily[target,preferred,userdefined,subfam,OptionValue["GenerateScheme"],OptionValue["DivideLevel"]];

	(*reduce sub-families in the reverse order*)
	masters={};
	opt=FilterRules[(#->OptionValue[BLReduce,#])&/@Keys[Options[BLReduce]],Except[{"JoinFamilyQ","DeleteDirectory"}]];
	res=Catch[Do[
		WriteMessage["Solve system id -> ", sysid,", Sector -> ", subfam[sysid,"top"]];
		If[subfam[sysid,"target"]=!={},
			(*include masters of sub-families in target to give a minimal set of basis. Only BLints is needed, ex[i] is bound to be masters*)
			tar=Flatten[{subfam[sysid,"target"],Select[BLIntegralsIn[masters],BLSectorOrSubsectorQ[BLSector[#],subfam[sysid,"top"]]&]}]//DeleteDuplicates;
			(*20221204: pre is necessary since we should carry out the reduction completely*)
			pre=Select[preferred, AllTrue[Union[BLSector/@BLIntegralsIn[#]],BLSectorOrSubsectorQ[#,subfam[sysid,"top"]]&]&];
			table[sysid]=MyTiming[BLBlackBoxReduce[tar,pre, subfam[sysid,"usints"], key<>"_"<>ToString[sysid],"JoinFamilyQ"->False,"DeleteDirectory"->False,Sequence@@opt],"BLReduce"];
			If[table[sysid]===$Failed,ErrorPrint["system id ->", sysid," failed."];Throw[$Failed]];
			masters=Join[masters,table[sysid][[1]]]//DeleteDuplicates,
			table[sysid]={{},{}};
		];
	,{sysid,Reverse[Range[subfam["Counter"]]]}]];
	If[res===$Failed,Return[$Failed]];
];

(*merge reduction table into minimal basis*)
MyTiming[res=mergeTable[table,subfam["Counter"],preferred],"MergeReductionTable"];

(*restore zero integrals. target may contain zero integrals as well.*)
res[[2]]=Join[res[[2]],Replace[Complement[target0,Keys[res[[2]]]],a_:>(a->ConstantArray[0,Length@res[[1]]]),{1}]];

(*restore complex numerical replacement(if any)*)
res=res/.BLReverseRule;

(*format transformation: return explicit linear combinations of master integrals*)
BLFamilyInf[BLFamily]["Masters"]=res[[1]];
BLFamilyInf[BLFamily]["MastersRules"]=preferred0;
res=Rule[#[[1]],#[[2]] . res[[1]]]&/@res[[2]];
res=origin/.Dispatch[res];

(*remove cache directory*)
If[OptionValue["DeleteDirectory"],
	DeleteDir[$BLWorkingDirectory];
	DeleteDir[$MaximalCutDirectory];
	(*explicit $ReductionDirectory and $MaximalCutDirectory*)
	DeleteDir[CacheDirectory[key<>"_"<>ToString[#]]]&/@Range[subfam["Counter"]]
	,
	CreateDir@FileNameJoin[{$ReductionDirectory,"results"}];
	Put[res,FileNameJoin[{$ReductionDirectory,"results/table"}]]
];

res,"BLReduce"]
];



(* ::Section:: *)
(*mergeTable*)


mergeTable[table_,count_,preferred_]:=Module[
{masters,heads, integralsIn, rule,mis,tab,result,tmp,posi,limit,res,finalrule},
(*masters: minimal basis*)
(*rule: reduction rule of sub-family master integrals to minimal-basis*)
masters={};
rule=Association[{}];
Do[
mis=table[sysid][[1]];
tab=table[sysid][[2]];
(*nontrivial reduction rule*)
(*fixbug: upgrade rule to project onto mis as much as possible. Otherwise, table[1]=b\[Rule]c, table[2]=d\[Rule]e,table[3]=a\[Rule]b, table[4]=b\[Rule]a, table[5]=a\[Rule]b would fail*)
(*Do[If[MemberQ[masters,tab[[i,1]]]&&(!MemberQ[mis,tab[[i,1]]]),rule[tab[[i,1]]]=tab[[i,2]].mis],{i,Length[tab]}];*)
Do[If[MemberQ[Join[masters,Keys[rule]],tab[[i,1]]]&&(!MemberQ[mis,tab[[i,1]]]),rule[tab[[i,1]]]=tab[[i,2]] . mis],{i,Length[tab]}];
(*It is allowed to drop a and b from masters because b would appear in the subsequent reduction undoubtedly(implement by BLReduce)*)
masters=Join[Complement[masters,Keys[rule]],mis]//DeleteDuplicates;
,{sysid,Reverse[Range[count]]}];

WriteMessage["# masters ->",Length[masters]];
masters=sortIntegrals[masters,preferred];
(*20231129: if there are many preferred, BL[xxx] may be neglected, leads to unexpected `finalrule`, e.g. {BL[a]\[Rule]BL[b], BL[b]\[Rule]EX[1]}*)
heads = Union[Head/@masters,{BL}];
integralsIn[expr00_]:=DeleteDuplicates[Cases[{expr00},Alternatives@@(Blank/@heads),Infinity]];
(*in case of {a\[Rule]b,b\[Rule]a} *)
tmp=rule;
limit=1;
finalrule={};
While[Length[tmp]>0 && limit<=2000,
finalrule=Join[finalrule,Select[Normal[tmp],ContainsAll[masters,integralsIn[Values[#]]]&]];
tmp=Association[Select[Normal[tmp],!ContainsAll[masters,integralsIn[Values[#]]]&]]/.finalrule//Collect[#,_BL,Together]&;
limit++];
If[limit>2000,ErrorPrint["reach max recursion limit 2000"];Abort[]];

result=Association[{}];
Do[
WriteMessage["merge system id -> ",sysid];
mis=table[sysid][[1]]/.finalrule;
If[!ContainsAll[masters,integralsIn[mis]],ErrorPrint["error: integral reduce to another basis"];Abort[]];
tab=table[sysid][[2]];
If[ContainsAll[masters,mis],
	posi=Position[masters,#]&/@mis//Flatten;
	(*20230504: add all conponent of mis, in case posi={5,5,4,1}, tmp[[{5,5,4,1}]]={1,0,0,0} \[Rule] {0,0,0,0}*)
	res=Table[tmp=ConstantArray[0,Length[masters]];Do[tmp[[posi[[j]]]]=tmp[[posi[[j]]]]+tab[[i,2]][[j]],{j,Length@posi}];tmp,{i,Length[tab]}];(*<-- reduce time consumption*)
	
	,
	If[$KernelCount==0,LaunchKernels[BLNthreads]];
	DistributeDefinitions[mis,tab,masters,heads];
	res=ParallelTable[
		tmp=Collect[tab[[i,2]] . mis,Alternatives@@(Blank/@heads),Together];
		If[tmp===0,ConstantArray[0,Length[masters]],CoefficientArrays[tmp,masters][[2]]//Normal]
	,{i,Length[tab]},DistributedContexts->None,Method->"FinestGrained"];
];
Do[result[tab[[i,1]]]=res[[i]],{i,Length[tab]}]
,{sysid,1,count}];

Return[{masters,result//Normal}]
];




(* ::Section:: *)
(*BLDifferentialEquation*)


(*Construct differential equations w.r.t invs.
1. Find a set of basis 'masters' that is large enough to construct differential equation, with \"preferred\" as preferred master integrals. 
MaximalCut masters are added to complete the basis.
2. Compute derivatives of 'masters' w.r.t invs.
3. Call BLReduce to reduce derivatives, with 'masters' being newly preferred.
4. Sort differential equations(block-triangular form) and return*)
Options[BLDifferentialEquation]=Join[{"PreferredMI"->Automatic,"RefineSchemeQ"->True},Options[BLReduce]];
SetOptions[BLDifferentialEquation,"DivideLevel"->0];
BLDifferentialEquation[invs_List, preferred_List:{},key_String:"diffeq", OptionsPattern[]]:=Block[
{$BLWorkingDirectory=CacheDirectory[key], masters, diffeqinfo, opt, learn, func, GenerateScheme, res, topo, indepvars, pos, diffeq, sortmasters,pindex, perm, protectq, learna},

checkIntegralForm[{preferred}];

If[OptionValue["ReadCacheQ"] &&FileExistsQ[FileNameJoin[{$ReductionDirectory,"results/diffeq"}]],Return[Get[FileNameJoin[{$ReductionDirectory,"results/diffeq"}]]]];

(*step 1*)
MyTiming[
WriteMessage["Begin determine master integrals..."];
If[OptionValue["PreferredMI"]=!=None,
	BLMaximalCutMasters[preferred,"PreferredMI"->OptionValue["PreferredMI"]]//MyTiming;
	masters = GetFile[FileNameJoin[{$MaximalCutDirectory,"results/rawmasters"}]];
	,
	masters = {}];
masters=Join[masters,preferred];

(*step 2*)
WriteMessage["Begin compute derivatives..."];
diffeqinfo={masters,invs,Table[BLComputeDerivative[masters,inv]/.BLRules//Collect[#,_BL,Together]&,{inv,invs}]};
CreateDir@FileNameJoin[{$ReductionDirectory,"results"}];
Put[diffeqinfo,FileNameJoin[{$ReductionDirectory,"results/diffeqinfo"}]];

(*step 3*)
opt = (#->OptionValue[#])&/@Keys[FilterRules[Options[BLDifferentialEquation],Except[{"PreferredMI","RefineSchemeQ"}]]];
protectq=MemberQ[Attributes[Evaluate[OptionValue["GenerateScheme"]]],Protected];
If[protectq,Unprotect[Evaluate[OptionValue["GenerateScheme"]]]];
(*refine GenerateScheme for constructing DE.*)
If[TrueQ@OptionValue["RefineSchemeQ"],
learn=Join[Normal@GroupBy[BLIntegralsIn[masters],BLIntPropagators->BLIntRank,Max],{Rule[Blank[],Infinity]}];
GenerateScheme=OptionValue["GenerateScheme"];
GenerateScheme["USFilter"]={Function[If[BLIntRank[#]>(BLIntPropagators[#]/.learna),False,True]]/.learna->Evaluate[learn]};
opt = Join[FilterRules[opt,Except[{"GenerateScheme"}]],{"GenerateScheme"->GenerateScheme}];
];
res = BLReduce[Join[diffeqinfo[[3]],diffeqinfo[[1]]],masters,key<>"_red",Sequence@@opt];  (*<-- in case derivative of MI does not reduce to itself*)
If[TrueQ@OptionValue["RefineSchemeQ"],GenerateScheme["USFilter"]=.];
If[protectq,Protect[Evaluate[OptionValue["GenerateScheme"]]]];
topo=FirstCase[diffeqinfo,_BL,{},Infinity][[1]];
indepvars=BLFamilyInf[topo]["Masters"];
If[Union[Head/@indepvars]=!={Symbol["BLInternalEX"]},ErrorPrint["error: reduced to another set of master integrals"];ErrorPrint[indepvars];Abort[]];

pos=indepvars[[All,1]];
diffeq=Table[Transpose[Coefficient[res[[i,pos]],#]&/@indepvars],{i,Length@diffeqinfo[[2]]}];

(*step 4*)
sortmasters = sortIntegrals[indepvars,BLFamilyInf[topo]["MastersRules"]];
pindex = PositionIndex[indepvars];
perm = Flatten[sortmasters/.pindex];
Table[diffeq[[i]]=diffeq[[i,perm,perm]],{i,Length@diffeq}];
If[OptionValue["DeleteDirectory"],DeleteDir[$BLWorkingDirectory],Put[{sortmasters,invs,diffeq},FileNameJoin[{$ReductionDirectory,"results/diffeq"}]]];

,"BLDifferentialEquation"];

{sortmasters/.BLFamilyInf[topo]["MastersRules"],invs,diffeq}
];



(*sort from simple to complex.  diffeq has lowertriangular form.*)
sortIntegrals[exp0_List,rep0_]:=Module[{rep,sort,ints},
rep=Dispatch[rep0];
sort = Table[
ints=BLIntegralsIn[exp0[[i]]/.rep];
If[ints==={},exp0[[i]]->ConstantArray[100,5],
exp0[[i]]-> {-BLIntPropagators[ints[[1]]],-BLIntDots[ints[[1]]],-BLIntRank[ints[[1]]],-Length[ints],BLSector[ints[[1]]]}],{i,Length@exp0}];

sort = SortBy[sort, Values[#]&];
sort = Join@@Reverse[Reverse/@GatherBy[sort, #[[2,5]]&]]//Keys
]


(* ::Section:: *)
(*End*)


End[];
