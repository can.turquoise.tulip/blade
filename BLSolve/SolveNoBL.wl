(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLSolveNoBL::usage="BLSolveNoBL[target] solves the traditional IBP systm rather than the block-triangular form and reduces 'target' integrals.";


Begin["`Private`"];
End[];


Begin["`SolveNoBL`"];


(* ::Section:: *)
(*template*)


(*Analogous to LearnCode[] except independent equations are solved*)
LearnCode2[]:={
"indep = Get@FileNameJoin@{resdir, \"indep\"};",

"FFNewGraph[graph,in,vars];
If[!(vars==={}),
FFAlgJSONSparseSolver[graph,ibps,{in},\"systemC.json\"],
eqsfiles = FileNames[\"ibps/ids_*.mx\"];
eqs=Join@@((Import[#]/.`ibprule`)&/@eqsfiles);
eqs=eqs[[indep]];
eqs=Thread[eqs\[Equal]0];
FFAlgSparseSolver[graph,ibps,{in},{},eqs,all,\"NeededVars\"\[Rule]target]];
FFSolverOnlyHomogeneous[graph,ibps];
FFGraphOutput[graph,ibps];
ibplearn = FFSparseSolverLearn[graph,all];
{nonmis, mis} = {\"DepVars\", \"IndepVars\"}/.ibplearn;
Print[\"number of integrals\" -> Length/@{nonmis,mis}];"
};


blSolveNoBLTemplate[dir_,printflag_:Automatic]:=Module[{extra,template,ibpvars,rule},
(*optimize the use of cpu for univariate case*)
extra = {
"sol = Switch[Length[vars],
0, FFReconstructNumeric[graph,vars,\"NThreads\"\[Rule]`nthreads`,\"MaxPrimes\"->`maxp`,\"MaxDegree\"->`maxd`,\"PrintDebugInfo\"->`prt`],
1, FFParallelReconstructUnivariate[graph,vars,\"NThreads\"\[Rule]`nthreads`,\"MaxPrimes\"->`maxp`,\"MaxDegree\"->`maxd`,\"PrintDebugInfo\"->`prt`],
_, FFReconstructFunction[graph,vars,\"NThreads\"\[Rule]`nthreads`,\"MaxPrimes\"->`maxp`,\"MaxDegree\"->`maxd`,\"PrintDebugInfo\"->`prt`]];
sol = Partition[sol,Length@mis];
sol = Join[sol, IdentityMatrix[Length@mis]];
rule = Thread[Join[nonmis,mis]->sol];",

"Put[{mis,rule},FileNameJoin@{resdir,\"table\"}];"
};
template = StringTemplate@StringJoin@Riffle[Join[BasicCode[],LearnCode2[],extra,QuitCode[]],"\n"];
rule = <|
"bladeibp"->$BLBladeIBPPath,
"ff"->$BLFiniteFlowPath,
"fflib"->$BLFiniteFlowLibraryPath,
"integralordering"->toStringInput[$IntegralOrdering],
"fam"->toStringInput[BLFamily],
"s" -> toStringInput[IBPRank],
"d" -> toStringInput[IBPDot],
"ibpvars"->toStringInput[BLIBPParameter],
"ibprule"->toStringInput[BLRules],
"maxp" -> toStringInput[$MaxPrime],
"maxd" -> toStringInput[$MaxDegree],
"prt" -> toStringInput[printflag],
"nthreads" -> toStringInput[BLNthreads]
|>;

FileTemplateApply[template,rule,FileNameJoin[{dir,"reducenobl.wl"}]];
];


(* ::Section:: *)
(*BLSolveNoBL*)


(*ibp system must be generated previously*)
Options[BLSolveNoBL]={"PrintDebugInfo"->Automatic};
BLSolveNoBL[target_,OptionsPattern[]]:=Module[{dir,time},
dir=$ReductionDirectory;
Put[target, FileNameJoin[{dir, "target"}]];
blSolveNoBLTemplate[dir,OptionValue["PrintDebugInfo"]];
BLRunCommand[{$WolframPath,"-noprompt", "-script",FileNameJoin[{dir,"reducenobl.wl"}]},"log"->FileNameJoin[{dir,"reducenobl.log"}]];

GetFile[FileNameJoin[{$ReductionDirectory,"results/table"}]]
];


(* ::Section:: *)
(*End*)


End[];
