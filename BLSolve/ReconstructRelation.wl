(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLReconstructRelations::usage ="BLReconstructRelations[datanameprefix_, job_]: search for block triangular relations under different prime fields and 
reconstruct rational coefficients. datanameprefix_0 must exist";


Begin["`Private`"];
End[];


Begin["`ReconstructRelation`"];


(* ::Section:: *)
(*chineseRemainderTheorm*)


evaluateM[p_List]:=Table[FiniteFlow`FFRatMod[p[[i]]/Times@@p, p[[i]]] Times@@p/p[[i]],{i,Length@p}];
(*a can be any data structure*)
chineseRemainderTheorm[a_List,p_List]:={Mod[Inner[Times,evaluateM[p],a,Plus],Times@@p],Times@@p};


(* ::Section:: *)
(*BLReconstructRelations*)


(*start from Prime_0*)
(*TODO: continue from the last running?*)
(*TODO: further maintainence?   DataNumber*)
BLReconstructRelations[datanameprefix_, job_]:=Block[{dir, null, getprimeid, getprime, dataname, reconstruct, rest, primeset, number, pid, rec, time1, time2},
Print["running ReconstructRelations..."];
time1 = AbsoluteTime[];
dir = FileNameJoin[{$SearchDirectory, job}];
null[name_, workid_, configid_]:=Import[FileNameJoin[{dir, ToString[workid], "fit", name, ToString[configid]}], "Table"];
getprimeid[name_]:=GetFile[FileNameJoin[{$SearchDirectory, "database", name, "red_pid"}]];
getprime[name_]:=Import[FileNameJoin[{$SearchDirectory, "database", name, "red_common"}],"Table"][[1,1]];
dataname[id_] := datanameprefix<>"_"<>ToString[id];

reconstruct[workid_, configid_]:=Block[{dir2, file, benchmark, testprime, nulls, primes, fit},
dir2 = FileNameJoin[{dir, ToString[workid], "fitM"}];
CreateDir[dir2];
file = FileNameJoin[{dir2, ToString[configid]}];

benchmark = null[dataname[0], workid, configid];
testprime = getprime[dataname[0]];

nulls = null[dataname[#], workid, configid]&/@primeset;
primes = getprime[dataname[#]]&/@primeset;
fit = chineseRemainderTheorm[nulls,primes];
fit = Table[FiniteFlow`FFRatRec[fit[[1,i]],fit[[2]]],{i,1,Length@fit[[1]]}];
(*fit === {{}}, fastwriteObject gives error*)
If[FiniteFlow`FFRatMod[fit, testprime] =!= benchmark, Return[0],
writeObject[file, fit, OpenWrite]; Return[1]];
];

rest = Join@@Table[{i, j}, {i, 0, workNumber[job]-1}, {j, 0, templateNumber[job,i,dataname[0]]-1}];
primeset = {};
number = GetFile@FileNameJoin[{$SearchDirectory,job,"fitnumber"}];

pid = 1;
If[Kernels[] === {}, LaunchKernels[BLNthreads]];
While[rest =!= {},

Print["rest works" -> Length[rest]];
AppendTo[primeset, pid];
BLDatabase[dataname[pid],number,pid];
BLFitRelations[dataname[pid], job];

rec = ParallelTable[reconstruct@@rest[[i]], {i,Length[rest]}, DistributedContexts -> All];
rest = Pick[rest, rec, 0];
pid++;
];
CloseKernels[];

time2 = AbsoluteTime[];
Print["ReconstructRelations finished in "<>ToString[IntegerPart[time2-time1]]<>"s."];
];


(* ::Section:: *)
(*End*)


End[];
