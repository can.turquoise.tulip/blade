(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLDumpDegrees::usage="BLDumpDegrees[file_] learns and dumps degrees by univariate reconstruction. It allows fflow to generate sample points before multivariate reconstruction";
BLRegisterNeeded::usage = "BLRegisterNeeded[need_,key_] registers needed integrals and generates the degree file \"selected_degrees.fflow\" from \"$BLWorkingDirectory/key/degrees.fflow\"";
BLDumpSamplePoints::usage = "BLDumpSamplePoints[pid_,ptsfile_] generates and dumps sample points within a finite field according to \"./selected_degrees.fflow\"";
BLLoadSamplePoints::usage = "BLLoadSamplePoints[infile_] loads sample points stored in \"infile\"";
BLSolveSemiBL::usage ="BLSolveSemiBL[nana_,key_:\"temporary\"] solves the block-triangular relations with 'nana' analytical parameters. results are stored in $BLWorkingDirectory/'key'";


Begin["`Private`"];
blDumpDegreesBT::usage="blDumpDegreesBT[job_,datanameprefix_,file_,key_] similar to BLDumpDegrees except the linear system comes from the block-triangualr form rather than IBP.";
blSolveSemiBLMod::usage ="blSolveSemiBLMod[job_, datanameprefix_, paras_, nana_, pid_, nthreads0_, key_] solves the block-triangular relations of 'job'+'datanameprefix' under the prime field FFPrimeNo['pid']. 'nana' represents the number of analytic parameters in the block-triangular relations. 'para' is the reconstructed parameters and 'nthreads0' is the number of threads used in parallel computation.";
blSolveLearning::usage ="blSolveLearning[job_,name_,primeid_] learn average sample time of block-triangular relations";
blSortDegrees::usage="blSortDegrees[paras_,indegfile_,outdegfile_,outparafile_] sort 'paras' by complexity and regenerate degree files.";
blReconstructParameter::usage="parameters for multivariate reconstruction.";
End[];


Begin["`SolveSemiBL`"];


(* ::Section:: *)
(*BLDumpDegrees*)


(*dump degrees.fflow of origin IBP system*)
BLDumpDegrees[file_]:=Module[{},
FFAllDegrees[$InternalGraph,BLDatabaseNthreads,"MaxDegree"->$MaxDegree];
CreateDir@DirectoryName[file];
FFDumpDegrees[$InternalGraph,file];
];


(* ::Section:: *)
(*blDumpDegreesBT*)


(*
		  S = I ("IBPParameter")
		     |
-----------------------------
None      Automatic        Full
|            |               |
|   ------------------       |
|   |"IBPParameter" |Auto    |
|  R = S        S = R = *     |
|      -----------           |
|            | adaptive-     | search
|            | search     R = * (blDumpDegreesBT)
|            |               |
|R = I       |        default |  TBD (t_ssolve > t_fflow)
|            |----------------------
|            |                      |
SolveNoBL SolveSemiBL          SolveFullBL
*)
(*
I: BLIBPParameter, ordering of numerical IBP;
S: BLSearchParameter, ordering of polynomial ansatz;
R: blReconstructParameter, ordering of degrees.fflow, points.fflow, toRationalMPI[...].*)
(*Transformation is needed for:
search: S \[TwoWayRule] I; 
	a) correct numerical IBP input for search.
reconstruction:  R \[TwoWayRule] S,  R \[TwoWayRule] I 
	b) introduce `ptsord` so that ssolve(S) can evaluate points.fflow(R), its output maintains points.fflow.
	c) FFGraphEvaluate depend on R \[TwoWayRule] I, its output maintains points.fflow.
	c) toRationalMPI[..., blReconstructParameter].
*)


(*dumpe degrees.fflow from the block-triangualr form*)
(*keeping the ordering of original IBP system, i.e. the result is the same as BLDumpDegrees+blSortDegrees*)
(*faster than BLDumpDegrees, used with BLReduce[..., "BladeMode"\[Rule]Full]*)
blDumpDegreesBT[job_,datanameprefix_,file_,key_]:=Module[
{cachefolder,pid,data,fitnumber,relations,allvariables,datainfo,depvars,indepvars,nzlearn,integralid,elems,len,take,newdeg},

cachefolder = FileNameJoin[{$BLWorkingDirectory,key}];
CreateDir[cachefolder];

pid=0;
data[n_] := datanameprefix<>"_"<>ToString[n];
(*collect relations*)
fitnumber = Get@FileNameJoin[{$SearchDirectory,job,"fitnumber"}];
If[! jobFitQ[data[pid], job], BLClearDatabase[data[pid]]; $Conservative={}; BLDatabase[data[pid], fitnumber,pid]//MyTiming; BLFitRelations[data[pid], job]//MyTiming;];
relations = MyTiming[Join@@BLCollectRelations[data[pid], job],"BLCollectRelations"];
allvariables=sortIntegrals[AllIntegrals,Join[USInts,EXInts]];

(*dump degrees.fflow and parameters*)
(*BLSearchParameter is just used for learning*)
BLReductionGraph[relations, BLSearchParameter,allvariables, allvariables ,MIs,pid,BLNthreads,cachefolder,"LearnParameterOrdering"->True,"LearnUnivariate"->True]//MyTiming;

(*check block-triangular form  vs. datainfo*)
(*the ordering of depvars differs and bt consists of MIs, the ordering of indepvars is the same*)
datainfo = Get[FileNameJoin[{$ReductionDirectory,"results/datainfo"}]];
depvars=Get[FileNameJoin[{cachefolder,"depvars"}]];
indepvars=Get[FileNameJoin[{cachefolder,"indepvars"}]];
nzlearn = Get[FileNameJoin[{cachefolder,"nzlearn"}]];
If[!ContainsAll[depvars,datainfo[[1]]] || indepvars=!=datainfo[[2]],ErrorPrint["error: dismatch pattern between block-triangular form and numerical IBP"];Abort[]];

(*tranform degrees.fflow to degrees.fflow*)
Do[integralid[depvars[[i]]]=i,{i,Length@depvars}];
elems=integralid/@datainfo[[1]];
(*the number of nonzero projection for depvars should be the same.*)
If[BinCounts["NonZero"/.nzlearn,{1,("All"/.nzlearn)+1,Length[MIs]}][[elems]]=!=Drop[Length/@datainfo[[5]],-Length[MIs]],
	ErrorPrint["error: dismatch pattern between block-triangular form and numerical IBP"];Abort[]];
len=Prepend[Accumulate[BinCounts["NonZero"/.nzlearn,{1,("All"/.nzlearn)+1,Length[MIs]}]],0];
take=Table[Range[len[[i]]+1,len[[i+1]]],{i,elems}]//Flatten;
newdeg=takedegrees[uint64load[FileNameJoin[{cachefolder,"degrees.fflow"}]],take];

(*back up*)
RenameFile[FileNameJoin[{cachefolder,"nzlearn"}],FileNameJoin[{cachefolder,"nzlearn.blorder"}],OverwriteTarget->True];
RenameFile[FileNameJoin[{cachefolder,"depvars"}],FileNameJoin[{cachefolder,"depvars.blorder"}],OverwriteTarget->True];
RenameFile[FileNameJoin[{cachefolder,"indepvars"}],FileNameJoin[{cachefolder,"indepvars.blorder"}],OverwriteTarget->True];
RenameFile[FileNameJoin[{cachefolder,"degrees.fflow"}],FileNameJoin[{cachefolder,"degrees.fflow.blorder"}],OverwriteTarget->True];

(*dump*)
uint64dump[newdeg,FileNameJoin[{cachefolder,file}]];
];


(* ::Section:: *)
(*blSortDegrees*)


(*numtot,dentot,x1nummax,x1nummin,x1denmax,x1denmin,x2nummax,x2nummin,x2denmax,x2denmin...*)
blSortDegrees[paras_,indegfile_,outdegfile_,outparafile_]:=Module[
{deg,nparsin,nparsout,step,temp,ordering,degp},
If[!FileExistsQ[indegfile],Print["error: file not exist ->",indegfile];Abort[]];
deg=uint64load[indegfile];
nparsin=deg[[1]];
nparsout=deg[[2]];
If[Length[paras]=!=nparsin,Print["error: unmatched degrees.fflow"];Abort[]];
step=nparsin*4+2;

(*numerator+denominator*)
temp=Table[
Max[deg[[2+(i-1)*4+3;; ;;step]]-deg[[2+(i-1)*4+4;; ;;step]]+deg[[2+(i-1)*4+5;; ;;step]]-deg[[2+(i-1)*4+6;; ;;step]]],{i,1,nparsin}];

(*sort parameter by complexity*)
ordering=Reverse[Ordering[temp]];
degp=Partition[deg[[3;;]],step];
temp=Flatten[Table[Range[(i-1)*4+1,i*4],{i,nparsin}][[ordering]]];
Do[degp[[i,3;;]]=degp[[i,3;;]][[temp]],{i,Length@degp}];

uint64dump[Flatten[{deg[[1;;2]],degp}],outdegfile];
Put[paras[[ordering]],outparafile];
];


(* ::Section:: *)
(*Interface to FiniteFlow*)


(*path: where degrees computed by fflow exist*)
uint64load[path_]:=BinaryReadList[path,"UnsignedInteger64"];
(*elems: selected output , e.g. {1,2,15,10} *)
takedegrees[deg_,elems_]:=Module[
{nparsin,nparsout,degp},
{nparsin,nparsout}=deg[[1;;2]];
degp=Partition[deg[[3;;]],2+nparsin*4];
{nparsin,Length@elems,degp[[elems]]}//Flatten
];
uint64dump[list_,path_]:=(BinaryWrite[path,list,"UnsignedInteger64"];Close[path]);


constBitArrayView[flagarray_]:=Join@@(Reverse/@IntegerDigits[flagarray,2,64]);


(*save memory, partition evallist to pieces and dump. otherwise the momory usage will double*)
(*"DivideFraction"\[GreaterEqual]1*)
Options[dumpevaluations]={"DivideFraction"->10};
dumpevaluations[data_,points_,nparsin_,nparsout_,name_,OptionsPattern[]]:=Module[
{diff,evalist,flaglist,evafflow,size,part,open},
(*20230221: if divide exactly, no need to minus 64*)
diff=With[{rem=QuotientRemainder[nparsout,64][[2]]},If[rem===0,64,rem]]-64;

size=Ceiling[Length[points]/OptionValue["DivideFraction"]];
part = With[{rem=Mod[Length[points],size],range=Range[Length[points]]},If[rem===0,Partition[range,size],Join[Partition[range,size],{range[[-rem;;]]}]]];

open=OpenWrite[name,BinaryFormat->True];
BinaryWrite[open,Flatten[{nparsin,nparsout+Ceiling[nparsout/64],Length@points,0}],"UnsignedInteger64"];
Do[
evalist=Table[
flaglist=Drop[constBitArrayView[points[[i,nparsin+2;;]]],diff];
Join[points[[i]],PadRight[Pick[data[[i]],flaglist,1],nparsout,0]],{i,kk}];
BinaryWrite[open,evalist,"UnsignedInteger64"];
,{kk,part}];

Close[open];
];


(* ::Section:: *)
(*BLRegisterNeeded*)


(*register needed integrals*)
BLRegisterNeeded[need_,key_]:=Module[{cachefolder,depvars,indepvars,nzlearn,degcache},
cachefolder=FileNameJoin[{$BLWorkingDirectory,key}];
CreateDir[cachefolder];

(*pick needed elements*)
{depvars,indepvars,nzlearn}=selectedInfo[need];
Put[depvars,FileNameJoin[{cachefolder,"depvars"}]];
Put[indepvars,FileNameJoin[{cachefolder,"indepvars"}]];
Put[nzlearn,FileNameJoin[{cachefolder,"nzlearn"}]];

(*selected degrees.fflow*)
degcache=FileNameJoin[{cachefolder,"selected_degrees.fflow"}];
selectedDegrees[FileNameJoin[{cachefolder,"degrees.fflow"}],degcache];
]


(*after FromDatabaseInfo[] *)
(*generate $Elements , return  {depvars,indepvars,nzlearn} *)
(*target0: drop MIs to be target. Because degrees.fflow does not contain information of MIs*)
(*$Elements: a sublist of output of $InternalGraph that corresponds to target integrals*)
(*nzlearn: nonzero information of $Elements(the same as nzlearn obtained by BLReductionGraph)*)
selectedInfo[target0_]:=Module[{target,intid,elems,datainfo, len, nzlearn},

target= Complement[target0,MIs];
$InternalTarget=target;
If[!ContainsAll[AllIntegrals,target],Print["error: current system is not sufficient to reduce target"];Abort[]];
intid[_]:=0;
Do[intid[AllIntegrals[[i]]]=i,{i,1,Length@AllIntegrals}];

elems=DeleteCases[intid/@target,0];

datainfo = Get[FileNameJoin[{$ReductionDirectory,"results/datainfo"}]][[5]];
len=Prepend[Accumulate[Length/@datainfo],0];
$Elements=Table[Range[len[[i]]+1,len[[i+1]]],{i,elems}]//Flatten;

datainfo=datainfo[[elems]];
Do[datainfo[[i]]=datainfo[[i]]+(i-1)*Length[MIs],{i,Length@datainfo}];
nzlearn={"All"->Length[datainfo]*Length[MIs],"NonZero"->Flatten[datainfo]};
{target,MIs,nzlearn}
]


(*after selectedInfo*)
selectedDegrees[degfile_,outfile_]:=Module[{intid,elems,deg,newdeg},
deg=uint64load[degfile];
newdeg=takedegrees[deg,$Elements];
uint64dump[newdeg,outfile];
];


(* ::Section:: *)
(*BLDumpSamplePoints*)


(*Directory*)
(*Caution: the ordering of points.fflow is the same as the ordering of degrees.fflow*)
BLDumpSamplePoints[pid_,ptsfile_]:=Module[{degcache,nparsin,nparsout,dummy,ord,pts,ptsp,maxdeg},
(*generate sample points*)
degcache=FileNameJoin[{"selected_degrees.fflow"}];
{nparsin,nparsout}={"NParsIn","NParsOut"}/.FFNParsFromDegreesFile[degcache];
CreateDir[DirectoryName[ptsfile]];
FFNewDummyGraph[dummy,nparsin,nparsout];
FFLoadDegrees[dummy,degcache];
maxdeg=If[nparsin===1,uint64load[degcache][[3;;]]//Max,$MaxDegree];(*<-- update univariate *)
FFDumpSamplePoints[dummy,ptsfile,"StartingPrimeNo"->pid,"MaxPrimes"->1,"MaxDegree"->maxdeg]
(*If[blReconstructParameter===BLSearchParameter,
	FFDumpSamplePoints[dummy,ptsfile,"StartingPrimeNo"->pid,"MaxPrimes"->1]
	,
	FFDumpSamplePoints[dummy,ptsfile<>".temp","StartingPrimeNo"->pid,"MaxPrimes"->1];
	ord=FirstPosition[blReconstructParameter,#]&/@BLSearchParameter//Flatten;
	pts=uint64load[ptsfile<>".temp"];
	ptsp=Partition[pts[[3;;]],pts[[1]]+1];
	ptsp[[All,1;;Length@blReconstructParameter]]=ptsp[[All,ord]];
	uint64dump[Flatten@{pts[[1]],Length@ptsp,ptsp},ptsfile];
	DeleteFile[ptsfile<>".temp"];
]*) (*<-- used to transform ordering in points.fflow to follow BLSearchParameter, abandom*)
];


BLLoadSamplePoints[infile_]:=Module[{pts,ptsp},
pts=uint64load[infile];
ptsp=Partition[pts[[3;;]],pts[[1]]+1];
ptsp];


(* ::Section:: *)
(*PrepareSparseSolve*)


(*infile:  #parameters + #flags, #samples, samples....*)
(*outfile : absolute path or specified file-type*)
gatherSamplePoints[infile_,nparsin_, nana_,primeid_,outfile_]:=Module[{pts,len,ptsp,ptspsort,ncons},
If[!FileExistsQ@infile,ErrorPrint["file not exists: ",infile];Abort[]];
pts=uint64load[infile];
ptsp=Partition[pts[[3;;]],pts[[1]]+1];
ptsp=Select[ptsp,#[[nparsin+1]]==FFPrimeNo[primeid]&];
ncons=nparsin-nana;
ptspsort=GatherBy[ptsp,#[[1;;ncons]]&];
uint64dump[Flatten@{pts[[1]],Length@ptsp,ptspsort},outfile];
ptspsort
];


mergeSearchID[job_]:=Module[{n},
If[FileExistsQ[#],Return[{}]]&[FileNameJoin[{$SearchDirectory,job,"0/multi_sch_intid"}]];
n=0;
While[DirectoryQ@FileNameJoin[{$SearchDirectory,job,ToString[n]}],
CopyFile[FileNameJoin[{$SearchDirectory,job,ToString[n],"sch_intid"}],FileNameJoin[{$SearchDirectory,job,ToString[n],"multi_sch_intid"}]];
n++]]


(*both input should be absolute path*)
relativePath[file_,dir_]:=Module[{f,d,pos},
f=StringSplit[file,"/"];
d=StringSplit[dir,"/"];
pos=SequenceAlignment[d,f][[1]]//Length;
StringRiffle[Join[Replace[d[[pos+1;;]],_:>"..",{1}],f[[pos+1;;]]],"/"]
];


collectSystemInfo[currentdir_,outputfilename_,dirlist_, databasename_]:=Module[{info,fp},
info=Import[FileNameJoin[{$SearchDirectory,"database",databasename,"red_common"}],"Table"][[1]];
CreateDir[DirectoryName@FileNameJoin[{currentdir,outputfilename}]];
writeObject[FileNameJoin[{currentdir,outputfilename}],Join[info[[1;;4]],{Length@$InternalTarget,IntegralID/@$InternalTarget-1, 1}],OpenWrite];
fp = OpenAppend[FileNameJoin[{currentdir,outputfilename}]];
WriteLine[fp,StringRiffle[Join[dirlist,{databasename}],","]];
Close[fp];
]


(* ::Section:: *)
(*blSolveLearning*)


(*solve block-triangular relations at three random numerical points and return average sample time.
This function is called during BLAdaptiveSearch to determine whether we should continue searching block-triangualr form*)
(*job: job name*)
(*name: database name*)
(*primeid: which prime field database is generated*)
(*'nana' is unnecessary because Kin_Table always match BLSearchParameter*)
ClearAll[blSolveLearning];
blSolveLearning[job_,name_,primeid_]:=Module[{parentdir,systemfile,prop},
parentdir=FileNameJoin[{$SearchDirectory,job}];
mergeSearchID[job];
systemfile="system_"<>name<>".txt";
Block[{$InternalTarget=NonMIs},collectSystemInfo[parentdir,systemfile,{"../../search/"~~job},name]];
BLRunCommand[{$BLSSolvePath, Length@BLSearchParameter, primeid,0,0,BLNthreads,systemfile,FromDigits[Range[Length@BLSearchParameter]],"points.fflow","eval.fflow"},ProcessDirectory->parentdir];
];


(* ::Section:: *)
(*blEvaluateAndDump*)


(*after BLNthreadsLearning[]*)
(*evalname: evalname_i => database name*)
(*create directory \"eval\", output evalname_*.fflow, evalname.txt*)
(*evalname.txt:  filenames of  evalname_*.fflow*)
(*paras: only Length[paras] is used*)
blEvaluateAndDump[job_,paras_, nana_,pointsfflow_,primeid_, evalname_, nthreads_]:=Module[{ncons, fitnumber,ptsord, ptsfile, ptspsort,startid,lengthid,name, systemfile},

ncons=Length[BLSearchParameter]-nana;

(*ptsord for BLSSolve*)
ptsord=FromDigits[FirstPosition[BLSearchParameter,#]&/@blReconstructParameter//Flatten];

(* #database,  if not exists, use plain IBP*)
fitnumber=If[FileExistsQ[#],Get[#],Infinity]&[FileNameJoin[{$SearchDirectory,job,"fitnumber"}]];

(*gather sample points*)
ptsfile=FileNameJoin[{StringReplace[pointsfflow,".fflow"->".bt"]}];
ptspsort=gatherSamplePoints[pointsfflow,Length[paras],nana,primeid,ptsfile];

startid=Prepend[Accumulate[Length/@ptspsort],0]//Most;
lengthid=Length/@ptspsort;

CreateDir@"eval";

(*if #database always larger than #sample, FFGraphEvaluate*)
If[AllTrue[lengthid,#<=fitnumber&],
ffEvaluateAndDump[Join@@ptspsort, Length@paras, primeid, FileNameJoin[{"eval",StringRiffle[{evalname,1},"_"]<>".fflow"}], BLDatabaseNthreads],

(*elseif #sample < #database, FFGraphEvaluate, else, BLSSolve*)
Do[
name=StringRiffle[{evalname,i},"_"];
(*break point*)
If[FileExistsQ[FileNameJoin[{"eval",name<>".fflow"}]],Continue[]];
If[Length[ptspsort[[i]]]<=fitnumber, ffEvaluateAndDump[ptspsort[[i]], Length@paras, primeid, FileNameJoin[{"eval",name<>".fflow"}], BLDatabaseNthreads], 

WriteMessage["sampling -> ", i,"/",Length[ptspsort]];
If[! jobFitQ[name,job],
BLClearDatabase[name];
$Conservative=BLSearchParameter[[1;;ncons]];
BLDatabase[name,fitnumber,primeid,ptspsort[[i,1,1;;ncons]]];
BLFitRelations[name,job]];
mergeSearchID[job];
systemfile=FileNameJoin[{"system",name<>".txt"}];
collectSystemInfo[Directory[],systemfile,{relativePath[FileNameJoin[{$SearchDirectory,job}],Directory[]]},name];

BLSSolve[Length[paras],primeid,startid[[i]],lengthid[[i]],nthreads,systemfile, ptsord, ptsfile,FileNameJoin[{"eval",name<>".fflow"}]];
BLClearDatabase[name]
],{i,Length@ptspsort}]];

Export[FileNameJoin[{"eval",evalname<>".txt"}],StringRiffle[FileNames@FileNameJoin[{"eval",evalname<>"_*"}],","]]
];


(*block-triangular form has ordering kin_table , e.g. {z1,z2,z3}, points.fflow has ordering, e.g. {z3,z1,z2}, 
then ptsord = 312; ordlist={3,1,2}; xin[[ordlist[[i]]]]=points[[i]]*)
BLSSolve[nparsin_,primeid_,start_,size_,nthreads_,systemfile_,ptsord_,ptsfile_,outfile_]:=Block[{file,prop,fp},
file=StringReplace[outfile,{"eval/"->"log/",".fflow"->""}];
CreateDir@DirectoryName[file];
BLRunCommand[{$BLSSolvePath, nparsin, primeid,start,size,nthreads,systemfile,ptsord,ptsfile,outfile},"log"->file];
];


(* ::Section:: *)
(*ffEvaluateAndDump*)


(*TODO:  use DataBase instead of FF*,  decouple *)
(*points: {{x,x,...prime,flag},...}*)
ffEvaluateAndDump[points_, nparsin_, primeid_,outfile_, nthreads_]:=Module[{pslist,posi,res},
(*evaluate*)
(*blReconstructParameter\[Rule]BLIBPParameter*)
pslist=points[[All,1;;nparsin]];
posi=Flatten[FirstPosition[blReconstructParameter,#]&/@BLIBPParameter];
pslist = pslist[[All,posi]];
(*20230429: output of original IBP may run out of memory. FFDeleteGraph after BLSolveSemiBL is done*)
If[!FiniteFlow`Private`FFGraphQ[$SelectedGraph],selectedGraph[$Elements];];
res= FFGraphEvaluateMany[$SelectedGraph, pslist, "PrimeNo" -> primeid, "NThreads"->nthreads];
(*select needed elements*)
(*dump*)
dumpevaluations[res, points,nparsin,Length[$Elements],outfile];
];


(*elems: {1,2,3,..}, analogous to X[[elems]]*)
selectedGraph[elems_]:=Module[{in,ibps,out},
FFNewGraph[$SelectedGraph,in,BLIBPParameter];
FFAlgSimpleSubgraph[$SelectedGraph,ibps,{in},$InternalGraph];
FFAlgTake[$SelectedGraph,out,{ibps},{Range[FFNParsOut[$InternalGraph]]}->elems];
FFGraphOutput[$SelectedGraph,out];
];


(* ::Section:: *)
(*BLReconstructMod*)


(*dir: eval/evalname.txt && degfile  exists*)
(*name.txt:  file names of evaluations*)
(*dir: recmod/* created*)
BLReconstructMod[nparsin_, nparsout_, evalname_, degfile_, nthreads_, primeid_]:=Module[{evalist,outprefix},
evalist=FileNameJoin[{"eval",evalname<>".txt"}];
outprefix=FileNameJoin[{"recmod",evalname}];
CreateDir[DirectoryName[outprefix]];
BLRecMod[nparsin,nparsout,primeid,nthreads,degfile,evalist,outprefix];
];


BLRecMod[nparsin_,nparsout_,primeid_,nthreads_,degfile_,evalist_,outprefix_]:=Block[{file,prop,fp},
file=StringReplace[outprefix,"recmod/"->"log/recmod_"];
CreateDir@DirectoryName[file];
BLRunCommand[{$BLRecModPath, nparsin, nparsout,primeid ,nthreads,degfile, evalist, outprefix},"log"->file];
];


(* ::Section:: *)
(*blSolveSemiBLMod*)


(*after BLRegisterNeeded[target,key]*)
(*solve BL in job*)
(*paras: only Length[paras] is used*)
(*nana=1\[Rule] one variable analytic BL, nana=2\[Rule] two variable analytic BL, ... *)
(*key: $BLWorkingDirectory/key/degrees.fflow(full output of $InternalGraph) should exist*)
(*key: $BLWorkingDirectory/key/points/points*.fflow,  system/system*.txt,  eval/* ,  recmod/*_coeff  created*)
blSolveSemiBLMod[job_, datanameprefix_, paras_, nana_, pid_, nthreads0_, key_]:=Module[{depvars,indepvars,nzlearn,degcache,nparsin,nparsout,dummy,ptsfile,evalname},

SetDirectory[FileNameJoin[{$BLWorkingDirectory,key}]];

(*generate sample points*)
ptsfile=FileNameJoin[{"points","points_"<>ToString[pid]<>".fflow"}];
If[FileExistsQ[ptsfile],WriteMessage[ptsfile<>" already exists, nothing to do"],BLDumpSamplePoints[pid,ptsfile]];

(*evaluate sample points*)
evalname=StringRiffle[{datanameprefix,pid},"_"];
MyTiming[blEvaluateAndDump[job,paras,nana,ptsfile,pid, evalname, nthreads0],"Sampling"];

(*reconstruction mod*)
degcache="selected_degrees.fflow";
{nparsin,nparsout}={"NParsIn","NParsOut"}/.FFNParsFromDegreesFile[degcache];
MyTiming[BLReconstructMod[nparsin, nparsout, evalname, degcache, nthreads0, pid],"FunctionalReconstruction"];

(*delete temporary file*)
DeleteFile[FileNames[FileNameJoin[{"eval",evalname<>"_*.fflow"}]]];

ResetDirectory[];
];


(* ::Section:: *)
(*BLSolveSemiBL*)


(*target0 could invlove zero integrals*)
BLSolveSemiBL[nana_Integer,key_String:"temporary"]:=BLSolveSemiBL[SearchOptions[nana]["Jobname"],SearchOptions[nana]["DatanamePrefix"],nana,key]
BLSolveSemiBL[job_String,datanameprefix_String,nana_Integer,key_String:"temporary"]:=BLSolveSemiBL[job,datanameprefix,blReconstructParameter,nana,BLNthreads,key];


BLSolveSemiBL[job_String,datanameprefix_String,paras_List,nana_Integer,nthreads_Integer,key_String]:=Module[
{cachefolder, pid,datamono,datacoeff,cachefile,benchmark,process,state,prime,finished,
nonzeroessol, coeff, mono, part, indepvars, depvars, nzlearn, list},

cachefolder = FileNameJoin[{$BLWorkingDirectory,key}];
CreateDir[cachefolder];

pid=0;
datamono[n_]:=FileNameJoin[{cachefolder,"recmod",datanameprefix<>"_"<>ToString[n]<>"_mono"}];
datacoeff[n_]:=FileNameJoin[{cachefolder,"recmod",datanameprefix<>"_"<>ToString[n]<>"_coeff"}];

If[FileExistsQ@datamono[pid],{},
MyTiming[blSolveSemiBLMod[job, datanameprefix, paras, nana, pid, nthreads, key],"SolveSemiBLMod"];
If[!FileExistsQ[datamono[pid]],ErrorPrint["error: blSolveSemiBLMod failed, can not reconstruct function under a prime field"];Abort[];Return[$Failed]]];

benchmark = Import[datamono[pid],"Table"][[1]];

(*start process of dynamicrr*)
SetOptions[StartProcess,ProcessDirectory->FileNameJoin[{cachefolder,"recmod"}]];
process=StartProcess[{$SystemShell,"-c" ,StringRiffle[{$BLDynamicRRPath,0,Length[benchmark]/Length[paras],nthreads,datanameprefix,"rrres"}]}];
state=FileNameJoin[{cachefolder,"recmod","state"}];
While[!FileExistsQ[state],Pause[1]];

(*state\[Equal]0|1|2, dynamicrr is waiting|running|finished*)
(*state\[Equal]0|1|2, MMA should running|waiting|finished*)
prime = 0;
finished=False;
While[prime<=$MaxPrime&&!finished,
(*RR running*)
While[Get[state]===1,Pause[1]];
WriteMessage[ReadString[process,EndOfBuffer]];
(*RR finish*)
If[Get[state]===2,finished=True;Break[];];
(*RR exit abnormaly*)
If[!ProcessStatus[process,"Running"],ErrorPrint["error, something wrong with dynamic RatRec"];KillProcess[process];Abort[]];
(*RR waiting for prime*)
If[FileExistsQ[datacoeff[prime]],{},
MyTiming[blSolveSemiBLMod[job, datanameprefix, paras, nana, prime, nthreads, key],"SolveSemiBLMod"];
If[Import[datamono[prime],"Table"][[1]]=!=benchmark, ErrorPrint["error: unsame pattern."]; KillProcess[process];Abort[]]];
Put[1,state];
Pause[0.1];
prime++;];

KillProcess[process];
If[!finished,ErrorPrint["error: primes are not sufficient to rational reconstruct the coefficients"];Abort[]];

(*avoid launch too many kernels*)
CloseKernels[];

nonzeroessol[sol_,learninfo_]:=Module[{tot,nonzero,ret},
  {tot,nonzero}={"All","NonZero"}/.learninfo;
  ret = SparseArray[{},{tot},0];
  ret[[nonzero]] = sol;
  ret
];

FFDeleteGraph[$SelectedGraph];

(*restore table*)
coeff = ReadList[FileNameJoin[{cachefolder,"recmod","rrres"}],Word]//ToExpression;
mono=Partition[benchmark,Length@paras];
part = Import[FileNameJoin[{cachefolder,"recmod",datanameprefix<>"_"<>ToString[pid]<>"_part"}],"Table"][[1]];
list=toRationalMPI[mono,coeff,part,paras];
nzlearn=GetFile[FileNameJoin[{cachefolder,"nzlearn"}]];
depvars=GetFile[FileNameJoin[{cachefolder,"depvars"}]];
indepvars=GetFile[FileNameJoin[{cachefolder,"indepvars"}]];
list=SparseArray[nzlearn[[2,2]]->list,nzlearn[[1,2]]]//Normal//Partition[#,Length@indepvars]&;
Put[{indepvars,Thread[depvars->list]},FileNameJoin[{$ReductionDirectory,"results/table"}]];
{indepvars,Thread[depvars->list]}
]


toRationalMPI[mono_,coeff_,part_,vars_]:=Module[{terms,partitionwithlength},
partitionwithlength[list_,len_]:=Module[{ap},
ap=Accumulate[len];
Inner[list[[#1;;#2]]&,Join[{0},Most[ap]]+1,ap,List]
];

terms=MapThread[#2*Times@@Power[vars,#1]&,{mono,coeff}];
terms=partitionwithlength[terms,part];
Function[{num,den},Identity[Plus@@num/Plus@@den]]@@@Partition[terms,2]]


(* ::Section:: *)
(*End*)


End[];
