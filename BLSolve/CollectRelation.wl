(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLCollectG1::usage = "BLCollectG1[job_] collect reduced integrals in each block";
BLCollectRelations::usage = "BLCollectRelations[dataname_,job_] collect relations whose coeffcients are rational functions under a finite field";
BLCollectRelationsRational::usage = "BLCollectRelationsRational[job_] collect relations whose coefficents are rational functions";


Begin["`Private`"];
End[];


Begin["`SearchRelation`"]


(* ::Section:: *)
(*Collect*)


(* ::Subsection:: *)
(*Basic*)


(* ::Subsubsection:: *)
(*readKinematics*)


readKinematics[job_]:=Module[{},
KinTable = Import[FileNameJoin[{$ReductionDirectory, "search", "kinematics",ToString[job], "kin_table"}], "Table"];
KinList = Times@@Power[BLSearchParameter, #]&/@KinTable;
]


(* ::Subsubsection:: *)
(*readIntegrals*)


readIntegrals[]:=Module[{},
{NonMIs, MIs} = GetFile[FileNameJoin[{$ReductionDirectory,"results/datainfo"}]][[{1,2}]];
AllIntegrals = Join[NonMIs, MIs];
];


(* ::Subsection:: *)
(*BLCollectG1*)


BLCollectG1[job_]:=Module[{workid, dir,ints},
If[Kernels[] === {}, LaunchKernels[BLNthreads]];
ParallelTable[collectWorkG1[job,i-1], {i, workNumber[job]}, DistributedContexts -> All]];


collectWorkG1[job_,workid_]:=Module[{dir,g1,ints},
dir = FileNameJoin[{$SearchDirectory, job, ToString[workid]}];
g1 =  Get[FileNameJoin[{dir, "sch_g1"}]];
ints = Import[FileNameJoin[{dir, "sch_intid"}], "Table"]//Flatten;
AllIntegrals[[ints[[1;;g1]]+1]]
];


(* ::Subsection:: *)
(*BLCollectRelations*)


BLCollectRelations[dataname_, job_]:=Module[{workid, dir},
readKinematics[job];
readIntegrals[];
If[Kernels[] === {}, LaunchKernels[BLNthreads]];
ParallelTable[collectWorkRelations[dataname, job, i-1], {i, workNumber[job]}, DistributedContexts -> All]
];


collectWorkRelations[dataname_, job_, workid_]:=Module[{dir, ints, transvar, relations, null, varlist},
dir = FileNameJoin[{$SearchDirectory, job, ToString[workid]}];
ints = Import[FileNameJoin[{dir, "sch_intid"}], "Table"]//Flatten;
transvar[{localid_, kinid_}]:=AllIntegrals[[1+ints[[localid+1]]]]*KinList[[kinid+1]];

relations = {};
Table[
null = Import[FileNameJoin[{dir, "fit", dataname, ToString[n]}], "Table"];
varlist = transvar/@Import[FileNameJoin[{dir, "config", ToString[n], "tmp_var"}], "Table"];
relations = {relations, Thread[null . varlist == 0]}, {n, 0, templateNumber[job, workid, dataname]-1}];

relations = Select[Flatten[relations], #=!=True&];
relations
];


(* ::Subsection:: *)
(*BLCollectRelationsRational*)


BLCollectRelationsRational[job_]:=Module[{workid, dir},
readKinematics[job];
readIntegrals[];
If[Kernels[] === {}, LaunchKernels[BLNthreads]];
ParallelTable[collectWorkRelationsRational[job, i-1], {i, workNumber[job]}, DistributedContexts -> All]
];


collectWorkRelationsRational[job_, workid_]:=Module[{dir, ints, transvar, relations, null, varlist,fitnumber},
dir = FileNameJoin[{$SearchDirectory, job, ToString[workid]}];
ints = Import[FileNameJoin[{dir, "sch_intid"}], "Table"]//Flatten;
transvar[{localid_, kinid_}]:=AllIntegrals[[1+ints[[localid+1]]]]*KinList[[kinid+1]];
fitnumber = workNumberGeneral[FileNameJoin[{dir, "fitM"}], FileExistsQ];
relations = {};
Table[
(*fix a bug, otherwise 3/2 would read as string*)
null = Import[FileNameJoin[{dir, "fitM", ToString[n]}], "Table"]//ToExpression;
varlist = transvar/@Import[FileNameJoin[{dir, "config", ToString[n], "tmp_var"}], "Table"];
relations = {relations, Thread[null . varlist == 0]}, {n, 0, fitnumber-1}];

relations = Select[Flatten[relations], #=!=True&];
relations
];


(* ::Section:: *)
(*End*)


End[];
