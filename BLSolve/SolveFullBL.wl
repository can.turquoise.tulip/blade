(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLReductionGraph::usage = "BLReductionGraph[ relations_, paras_, all0_, need_, mis_, pid_, nthreads0_, cachefolder_] calls FiniteFlow to solve 'relations' under the prime field FFPrimeNo['pid'] and reduces the target integrals 'need' to master integrals 'mis'. 'para' represents the parameters in the 'relations'. 'all0' represents the integrals in the 'relations'. 'nthreads0' is the number of threads used for parallel computation and 'cachefolder' is the location where results are stored";
BLSolveFullBL::usage= "BLSolveFullBL[job_,datanameprefix_, target_,key_:\"reduce\"] solves block-triangular relations of 'job'+'datanameprefix_*' and reduce 'target'.
\"SortParameters\": if set to False, the function will use 'blReconstructParameter' for reconstruction, otherwise, it will sort 'blReconstructParameter' and 
use the optimum parameter ordering for reconstruction. False by default";


Begin["`Private`"];
End[];


Begin["`SolveFullBL`"];


(* ::Section:: *)
(*toRational*)


dynamicPartition[l_,p_]:=MapThread[l[[#;;#2]]&,{{0}~Join~Most@#+1,#}&@Accumulate@p];


toPoly[poly_,paras_]:=Total[#[[2]] Times@@Power[paras, #[[1]]]&/@poly];
toRational[rat_,paras_]:=Identity[toPoly[rat[[1]],paras]/toPoly[rat[[2]],paras]];


(* ::Section:: *)
(*BLReductionGraph*)


(*FFSample would gen_sample_points internal*)
(*univariate-reconstruction is controlled by 'MaxDegree'*)


(*all0 should be sorted*)
(*if "LearnParameterOrdering" is set to True, the function will 
1. generate degrees.fflow.original w.r.t input parameter;
2. sort input parameter to reduce the number of sampling;
3. dump degrees.fflow w.r.t the optimum parameter;
4. return the optimum parameter;
Note: for univariate case, nothing to do*)
(*if "LearnUnivariate" is set to True, the function will perform the operations above in univariate case.*)
Options[BLReductionGraph]={"LearnParameterOrdering"->False,"LearnUnivariate"->False, "PrintSamplePoints"->False};
BLReductionGraph[ relations_, paras_, all0_, need_, mis_, pid_, nthreads0_, cachefolder_,OptionsPattern[]]:=Block[
{exints,exrels,ex,all, time1, graph, in ,sys, learn, sysnz, nzlearn, cachefile, time2, opt ,BLtoFFInternalRatFun, rec, nthreads, mem1, mem2, aval},

If[OptionValue["LearnParameterOrdering"]&&Length[paras]===1&&(!TrueQ@OptionValue["LearnUnivariate"]),Put[paras,FileNameJoin[{cachefolder,"parameters"}]];Return[paras]];

exints=Thread[(ex/@Range[Length@mis])->mis];
exrels=(#[[1]]-#[[2]]==0)&/@exints;
all = Join[all0,First/@exints];

WriteMessage@StringTemplate["BL system: relations: `1`, variables: `2`, needed: `3`, masters: `4`"][
ToString[Length[relations]+Length[exrels]], ToString[Length[all]], ToString[Length[need]], ToString[Length[mis]]];

time1 = AbsoluteTime[];
FFNewGraph[graph];
FFGraphInputVars[graph, in, paras];
FFAlgSparseSolver[graph, sys, {in}, paras, Join[relations,exrels], all, "NeededVars" -> need];
FFSolverOnlyHomogeneous[graph, sys];
FFGraphOutput[graph, sys];
FFSetLearningOptions[graph, sys, "PrimeNo" -> pid];
learn = FFSparseSolverLearn[graph, all];
If[!ContainsExactly[learn[[1,2]], need], ErrorPrint["error: wrong relations"];ErrorPrint[learn[[1,2]],need]; Abort[]];
(*fix bug: if diffeq ===0, then learn[[2,2]]==={}*)
If[learn[[2,2]]==={},ErrorPrint["masters do not depend on invs with which compute derivatives!"];Return["ZeroDiffeq"]];
If[(learn[[2,2]]/.exints)=!=mis, ErrorPrint["error: cannot reduce needed to masters."];ErrorPrint[learn[[2,2]],mis]; Abort[]];

FFSparseSolverMarkAndSweepEqs[graph,sys];
FFSparseSolverDeleteUnneededEqs[graph,sys]; 
WriteMessage[{"number of independent eqs. ->",FFSolverNIndepEqs[graph,sys]}];

FFAlgNonZeroes[graph,sysnz,{sys}];
FFGraphOutput[graph,sysnz];
FFSetLearningOptions[graph,sysnz,"PrimeNo"->pid];
nzlearn = FFNonZeroesLearn[graph];
CreateDir[cachefolder];
cachefile=FileNameJoin[{cachefolder,"nzlearn"}];
Put[nzlearn,cachefile];
cachefile=FileNameJoin[{cachefolder,"depvars"}];
Put[learn[[1,2]],cachefile];
cachefile=FileNameJoin[{cachefolder,"indepvars"}];
Put[learn[[2,2]]/.exints,cachefile];

time2 = AbsoluteTime[];
WriteMessage["graph defined in "<>ToString[Round[time2-time1,0.001]]<>"s."];

opt = Sequence["MaxDegree" -> $MaxDegree, "StartingPrimeNo" -> pid];
nthreads = nthreads0;

If[OptionValue["LearnParameterOrdering"],
cachefile=FileNameJoin[{cachefolder,"degrees.fflow.original"}];
If[FileExistsQ[cachefile]&&Get[FileNameJoin[{cachefolder,"parameters.original"}]]===paras, {},FFAllDegrees[graph, opt]; FFDumpDegrees[graph, cachefile];Put[paras,FileNameJoin[{cachefolder,"parameters.original"}]]];
blSortDegrees[paras,cachefile,FileNameJoin[{cachefolder,"degrees.fflow"}],FileNameJoin[{cachefolder,"parameters"}]];
FFDeleteGraph[graph];
Return[Get[FileNameJoin[{cachefolder,"parameters"}]]]];

BLtoFFInternalRatFun[ratfun_,vars_]:={FiniteFlow`Private`toFFInternalPoly[Numerator[ratfun],vars],FiniteFlow`Private`toFFInternalPoly[Denominator[ratfun],vars]};
(*treat univariate and multivariate cases differently*)
If[ Length@paras ==1,
MyTiming[
rec = FFParallelReconstructUnivariateMod[graph,paras,"NThreads"->nthreads,opt];,"Univariate Reconstruction"];
If[!TrueQ[rec[[0]]===List],ErrorPrint["error in FFParallelReconstructUnivariateMod ->",rec];Abort[]];
rec = BLtoFFInternalRatFun[#,paras]&/@rec;
cachefile = FileNameJoin[{cachefolder,"points.fflow"}];
If[!FileExistsQ[cachefile], FFDumpEvaluations[graph,cachefile];WriteMessage[{"number of sample points->" , BinaryReadList[cachefile,"UnsignedInteger64",3][[3]]}]]
,

cachefile=FileNameJoin[{cachefolder,"degrees.fflow"}];
If[!FileExistsQ[cachefile], FFAllDegrees[graph, opt]; FFDumpDegrees[graph, cachefile], FFLoadDegrees[graph, cachefile]];
If[OptionValue["PrintSamplePoints"],WriteMessage[{"number of sample points ->" , FFNSamplePoints[graph, opt][[1]]}]];

time1 = AbsoluteTime[];
FFGraphEvaluateMany[graph,RandomInteger[{1,FFPrimeNo[pid]-1},{3,Length[paras]}],"PrimeNo"->pid,"NThreads"->1];
time2 = AbsoluteTime[];
WriteMessage["average sample time for single core-> "<>ToString[Round[(time2-time1)/3,0.001]]<>"s."];

(*20220716: nthreads learning, to make the best use of CPUs, no change to univariate cases(assume enough memory)*)
mem1=MemoryAvailable[];
FFGraphEvaluateMany[graph,RandomInteger[{1,FFPrimeNo[pid]-1},{2,Length[paras]}],"NThreads"->2];
mem2=MemoryAvailable[];
aval=If[mem1<=mem2+1,Infinity,2+Quotient[0.8*mem2,(mem1-mem2)]];
If[nthreads>aval,nthreads=aval;WriteMessage["sampling Nthreads-> ",aval,", constraint by memory."],
WriteMessage["sampling Nthreads-> ",nthreads,", there is enough memory"]];

MyTiming[FFSample[graph, nthreads, opt],"sampling"];

MyTiming[
rec = FiniteFlow`Private`FFReconstructFromCurrentEvaluationsModImplem[
FiniteFlow`Private`GetGraphId[graph], 
FiniteFlow`Private`toFFInternalUnsignedFlag["nthreads", nthreads], 
FiniteFlow`Private`FFAlgorithmSetReconstructionOptions[opt]];,"reconstruction"];
];

FFDeleteGraph[graph];

rec
];


(* ::Section:: *)
(*BLSolveFullBL*)


(*target0 could invlove zero integrals*)
Options[BLSolveFullBL]={"SortParameters"->False};
BLSolveFullBL[job_,datanameprefix_,target_,key_String:"reduce",OptionsPattern[]]:=Module[
{cachefolder, relations,allvariables,pid,prime,data,getprime,cachefile,benchmark,patt,part,entry,fitnumber,datacoeff,
process,state,finished,final,rec,res,time1,time2,nonzeroessol,coe,nzlearn,depvars,table,rule,paras},

cachefolder = FileNameJoin[{$ReductionDirectory,"temporary",ToString[BLFamily]<>"_"<>key}];
CreateDir[cachefolder];

pid=0;
data[n_] := datanameprefix<>"_"<>ToString[n];
datacoeff[n_]:=FileNameJoin[{cachefolder,datanameprefix<>"_"<>ToString[n]<>"_coeff"}];
getprime[id_]:=Import[FileNameJoin[{$SearchDirectory, "database", data[id], "red_common"}],"Table"][[1,1]];

cachefile=FileNameJoin[{cachefolder,data[pid]}];
If[FileExistsQ@cachefile,
	benchmark=Get[cachefile];
	paras=Get[FileNameJoin[{cachefolder,"parameters"}]]
	,
	fitnumber = Get@FileNameJoin[{$SearchDirectory,job,"fitnumber"}];
	If[! jobFitQ[data[pid], job], BLClearDatabase[data[pid]]; $Conservative={}; BLDatabase[data[pid], fitnumber,pid]//MyTiming; BLFitRelations[data[pid], job]//MyTiming;];
	relations = MyTiming[Join@@BLCollectRelations[data[pid], job],"BLCollectRelations"];
	allvariables=sortIntegrals[AllIntegrals,Join[USInts,EXInts]];
	
	If[OptionValue["SortParameters"],
		paras = MyTiming[BLReductionGraph[relations, blReconstructParameter,allvariables, target ,MIs,pid,BLNthreads,cachefolder,"LearnParameterOrdering"->True],"LearnParameterOrdering"];
		WriteMessage["Sorted parameters: ",ToString@paras]
		,
		paras = blReconstructParameter;
		Put[paras,FileNameJoin[{cachefolder,"parameters"}]];
	];
	
	benchmark = BLReductionGraph[relations, paras,allvariables, target ,MIs,pid,BLNthreads,cachefolder,"PrintSamplePoints"->True]//MyTiming;
	Put[benchmark, cachefile]; 
	writeObject[datacoeff[pid],{ToExpression/@Flatten[benchmark[[All,All,All,2]]]},OpenWrite];
];

patt = benchmark[[All,All,All,1]];
part = Flatten[Map[Length,patt,{2}],1];
entry = ToExpression/@Flatten[benchmark[[All,All,All,2]]];

(*start process of dynamicrr*)
SetOptions[StartProcess,ProcessDirectory->cachefolder];
process=StartProcess[{$SystemShell,"-c" ,StringRiffle[{$BLDynamicRRPath,0,Length@entry,BLNthreads,datanameprefix,"rrres"}]}];
state=FileNameJoin[{cachefolder,"state"}];
Pause[0.1];

(*state\[Equal]0|1|2, dynamicrr is waiting|running|finished*)
(*state\[Equal]0|1|2, MMA should running|waiting|finished*)
prime = 0;
finished=False;
While[prime<=$MaxPrime&&!finished,
(*RR running*)
While[Get[state]===1,Pause[1]];
WriteMessage[ReadString[process,EndOfBuffer]];
(*RR finish*)
If[Get[state]===2,finished=True;Break[];];
(*RR exit abnormaly*)
If[!ProcessStatus[process,"Running"],ErrorPrint["error, something wrong with dynamic RatRec"];KillProcess[process];Abort[]];
(*RR waiting for prime*)
If[FileExistsQ[datacoeff[prime]],{},
fitnumber = Get@FileNameJoin[{$SearchDirectory,job,"fitnumber"}];
If[! jobFitQ[data[prime], job], BLClearDatabase[data[prime]]; $Conservative={}; BLDatabase[data[prime], fitnumber,prime]//MyTiming; BLFitRelations[data[prime], job]//MyTiming;];
relations = MyTiming[Join@@BLCollectRelations[data[prime], job],"BLCollectRelations"];
allvariables=sortIntegrals[AllIntegrals,Join[USInts,EXInts]];
res = BLReductionGraph[relations, paras,allvariables, target ,MIs,prime,BLNthreads,cachefolder]//MyTiming;
If[res[[All,All,All,1]]=!=patt, ErrorPrint["error: unsame pattern."]; KillProcess[process];Abort[]];
writeObject[datacoeff[prime],{ToExpression/@Flatten[res[[All,All,All,2]]]},OpenWrite];];
Put[1,state];
Pause[0.1];
prime++;];

KillProcess[process];
If[!finished,ErrorPrint["error: primes are not sufficient to rational reconstruct the coefficients"];Abort[]];

(*avoid launch too many kernels*)
CloseKernels[];

nonzeroessol[sol_,learninfo_]:=Module[{tot,nonzero,ret},
  {tot,nonzero}={"All","NonZero"}/.learninfo;
  ret = SparseArray[{},{tot},0];
  ret[[nonzero]] = sol;
  ret
];

(*final = Import[FileNameJoin[{cachefolder,"rrres"}],"Table"][[1]]//ToExpression;*)
final = ReadList[FileNameJoin[{cachefolder,"rrres"}],Word]//ToExpression;
coe = Partition[dynamicPartition[final, part],2];
benchmark[[All,All,All,2]] = coe;
cachefile=FileNameJoin[{cachefolder,"nzlearn"}];
nzlearn=Get[cachefile];
cachefile=FileNameJoin[{cachefolder,"depvars"}];
depvars=Get[cachefile];
rec=nonzeroessol[Map[toRational[#,paras]&, benchmark, {1}],nzlearn]//Normal;
table = ArrayReshape[rec,{Length@depvars,Length@MIs}];
rule = Thread[depvars->table];
Put[{MIs,rule},FileNameJoin@{$ReductionDirectory,"results/table"}];
{MIs,rule}
]


(* ::Section:: *)
(*End*)


End[];
