# Sample Installation
This is a sample installation for Blade and its dependencies.

You need to have a C/C++ compiler and a ``make`` installed.

All packages are installed in a custom path so that no root privilege is required. The installation can be simplified by using package managers for most Linux distributions(e.g. `sudo apt install` ) and using Homebrew on MacOS if you do have root privilege.

### CMake
CMake is preinstalled on many systems, or can be installed with package managers. 

Anyway, Binary distributions can be obtained by:
```
mkdir -p /home/guanxin/mylocal/src/cmake
cd /home/guanxin/mylocal/src/cmake
wget https://cmake.org/files/v3.26/cmake-3.26.0-linux-x86_64.tar.gz
tar -xvf ./cmake-3.26.0-linux-x86_64.tar.gz
```
You should download the correct version for your operation system.
Then, `cmake` can be called by `/home/guanxin/mylocal/src/cmake/cmake-3.26.0-linux-x86_64/bin/cmake`


### GMP
To download and install GMP, use:
```
mkdir -p /home/guanxin/mylocal/src/gmp
cd /home/guanxin/mylocal/src/gmp
wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.bz2
tar -jxvf gmp-6.2.1.tar.bz2
cd gmp-6.2.1
./configure --prefix=/home/guanxin/mylocal
make
make check
make install
```

The installation may fail with the error "cannot find m4". It can be solved by installing the `m4` package.

### MPFR
To download and install MPFR, use:
```
mkdir -p /home/guanxin/mylocal/src/mpfr
cd /home/guanxin/mylocal/src/mpfr
wget https://www.mpfr.org/mpfr-current/mpfr-4.2.0.tar.gz
tar -xvf ./mpfr-4.2.0.tar.gz
cd mpfr-4.2.0
./configure --prefix=/home/guanxin/mylocal --with-gmp=/home/guanxin/mylocal
make
make check
make install
```

### FLINT
To download and install FLINT, use:
```
mkdir -p /home/guanxin/mylocal/src/flint
cd /home/guanxin/mylocal/src/flint
wget https://www.flintlib.org/flint-2.9.0.tar.gz
tar -xvf ./flint-2.9.0.tar.gz
cd flint-2.9.0
./configure --prefix=/home/guanxin/mylocal --with-gmp=/home/guanxin/mylocal --with-mpfr=/home/guanxin/mylocal
make
make check
make install
```

### FFLOW
To download and install FFLOW, use:
```
mkdir -p /home/guanxin/mylocal/src/fflow
cd /home/guanxin/mylocal/src/fflow
wget -cO - https://github.com/peraro/finiteflow/archive/refs/heads/master.zip > finiteflow-master.zip
unzip ./finiteflow-master.zip
cd finiteflow-master
cmake . -DCMAKE_INSTALL_PREFIX=/home/guanxin/mylocal -DCMAKE_PREFIX_PATH=/home/guanxin/mylocal
make install
```
You need to replace `cmake` by `/home/guanxin/mylocal/src/cmake/cmake-3.26.0-linux-x86_64/bin/cmake` if `cmake` is not installed in the default path.

The installation may fails with the error "cannot find -luuid". It can be solved by installing the `uuid-dev` package. More information can be found at https://github.com/peraro/finiteflow/ .

### Blade
To download Blade, use
```
mkdir -p /home/guanxin/mylocal/src/blade
cd /home/guanxin/mylocal/src/blade
wget -cO - https://gitlab.com/multiloop-pku/blade/-/archive/main/blade-main.zip > blade-main.zip
unzip ./blade-main.zip
cd blade-main
```

To install Blade, it is recommended to follow instructions in README.md. Linux users should modify the file `install.in.txt` as
```
DFFLOWMLINK_DIR="/home/guanxin/mylocal/src/fflow/finiteflow-master"
DCMAKE_PREFIX_PATH="/home/guanxin/mylocal"
```
Additional instructions for MacOS users can be found in the `install.in.txt` file.

Then use
```
cmake .
make install
```

