(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


BLRunCommand::usage="BLRunCommand[command : _String | _List] call RunProcess and write log file";


Begin["`Private`"];
GetFile;
DeleteDir;
CreateDir;
writeObject;
fastwriteObject;
toStringInput;
stringToTemplate;
frobeniusSolve2;
End[];


Begin["`AuxiliaryFunctions`"];


(* ::Section:: *)
(*BLRunCommand*)


(*"log" \[Rule] False or "log" \[Rule] "abc"*)
Options[BLRunCommand] = {"log" -> False, Sequence@@Options[RunProcess]};
BLRunCommand[command : _String | _List, OptionsPattern[]] := Module[{opt, time, prop, fout, ferr,log,fp, wasted, cn},
opt = #[[1]]->OptionValue@#[[1]]&/@Options[RunProcess];
log = OptionValue@"log";

wasted=0;
cn=0;
While[wasted<10 || cn <3,
{time, prop} = AbsoluteTiming[RunProcess[command, Sequence@@opt]];
If[prop["ExitCode"] ===0, Break[]];
wasted+=time;
cn++;
WriteMessage["Caution: BLRunCommand failed for "<>ToString@cn<>" time"<>If[cn>1, "s", ""]<>": ->", {command,", ", ProcessDirectory/.opt}];
WriteMessage["Time wasted: "<>ToString@wasted<>" s."];
];

If[log =!= False,
fp = OpenWrite[log <> ".out"];
WriteString[fp, prop["StandardOutput"]];
Close[fp];
fp = OpenWrite[log <> ".err"];
WriteString[fp, prop["StandardError"]];
Close[fp];
];

If[prop["ExitCode"] =!= 0, 
ErrorPrint["error: BLRunCommand failed" -> command];ErrorPrint[prop["StandardError"]];Abort[]]
];


(* ::Section:: *)
(*I/O*)


GetFile[file_]:=If[!FileExistsQ[file],Print["error. file not found: " <> file];Abort[],Get[file]];
DeleteDir[dir_]:=If[DirectoryQ[dir],DeleteDirectory[dir,DeleteContents->True]];
CreateDir[dir_]:=If[!DirectoryQ[dir],CreateDirectory[dir]];


toStringInput[exp_]:=ToString[exp,InputForm];
stringToTemplate[list_]:=StringTemplate[StringJoin[Riffle[list,"\n"]]];


writeObject[file_,obj_,openfunc_]:=Block[{fp}, fp = openfunc[file]; WriteLine[fp,StringReplace[ToString[#,InputForm],{"{"->"", ","->"", "}"->""}]]&/@obj; Close[fp];]


(*only matrix or list*)
(*reduce the time to write many raws. suitable for PolynomialAnsatz*)
(*Database is not appliable, 1. # raw << # collum, 2.total element length > 10^9  will fail, <10^6 is safe.*)
(*WriteString does not put a newline at the end of the output it generates. Export "\n" at the proper position.*)
fastwriteObject[file_,obj_,openfunc_]:=Block[{fp}, 
If[FileExistsQ[file],
fp=OpenRead[file];
If[Read[fp,Word]===EndOfFile,Close[fp],Close[fp];fp=OpenAppend[file];WriteString[fp,"\n"];Close[fp]]];
fp = openfunc[file]; WriteString[fp,StringReplace[StringReplace[toStringInput@If[Head[obj[[1]]]=!=List,Transpose[{obj}],obj],{"{"->"", ","->""}],{"} "->"\n",("}}" ~~ EndOfString) -> "",("}" ~~ EndOfString) -> ""}]]; Close[fp];];


(* ::Section:: *)
(*frobeniusSolve2*)


frobeniusSolve2[{},0]:={{}};
frobeniusSolve2[{},b_]:={};
frobeniusSolve2[{a_},b_]:={{b/a}};
frobeniusSolve2[a_, b_]:=FrobeniusSolve[a,b];


(* ::Section:: *)
(*End*)


End[];
