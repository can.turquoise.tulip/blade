(* ::Package:: *)

(* ::Section:: *)
(*Begin*)


$BLWorkingDirectory::usage="$BLWorkingDirectory, working directory. may be modified during computation";
$BLBladeIBPPath::usage="$BLBladeIBPPath, directory where 'BladeIBP.m' should exist";
$BLFiniteFlowPath::usage = "$BLFiniteFlowPath, directory where 'FiniteFlow.m' should exist";
$BLFiniteFlowLibraryPath::usage ="$BLFiniteFlowLibraryPath, directory where 'fflowmlink.so' should exist";
$BLRedG1Path::usage ="$BLRedG1Path, path to binary file 'redg1'";
$BLFitRelPath::usage= "$BLFitRelPath, path to binary file 'fitrel'";
$BLSSolvePath::usage = "$BLSSolvePath, path to binary 'ssolve'";
$BLRecModPath::usage = "$BLRecModPath, path to binary 'recmod'";
$BLDynamicRRPath::usage = "$BLDynamicRRPath, path to binary 'dynamicrr'";
BLPrintLevel::usage = "BLPrintLevel, The larger the BLPrintLevel, the more verbose output you see";


Begin["`Private`"];
BLInitialize::usage ="BLInitialize[] Print information of Blade.wl and check dependence of the package.";
CacheDirectory::usage="CacheDirectory[key] determine cache directory";
$ReductionDirectory;
$SearchDirectory;
$MaximalCutDirectory;
$WolframPath;
WriteMessage;
MyTiming;
ErrorPrint;
DebugPrint;
End[];


Begin["`Control`"];


(* ::Section:: *)
(*Message*)


(* ::Subsection:: *)
(*$GlobalSpace*)


$GlobalSpace=0;
$GlobalSpaceN=6;


(* ::Subsection:: *)
(*WriteMessage*)


WriteMessage//ClearAll;
WriteMessage::"usage"="WriteMessage[exp__] prints 'exp'.";
Options[WriteMessage]={"Length"->40,"Spacings":>$GlobalSpace};
WriteMessage[exps___,OptionsPattern[]]:=If[BLPrintLevel>0&&BLPrintLevel>=($GlobalSpace/$GlobalSpaceN),WriteString["stdout","\n"<>StringJoin@Table[" ", {OptionValue["Spacings"]}],If[Head@#===String,#,ToString[#,InputForm]]&/@(Flatten@{exps})//StringJoin]
];


(* ::Subsection::Closed:: *)
(*MyTiming*)


MyTiming//Clear;
MyTiming::"usage"="MyTiming[exp] evaluates 'exp' and prints the wall-time used.";
Attributes[MyTiming]={HoldAll};
Options[MyTiming]={"Print"->True};
MyTiming[exp_,opt:OptionsPattern[]]:=Module[
{name},
name=Hold[exp][[1,0]]//ToString;
MyTiming[exp,name//Evaluate,opt]
];
MyTiming[exp_,name_String,OptionsPattern[]]:=Module[
{time,res,temp=$GlobalSpace,DateDifference2},
DateDifference2[x___]:=DateDifference[x][[1]];

If[OptionValue["Print"]===False||BLPrintLevel<=($GlobalSpace/$GlobalSpaceN),
$GlobalSpace+=$GlobalSpaceN;
res=exp;
$GlobalSpace-=$GlobalSpaceN;
Return[res]
];
Block[
{$GlobalSpace=temp},
WriteMessage["Begin "<>name<>"..."];
$GlobalSpace+=$GlobalSpaceN;
time=DateList[];
res=exp;
$GlobalSpace-=$GlobalSpaceN;
WriteMessage["      "<>name<>" use time : ",Round[DateDifference2[time,DateList[]]*24*3600,0.001],"s,"];WriteString["stdout","   LeafCount = ",LeafCount@res];
];
Return[res];
];


(* ::Subsection::Closed:: *)
(*MyMonitor*)


MyMonitor//Clear;
Attributes[MyMonitor]={HoldAll};
MyMonitor::"usage"="MyMonitor[exp,i] is the same as Monitor but it is active only if BLPrintLevel>0&&BLPrintLevel\[GreaterEqual]($GlobalSpace/$GlobalSpaceN) (when MyTiming is active).";
MyMonitor[exp_,i_]:=If[BLPrintLevel>0&&BLPrintLevel>=($GlobalSpace/$GlobalSpaceN),Monitor[exp,i],exp];


(* ::Subsection::Closed:: *)
(*DebugPrint*)


DebugPrint//ClearAll;
DebugPrint::"usage"="DebugPrint[exp] prints 'exp' if OptionValue[\"DebugQ\"] is True. Or else, it does nothing.";
Options[DebugPrint]={"DebugQ"->False};
DebugPrint[x_,OptionsPattern[]]:=If[OptionValue["DebugQ"]&&BLPrintLevel>0&&BLPrintLevel>=($GlobalSpace/$GlobalSpaceN),Print[x];Pause[0.1]];


(* ::Subsection::Closed:: *)
(*ErrorPrint*)


ErrorPrint//ClearAll;
ErrorPrint::"usage"="ErrorPrint[exp] prints 'exp'.";
Options[ErrorPrint]={};
ErrorPrint[exp___,OptionsPattern[]]:=(Print[exp];Pause[0.1]);


(* ::Section:: *)
(*BLInitialize*)


BLInitialize[]:=Module[{},
(*working directory*)
$BLWorkingDirectory = FileNameJoin[{If[$FrontEnd===Null, If[$InputFileName==="",Directory[],$InputFileName//DirectoryName], Off[NotebookDirectory::nosv];If[NotebookDirectory[]=!=$Failed,Off[NotebookFileName::nosv];NotebookDirectory[],Off[NotebookFileName::nosv];Directory[]]], "cache"}];
$ReductionDirectory := $BLWorkingDirectory;
$SearchDirectory := FileNameJoin@{$ReductionDirectory,"search"};
$MaximalCutDirectory := CacheDirectory["sectormappings"];
$WolframPath = First[$CommandLine];
BLNthreads = $ProcessorCount;

WriteMessage["--------------------------------------------------"];
(*options*)
Get@FileNameJoin[{$BLBladePath,"install.m"}];
(*WriteMessage["Dependencies of IBP Generator:"];
WriteMessage[{"BladeIBP.m-> ",FileExistsQ[FileNameJoin[{$BLBladeIBPPath, "BladeIBP.m"}]]}];

WriteMessage["Dependencies of FiniteFlow:"];
WriteMessage[{"FiniteFlow.m -> ", FileExistsQ[FileNameJoin[{$BLFiniteFlowPath, "FiniteFlow.m"}]]}];
WriteMessage[{"fflowmlink.so-> " , !StringContainsQ[RunProcess[{"ldd" ,FileNameJoin[{$BLFiniteFlowLibraryPath, "fflowmlink.so"}]}]["StandardOutput"],"not found"|"No such file"]}];

WriteMessage["Dependencies of Blade executable:"];
WriteMessage["redg1-> ", !StringContainsQ[RunProcess[{"ldd" ,$BLRedG1Path}]["StandardOutput"],"not found"|"No such file"]];
WriteMessage["fitrel-> ", !StringContainsQ[RunProcess[{"ldd" ,$BLFitRelPath}]["StandardOutput"],"not found"|"No such file"]];
WriteMessage["ssolve-> ", !StringContainsQ[RunProcess[{"ldd" ,$BLSSolvePath}]["StandardOutput"],"not found"|"No such file"]];
WriteMessage["recmod-> ", !StringContainsQ[RunProcess[{"ldd" ,$BLRecModPath}]["StandardOutput"],"not found"|"No such file"]];
WriteMessage["dynamicrr-> ", !StringContainsQ[RunProcess[{"ldd" ,$BLDynamicRRPath}]["StandardOutput"],"not found"|"No such file"]];
*)
BLSetReducerOptions@@(*ATMOptions[BLSetReducerOptions]*)Options[BLSetReducerOptions];
BLSetSchemeOptions@@Options[BLSetSchemeOptions];
Print["--------------------------------------------------"];
];


CacheDirectory[key_]:=FileNameJoin[{If[$FrontEnd===Null, If[$InputFileName==="",Directory[],$InputFileName//DirectoryName], Off[NotebookDirectory::nosv];If[NotebookDirectory[]=!=$Failed,Off[NotebookFileName::nosv];NotebookDirectory[],Off[NotebookFileName::nosv];Directory[]]], "cache", toStringInput[BLFamily]<>"_"<>key}];


(* ::Section:: *)
(*End*)


End[];
