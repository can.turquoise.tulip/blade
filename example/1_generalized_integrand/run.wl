(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*configuration of box*)
family = box;
dimension = 4-2eps;
loop = {l1};
leg = {k1,k2,k3};
conservation = {};
replacement = {k1^2->0,k2^2->0,k3^2->0,k1*k2->s/2, k2*k3->t/2, (k1+k2+k3)^2->0};
propagator = {l1^2, (l1+k1)^2, (l1+k1+k2)^2, l1*k3};
topsector = {1,1,1,0};
numeric = {s ->1};


(* ::Subsection:: *)
(*{D_ 4}^x*)


(*General integrand: insert a factor, denoted as F, into the integrand of standard Feynman integrals. 
The IBP reduction method can be equally appied to those generalized integrals.
for syntax, refer to the options 'ExtraIntDerivDen' and 'ExtraIntDerivPara' of BLFamilyDefine*)
?BLFamilyDefine


(*This is an example of F = {D_4}^x *)
BLFamilyDefine[boxGF1,dimension,propagator,loop,leg,conservation,replacement,topsector,numeric,
"ExtraIntDerivDen"->{4->x*BL[boxGF1,{0,0,0,1}]}]


(*Reduce integrals whose rank is larger than that of master integrals by one*)
targetGF1=BLGenerateScheme[BL[boxGF1,{1,1,1,-2}]];
resGF1=BLReduce[targetGF1,"BladeMode"->None,"DivideLevel"->0];


(*Transform the D_4^x to index represention , we obtain integrals in the original family.
Each reduction rule of BL[boxGF1,...] is a recurrence relation of BL[box,...]*)
recur=Thread[targetGF1->Collect[resGF1,_BL,Together]]/.BL[boxGF1,ind_]:>BL[box,ind]*BL[box,{0,0,0,-x}];


(*Sow seeds of recurence relations and solve the linear system, 
we get reduction rules for integrals with arbitary rank.*)
eqs=(Join@@Table[recur/.x->i,{i,-1,5}])/.Rule->Equal//DeleteCases[#,True]&;
sol=FiniteFlow`FFSparseSolve[eqs,(BLSortIntegrals@BLIntegralsIn[eqs])];


(*For example, the reduction rule for BL[box,{1,1,1,-6}] looks like*)
BL[box,{1,1,1,-6}]/.sol


(*As a validation, we perform reduction in the original family.*)
BLFamilyDefine[box,dimension,propagator,loop,leg,conservation,replacement,topsector,numeric]
target={BL[box,{1,1,1,-6}]};
res=BLReduce[target,"BladeMode"->None,"DivideLevel"->0];


(*The results are the same!*)
(BL[box,{1,1,1,-6}]/.sol)-(BL[box,{1,1,1,-6}]/.Thread[target->res])


Put[Thread[target->res],FileNameJoin[{current,"blade_table_box"}]];


(* ::Subsection:: *)
(*Exp[x D_ 4]*)


(*This is an example of F = Exp[x D_4]
For syntax, please see ?BLFamilyDefine *)
BLFamilyDefine[boxGF2,dimension,propagator,loop,leg,conservation,replacement,topsector,numeric,
"ExtraIntDerivDen"->{4->x}, "ExtraIntDerivPara"->{x->BL[boxGF2,{0,0,0,-1}]}]


(*We can construct the differential equation w.r.t 'x'*)
diffeq=BLDifferentialEquation[{x}];


Put[diffeq,FileNameJoin[{current,"blade_diffeq_boxGF2"}]]


(*Computation for later use*)
target={BL[boxGF2,{1,1,1,-3}]};
res=BLReduce[target];
Put[Thread[target->res],FileNameJoin[{current,"blade_table_boxGF2"}]]


(* ::Subsection:: *)
(*F_1=sin(x D_4) & F_2=cos(x D_4)*)


(*This is an example of F_1=sin(x D_4) & F_2=cos(x D_4).
For syntax, please see ?BLFamilyDefine *)
BLFamilyDefine[boxGF3,dimension,propagator,loop,leg,conservation,replacement,topsector,
Join[numeric,{im->I}],
"ExtraIntDerivDen"->{{1,4,2}->x,{2,4,1}->-x}, "ExtraIntDerivPara"->{x->{{1,2}->BL[boxGF3,{0,0,0,-1}],{2,1}->-BL[boxGF3,{0,0,0,-1}]}}]


(*boxGF2: Exp[x D_4] ---x -> i x --> cos(x D_4) + I sin(x D_4)  ---> boxGF3 
Integrals within the boxGF2 family can be transformed into the boxGF3 family through approriate variable changes.*)
(*We introduce a third argument in BL to label the general integrands.
The symbol 'im' is utilized to represent 'I'.*)
exp2trig[exp_]:=exp/.BL[boxGF2,a_]:>BL[boxGF3,a,2]+im* BL[boxGF3,a,1];


(*load the reduction results in the boxGF2 family*)
table=Get[FileNameJoin[{current,"blade_table_boxGF2"}]]


(*convert integrals into boxGF3 family*)
target=Keys[table]//exp2trig
prefer=BLIntegralsIn[Values[table]]//exp2trig


res=BLReduce[target,prefer,"ReadCacheQ"->False]


(*The outcomes of two families are identical *)
(Values[table]/.x->I x//exp2trig)/.{im->I};
res/.BLFamilyInf[boxGF3]["MastersRules"]/.{im->I};
%-%%//Simplify


(*Note that we do not specify dF_\[Rho]/dt in the BLFamilyDefine because it equals 0 in this example.
However, in cases where the derivatives are nonvanishing, it is necessary to input dF_\[Rho]/dt to obtain correct differential equations.*)
res=BLDifferentialEquation[{x,t},"ReadCacheQ"->False];


(*self-consistent check*)
Ax=res[[3,1]];
At=res[[3,2]];
D[At,x]+At . Ax - (D[Ax,t]+Ax . At)//Simplify


Put[res,FileNameJoin[{current,"blade_diffeq_boxGF3"}]];


(* ::Subsection:: *)
(*F_1=sin^2(D_4) & F_2= sin(D_4) cos(D_4) & F_3 = cos^2(D_4)*)


(*This is an example of F_1=sin^2(D_4) & F_2= sin(D_4)*cos(D_4) & F_3 = cos^2(D_4)*)
BLFamilyDefine[boxGF4,dimension,propagator,loop,leg,conservation,replacement,topsector,numeric,
"ExtraIntDerivDen"->{{1,4,2}->2,{2,4,1}->-1,{2,4,3}->1,{3,4,2}->-2}]


(*box: F=1 ---> sin^2(D_4) + cos^2(D_4) ---> boxGF4 
Integrals within the box family can be expressed as linear combinations of integrals in the boxGF4 family.*)
one2trig[exp_]:=exp/.BL[box,a_]:>BL[boxGF4,a,1]+ BL[boxGF4,a,3];


(*load the reduction results in the box family*)
table=Get[FileNameJoin[{current,"blade_table_box"}]];


(*convert integrals into boxGF4 family*)
target=Keys[table]//one2trig;
prefer=BLIntegralsIn[Values[table]]//one2trig;


res=BLReduce[target,prefer,"ReadCacheQ"->False];


(*The outcomes of two families are identical *)
(Values[table]//one2trig);
res/.BLFamilyInf[boxGF4]["MastersRules"];
%-%%//Simplify
