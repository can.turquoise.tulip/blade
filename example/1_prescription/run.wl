(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Topology arise from VV corrections to top-quark pair production total cross section*)
family = LxR;
dimension = 4-2eps;
loop = {l1,l2,k3};
leg = {k1, k2};
conservation = {k2 -> k1 - k3};
replacement = {k1^2 -> s};
propagator = {l1^2,(k3+l1)^2-mt2,(-k1+k3+l1)^2-mt2,l2^2,(k3+l2)^2-mt2,(-k1+k3+l2)^2-mt2,(l1+l2)^2, (k1-k3)^2-mt2, k3^2-mt2};
topSector = {1,1,1,1,1,1,0,1,1};
numeric = {s->1};


(**********Note************)
(*cut: 1 cut-propagators, 0 uncut-propagators.
 Here, {(k1-k3)^2-mt2, k3^2-mt2} arise from reverse-unitarity*)
cut = {0, 0, 0, 0, 0, 0, 0, 1, 1};
(*prescription: 
1 \[Rule] l1 \[Rule] propagators with l1 have +i0 Feynman prescription,
-1 \[Rule] l2 \[Rule] propagators with l2 have -i0 Feynman prescription, 
0 \[Rule] k3 \[Rule] cut momenta to be integrated*)
(*Care should be taken to identify symmetry identities:
loop momenta with prescritions 1 and -1 cannot be interchanged,
cut-propagators and uncut-propagators cannot be mapped with each other*)
prescription = {1, -1, 0};


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topSector,numeric,cut,prescription]


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Target Feynman integrals*)
target={BL[LxR,{1,1,2,1,1,1,-2,1,1}],BL[LxR,{1,2,1,1,1,1,-2,1,1}],
BL[LxR,{1,1,2,1,1,1,-1,1,1}],BL[LxR,{1,2,1,1,1,1,-1,1,1}],
BL[LxR,{1,1,2,1,1,1,0,1,1}],BL[LxR,{1,2,1,1,1,1,0,1,1}],
BL[LxR,{1,1,1,1,1,1,-2,1,1}],BL[LxR,{1,1,1,1,1,1,-1,1,1}],
BL[LxR,{1,1,1,1,1,1,0,1,1}],BL[LxR,{0,1,1,0,1,1,0,1,1}],
BL[LxR,{0,0,1,0,1,1,0,1,1}],BL[LxR,{0,1,0,0,1,1,0,1,1}],
BL[LxR,{0,1,1,0,0,1,0,1,1}],BL[LxR,{0,1,1,0,1,0,0,1,1}],
BL[LxR,{0,0,1,0,0,1,0,1,1}],BL[LxR,{0,0,1,0,1,0,0,1,1}],
BL[LxR,{0,1,0,0,0,1,0,1,1}],BL[LxR,{0,1,0,0,1,0,0,1,1}]};


(*BLReduce return reduction results for the target integrals*)
res=BLReduce[target];


Put[Thread[target->res],FileNameJoin[{current,"blade_table"}]];
