(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of massless double-box*)
family = db;
dimension = 4-2eps;
loop = {l1, l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p1*p2 -> s/2, 
   p1*p3 -> (-s-t)/2, p2*p3 -> t/2};
propagator = {l1^2, (l1-p1)^2, (l1-p1-p2)^2, (l2+p1+p2)^2, (l2-p4)^2, 
(l2)^2, (l1+l2)^2, (l1-p1-p2-p3)^2, (l2+p1)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s ->1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topsector,numeric]


target = {BL[db,{1,1,1,1,1,1,1,0,-3}],BL[db,{1,1,1,1,1,1,1,-1,-2}],
BL[db,{1,1,1,1,1,1,1,-2,-1}],BL[db,{1,1,1,1,1,1,1,-3,0}],
BL[db,{2,1,1,1,1,1,0,0,-2}],BL[db,{2,1,1,1,1,1,0,-2,0}],
BL[db,{0,1,2,2,1,1,0,0,-2}],BL[db,{0,1,2,2,1,1,0,-2,0}]}


(*User can define preferred master integrals *)
exints = {BL[db,{1,1,1,1,1,1,2,0,0}],BL[db,{1,1,1,1,1,1,0,0,0}]+BL[db,{1,1,1,1,1,1,-1,0,0}]};


res=BLReduce[target,exints];


(*The master integrals are stored in BLFamilyInf*)
masters=BLFamilyInf[family]["Masters"]


(*If there are preferred master integrals, the replacement rules are stored in BLFamilyInf*)
mastersRules=BLFamilyInf[family]["MastersRules"]


Put[{masters,mastersRules,Thread[target->res]},FileNameJoin[{current,"blade_table"}]];
