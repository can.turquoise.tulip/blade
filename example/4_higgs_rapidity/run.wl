(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of higgs rapidity distribution*)
family = rrrnp;
dimension = 4-2eps;
loop = {l1, l2, l3};
leg = {k1, k2, k3};
conservation = {k3 -> k1+k2-l1-l2-l3};
replacement = {k1*k2 -> s/2, k1^2 -> 0, k2^2 -> 0};
propagator = {l1^2-msq,l2^2,(l1+l2)^2,(k1+l1+l2)^2,(k1+k2+l1+l2)^2,l3^2,
(k1+k2+l1+l2+l3)^2,(k1+k2+l2+l3)^2,(k1+l2+l3)^2,(l2+l3)^2,l1 (k1-k2 u),(k2+l1)^2};
topsector = {1,1,1,1,1,1,1,1,1,1,1,0};
numeric = {s -> 1};
cut = {1,1,0,0,0,1,1,0,0,0,1,0};
prescription = {0,0,0};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topsector,numeric,cut,prescription]


(*Extend integral set*)
BLSetSchemeOptions["MinimalSchemeRank"->2];


(*Target integrals*)
target=Select[BLGenerateScheme[{BL[rrrnp,{1,1,1,1,1,1,1,1,1,1,1,-6}]}],
(#[[2]])[[{1,2,6,7,11}]]==={1,1,1,1,1}&&BLIntPropagators[#]>=10&&BLIntRank[#]>=5&]//BLSortIntegrals


(*Set search options*)
(*Preliminary studies showed that it is difficult to
obtain the full-analytic block-triangular form.*)
SetOptions[BLAdaptiveSearch,"SearchTimeFactor"->1/10];


(*Preliminary studies showed that the time required
for constructing the block-triangular form is larger
than that for generating input data from numerical
IBP. So there is no need to reduce the number of input data.
Besides, abundant input will accelerate the construction
of the block-triangular form. *)
SetOptions[BLSearchRelations,"FitDataPercent"->1];


res=BLReduce[target];//AbsoluteTiming


Put[Thread[target->res],FileNameJoin[{current,"table"}]];
