(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of massless double-box*)
family = db;
dimension = 4-2eps;
loop = {l1, l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p1*p2 -> s/2, 
   p1*p3 -> (-s-t)/2, p2*p3 -> t/2};
propagator = {l1^2, (l1-p1)^2, (l1-p1-p2)^2, (l2+p1+p2)^2, (l2-p4)^2, 
(l2)^2, (l1+l2)^2, (l1-p1-p2-p3)^2, (l2+p1)^2};
topSector = {1,1,1,1,1,1,1,0,0};
numeric = {s ->1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topSector,numeric]


(*Suppose we want to reduce the amplitude directly, rather than
generating a reduction table for Feynman integrals*)
usints={us[1]-> BL[db,{1,1,1,1,1,1,1,-3,0}]+ t * BL[db,{1,1,1,1,1,1,1,0,-2}]
+ (s^2 +t^2)*BL[db,{1,1,2,1,1,1,0,0,-1}] + (s+t)*BL[db,{2,1,2,1,0,1,0,0,-2}]}/.numeric;


(*(*The IBP system will become too large to generate and solve if we 
create seeds with uniform rank and dots.*)
If "JoinFamilyQ" is set to True, it is possible to determine appropriate IBP systems 
for (sub-)families and then merge them together, so we can generate a refined IBP system.*)
(*sub-families are defined by "DivideLevel", please refer to ?BLReduce*)
res=BLReduce[Values[usints]];


Put[Thread[Values[usints]->res],FileNameJoin[{current,"blade_table"}]]
