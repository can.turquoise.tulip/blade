(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of nonplanar massive double box*)
family = npdbox;
dimension = 4-2*eps;
loop = {k1,k2};
leg = {p1,p2,q1,q2};
conservation = {q1->p1+p2-q2};
replacement = {p1^2 -> 0, p2^2 -> 0, q2^2->0, (p1+p2)^2->s, (q2-p1)^2->t , (q2-p2)^2 -> (-s -t +m12)};
propagator = {k1^2, k2^2, (q2-k1)^2 -m12, (p1-k2)^2, (q1+k1)^2-m12, (q1+k1-k2)^2-m12, (-p2+q1+k1-k2)^2-m12,(k1-p1)^2, (k2-q2-p2)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s -> 1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension, propagator,loop,leg,conservation, replacement,topsector,numeric]


(*It is possible to set "IBPParameter" freely, but it is recommended 
to put the most complex parameter in the first position of 
"IBPParameter". This can have a significant impact on the
multivariate functional reconstruction implemented in FiniteFlow.
If "IBPParameter" is set to Automatic, the program will learn the best 
ordering internally.
"CheckMastersQ" determines whether to verify that the master integrals form
a subset of the maximal-cut masters.*)
(*We have found that "FilterLevel" in combination with 
BLReduce["DivideLevel"\[Rule]Infinity] can effectively reduce both the 
size and the numeric evaluation time of the IBP system. 
Empirically, a value of 3 has been found to be appropriate(default). 
We set FilterLevel to Infinity to compare the difference*)
(*For more information, see ?BLSetReducerOptions.*)
BLSetReducerOptions["IBPParameter"->{t,m12,eps},"IntegralOrdering"->1, 
"FilterLevel"->Infinity];
BLIBPParameter


(*If "IBPParameter" is not equal to Automatic, it will be necessary to modify 
the SearchOptions to run BLReduce[...,"BladeMode"\[Rule]Automatic].
The SearchVariables are read from the last elements of BLIBPParameter to the 
first elements. A convenient way to do this is to run BLSetDefaultSearchOptions[]*)
BLSetDefaultSearchOptions[]
BLSetSearchOptions[]


(*It is advisable to increase "MinimalSchemeRank" to include integrals in 
sub-sectors that have a similar level of complexity compared to the master 
integrals.*)
(*If not specified, "MinimalSchemeRank" is set to 1 by default*)
(*For simple problems, "MinimalSchemeRank" \[Rule] 0(-1) is sufficient.*)
(*For more complex problems, "MinimalSchemeRank"->2 is preferred.*)
(*As "MinimalSchemeRank" increase, it is more easy to search for 
the block-triangular form while the average sample time of 
the block-triangular form will increase as well.*)
BLSetSchemeOptions["MinimalSchemeRank"->0]


(*We choose top-sector integrals with rank 3 as target integrals*)
target={BL[npdbox,{1,1,1,1,1,1,1,0,-3}],BL[npdbox,{1,1,1,1,1,1,1,-1,-2}],
BL[npdbox,{1,1,1,1,1,1,1,-2,-1}],BL[npdbox,{1,1,1,1,1,1,1,-3,0}]
};


(*The following differences can be observed when comparing this 
example to "example/1_blade_vs_plain_ibp, which reduces the same 
set of target integrals:
1. The time required to generate IBP increased by roughly 30% 
due to "FilterLevel"->Infinity.
  'GenerateIBP use time : 40.933'  vs.
  'GenerateIBP use time : 26.48s' 
2. The average sample time of IBP increased by roughly 30% due to 
the larger IBP system.
  '102 data generated in 2.481s. name: 3_1_1' vs.
  '102 data generated in 1.859s. name: 3_1_1' 
3. The number of sample points increased marginally due to the 
ordering of BLIBPParameter.
  'number of sample points -> 69789' vs.
  'number of sample points -> 61371'
4. The actual sample time of the block-triangular form is decreased 
by roughly 30% by reducing the size of the block-triangular form 
through "MinimalSchemeRank".
  'Sampling use time : 33.555s' vs.
  'Sampling use time : 55.289s'
  'block number: 74, all integrals: 292, ...' vs.
  'block number: 81, all integrals: 526, ...'*)
(*Although the runtime should be read with uncertainties of a few percent 
in mind, We want to point out that all these effects will be more 
pronounced in more complex problems, which can make an obvious effect
on the reduction.*)
res=BLReduce[target];


Put[Thread[target->res],FileNameJoin[{current,"blade_table"}]];
