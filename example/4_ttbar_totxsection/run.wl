(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of forward scattering amplitude*)
family = f3;
dimension = 4-2eps;
loop = {l1, l2, l3, l4};
leg = {k1};
conservation = {};
replacement = {k1^2 -> s};
propagator = {l1^2,l2^2,l3^2,l4^2-mt2,(k1+l4)^2-mt2,(l1+l4)^2-mt2,(k1+l2+l4)^2-mt2,(l1+l2+l4)^2-mt2,(k1+l2+l3+l4)^2-mt2,(l1+l2+l3+l4)^2-mt2,(k1+l1+l2+l3+l4)^2-mt2,(k1+l1)^2,(k1+l2)^2,(l2+l3)^2};
topsector = {1,1,1,1,1,1,1,1,1,1,1,0,0,0};
numeric = {s -> 1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topsector,numeric];




(*Target integrals*)
target = {BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-4,0,0}],BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-3,-1,0}],
BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-3,0,-1}],BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-2,-2,0}],
BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-2,-1,-1}],BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-2,0,-2}],
BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-1,-2,-1}],BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-1,-1,-2}],
BL[f3,{1,1,1,1,1,1,1,1,1,1,1,-1,0,-3}],BL[f3,{1,1,1,1,1,1,1,1,1,1,1,0,-2,-2}]};


(*It is very likely that the block-triangular form with 
two analytic variables can be constructed successfully. 
If "BladeMode" is set to Full, BLReduce will try to construct full-analytic 
block-triangular form without using adaptive-search strategy.
In this case, the time consumption for generating the degrees.fflow file 
using numerical IBP sampling is avoided. 
This is helpful when numerical IBP costs the most of time.*)
res=BLReduce[target,"BladeMode"->Full];


Put[Thread[target->res],FileNameJoin[{current,"table"}]];
