(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of massless double-box*)
family = db;
dimension = 4-2eps;
loop = {l1, l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p1*p2 -> s/2, 
   p1*p3 -> (-s-t)/2, p2*p3 -> t/2};
propagator = {l1^2, (l1-p1)^2, (l1-p1-p2)^2, (l2+p1+p2)^2, (l2-p4)^2, (l2)^2, (l1+l2)^2, (l1-p1-p2-p3)^2, (l2+p1)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s ->1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topsector,numeric]


(*Target integrals we want to reduce*)
target = {BL[db,{1,1,1,1,1,1,1,0,-3}]};


(*The key idea of the block-triangular form is to express complex integrals
as linear combinations of simpler integrals. Repeat this step, we can reduce
all integrals to linear combinations of master integrals, which are the 
simplest by definition.*)
(*It is easier to find such linear equations among a delicate integral set.
For syntax, see ?BLGenerateScheme*)
full=BLGenerateScheme[target];


(*Numerical IBP is served as input data for the construction of the block-
triangular form. So we should generate IBP system according to target integrals. 
For syntax, see ?BLIBPSystem*)
BLIBPSystem[topsector,3,1];


(*There are many redudant equations in IBP system. We call BLDatabaseInfo
to trim the IBP system according to target integrals 'full'.*)
BLDatabaseInfo[full];


(*Although user could set BLNthreads to determine the number of threads used 
in the program, the numerical IBP invloves a huge number of equations and 
the memory-consumption may be untolerable. So we should determine the 
available threads for numerical IBP given memory constraints. We can also 
estimate the average time for numerical IBP sampling at this step*)
BLNthreadsLearning[]


(*Load information required for the construction of block-triangular form,
e.g, integrals.*)
BLDataInfoInit[];


(*Using the numerical IBP as input, we can search block-triangular relations 
under a finite field. For syntax, see ?BLSearchRelations*)
BLSearchRelations["job","data_0",{{eps},{t}},{"uniform","uniform"},{"uniform","weight"}];


(*We could call BLCollectRelations to collect block-triangular relations 
under a finite field. For syntax, see ?BLCollectRelations*)
(*The 3rd relation in the 1st block reads: *)
relsmod=BLCollectRelations["data_0","job"];
relsmod[[1,3]]//Short


(*By constructing the block-triangular relations under different prime
fields, we can obtain the relations with rational coefficients.
For syntax, see ?BLReconstructRelations*)
BLReconstructRelations["data","job"]


(*We use BLCollectRelationsRational to collect block-triangular relations 
with rational coefficients*)
(*The 3rd relation in the 1st block reads: *)
rels=BLCollectRelationsRational["job"];
rels[[1,3]]//Short


(*We would like to display the block-triangular relations in a 
vivid way. As mentioned before, the system reduce complex integrals to
linear combinations of simpler integrals. So we sort integrals by 
complexity(sectorwise) and perform MatrixPlot*)
(*Each row is a relation, each collum represents an integral.
The system exhibits block-triangular form.*)
ints=rels//BLIntegralsIn//BLSortIntegrals;
ints=Reverse/@GatherBy[ints,BLSector]//Flatten;
ints=Join[Select[ints,!MemberQ[Blade`Private`MIs,#]&],
	Select[ints,MemberQ[Blade`Private`MIs,#]&]];
mat = CoefficientArrays[Flatten[rels],ints][[2]]//Normal;
MatrixPlot[mat,ColorFunction->"Monochrome"]


(*The block-triangular form has orders of magnitudes of equations 
fewer than traditional IBP system, resulting in faster numerical 
sampling and reduced memory consumption*)
(*Some sub-sectors are not present because "UniqueSubsectorQ" is set to 
True in BLSetSchemeOptions[]. This means that mapped sectors are replaced by 
unique sectors before the search for block-triangular form is performed*)
(*****We can use the system to reconstruct analytic reduction results*******)
(**********We can alse solve the system using floating point numbers**********)
