(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of massless double-box*)
family = db;
dimension = 4-2eps;
loop = {l1, l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p1*p2 -> s/2, 
   p1*p3 -> (-s-t)/2, p2*p3 -> t/2};
propagator = {l1^2, (l1-p1)^2, (l1-p1-p2)^2, (l2+p1+p2)^2, (l2-p4)^2, 
(l2)^2, (l1+l2)^2, (l1-p1-p2-p3)^2, (l2+p1)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s ->1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension,propagator,loop,leg,conservation, replacement,topsector,numeric]


(*Generate differential equations with respect to the kinematic variables. *)
(*The master integrals are identified using the Maximal-cut method.*)
{masters,invs,diffeq}=BLDifferentialEquation[{t},"PreferredMI"->"Dot"];


(*'invs' is a list of kinematic variables and 'diffeq' is a set of 
differential equations corresponding to 'invs'*)
Length@masters
Length@invs
Dimensions@diffeq


Put[{masters,invs,diffeq},FileNameJoin[{current,"blade_diffeq"}]];


(*Alternatively, it is possible to construct differential equations for
preferred master integrals*)
(*Preferred master integrals can be linear combinations of standard Feynman integrals.*)
exints={
ex[1]->s^2 BL[db,{1,1,1,1,1,1,1,-1,0}],ex[2]->s^3 t BL[db,{1,1,1,1,1,1,1,0,0}],
ex[3]->s (1+t) BL[db,{0,1,1,0,1,1,1,0,0}],ex[4]->-((s^2 t BL[db,{0,2,0,1,1,1,1,0,0}])/eps),
ex[5]->(s^2 BL[db,{2,0,1,2,0,1,0,0,0}])/eps^2,ex[6]->-((s BL[db,{0,2,0,1,0,1,1,0,0}])/eps),
ex[7]->-((s BL[db,{0,0,2,0,0,2,1,0,0}])/eps^2),ex[8]->(s t BL[db,{0,2,0,0,2,0,1,0,0}])/eps^2}/.numeric;


(*If the preferred masters are complete, The Maximal-cut can be closed*)
{masters2,invs2,diffeq2}=BLDifferentialEquation[{t},Values[exints],"PreferredMI"->None,"ReadCacheQ"->False];


(*'diffeq2' is in canonical form*)
diffeq2[[1]]/.eps->0


Put[{masters2,invs2,diffeq2},FileNameJoin[{current,"blade_diffeq2"}]];
