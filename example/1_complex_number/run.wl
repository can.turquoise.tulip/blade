(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of the massive double box*)
family = dbox;
dimension = 4-2*eps;
loop = {l1,l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> msq, p1 p2 -> s/2, p1 p3 -> (t-msq)/2, p2 p3 -> (msq-s-t)/2};
propagator = {(l1)^2,(l1+p1)^2,(l1+p1+p2)^2,l2^2-msq, (l1+l2)^2-msq, (l2-p1-p2)^2-msq, (l2+p3)^2, (l1-p3)^2,(l2+p1)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s -> 1,msq->13-7 I, t->3+100 I, eps->10^-3};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension, propagator,loop,leg,conservation, replacement,topsector,numeric]


(*When considering the squared-amplitude, it may be necessary to reduce 
both higher rank and higher dots Feynman integrals. 
The IBP system would become too large to generate and solve if we 
create seeds with uniform rank and dots.*)
target={BL[dbox,{1,1,1,1,1,1,1,0,-3}],BL[dbox,{1,1,1,1,1,1,1,-1,-2}],
BL[dbox,{1,1,1,1,1,1,1,-2,-1}],BL[dbox,{1,1,1,1,1,1,1,-3,0}],
BL[dbox,{2,1,1,1,1,1,0,0,-2}], BL[dbox,{2,1,1,1,1,1,0,-2,0}],
BL[dbox,{0,1,2,2,1,1,0,0,-2}], BL[dbox,{0,1,2,2,1,1,0,-2,0}]
};


(*It would be beneficial to divide them into several sub-families and perform the
 reduction individually.*)
(*The structure of 'res' is the same as that of 'target', except that each
element if replaced by a linear combinations of master integrals.*)
res=BLReduce[target];


(*Master integrals are stored in BLFamilyInf*)
BLFamilyInf[family]["Masters"]


Put[Thread[target->res],FileNameJoin[{current,"blade_table"}]];
