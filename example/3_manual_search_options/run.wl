(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of the massive double box*)
family = dbox;
dimension = 4-2*eps;
loop = {l1,l2};
leg = {p1,p2,p3,p4};
conservation = {p4->-p1-p2-p3};
replacement = {p1^2 -> 0, p2^2 -> 0, p3^2 -> msq, p1 p2 -> s/2, p1 p3 -> (t-msq)/2, p2 p3 -> (msq-s-t)/2};
propagator = {(l1)^2,(l1+p1)^2,(l1+p1+p2)^2,l2^2-msq, (l1+l2)^2-msq, (l2-p1-p2)^2-msq, (l2+p3)^2, (l1-p3)^2,(l2+p1)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s -> 1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension, propagator,loop,leg,conservation, replacement,topsector,numeric]


(*Target integrals to reduce*)
target={BL[dbox,{1,1,1,1,1,1,1,0,-3}],BL[dbox,{1,1,1,1,1,1,1,-1,-2}],
BL[dbox,{1,1,1,1,1,1,1,-2,-1}],BL[dbox,{1,1,1,1,1,1,1,-3,0}]
};


(*Extend integral set*)
full=BLGenerateScheme[target];
Length@full


(*Generate IBP system with rank\[Rule]3, dots\[Rule]0*)
BLIBPSystem[topsector,3,0];


(*Trim the IBP system according to target integrals 'full'*)
BLDatabaseInfo[full]


(*Learn average time for numerical IBP sampling and the number of 
available threads for numerical IBP*)
BLNthreadsLearning[]


(*Load the masters integrals and other needed information for search*)
BLDataInfoInit[]


(*Generate the degrees.fflow file for further analysis*)
(*The degrees.fflow file contains information on the maximal and 
minimal degrees for IBP parameters, which can be used by finiteflow
 to generate sample points before any reconstruction.*)
cache="temporary";
BLDumpDegrees[FileNameJoin[{$BLWorkingDirectory,cache,"degrees.fflow"}]];


(*For simplicity, we do not sort parameters by complexity here.
We set the reconstruction parameter manually.
You can run this line and go to next line without thinking.
For more details, refer to Blade`Private`BLBlackBoxReduce*)
Blade`Private`blReconstructParameter = BLIBPParameter


(*We only want to reduce 'target' integrals rather than all integrals in
the block-triangular system. So we call BLRegisterNeeded to register needed
integrals. This function will generate selected_degrees.fflow 
correspond to needed integrals.*)
BLRegisterNeeded[target,cache];


(*Generate and load sample points for further analysis*)
(*We can estimate the total time for sampling with trimmed IBP under a finite field
by the equation (# samples *Subscript[t, sample])/(# threads)*)
SetDirectory[FileNameJoin[{$BLWorkingDirectory,cache}]];
ptsfile=FileNameJoin[{"points","points_"<>ToString[0]<>".fflow"}];
BLDumpSamplePoints[0,ptsfile];
points=BLLoadSamplePoints[ptsfile];
Print["number of sample points -> ",Length[points]]
Print["Estimated wall time(IBP FiniteField) -> ", Round[Length[points]*Blade`Private`BLTimeIBP/Blade`Private`BLDatabaseNthreads,0.001]," s."]


(*It is sometimes difficult to construct the block-triangular form.
We set the allowed time for the construction of the block-triangular form(
normalized to the estimated total time under a finite field).*)
SetOptions[BLAdaptiveSearch,"SearchTimeFactor"->1/2];


(*The program has a set of default search options. We can see the default 
options by: *)
BLSetSearchOptions[]


(*We set the search options similar to the strategy adopted in 1912.09294.
For syntax, see ?BLSetSearchOptions*)
BLSetSearchOptions[
"NVariables"->3,
"SearchVariable"->{{msq,t},{eps}},
"VariableWeight"->{"uniform","uniform"}, 
"IntegralWeight"->{"weight","uniform"},
"DatanamePrefix"->"data3","Jobname"->"job3"]


(*To search block-triangular relations: 
Start from one-variable-analytic(with other variables set to numerical values),
then progress to two variables analytic, and so on, until full-analytic 
block-triangular form are successfully constructed or the time-limit is reached(
specified by SearchTimeFactor\[Rule]1/2).*)
(*The estimated total time for complete sampling with IBP or Block-triangular form 
will be printed.*)
(*Return `nana` that gives the minimal estimated total time.*)
(*Note that different machines and BLNthreads setting can lead to 
different `nana`.*)
nana=BLAdaptiveSearch[points];//AbsoluteTiming


(*We can see from above that solving the "NVariables"\[Rule]3 block-triangular relations 
is the most efficient way.*)
(*We use a custom solver to perfom sampling with block-triangular form relations 
and use finiteflow to perform multivariate functional reconstruction*)
(*By repeating this procedure under different primes, we can obtain the final result*)
res=BLSolveSemiBL[3,cache];


(*The first element of 'res' is a list of master integrals.
The second element of 'res' is a list of rules. Each rule represent a 
reduction relation. The key is a Feynman integral, the value is its projections
on master integrals.*)
res[[1]]//Short
Length[res[[1]]]
res[[2]]//Short
Length[res[[2,1,2]]]


(*In general, block-triangular relations with more analytic variables(if any) 
are more efficient than those with fewer analytic variables due to 
fewer numerical IBP samplings.
However, more variables being analytic means that there are more free parameters 
to determine for constructing the block-triangular relations. 
As a result, it can be difficult to construct full-analytic block-triangular 
form for more complex problems.
Given the time required to construct block-triangular form, full-analytic 
block-triangular form may not always be the optimal choice.*)
(*In such cases, It is still possible to make use of semi-analytic block-triangular 
form to improve IBP reduction*)
(*We use a custom solver to perfom sampling with block-triangular form relations 
and use finiteflow to perform multivariate functional reconstruction*)
(*The actual run time may be larger than estimated time due to disk 
reading/writing. But this performance penalty plays a minor role 
in complex problems.*)
res2=BLSolveSemiBL[2,cache];


(*Results must be identical*)
Table[
((target[[i]]/.res[[2]]).res[[1]])-((target[[i]]/.res2[[2]]).res2[[1]])//Simplify
,{i,1,Length@target}]//Flatten//Union


Put[res,FileNameJoin[{current,"blade_table"}]];
Put[res2,FileNameJoin[{current,"blade_table2"}]];
