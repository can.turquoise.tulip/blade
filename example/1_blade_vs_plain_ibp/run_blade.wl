(* ::Package:: *)

(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of nonplanar massive double box*)
family = npdbox;
dimension = 4-2*eps;
loop = {k1,k2};
leg = {p1,p2,q1,q2};
conservation = {q1->p1+p2-q2};
replacement = {p1^2 -> 0, p2^2 -> 0, q2^2->0, (p1+p2)^2->s, (q2-p1)^2->t , (q2-p2)^2 -> (-s -t +m12)};
propagator = {k1^2, k2^2, (q2-k1)^2 -m12, (p1-k2)^2, (q1+k1)^2-m12, (q1+k1-k2)^2-m12, (-p2+q1+k1-k2)^2-m12,(k1-p1)^2, (k2-q2-p2)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s -> 1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension, propagator,loop,leg,conservation, replacement,topsector,numeric]


(*We choose top-sector integrals with rank 3 as target integrals*)
target={BL[npdbox,{1,1,1,1,1,1,1,0,-3}],BL[npdbox,{1,1,1,1,1,1,1,-1,-2}],
BL[npdbox,{1,1,1,1,1,1,1,-2,-1}],BL[npdbox,{1,1,1,1,1,1,1,-3,0}]
};


(*BLReduce adopts an adaptive-search strategy that will estimate
sampling time under a finite field by solving the traditional IBP 
system and the block-triangular form.*) 


(*User may pay attention to these sentences in output, which 
demonstrates how the adaptive-search strategy works:
...
'Database Nthreads-> 6, there is enough memory'
'Average sample time for ibps (single core) -> 0.0902s.'
...
'number of sample points -> 61371'
'Estimated wall time(IBP FiniteField) -> 923.112 s.'
...
'Variables: {{eps}}, Weights: {{1}}'
...
'Estimated wall time(BL FiniteField) -> 1021.885 s'
...
'Variables: {{t, eps}}, Weights: {{1, 1}}'
...
'Estimated wall time(BL FiniteField) -> 248.527 s.'
...
'Variables: {{m12, t, eps}}, Weights: {{1, 1, 1}}'
...
# database -> 102, time for fitting -> 1.5167s, time for solving -> 0.0202s.
...
'Estimated wall time(BL FiniteField) -> 210.055 s.'
'The optimal choice is NVariables -> 3'
...
'rational reconstruct success with primes: 4 + 1, total use 282.000000s....'
...
'BLReduce use time : 536.291s'*)
(*The optimal linear system is the full-analytic block-triangular form(BL), which 
reduces the estimated time of sampling under one prime field from 
923 second to 210 second.*)
(*The estimated time of solving the BL is obtained by averaging the time for
solving three probes. There is a learning stage that can effect the estimated 
time, but is ignorable for a huge number of probes. So the actual sampling 
time is smaller than 210 second.*)
(*The rational reconstruction uses four primes. The BL performs a full reduction
under another prime for validation, although several probes are sufficient. 
The performance penalty due to this oversampling is small compared to what
we gain from the BL.*)
(*The estimated wall time of sampling using IBP is 
923 * 4 \[Rule] 3692 second, which is the dominant part in reduction.
The actual run time for IBP reduction without BL can be
read from ./run_ibp.pdf as
'BLSolveNoBL use time : 4985.904s'
...
'BLReduce use time : 5042.399s'
We can see that the estimated wall time and actual run time are comparable.
*)
(*To conclude, the performance with BL increases 
by a factor of 9 compared to plain IBP in this problem*)
res=BLReduce[target];


Put[Thread[target->res],FileNameJoin[{current,"blade_table_adaptive"}]];
