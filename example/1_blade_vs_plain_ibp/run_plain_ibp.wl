(* ::Package:: *)

(*It is recommended to run ./run.wl first*)
(*This file is a verification of the conclusion in ./run.wl*)


(*Load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current,"..","..","Blade.wl"}]];


(*Configuration of nonplanar massive double box*)
family = npdbox;
dimension = 4-2*eps;
loop = {k1,k2};
leg = {p1,p2,q1,q2};
conservation = {q1->p1+p2-q2};
replacement = {p1^2 -> 0, p2^2 -> 0, q2^2->0, (p1+p2)^2->s, (q2-p1)^2->t , (q2-p2)^2 -> (-s -t +m12)};
propagator = {k1^2, k2^2, (q2-k1)^2 -m12, (p1-k2)^2, (q1+k1)^2-m12, (q1+k1-k2)^2-m12, (-p2+q1+k1-k2)^2-m12,(k1-p1)^2, (k2-q2-p2)^2};
topsector = {1,1,1,1,1,1,1,0,0};
numeric = {s -> 1};


(*The number of threads used in parallel computation*)
BLNthreads = 6;


(*Save configuration*)
BLFamilyDefine[family,dimension, propagator,loop,leg,conservation, replacement,topsector,numeric]


(*We choose top-sector integrals with rank 3 as target integrals*)
target={BL[npdbox,{1,1,1,1,1,1,1,0,-3}],BL[npdbox,{1,1,1,1,1,1,1,-1,-2}],
BL[npdbox,{1,1,1,1,1,1,1,-2,-1}],BL[npdbox,{1,1,1,1,1,1,1,-3,0}]
};


(*Perform reduction with plain IBP system*)
res=BLReduce[target,"BladeMode"->None,"ReadCacheQ"->False];


Put[Thread[target->res],FileNameJoin[{current,"blade_table_ibp"}]];


res2=Get[FileNameJoin[{current,"blade_table_adaptive"}]];


(*validation of reduction result*)


Values[res2]-res/.Thread[{eps,m12,t}->{7,17,23}]
