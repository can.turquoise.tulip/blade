## BLSetReducerOptions

- "IBPParameter": a list of parameters invloved in IBPSystem, like {eps, s, t}. The ordering has impact on the construction of the block-triangular form. Automatic by default.

- "IntegralOrdering": an integer represents integral ordering. We denote the number of propagators of the integral as p, the rank of the integral as r, the dots of the integrals as d, then each integral ordering means: 
	+ 1: p > d > r;
	+ 2: p > r > d;
	+ 3: p > r+d > d > r;
	+ 4: p > r+d > r > d;

	where '>' means the left hand side is more complex than the right hand side. 1 by default.

- "CheckMastersQ": whether to check master integrals using the maximal-cut method. True by default.

- "FilterLevel": an integer that determines how to filter IBP seeds. Infinity by default.

- "MaxIncrement": an integer that specifies the maximal increment of IBP seeding parameters(rank and dots). It take effects when the obtained IBP system is not able to reduce target integrals completely. 2 by default.

- "CloseSymmetry": whether to close symmetry when generating IBP system, False by default.

- "CutNoDot": whether to remove seeds that have dots on cut propagators. False by default.

- "BlackBoxRank": an integer that specifies the minimal rank of IBP seeds, 2 by default.

- "BlackBoxDot": an integer that specifies the minimal dots of IBP seeds, 0 by default.

- "MaxPrime": an integer that specifies the maximal number of primes in use to perform 
 rational reconstruction, 200 by default.

- "MaxDegree": an integer that specifies the maximal degree of polynomials that can be reconstructed, 1000 by default.

## BLSetSchemeOptions

- "OperatorExtendGQ": whether to use n-circled-dash scheme to extend the integral set, which is used to constructing the block-triangular form. True by default.

- "MinimalSchemeRank": all integrals whose rank is equal to or larger than this value are included in the block-triangular system. 1 by default.

- "PartitionDot": whether to partition integrals that belong to the same sector but differ by dots into different blocks when construct the block-triangular form. False by default.

- "UniqueSubsectorQ": whether to map the sub-sector integrals to unique-sectors when constructing the block-triangular form. True by default.

## BLSetSearchOptions

The program adopts the search algorithm to construct the block-triangular form. The search algorithm can find out all linear relations among a given integral set under certain degree bound of polynomal coefficients. The polynomial ansatz is determined by "SearchVariable", VariableWeight" and "IntegralWeight". The program will increase the degree bound automatically untill the obtained relations are able to perform the reduction. It is trival to increase degree bound if there is only one group of "SearchVariable". However, if there are more than one group of variables, we should set cut-off of degree bound for all but the first group of variables. This cut-off is determined by "CutIncrement". Each block-triangular system has a name called "Jobname", its input data generated by numerical IBP also has a name called "DatanamePrefix". 


- "SearchVariable": a list of groups of variables, looks like {{x1,x2},{x3,x4}}. 

- "VariableWeight": a list that has the same length as "SearchVariable", each element can be "uniform","adaptive" or an integer:
	+ "uniform": weights of variables in the same group are 1.
	+ "adaptive": weights of variables in the same groups are determined by their complexity. The program learns the complexity of variables by comparing the maximal degree of monomials appeared in the single-variable-analytic block-triangular system.
	+ Integer: exact number also works. The larger the weight, the more complex polynomial ansatzs it has.

- "IntegralWeight": a list that has the same length as "SearchVariable", each element could be "uniform" or "weight":
	+ "uniform": weights of integrals are 0.
	+ "weight":  weights of integrals equal to their normalized mass dimension.

- "CutIncrement": a list of integer whose length is less than the length of "SearchVariable" by 1. An integer 'c' would be treated as {c,c,...}. This list plus maximal degree of polynomials appeared in single-group block-triangular system serves as cut-off of degree bound for the construction of multi-group block-triangular form.

- "DatanamePrefix": name of database.

- "Jobname": name of job.

## BLReduce

- "DivideLevel": an integer that determines the degree to which 'target' is split into sub-families. BLReduce will try to divide target integrals into several sub-families and reduces them separately. Roughly speaking, it starts by selecting the most complex integral in the target(F_1) and defining an integral family to reduce all integrals in target that have a rank and dots that are both less than or equal to those of F_1. By repeating this process for the remaining target integrals, all integrals can be reduced. The results of each sub-family are then merged together. This is useful in problems where sub-sectors have high dots while the top-sector has high rank. It is benefical to treate different target structures separately rather than jumbling everything together. Possible values are
	+ 0: don't split sub-families;
	+ 'n'(Positive integer), sub-sectors whose number of propagators is greater than or equal to the number of propagators in the top-sector minus 'n' will be split, while sub-sectors with 'n' fewer propagators than the top-sector will be combined together;
	+ Infinity: complete decomposition.

	Infinity by default.

- "JoinFamilyQ": whether to merge sub-families at the level of IBP system rather than reduction table, False by default.

- "GenerateScheme": function that determines how the target integral set is extended for the purpose of construction of the block-triangular form and how to define sub-families as demonstrated in "DivideLevel". **BLGenerateScheme** by default.

- "BladeMode": specify the reduction mode to use:
	+  None: do reduction without block-triangular form, i.e. using the IBP system;
	+  Full: do reduction with fully-analytic block-triangular form;
	+  Automatic: adopt the adaptive-search strategy to find the optimal linear system for the reduction(IBP system, semi-analytic block-triangular form or fully-analytic block-triangular form).
	
	Automatic by default.

- "ReadCacheQ": whether to resume a previous reduction job.

- "DeleteDirectory": whether to delete the cache directory after the reduction.

## BLDifferentialEquation

This function has one more options than **BLReduce**.

- "PreferredMI": user can input user-preferred master integrals freely. The program will set user-preferred master integrals and the maxiaml-cut master integrals(identified using the maximal-cut method) as raw masters. The derivatives of raw master integrals are reduced by **BLReduce**. possible choice:
	+ Automatic: use an IntegralOrdering that is more likely to give master integrals that has similar structure with user-preferred master integrals. If no preferred is input, use current setting of "IntegralOrdering".
	+ "Rank": give maximal-cut masters whose dots equal to zero("IntegralOrdering" is set to 1 locally);
	+ "Dot": give maximal-cut masters whose rank equal to zero("IntegralOrdering" is set to 2 locally);
	+ "MixedRank": similar to "Rank", but "Rank"+"Dot" has higher priority("IntegralOrdering" is set to 3 locally);
	+ "MixedDot": similar to "Dot", but "Rank"+"Dot" has higher priority("IntegralOrdering" is set to 4 locally).;
	+ "None": assume that the user-preferred master integrals constitutes a complete set of master integrals. Do not use the maximal-cut method. 

	Automatic by default.
