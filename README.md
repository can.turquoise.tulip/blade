# Blade

**Blade** is a **BL**ock-tri**A**ngular form improved Feynman integral **DE**composition package that uses a search algorithm to construct a system of simple relations among Feynman integrals, using input from integration-by-parts (IBP) decomposition at specific numerical phase space points over a finite field. The system usually has orders of magnitude fewer equations than traditional IBP method and exhibits a block triangular form, which leads to fast numerical evaluation and reduced resource consumption.

The search algorithm is based on the following publications:
- Xiao Liu and Yan-Qing Ma. Determining arbitrary Feynman integrals by vacuum integrals. [Phys.Rev.D 99 (2019) 071501][arXiv: 1801.10523]
- Xin Guan, Xiao Liu and Yan-Qing Ma. Complete reduction of integrals in two-loop five-light-parton scattering amplitudes. [Chin.Phys.C 44 (2020) 9, 093106][arXiv: 1912.09294]
- Xin Guan, Xiao Liu, Yan-Qing Ma and Wen-Hao Wu. Blade: A package for block-triangular form improved Feynman integrals
decomposition. [arXiv: 2306.xxxxx]

## Installation

If you use Ubuntu system and have root privilege, you can install Blade as well as its dependencies (except `Mathematica`) fully automatically. To this end, download the `auto_install` file (such as using the command `wget https://gitlab.com/multiloop-pku/blade/-/raw/main/auto_install`) and then apply the following command
```
chmod +x auto_install
./auto_install
```

For those who do not have root privilege or use different operation system, we also provide a sample installation document `sample_install.md` that users can follow. 

### Dependence

We use FintieFlow as a sparse solver and finite-field reconstructor. If FiniteFlow is already installed and functioning properly, no other packages are required. If not, the following packages should be installed in order.

##### Mathematica

https://www.wolfram.com/

##### CMake

 https://cmake.org/

##### GMP

https://gmplib.org/

##### MPFR

https://www.mpfr.org/

##### FLINT

http://www.flintlib.org/ 

##### FiniteFlow

https://github.com/peraro/finiteflow/


### Installation of Blade

To set up the paths for dependencies, you should edit the following information in the `install.in.txt` file:
```
DFFLOWMLINK_DIR="/path/to/finiteflow-master"
DCMAKE_PREFIX_PATH="/dependencies/installation/path/prefix"
```
The `/path/to/finiteflow-master` directory should contain the dynamic-link library `libfflow.so`(or `libfflow.dylib`), the mathlink file `fflowmlink.so`(or `fflowmlink.dylib`), and the Mathematica file for FiniteFlow `mathlink/FiniteFlow.m`.
\
The `/dependencies/installation/path/prefix` refers to the installation paths for `GMP`, `MPFR` and `FLINT`, separated by semicolons. You can omit any of the directories if the corresponding dependencies were installed in the default installation path (e.g., /usr/local).

Then to install the binaries and generate the necessary files, use the following command:
```
cmake .
make install
```

To remove the files generated during the `cmake` and `make install` processes, use the following command:
```
./uninstall
```

To update Blade to the latest version, use the following command:
```
./update
```

Additional instructions for MacOS users can be found in the `install.in.txt` file.


## Usage

Several examples are provided in `example` folder, in the order of complexity. You are encouraged to run examples that name begin with `1_` to get familiar with the package. 

- 1_automatic - introduction to automatic reduction of Feynman loop integrals;
- 1_preferred_masters - introduction to automatic reduction with user-defined master integrals;
- 1_userdefined_target - introduction to automatic reduction of user defined target integrals;
- 1_differential_equations - introduction to automatic construction of differential equations.
- 1_prescription - introduction to automatic reduction of Feynman integrals with cut propagators;
- 1_complex_number - introduction to reduction with complex replacement rules;
- 1_blade_vs_plain_ibp - comparision between the block-triangular form improved reduction and the traditional IBP reduction;
- 1_generalized_integrand - introduction to automatic reduction of Feynman integrals with additional integrand;
- 2_reducer_options - introduction to a few options;
- 2_block_triangular_form - introduction to the block-triangular form;
- 3_manual_search_options - introduction to the manual reduction with adaptive-search algorithm;
- 4_higgs_rapidity - introduction to the more complex problem;
- 4_ttbar_totxsection - introduction to the more complex problem.

## Support

If you have any questions or advices, please do not hesitate to contact us: guanxin0507@pku.edu.cn, xiao.liu@physics.ox.ac.uk, yqma@pku.edu.cn, wuwenhao21@mails.ucas.ac.cn
